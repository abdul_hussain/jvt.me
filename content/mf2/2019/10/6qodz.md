{
  "kind": "rsvps",
  "slug": "2019/10/6qodz",
  "client_id": "https://indigenous.realize.be",
  "date": "2019-10-30T11:39:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/DevOps-Notts/events/266072465/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/DevOps-Notts/events/266072465/"
    ],
    "published": [
      "2019-10-30T11:39:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/DevOps-Notts/events/266072465/"
      ],
      "name": [
        "DevOps Notts - November 2019"
      ],
      "start": [
        "2019-11-26T18:00:00Z"
      ],
      "end": [
        "2019-11-26T21:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Rebel Recruiters, Huntingdon St"
          ],
          "locality": [
            "Nottinghamshire"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "aliases": [
  ]
}
