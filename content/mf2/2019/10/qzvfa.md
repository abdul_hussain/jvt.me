{
  "kind": "rsvps",
  "slug": "2019/10/qzvfa",
  "client_id": "https://indigenous.realize.be",
  "date": "2019-10-23T19:17:00+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Tech-Nottingham/events/265901272/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Tech-Nottingham/events/265901272/"
    ],
    "published": [
      "2019-10-23T19:17:00+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Tech-Nottingham/events/265901272/"
      ],
      "name": [
        "Tech Nottingham November 2019: Lightning Talks!"
      ],
      "start": [
        "2019-11-11T18:30:00Z"
      ],
      "end": [
        "2019-11-11T21:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, Beck Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "aliases": [
  ]
}
