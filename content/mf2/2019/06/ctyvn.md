{
  "properties": {
    "rsvp": [
      "yes"
    ],
    "in-reply-to": [
      "https://www.meetup.com/NottsJS/events/qhnpfqyzkbmb/"
    ],
    "content": [
      {
        "value": "I'm super excited for another chance to see Terence Eden speak! He's always brilliant, and most definitely knows his stuff.",
        "html": ""
      }
    ],
    "published": [
      "2019-06-27T15:15:00+0100"
    ],
    "category": [
      "notts-js"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/NottsJS/events/qhnpfqyzkbmb/"
      ],
      "name": [
        "NottsJS: SVG + JS"
      ],
      "start": [
        "2019-07-09T18:00:00+0100"
      ],
      "end": [
        "2019-07-09T20:00:00+0100"
      ],
      "location": {
        "properties": {
          "street-address": [
            "JH, 34a Stoney Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "aliases": [
    "/rsvps/837ba2fc-c47f-48b1-8c5b-787324923d80/",
    "/mf2/837ba2fc-c47f-48b1-8c5b-787324923d80/"
  ],
  "h": "h-entry",
  "date": "2019-06-27T15:15:00+0100",
  "tags": [
    "notts-js"
  ],
  "kind": "rsvps",
  "slug": "2019/06/ctyvn"
}
