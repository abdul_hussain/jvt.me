{
  "properties": {
    "rsvp": [
      "yes"
    ],
    "in-reply-to": [
      "https://www.meetup.com/DevOps-Notts/events/262473868/"
    ],
    "content": [
      {
        "value": "After the previous iteration of DevOps Nottingham died a death, it's exciting to see a new one starting again!",
        "html": ""
      }
    ],
    "published": [
      "2019-06-26T10:44:00+0100"
    ],
    "category": [
      "devops-notts"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/DevOps-Notts/events/262473868/"
      ],
      "name": [
        "DevOps Notts"
      ],
      "start": [
        "2019-08-27T18:30:00+0100"
      ],
      "end": [
        "2019-08-27T21:00:00+0100"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Rebel Recruiters, Huntingdon Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "aliases": [
    "/rsvps/3bfbac9e-e408-41c6-88d4-a40cac0de068/",
    "/mf2/3bfbac9e-e408-41c6-88d4-a40cac0de068/"
  ],
  "h": "h-entry",
  "date": "2019-06-26T10:44:00+0100",
  "tags": [
    "devops-notts"
  ],
  "kind": "rsvps",
  "slug": "2019/06/gqszv"
}
