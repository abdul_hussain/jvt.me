{
  "properties": {
    "rsvp": [
      "yes"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Tech-Nottingham/events/262759428/"
    ],
    "published": [
      "2019-06-30T20:00:00+0100"
    ],
    "category": [
      "tech-nottingham"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Tech-Nottingham/events/262759428/"
      ],
      "name": [
        "Tech Nottingham"
      ],
      "start": [
        "2019-07-08T18:30:00+0100"
      ],
      "end": [
        "2019-07-08T21:00:00+0100"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, 9A Back Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "aliases": [
    "/rsvps/c7bb0289-c1e3-4371-9686-3827a6ae1352/",
    "/mf2/c7bb0289-c1e3-4371-9686-3827a6ae1352/"
  ],
  "h": "h-entry",
  "date": "2019-06-30T20:00:00+0100",
  "tags": [
    "tech-nottingham"
  ],
  "kind": "rsvps",
  "slug": "2019/06/vbegv"
}
