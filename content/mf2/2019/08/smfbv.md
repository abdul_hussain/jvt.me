{
  "properties": {
    "rsvp": [
      "yes"
    ],
    "in-reply-to": [
      "https://www.meetup.com/dotnetnotts/events/263866046/"
    ],
    "published": [
      "2019-08-09T16:55:00Z"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/dotnetnotts/events/263866046/"
      ],
      "name": [
        "Alexa.Net with Steve Pears + Writing Business Apps in F# with Ian Russell"
      ],
      "start": [
        "2019-09-02T19:00:00+0100"
      ],
      "end": [
        "2019-09-02T21:00:00+0100"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Fothergill House, 16 King Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "h": "h-entry",
  "date": "2019-08-09T16:55:00Z",
  "kind": "rsvps",
  "slug": "2019/08/smfbv",
  "aliases": [
    "/mf2/00276d65-8767-40fe-bb37-59fc075a4942/"
  ]
}
