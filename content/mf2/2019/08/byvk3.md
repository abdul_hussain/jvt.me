{
  "properties": {
    "rsvp": [
      "yes"
    ],
    "in-reply-to": [
      "https://indiewebcamp.nl/"
    ],
    "content": [
      {
        "value": "I'm looking forward to my first IndieWebCamp, as well as visiting Amsterdam again with Anna. We're planning on taking a couple days' holiday post-IWC, so that should be really nice too!",
        "html": ""
      }
    ],
    "published": [
      "2019-08-29T22:21:00+0100"
    ],
    "category": [
      "indiewebcamp"
    ],
    "event": {
      "url": [
        "https://indiewebcamp.nl/"
      ],
      "name": [
        "IndieWebCamp Amsterdam"
      ],
      "start": [
        "2019-09-28T10:00:00+0200"
      ],
      "end": [
        "2019-09-29T18:00:00+0200"
      ],
      "location": {
        "properties": {
          "street-address": [
            "CODAM Coding College, Building 039, Marineterrein Amsterdam"
          ],
          "locality": [
            "Kattenburgerstraat 7, Amsterdam"
          ],
          "country-name": [
            "Netherlands"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "h": "h-entry",
  "date": "2019-08-29T22:21:00+0100",
  "tags": [
    "indiewebcamp"
  ],
  "kind": "rsvps",
  "slug": "2019/08/byvk3",
  "aliases": [
    "/mf2/37517bd2-4fb5-4469-a245-b8f730609d28/"
  ]
}
