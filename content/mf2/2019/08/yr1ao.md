{
  "kind": "rsvps",
  "date": "2019-08-30T13:31:00+0100",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/NottsJS/events/mcrkhryzmbnb/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/NottsJS/events/mcrkhryzmbnb/"
    ],
    "published": [
      "2019-08-30T13:31:00+0100"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/NottsJS/events/mcrkhryzmbnb/"
      ],
      "name": [
        "⚡️Lightning Talks!⚡️"
      ],
      "start": [
        "2019-09-10T18:00:00+01:00"
      ],
      "end": [
        "2019-09-10T21:00:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Capital One (Europe) plc, Station St"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2019/08/yr1ao",
  "aliases": [
    "/mf2/585313f7-fa86-4cc7-b999-7b72e35019fe/"
  ]
}
