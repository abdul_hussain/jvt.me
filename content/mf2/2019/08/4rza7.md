{
  "properties": {
    "rsvp": [
      "yes"
    ],
    "in-reply-to": [
      "https://www.jvt.me/events/homebrew-website-club-nottingham/2019/09/04/"
    ],
    "published": [
      "2019-08-28 09:53:03+0100"
    ],
    "category": [
      "homebrew-website-club"
    ],
    "event": {
      "url": [
        "https://www.jvt.me/events/homebrew-website-club-nottingham/2019/09/04/"
      ],
      "name": [
        "Homebrew Website Club Nottingham"
      ],
      "start": [
        "2019-09-04T17:30:00+0100"
      ],
      "end": [
        "2019-09-04T19:30:00+0100"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Ludorati Cafe, 72 Maid Marian Way"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ],
          "postal-code": [
            "NG1 6BJ"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "h": "h-entry",
  "date": "2019-08-28 09:53:03+0100",
  "tags": [
    "homebrew-website-club"
  ],
  "kind": "rsvps",
  "slug": "2019/08/4rza7",
  "aliases": [
    "/mf2/fd2c2c8b-8837-4f46-94f2-d5494f42537b/"
  ]
}
