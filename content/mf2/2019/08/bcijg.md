{
  "kind": "rsvps",
  "date": "2019-08-30T13:31:00+0100",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Women-In-Tech-Nottingham/events/264412304/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Women-In-Tech-Nottingham/events/264412304/"
    ],
    "published": [
      "2019-08-30T13:31:00+0100"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Women-In-Tech-Nottingham/events/264412304/"
      ],
      "name": [
        "Women in Tech September 2019 - Elevating to Expert: brand enhancement"
      ],
      "start": [
        "2019-09-12T18:30:00+01:00"
      ],
      "end": [
        "2019-09-12T21:00:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, 9A Beck St"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2019/08/bcijg",
  "aliases": [
    "/mf2/6a9b4de0-e756-4aeb-8333-acb41c66bb9c/"
  ]
}
