{
  "properties": {
    "rsvp": [
      "yes"
    ],
    "in-reply-to": [
      "https://www.jvt.me/events/homebrew-website-club-nottingham/2019/09/18/"
    ],
    "published": [
      "2019-08-28T09:53:03+0100"
    ],
    "category": [
      "homebrew-website-club"
    ],
    "event": {
      "url": [
        "https://www.jvt.me/events/homebrew-website-club-nottingham/2019/09/18/"
      ],
      "name": [
        "Homebrew Website Club Nottingham"
      ],
      "start": [
        "2019-09-18T17:30:00+0100"
      ],
      "end": [
        "2019-09-18T19:30:00+0100"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Ludorati Cafe, 72 Maid Marian Way"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ],
          "postal-code": [
            "NG1 6BJ"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "h": "h-entry",
  "date": "2019-08-28T09:53:03+0100",
  "tags": [
    "homebrew-website-club"
  ],
  "kind": "rsvps",
  "slug": "2019/08/0iufi",
  "aliases": [
    "/mf2/896441cf-3d73-4330-8158-6c956caf18b8/"
  ]
}
