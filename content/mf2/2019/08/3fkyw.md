{
  "properties": {
    "rsvp": [
      "yes"
    ],
    "in-reply-to": [
      "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/264008439/"
    ],
    "published": [
      "2019-08-16T08:27:00+0100"
    ],
    "category": [
      "phpminds"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/264008439/"
      ],
      "name": [
        "September Double Whammy - Noobs on Ubs and the IndieWeb"
      ],
      "start": [
        "2019-09-05T19:00:00+0100"
      ],
      "end": [
        "2019-09-05T21:00:00+0100"
      ],
      "location": {
        "properties": {
          "street-address": [
            "JH, 34a Stoney Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "h": "h-entry",
  "date": "2019-08-16T08:27:00+0100",
  "tags": [
    "phpminds"
  ],
  "kind": "rsvps",
  "slug": "2019/08/3fkyw",
  "aliases": [
    "/mf2/f365e9a6-4f3e-4a95-b70f-fb38799a2d57/"
  ]
}
