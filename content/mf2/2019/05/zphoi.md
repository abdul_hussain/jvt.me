{
  "properties": {
    "rsvp": [
      "yes"
    ],
    "in-reply-to": [
      "https://www.meetup.com/digitallincoln/events/261313886/"
    ],
    "published": [
      "2019-05-18T09:08:09+0100"
    ],
    "category": [
      "digital-lincoln"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/digitallincoln/events/261313886/"
      ],
      "name": [
        "Digital Lincoln - TAKEOVER! Women in Tech"
      ],
      "start": [
        "2019-06-11T18:30:00+0100"
      ],
      "end": [
        "2019-06-11T21:00:00+0100"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Boole Technology Centre, Beevor St"
          ],
          "locality": [
            "Lincoln"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "aliases": [
    "/rsvps/bf8e4030-7b7c-4136-97c3-d1769b68e596/",
    "/mf2/bf8e4030-7b7c-4136-97c3-d1769b68e596/"
  ],
  "h": "h-entry",
  "date": "2019-05-18T09:08:09+0100",
  "tags": [
    "digital-lincoln"
  ],
  "kind": "rsvps",
  "slug": "2019/05/zphoi"
}
