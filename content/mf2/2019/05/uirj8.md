{
  "properties": {
    "rsvp": [
      "yes"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Tech-Nottingham/events/260665390/"
    ],
    "published": [
      "2019-05-13T09:16:16+0100"
    ],
    "category": [
      "tech-nottingham"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Tech-Nottingham/events/260665390/"
      ],
      "name": [
        "Tech Nottingham May 2019: IoT With Only Your Laptop (and the next steps)"
      ],
      "start": [
        "2019-05-13T18:30:00+0100"
      ],
      "end": [
        "2019-05-13T20:30:00+0100"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, 9A Back Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "aliases": [
    "/rsvps/b6afef2c-bc82-49b9-856b-17d0ac658593/",
    "/mf2/b6afef2c-bc82-49b9-856b-17d0ac658593/"
  ],
  "h": "h-entry",
  "date": "2019-05-13T09:16:16+0100",
  "tags": [
    "tech-nottingham"
  ],
  "kind": "rsvps",
  "slug": "2019/05/uirj8"
}
