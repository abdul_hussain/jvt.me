{
  "properties": {
    "rsvp": [
      "maybe"
    ],
    "in-reply-to": [
      "https://indieweb.org/2019/Brighton"
    ],
    "content": [
      {
        "value": "This is more difficult to get to than the Oxford IWC, but let's see!",
        "html": ""
      }
    ],
    "published": [
      "2019-05-18T09:08:09+0100"
    ],
    "category": [
      "indiewebcamp"
    ],
    "event": {
      "url": [
        "https://indieweb.org/2019/Brighton"
      ],
      "name": [
        "IndieWebCamp Brighton 2019"
      ],
      "start": [
        "2019-10-19T09:00:00+0100"
      ],
      "end": [
        "2019-10-20T18:00:00+0100"
      ],
      "location": {
        "properties": {
          "street-address": [
            "68 Middle Street"
          ],
          "locality": [
            "Brighton"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "aliases": [
    "/rsvps/bb9006a1-486a-4654-8292-5b4cc0640b97/",
    "/mf2/bb9006a1-486a-4654-8292-5b4cc0640b97/"
  ],
  "h": "h-entry",
  "date": "2019-05-18T09:08:09+0100",
  "tags": [
    "indiewebcamp"
  ],
  "kind": "rsvps",
  "slug": "2019/05/qzh9e"
}
