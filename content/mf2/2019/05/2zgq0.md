{
  "properties": {
    "rsvp": [
      "yes"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Tech-Nottingham/events/261626896/"
    ],
    "published": [
      "2019-05-20T22:31:00+0100"
    ],
    "category": [
      "tech-nottingham"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Tech-Nottingham/events/261626896/"
      ],
      "name": [
        "Tech Nottingham June 2019: DevOps And Time-Travelling Code"
      ],
      "start": [
        "2019-06-10T18:30:00+0100"
      ],
      "end": [
        "2019-06-10T20:30:00+0100"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, 9A Back Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "aliases": [
    "/rsvps/b007d269-1e19-4784-97a9-c0c7a73843a9/",
    "/mf2/b007d269-1e19-4784-97a9-c0c7a73843a9/"
  ],
  "h": "h-entry",
  "date": "2019-05-20T22:31:00+0100",
  "tags": [
    "tech-nottingham"
  ],
  "kind": "rsvps",
  "slug": "2019/05/2zgq0"
}
