{
  "properties": {
    "rsvp": [
      "no"
    ],
    "in-reply-to": [
      "https://www.jvt.me/events/homebrew-website-club-nottingham/2019/05/15/"
    ],
    "content": [
      {
        "value": "Unfortunately this clashes https://www.meetup.com/Nottingham-Cyber-Capital-One/events/260280774/ so I'll be attending that instead.",
        "html": ""
      }
    ],
    "published": [
      "2019-05-13T09:15:45+0100"
    ],
    "category": [
      "homebrew-website-club-nottingham"
    ],
    "event": {
      "url": [
        "https://www.jvt.me/events/homebrew-website-club-nottingham/2019/05/15/"
      ],
      "name": [
        "Homebrew Website Club"
      ],
      "start": [
        "2019-07-24T17:30:00+0100"
      ],
      "end": [
        "2019-07-24T19:30:00+0100"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Ludorati Cafe, 72 Maid Marian Way"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "aliases": [
    "/rsvps/87e6dc65-6028-42b4-ac89-e75b29560f51/",
    "/mf2/87e6dc65-6028-42b4-ac89-e75b29560f51/"
  ],
  "h": "h-entry",
  "date": "2019-05-13T09:15:45+0100",
  "tags": [
    "homebrew-website-club-nottingham"
  ],
  "kind": "rsvps",
  "slug": "2019/05/mqkcl"
}
