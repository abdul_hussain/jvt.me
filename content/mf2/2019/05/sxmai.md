{
  "properties": {
    "rsvp": [
      "yes"
    ],
    "in-reply-to": [
      "https://www.jvt.me/events/homebrew-website-club-nottingham/2019/05/29/"
    ],
    "published": [
      "2019-05-18T09:08:09+0100"
    ],
    "category": [
      "homebrew-website-club"
    ],
    "event": {
      "url": [
        "https://www.jvt.me/events/homebrew-website-club-nottingham/2019/05/29/"
      ],
      "name": [
        "Homebrew Website Club Nottingham"
      ],
      "start": [
        "2019-05-29T17:30:00+0100"
      ],
      "end": [
        "2019-05-29T19:30:00+0100"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Ludorati Cafe, 72 Maid Marian Way"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ],
          "postal-code": [
            "NG1 6BJ"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "aliases": [
    "/rsvps/0f3c4917-b043-4593-8290-7361d1837d7d/",
    "/mf2/0f3c4917-b043-4593-8290-7361d1837d7d/"
  ],
  "h": "h-entry",
  "date": "2019-05-18T09:08:09+0100",
  "tags": [
    "homebrew-website-club"
  ],
  "kind": "rsvps",
  "slug": "2019/05/sxmai"
}
