{
  "properties": {
    "rsvp": [
      "yes"
    ],
    "in-reply-to": [
      "https://www.dddeastmidlands.com/"
    ],
    "published": [
      "2019-05-18T09:08:09+0100"
    ],
    "category": [
      "developers-developers-developers"
    ],
    "event": {
      "url": [
        "https://www.dddeastmidlands.com/"
      ],
      "name": [
        "DDD East Midlands"
      ],
      "start": [
        "2019-10-26T09:00:00+0100"
      ],
      "end": [
        "2019-10-26T18:00:00+0100"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Nottingham Conference Centre, 30 Burton St, Nottingham NG1 4BU"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "aliases": [
    "/rsvps/ee155094-1b28-446f-b0ce-c2cd39a13a9c/",
    "/mf2/ee155094-1b28-446f-b0ce-c2cd39a13a9c/"
  ],
  "h": "h-entry",
  "date": "2019-05-18T09:08:09+0100",
  "tags": [
    "developers-developers-developers"
  ],
  "kind": "rsvps",
  "slug": "2019/05/mp6tl"
}
