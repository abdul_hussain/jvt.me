{
  "properties": {
    "rsvp": [
      "yes"
    ],
    "in-reply-to": [
      "https://www.meetup.com/dotnetnotts/events/261020929/"
    ],
    "published": [
      "2019-05-19T18:47:00+0100"
    ],
    "category": [
      "dotnetnotts"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/dotnetnotts/events/261020929/"
      ],
      "name": [
        "DotNetNotts - Matteo Emili - 'The computer says no' - Software quality in the DevOps world"
      ],
      "start": [
        "2019-05-20T18:30:00+0100"
      ],
      "end": [
        "2019-05-20T20:00:00+0100"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Fothergill House, 16 King Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "aliases": [
    "/rsvps/f7e81d82-059d-4bdb-8993-caa8c4cb9883/",
    "/mf2/f7e81d82-059d-4bdb-8993-caa8c4cb9883/"
  ],
  "h": "h-entry",
  "date": "2019-05-19T18:47:00+0100",
  "tags": [
    "dotnetnotts"
  ],
  "kind": "rsvps",
  "slug": "2019/05/vlfqr"
}
