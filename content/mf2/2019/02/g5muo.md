{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2019-02-08T17:15:28+00:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Tech-Nottingham/events/258091947/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Tech-Nottingham/events/258091947/"
    ],
    "published": [
      "2019-02-08T17:15:28+00:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Tech-Nottingham/events/258091947/"
      ],
      "name": [
        "Tech Nottingham February 2019: Lightning Talks!"
      ],
      "start": [
        "2019-02-11T18:30:00Z"
      ],
      "end": [
        "2019-02-11T21:15:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, Beck Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2019/02/g5muo",
  "aliases": [
    "/mf2/7ff28da5-3177-4eaf-8ba1-d54a1f211a9c/"
  ]
}
