{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2019-02-08T17:15:45+00:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/NottsJS/events/qhnpfqyzdbqb/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/NottsJS/events/qhnpfqyzdbqb/"
    ],
    "published": [
      "2019-02-08T17:15:45+00:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/NottsJS/events/qhnpfqyzdbqb/"
      ],
      "name": [
        "Securing your site like it's 1999"
      ],
      "start": [
        "2019-02-12T18:00:00Z"
      ],
      "end": [
        "2019-02-12T20:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "JH, 34a Stoney Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2019/02/anirf",
  "aliases": [
    "/mf2/fb598db5-03e6-48f8-8b09-f0ce3a63174b/"
  ]
}
