{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2019-01-02T22:10:38+00:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/NottsJS/events/qhnpfqyzcblb/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/NottsJS/events/qhnpfqyzcblb/"
    ],
    "published": [
      "2019-01-02T22:10:38+00:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/NottsJS/events/qhnpfqyzcblb/"
      ],
      "name": [
        "Indie What?"
      ],
      "start": [
        "2019-01-08T18:00:00Z"
      ],
      "end": [
        "2019-01-08T20:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "JH, 34a Stoney Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2019/01/atmpr",
  "aliases": [
    "/mf2/2b0fedcc-06c4-45fa-b5f5-bf9bb16e956b/"
  ]
}
