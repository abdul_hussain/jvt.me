{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2019-01-14T11:34:07+00:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Tech-Nottingham/events/257307390/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Tech-Nottingham/events/257307390/"
    ],
    "published": [
      "2019-01-14T11:34:07+00:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Tech-Nottingham/events/257307390/"
      ],
      "name": [
        "PLEASE NOTE CHANGE OF DETAILS: Tech Nottingham Jan 2019: Innovation And A Retro"
      ],
      "start": [
        "2019-01-14T18:30:00Z"
      ],
      "end": [
        "2019-01-14T21:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, Beck Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2019/01/ppdgf",
  "aliases": [
    "/mf2/3f84c744-8d5a-4ccf-82e4-40fdddcc3fb1/"
  ]
}
