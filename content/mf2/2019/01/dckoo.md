{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2019-01-10T22:43:25+00:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/ministry-of-testing-nottingham/events/rbwkqqyzdbjb/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/ministry-of-testing-nottingham/events/rbwkqqyzdbjb/"
    ],
    "published": [
      "2019-01-10T22:43:25+00:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/ministry-of-testing-nottingham/events/rbwkqqyzdbjb/"
      ],
      "name": [
        "#NottsTest - Careers Special"
      ],
      "start": [
        "2019-02-06T19:00:00Z"
      ],
      "end": [
        "2019-02-06T22:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Experian - The Sir John Peace Building, Experian Way"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2019/01/dckoo",
  "aliases": [
    "/mf2/32cb2a00-14dd-49c2-9567-aae63a75860d/"
  ]
}
