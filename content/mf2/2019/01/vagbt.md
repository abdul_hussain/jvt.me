{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2019-01-14T11:33:59+00:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/ministry-of-testing-nottingham/events/257426170/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/ministry-of-testing-nottingham/events/257426170/"
    ],
    "published": [
      "2019-01-14T11:33:59+00:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/ministry-of-testing-nottingham/events/257426170/"
      ],
      "name": [
        "SWTC Midlands - What is Testing?"
      ],
      "start": [
        "2019-01-16T18:00:00Z"
      ],
      "end": [
        "2019-01-16T21:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Experian - The Sir John Peace Building, Experian Way"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2019/01/vagbt",
  "aliases": [
    "/mf2/93f34b54-ab07-4426-a64b-03612e4803d4/"
  ]
}
