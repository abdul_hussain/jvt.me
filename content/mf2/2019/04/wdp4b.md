{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2019-04-30T08:18:04+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Women-In-Tech-Nottingham/events/260564538/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Women-In-Tech-Nottingham/events/260564538/"
    ],
    "published": [
      "2019-04-30T08:18:04+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Women-In-Tech-Nottingham/events/260564538/"
      ],
      "name": [
        "Women In Tech May 2019 - UX Clichés (and a new venue!)"
      ],
      "start": [
        "2019-05-02T18:30:00+01:00"
      ],
      "end": [
        "2019-05-02T21:00:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, 9A Beck St"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2019/04/wdp4b",
  "aliases": [
    "/mf2/6f737c14-b7e3-4d89-ad0f-6ae3b160abf1/"
  ]
}
