{
  "kind": "rsvps",
  "client_id": "https://indigenous.realize.be",
  "date": "2019-09-30T21:05:00+0200",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/NottsJS/events/264424556/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/NottsJS/events/264424556/"
    ],
    "published": [
      "2019-09-30T21:05:00+0200"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/NottsJS/events/264424556/"
      ],
      "name": [
        "Next Frontier in Front-end Development: Real-time monitoring and alerting"
      ],
      "start": [
        "2019-10-08T18:00:00+01:00"
      ],
      "end": [
        "2019-10-08T21:00:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Capital One (Europe) plc, Station St"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2019/09/fu7di",
  "aliases": [
    "/mf2/260369ef-9b45-45c7-9454-03a76f1714d1/"
  ]
}
