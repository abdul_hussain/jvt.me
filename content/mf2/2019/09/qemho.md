{
  "kind": "rsvps",
  "client_id": "https://indigenous.realize.be",
  "date": "2019-09-29T18:46:00+0200",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP no to https://2019.indieweb.org/nyc"
    ],
    "in-reply-to": [
      "https://2019.indieweb.org/nyc"
    ],
    "published": [
      "2019-09-29T18:46:00+0200"
    ],
    "rsvp": [
      "no"
    ],
    "content": [
      {
        "html": "",
        "value": "This would be cool, but don't think I'll be able to do two IndieWebCamps in a row, especially across the world!"
      }
    ],
    "event": {
      "url": [
        "https://2019.indieweb.org/nyc"
      ],
      "name": [
        "IndieWebCamp NYC"
      ],
      "start": [
        "2019-10-05"
      ],
      "end": [
        "2019-10-06"
      ],
      "location": {
        "properties": {
          "street-address": [
            "163 William St 2nd Floor"
          ],
          "locality": [
            "New York City"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2019/09/qemho",
  "aliases": [
    "/mf2/d8dbcb3a-4b45-4dbf-bcd6-4f4521ba762a/"
  ]
}
