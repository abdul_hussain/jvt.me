{
  "kind": "rsvps",
  "date": "2019-09-12T23:19:00+0100",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.jvt.me/events/homebrew-website-club-nottingham/2019/10/30/"
    ],
    "in-reply-to": [
      "https://www.jvt.me/events/homebrew-website-club-nottingham/2019/10/30/"
    ],
    "published": [
      "2019-09-12T23:19:00+0100"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.jvt.me/events/homebrew-website-club-nottingham/2019/10/30/"
      ],
      "name": [
        "Homebrew Website Club: Nottingham"
      ],
      "start": [
        "2019-10-30T17:30:00+0100"
      ],
      "end": [
        "2019-10-30T19:30:00+0100"
      ],
      "location": {
        "properties": {
          "street-address": [
            "72 Maid Marian Way"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ],
          "postal-code": [
            "NG1 6BJ"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2019/09/1xgg1",
  "aliases": [
    "/mf2/6b4544d3-d5fe-43e2-9cec-2d7a3d45ede0/"
  ]
}
