{
  "kind": "rsvps",
  "slug": "2019/09/9mphs",
  "date": "2019-09-28T07:30:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.eventbrite.com/e/indiewebcamp-amsterdam-tickets-68004881431"
    ],
    "in-reply-to": [
      "https://www.eventbrite.com/e/indiewebcamp-amsterdam-tickets-68004881431"
    ],
    "published": [
      "2019-09-28T07:30:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.eventbrite.com/e/indiewebcamp-amsterdam-tickets-68004881431"
      ],
      "name": [
        "IndieWebCamp Amsterdam"
      ],
      "start": [
        "2019-09-28T07:30:00Z"
      ],
      "end": [
        "2019-09-29T16:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "7 Kattenburgerstraat, #Building 039"
          ],
          "locality": [
            "Amsterdam"
          ],
          "postal-code": [
            "1018 JA"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
