{
  "kind": "rsvps",
  "date": "2019-09-03T07:42:00+0100",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Tech-Nottingham/events/264505430/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Tech-Nottingham/events/264505430/"
    ],
    "published": [
      "2019-09-03T07:42:00+0100"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Tech-Nottingham/events/264505430/"
      ],
      "name": [
        "Tech Nottingham September 2019 - Working Smart"
      ],
      "start": [
        "2019-09-09T18:30:00+01:00"
      ],
      "end": [
        "2019-09-09T21:00:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, Beck Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2019/09/vetdl",
  "aliases": [
    "/mf2/1744f324-2fc0-4313-a7b0-2f2af6152363/"
  ]
}
