{
  "kind": "rsvps",
  "date": "2019-09-12T23:19:00+0100",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.jvt.me/events/homebrew-website-club-nottingham/2019/10/16/"
    ],
    "in-reply-to": [
      "https://www.jvt.me/events/homebrew-website-club-nottingham/2019/10/16/"
    ],
    "published": [
      "2019-09-12T23:19:00+0100"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.jvt.me/events/homebrew-website-club-nottingham/2019/10/16/"
      ],
      "name": [
        "Homebrew Website Club: Nottingham"
      ],
      "start": [
        "2019-10-16T17:30:00+0100"
      ],
      "end": [
        "2019-10-16T19:30:00+0100"
      ],
      "location": {
        "properties": {
          "street-address": [
            "72 Maid Marian Way"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ],
          "postal-code": [
            "NG1 6BJ"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2019/09/gkebu",
  "aliases": [
    "/mf2/d7915978-79c1-4719-a5be-4db5469c630c/"
  ]
}
