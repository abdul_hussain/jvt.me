{
  "kind": "rsvps",
  "client_id": "https://indigenous.realize.be",
  "date": "2019-09-30T20:58:00+0200",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/PHPMiNDS-in-Nottingham/events/265265234/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/265265234/"
    ],
    "published": [
      "2019-09-30T20:58:00+0200"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/265265234/"
      ],
      "name": [
        "DEVEL-OPS - BRINGING DEVELOPMENT PRACTICES TO OPERATIONS TASKS"
      ],
      "start": [
        "2019-10-10T19:00:00+01:00"
      ],
      "end": [
        "2019-10-10T21:00:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "JH,  34a Stoney Street, Nottingham, NG1 1NB."
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2019/09/vfhel",
  "aliases": [
    "/mf2/8d8dc4fe-19cc-4c27-83cd-abc75356192a/"
  ]
}
