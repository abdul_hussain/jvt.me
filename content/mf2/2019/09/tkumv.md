{
  "kind": "rsvps",
  "client_id": "https://indigenous.realize.be",
  "date": "2019-09-18T21:12:00+0100",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/DevOps-Notts/events/264334937/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/DevOps-Notts/events/264334937/"
    ],
    "published": [
      "2019-09-18T21:12:00+0100"
    ],
    "rsvp": [
      "no"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/DevOps-Notts/events/264334937/"
      ],
      "name": [
        "DevOps Notts - September 2019"
      ],
      "start": [
        "2019-09-24T18:30:00+01:00"
      ],
      "end": [
        "2019-09-24T21:30:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Rebel Recruiters, Huntingdon St"
          ],
          "locality": [
            "Nottinghamshire"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2019/09/tkumv",
  "aliases": [
    "/mf2/06e9688f-5d48-4d64-bcf3-b431c24750b7/"
  ]
}
