{
  "kind": "rsvps",
  "client_id": "https://indigenous.realize.be",
  "date": "2019-09-30T21:15:00+0200",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Tech-Nottingham/events/265293037/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Tech-Nottingham/events/265293037/"
    ],
    "published": [
      "2019-09-30T21:15:00+0200"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Tech-Nottingham/events/265293037/"
      ],
      "name": [
        "Tech Nottingham October 2019 - Hacktoberfest And Evidence-Based Innovation"
      ],
      "start": [
        "2019-10-14T18:30:00+01:00"
      ],
      "end": [
        "2019-10-14T21:00:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, Beck Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2019/09/lhey6",
  "aliases": [
    "/mf2/54703f9d-e9b9-4991-a55f-f752bd724ada/"
  ]
}
