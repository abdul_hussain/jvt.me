{
  "kind": "rsvps",
  "slug": "2019/12/d75iw",
  "client_id": "https://indigenous.realize.be",
  "date": "2019-12-19T14:23:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Women-In-Tech-Nottingham/events/266971867/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Women-In-Tech-Nottingham/events/266971867/"
    ],
    "published": [
      "2019-12-19T14:23:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Women-In-Tech-Nottingham/events/266971867/"
      ],
      "name": [
        "Women in Tech Januray 2nd 2020 - Sensing Change: The Rise of the Smart bins"
      ],
      "start": [
        "2020-01-02T18:30:00Z"
      ],
      "end": [
        "2020-01-02T21:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, 9A Beck St"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
