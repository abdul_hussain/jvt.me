{
  "kind": "rsvps",
  "slug": "2019/12/w07tw",
  "client_id": "https://indigenous.realize.be",
  "date": "2019-12-28T20:14:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.jvt.me/events/homebrew-website-club-nottingham/2020/01/08/"
    ],
    "in-reply-to": [
      "https://www.jvt.me/events/homebrew-website-club-nottingham/2020/01/08/"
    ],
    "published": [
      "2019-12-28T20:14:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.jvt.me/events/homebrew-website-club-nottingham/2020/01/08/"
      ],
      "name": [
        "Homebrew Website Club: Nottingham"
      ],
      "start": [
        "2020-01-08T17:30:00+0000"
      ],
      "end": [
        "2020-01-08T19:30:00+0000"
      ],
      "location": {
        "properties": {
          "street-address": [
            "72 Maid Marian Way"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ],
          "postal-code": [
            "NG1 6BJ"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
