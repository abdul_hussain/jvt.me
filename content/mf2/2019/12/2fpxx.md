{
  "kind": "rsvps",
  "slug": "2019/12/2fpxx",
  "client_id": "https://indigenous.realize.be",
  "date": "2019-12-19T14:24:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Nottingham-AWS-Meetup/events/266207923/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Nottingham-AWS-Meetup/events/266207923/"
    ],
    "published": [
      "2019-12-19T14:24:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Nottingham-AWS-Meetup/events/266207923/"
      ],
      "name": [
        "Lightning Talks - AWS Nottingham"
      ],
      "start": [
        "2020-02-25T18:00:00Z"
      ],
      "end": [
        "2020-02-25T20:30:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "BJSS, 16 King St"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
