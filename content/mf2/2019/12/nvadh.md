{
  "kind": "rsvps",
  "slug": "2019/12/nvadh",
  "client_id": "https://indigenous.realize.be",
  "date": "2019-12-19T18:44:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Tech-Nottingham/events/267300253/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Tech-Nottingham/events/267300253/"
    ],
    "published": [
      "2019-12-19T18:44:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Tech-Nottingham/events/267300253/"
      ],
      "name": [
        "Tech Nottingham January 2020 - Ethics In Advertising And High Performance Teams"
      ],
      "start": [
        "2020-01-13T18:30:00Z"
      ],
      "end": [
        "2020-01-13T21:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, Beck Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
