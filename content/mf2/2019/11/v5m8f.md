{
  "kind": "rsvps",
  "slug": "2019/11/v5m8f",
  "client_id": "https://indigenous.realize.be",
  "date": "2019-11-05T22:55:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.jvt.me/events/homebrew-website-club-nottingham/2019/11/27/"
    ],
    "in-reply-to": [
      "https://www.jvt.me/events/homebrew-website-club-nottingham/2019/11/27/"
    ],
    "published": [
      "2019-11-05T22:55:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.jvt.me/events/homebrew-website-club-nottingham/2019/11/27/"
      ],
      "name": [
        "Homebrew Website Club: Nottingham"
      ],
      "start": [
        "2019-11-27T17:30:00+0000"
      ],
      "end": [
        "2019-11-27T19:30:00+0000"
      ],
      "location": {
        "properties": {
          "street-address": [
            "72 Maid Marian Way"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ],
          "postal-code": [
            "NG1 6BJ"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "aliases": [
  ]
}
