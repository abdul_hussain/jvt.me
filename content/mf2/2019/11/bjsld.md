{
  "kind": "rsvps",
  "slug": "2019/11/bjsld",
  "client_id": "https://indigenous.realize.be",
  "date": "2019-11-14T14:50:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP no to https://www.meetup.com/London-Gitlab-Meetup-Group/events/266440924/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/London-Gitlab-Meetup-Group/events/266440924/"
    ],
    "published": [
      "2019-11-14T14:50:00Z"
    ],
    "rsvp": [
      "no"
    ],
    "content": [
      {
        "html": "",
        "value": "Unfortunately I won't be able to make this despite being in London that day - should be a good one!"
      }
    ],
    "event": {
      "url": [
        "https://www.meetup.com/London-Gitlab-Meetup-Group/events/266440924/"
      ],
      "name": [
        "Gitlab London Christmas Meetup"
      ],
      "start": [
        "2019-12-04T18:30:00Z"
      ],
      "end": [
        "2019-12-04T20:30:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "4 Christopher St, 4 Christopher St"
          ],
          "locality": [
            "London"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
