{
  "kind": "rsvps",
  "slug": "2019/11/iw2n0",
  "client_id": "https://micropublish.net",
  "date": "2019-11-10T22:44:53.864+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP no to https://www.meetup.com/PHPMiNDS-in-Nottingham/events/266300756/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/266300756/"
    ],
    "published": [
      "2019-11-10T22:44:53.864+01:00"
    ],
    "rsvp": [
      "no"
    ],
    "content": [
      {
        "html": "",
        "value": "Unfortunately I won't be able to make this due to family circumstances, but hope everyone has a great time!"
      }
    ],
    "event": {
      "url": [
        "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/266300756/"
      ],
      "name": [
        "PHPMiNDS November 2019:  \t⚡ Lightning Talks  \t⚡"
      ],
      "start": [
        "2019-11-14T19:00:00Z"
      ],
      "end": [
        "2019-11-14T21:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "JH,  34a Stoney Street, Nottingham, NG1 1NB."
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
