{
  "kind": "rsvps",
  "slug": "2019/03/p7vbl",
  "date": "2019-03-21T18:00:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.eventbrite.co.uk/e/beyond-rest-the-future-of-web-apis-tickets-54846386017"
    ],
    "in-reply-to": [
      "https://www.eventbrite.co.uk/e/beyond-rest-the-future-of-web-apis-tickets-54846386017"
    ],
    "published": [
      "2019-03-21T18:00:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.eventbrite.co.uk/e/beyond-rest-the-future-of-web-apis-tickets-54846386017"
      ],
      "name": [
        "Beyond REST - The Future of Web APIs"
      ],
      "start": [
        "2019-03-21T18:00:00Z"
      ],
      "end": [
        "2019-03-21T21:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Fothergill House,16 King St"
          ],
          "locality": [
            "Nottingham"
          ],
          "postal-code": [
            "NG1 2AS"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
