{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2019-03-27T17:59:44+00:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/PHPMiNDS-in-Nottingham/events/259712515/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/259712515/"
    ],
    "published": [
      "2019-03-27T17:59:44+00:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/259712515/"
      ],
      "name": [
        "Lightening talks and Retrospective"
      ],
      "start": [
        "2019-04-11T19:00:00+01:00"
      ],
      "end": [
        "2019-04-11T21:00:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "JH, 34a Stoney Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2019/03/a3z4j",
  "aliases": [
    "/mf2/141c0a99-377c-40d4-aada-e972a3962496/"
  ]
}
