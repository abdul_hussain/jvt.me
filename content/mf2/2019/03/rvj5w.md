{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2019-03-03T21:40:59+00:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Tech-Nottingham/events/259263225/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Tech-Nottingham/events/259263225/"
    ],
    "published": [
      "2019-03-03T21:40:59+00:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Tech-Nottingham/events/259263225/"
      ],
      "name": [
        "Tech Nottingham March 2019: HTTPS is Broken and DrawUX"
      ],
      "start": [
        "2019-03-11T18:30:00Z"
      ],
      "end": [
        "2019-03-11T21:15:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, Beck Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2019/03/rvj5w",
  "aliases": [
    "/mf2/b3bbe9b7-507b-4aa4-bc0e-041796ff1bbf/"
  ]
}
