{
  "properties": {
    "rsvp": [
      "no"
    ],
    "in-reply-to": [
      "https://www.jvt.me/events/homebrew-website-club-nottingham/2019/07/24/"
    ],
    "content": [
      {
        "value": "Unfortunately I won't be able to make it as Anna and I have plans that night - but please do go ahead and meet!",
        "html": ""
      }
    ],
    "published": [
      "2019-07-10T21:55:07+0100"
    ],
    "category": [
      "homebrew-website-club"
    ],
    "event": {
      "url": [
        "https://www.jvt.me/events/homebrew-website-club-nottingham/2019/07/24/"
      ],
      "name": [
        "Homebrew Website Club"
      ],
      "start": [
        "2019-07-24T17:30:00+0100"
      ],
      "end": [
        "2019-07-24T19:30:00+0100"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Ludorati Cafe, 72 Maid Marian Way"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "aliases": [
    "/rsvps/28540bc1-a20b-4550-a826-ca59ba981a14/",
    "/mf2/28540bc1-a20b-4550-a826-ca59ba981a14/"
  ],
  "h": "h-entry",
  "date": "2019-07-10T21:55:07+0100",
  "tags": [
    "homebrew-website-club"
  ],
  "kind": "rsvps",
  "slug": "2019/07/qsgqg"
}
