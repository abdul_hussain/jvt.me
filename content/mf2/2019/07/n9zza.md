{
  "properties": {
    "rsvp": [
      "no"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Nottingham-Cyber-Capital-One/events/262255726/"
    ],
    "content": [
      {
        "value": "I've decided to attend Homebrew Website Club instead",
        "html": ""
      }
    ],
    "published": [
      "2019-07-09T22:48:38+0100"
    ],
    "category": [
      "cyber-nottingham"
    ],
    "name": [
      "RSVP no to https://www.meetup.com/Nottingham-Cyber-Capital-One/events/262255726/"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Nottingham-Cyber-Capital-One/events/262255726/"
      ],
      "name": [
        "Cyber Nottingham - July Meetup"
      ],
      "start": [
        "2019-07-10T18:00:00+01:00"
      ],
      "end": [
        "2019-07-10T21:00:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Capital One (Europe) plc, Station St"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "aliases": [
    "/rsvps/a63eea79-747a-4145-bcd2-a81b966d5004/",
    "/mf2/a63eea79-747a-4145-bcd2-a81b966d5004/"
  ],
  "h": "h-entry",
  "date": "2019-07-09T22:48:38+0100",
  "tags": [
    "cyber-nottingham"
  ],
  "kind": "rsvps",
  "slug": "2019/07/n9zza"
}
