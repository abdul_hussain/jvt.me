{
  "properties": {
    "rsvp": [
      "yes"
    ],
    "in-reply-to": [
      "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/263270719/"
    ],
    "content": [
      {
        "value": "This is going to be a very interesting talk - there is so much of this around (I'm guilty too) and it'll be good to hear some more about it from James Seconde",
        "html": ""
      }
    ],
    "published": [
      "2019-07-19T00:09:26+0200"
    ],
    "category": [
      "phpminds"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/263270719/"
      ],
      "name": [
        "PHPMiNDS: The Politics of Tool-Shaming"
      ],
      "start": [
        "2019-08-08T19:00:00+0100"
      ],
      "end": [
        "2019-08-08T21:00:00+0100"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, 9A Back Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "aliases": [
    "/rsvps/e30355c4-b6c1-42d1-b26a-853bfc98038e/",
    "/mf2/e30355c4-b6c1-42d1-b26a-853bfc98038e/"
  ],
  "h": "h-entry",
  "date": "2019-07-19T00:09:26+0200",
  "tags": [
    "phpminds"
  ],
  "kind": "rsvps",
  "slug": "2019/07/8ibgn"
}
