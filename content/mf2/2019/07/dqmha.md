{
  "properties": {
    "rsvp": [
      "yes"
    ],
    "in-reply-to": [
      "https://www.jvt.me/events/homebrew-website-club-nottingham/2019/07/10/"
    ],
    "published": [
      "2019-07-09T22:48:04+0100"
    ],
    "category": [
      "homebrew-website-club-nottingham"
    ],
    "event": {
      "url": [
        "https://www.jvt.me/events/homebrew-website-club-nottingham/2019/07/10/"
      ],
      "name": [
        "Homebrew Website Club"
      ],
      "start": [
        "2019-07-10T17:30:00+0100"
      ],
      "end": [
        "2019-07-10T19:30:00+0100"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Ludorati Cafe, 72 Maid Marian Way"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "aliases": [
    "/rsvps/2a15a5b8-f1cf-4313-92e8-aac3f439b688/",
    "/mf2/2a15a5b8-f1cf-4313-92e8-aac3f439b688/"
  ],
  "h": "h-entry",
  "date": "2019-07-09T22:48:04+0100",
  "tags": [
    "homebrew-website-club-nottingham"
  ],
  "kind": "rsvps",
  "slug": "2019/07/dqmha"
}
