{
  "kind": "rsvps",
  "slug": "2015/12/p6vj6",
  "date": "2015-12-19T10:00:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.eventbrite.co.uk/e/hack-the-holidays-registration-19240652309"
    ],
    "in-reply-to": [
      "https://www.eventbrite.co.uk/e/hack-the-holidays-registration-19240652309"
    ],
    "published": [
      "2015-12-19T10:00:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.eventbrite.co.uk/e/hack-the-holidays-registration-19240652309"
      ],
      "name": [
        "Hack the Holidays"
      ],
      "start": [
        "2015-12-19T10:00:00Z"
      ],
      "end": [
        "2015-12-20T17:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "London Road"
          ],
          "locality": [
            "Peterborough"
          ],
          "postal-code": [
            "PE2 8AL"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
