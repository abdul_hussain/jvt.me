{
  "kind": "rsvps",
  "slug": "2015/12/9avrt",
  "date": "2015-12-01T18:30:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.eventbrite.co.uk/e/nottingham-tech-community-christmas-party-tickets-19502554666"
    ],
    "in-reply-to": [
      "https://www.eventbrite.co.uk/e/nottingham-tech-community-christmas-party-tickets-19502554666"
    ],
    "published": [
      "2015-12-01T18:30:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.eventbrite.co.uk/e/nottingham-tech-community-christmas-party-tickets-19502554666"
      ],
      "name": [
        "Nottingham Tech Community Christmas Party"
      ],
      "start": [
        "2015-12-01T18:30:00Z"
      ],
      "end": [
        "2015-12-01T22:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "24-32 Carlton Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "postal-code": [
            "NG1 1NN"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
