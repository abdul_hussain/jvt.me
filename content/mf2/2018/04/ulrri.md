{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2018-04-11T23:04:04+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Chef-Users-UK/events/249461424/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Chef-Users-UK/events/249461424/"
    ],
    "published": [
      "2018-04-11T23:04:04+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Chef-Users-UK/events/249461424/"
      ],
      "name": [
        "It's time for Chef! Four! Teen!"
      ],
      "start": [
        "2018-04-18T18:30:00+01:00"
      ],
      "end": [
        "2018-04-18T21:00:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Ticketmaster, 4 Pentonville Road"
          ],
          "locality": [
            "London, N1 9HF"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2018/04/ulrri",
  "aliases": [
    "/mf2/878eca57-4d6a-41ae-893d-56467efde9da/"
  ]
}
