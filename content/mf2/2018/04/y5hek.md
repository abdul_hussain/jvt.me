{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2018-04-27T16:54:05+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Notts-Dev-Workshop/events/250212920/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Notts-Dev-Workshop/events/250212920/"
    ],
    "published": [
      "2018-04-27T16:54:05+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Notts-Dev-Workshop/events/250212920/"
      ],
      "name": [
        "Beginning With Terraform with Chris Emerson"
      ],
      "start": [
        "2018-05-08T18:00:00+01:00"
      ],
      "end": [
        "2018-05-08T21:00:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "UNiDAYS, 2 Castle Boulevard, Nottingham, NG7 1FB, England"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2018/04/y5hek",
  "aliases": [
    "/mf2/c5a2b987-be3d-4ca9-91b8-d9f26c45abc0/"
  ]
}
