{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2018-04-29T10:00:04+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Tech-Nottingham/events/250061017/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Tech-Nottingham/events/250061017/"
    ],
    "published": [
      "2018-04-29T10:00:04+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Tech-Nottingham/events/250061017/"
      ],
      "name": [
        "Tech Nottingham May 2018: Quantifying Feelings and Automating Curtains"
      ],
      "start": [
        "2018-05-14T18:30:00+01:00"
      ],
      "end": [
        "2018-05-14T21:00:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, Beck Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2018/04/kp51e",
  "aliases": [
    "/mf2/cb8b3cf5-00d4-4293-b310-b2b1729b3919/"
  ]
}
