{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2018-03-15T12:03:53+00:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/NottsJS/events/jrhlfpyxgbnb/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/NottsJS/events/jrhlfpyxgbnb/"
    ],
    "published": [
      "2018-03-15T12:03:53+00:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/NottsJS/events/jrhlfpyxgbnb/"
      ],
      "name": [
        "April 2018 - Alexa Skill Development with Amazon"
      ],
      "start": [
        "2018-04-10T18:45:00+01:00"
      ],
      "end": [
        "2018-04-10T21:00:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "JH, 34a Stoney Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2018/03/numfe",
  "aliases": [
    "/mf2/b637ce4b-ae3b-4468-b27e-2ce95d137283/"
  ]
}
