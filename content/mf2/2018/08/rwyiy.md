{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2018-08-30T21:41:51+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/ministry-of-testing-nottingham/events/nggkcqyxmbhb/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/ministry-of-testing-nottingham/events/nggkcqyxmbhb/"
    ],
    "published": [
      "2018-08-30T21:41:51+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/ministry-of-testing-nottingham/events/nggkcqyxmbhb/"
      ],
      "name": [
        "#NottsTest Mobile Dev Special"
      ],
      "start": [
        "2018-09-05T19:00:00+01:00"
      ],
      "end": [
        "2018-09-05T22:00:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "UNiDAYS, 2 Castle Boulevard, Nottingham, NG7 1FB"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2018/08/rwyiy",
  "aliases": [
    "/mf2/88165b6f-c0e6-40fd-938b-20ff19ba1fad/"
  ]
}
