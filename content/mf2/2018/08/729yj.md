{
  "kind": "rsvps",
  "slug": "2018/08/729yj",
  "date": "2018-08-18T09:00:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.eventbrite.co.uk/e/oggcamp-2018-tickets-41947075833"
    ],
    "in-reply-to": [
      "https://www.eventbrite.co.uk/e/oggcamp-2018-tickets-41947075833"
    ],
    "published": [
      "2018-08-18T09:00:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.eventbrite.co.uk/e/oggcamp-2018-tickets-41947075833"
      ],
      "name": [
        "OggCamp 2018"
      ],
      "start": [
        "2018-08-18T09:00:00Z"
      ],
      "end": [
        "2018-08-19T16:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Charles Street"
          ],
          "locality": [
            "Sheffield"
          ],
          "postal-code": [
            "S1 2ND"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
