{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2018-12-01T22:18:17+00:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/PHPMiNDS-in-Nottingham/events/256554079/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/256554079/"
    ],
    "published": [
      "2018-12-01T22:18:17+00:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/256554079/"
      ],
      "name": [
        "AWS Re:Invent replay and your Devops questions answered"
      ],
      "start": [
        "2018-12-06T19:00:00Z"
      ],
      "end": [
        "2018-12-06T21:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "JH,  34a Stoney Street, Nottingham, NG1 1NB."
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2018/12/uteco",
  "aliases": [
    "/mf2/cec99c04-dac1-4798-bef0-18f67ea74c46/"
  ]
}
