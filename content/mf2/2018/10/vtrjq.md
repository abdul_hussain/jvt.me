{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2018-10-29T15:17:03+00:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/ministry-of-testing-nottingham/events/fgtpfqyxpbkb/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/ministry-of-testing-nottingham/events/fgtpfqyxpbkb/"
    ],
    "published": [
      "2018-10-29T15:17:03+00:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/ministry-of-testing-nottingham/events/fgtpfqyxpbkb/"
      ],
      "name": [
        "#NottsTest - Lightning Talks & Lean Coffee"
      ],
      "start": [
        "2018-11-07T19:00:00Z"
      ],
      "end": [
        "2018-11-07T22:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "UNiDAYS, 2 Castle Boulevard, Nottingham, NG7 1FB"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2018/10/vtrjq",
  "aliases": [
    "/mf2/9721f174-dfb2-4236-ac4e-b718db76ee35/"
  ]
}
