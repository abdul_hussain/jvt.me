{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2018-06-09T23:47:34+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/PHPMiNDS-in-Nottingham/events/250839745/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/250839745/"
    ],
    "published": [
      "2018-06-09T23:47:34+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/250839745/"
      ],
      "name": [
        "The Test Suite Holy Trinity - Dave Liddament"
      ],
      "start": [
        "2018-06-14T19:00:00+01:00"
      ],
      "end": [
        "2018-06-14T21:00:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "JH, 34a Stoney Street, Nottingham, NG1 1NB"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2018/06/aakwv",
  "aliases": [
    "/mf2/1be45762-4be1-474f-9f28-7efb89932df4/"
  ]
}
