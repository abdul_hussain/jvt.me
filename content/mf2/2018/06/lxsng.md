{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2018-06-05T21:52:15+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/NottsJS/events/sfwwlpyxjbqb/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/NottsJS/events/sfwwlpyxjbqb/"
    ],
    "published": [
      "2018-06-05T21:52:15+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/NottsJS/events/sfwwlpyxjbqb/"
      ],
      "name": [
        "Saving the World with JSON Schema!"
      ],
      "start": [
        "2018-06-12T18:30:00+01:00"
      ],
      "end": [
        "2018-06-12T20:00:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "JH, 34a Stoney Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2018/06/lxsng",
  "aliases": [
    "/mf2/b564f5dd-1812-405a-ac7b-4e6fef92e877/"
  ]
}
