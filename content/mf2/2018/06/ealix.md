{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2018-06-03T19:42:46+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Tech-Nottingham/events/251303614/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Tech-Nottingham/events/251303614/"
    ],
    "published": [
      "2018-06-03T19:42:46+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Tech-Nottingham/events/251303614/"
      ],
      "name": [
        "Tech Nottingham June 2018: Marketing Technical Products and Async in Javascript"
      ],
      "start": [
        "2018-06-11T18:30:00+01:00"
      ],
      "end": [
        "2018-06-11T21:00:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, Beck Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2018/06/ealix",
  "aliases": [
    "/mf2/9aba80cf-010c-43fc-85b4-53dadf427eca/"
  ]
}
