{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2018-05-14T20:53:07+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/NottAgile/events/246673749/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/NottAgile/events/246673749/"
    ],
    "published": [
      "2018-05-14T20:53:07+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/NottAgile/events/246673749/"
      ],
      "name": [
        "May Meetup - The Ultimate DevSecOps and Well what would you do next...?"
      ],
      "start": [
        "2018-05-23T18:30:00+01:00"
      ],
      "end": [
        "2018-05-23T21:00:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Capital One, Station Street, NG2 3HX"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2018/05/xw5ph",
  "aliases": [
    "/mf2/18d6a980-6657-492c-99cd-59d1b38f3c64/"
  ]
}
