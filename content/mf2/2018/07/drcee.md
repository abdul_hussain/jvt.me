{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2018-07-27T11:12:06+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Tech-Nottingham/events/253202050/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Tech-Nottingham/events/253202050/"
    ],
    "published": [
      "2018-07-27T11:12:06+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Tech-Nottingham/events/253202050/"
      ],
      "name": [
        "Tech Nottingham August 2018: Secure Signups and Visual Testing"
      ],
      "start": [
        "2018-08-13T18:30:00+01:00"
      ],
      "end": [
        "2018-08-13T21:00:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, Beck Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2018/07/drcee",
  "aliases": [
    "/mf2/29a6e459-3597-4fb1-a46c-7bd11e3b0236/"
  ]
}
