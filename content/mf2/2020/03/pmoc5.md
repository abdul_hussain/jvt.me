{
  "kind": "likes",
  "slug": "2020/03/pmoc5",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1235838751508553729"
      ],
      "url": [
        "https://twitter.com/whoisgraham/status/1235838751508553729"
      ],
      "video": [
        "https://video.twimg.com/tweet_video/ESaUyfeU0AAVouq.mp4"
      ],
      "published": [
        "2020-03-06T08:04:53+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/JamieTanna/status/1235637201821675522"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:whoisgraham"
            ],
            "numeric-id": [
              "133487462"
            ],
            "name": [
              "Graham : A Star Wars Story"
            ],
            "nickname": [
              "whoisgraham"
            ],
            "url": [
              "https://twitter.com/whoisgraham"
            ],
            "published": [
              "2010-04-15T23:03:34+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/2907221110/712dfcecc7fd591915d1355bdbe4adcd.jpeg"
            ]
          }
        }
      ],
      "location": [
        {
          "type": [
            "h-card",
            "p-location"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:3554e1ec5e78353e"
            ],
            "name": [
              "Clifton, England"
            ]
          }
        }
      ],
      "content": [
        {
          "html": "\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-06T08:06:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @whoisgraham's tweet"
    ],
    "like-of": [
      "https://twitter.com/whoisgraham/status/1235838751508553729"
    ],
    "published": [
      "2020-03-06T08:06:00Z"
    ],
    "syndication": [
      "https://twitter.com/whoisgraham/status/1235838751508553729"
    ]
  }
}
