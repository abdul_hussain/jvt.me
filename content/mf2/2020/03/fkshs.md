{
  "kind": "likes",
  "slug": "2020/03/fkshs",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1243457417725313024"
      ],
      "url": [
        "https://twitter.com/technottingham/status/1243457417725313024"
      ],
      "video": [
        "https://video.twimg.com/tweet_video/EUGl7SKXsAAFSi_.mp4"
      ],
      "published": [
        "2020-03-27T08:38:45+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:technottingham"
            ],
            "numeric-id": [
              "384492431"
            ],
            "name": [
              "Tech Nottingham"
            ],
            "nickname": [
              "technottingham"
            ],
            "url": [
              "https://twitter.com/technottingham",
              "http://technottingham.com"
            ],
            "published": [
              "2011-10-03T19:47:31+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1023974499757293570/ZoPc_QsO.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Morning #TechNott! What's everyone got planned this weekend??\n\nAs we approach a weekend where a lot of people can't see their friends, family or do their usual hobbies - we'd love to hear about activities where anyone can join remotely 💻📲🎧📽️❤️",
          "html": "<div style=\"white-space: pre\">Morning <a href=\"https://twitter.com/search?q=%23TechNott\">#TechNott</a>! What's everyone got planned this weekend??\n\nAs we approach a weekend where a lot of people can't see their friends, family or do their usual hobbies - we'd love to hear about activities where anyone can join remotely 💻📲🎧📽️❤️</div>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-27T09:14:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @technottingham's tweet"
    ],
    "like-of": [
      "https://twitter.com/technottingham/status/1243457417725313024"
    ],
    "published": [
      "2020-03-27T09:14:00Z"
    ],
    "syndication": [
      "https://twitter.com/technottingham/status/1243457417725313024"
    ]
  }
}
