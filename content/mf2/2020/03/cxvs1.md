{
  "kind": "reposts",
  "slug": "2020/03/cxvs1",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1239329370856214528"
      ],
      "url": [
        "https://twitter.com/ohhelloana/status/1239329370856214528"
      ],
      "published": [
        "2020-03-15T23:15:22+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:ohhelloana"
            ],
            "numeric-id": [
              "434457570"
            ],
            "name": [
              "Ana Rodrigues"
            ],
            "nickname": [
              "ohhelloana"
            ],
            "url": [
              "https://twitter.com/ohhelloana",
              "https://ohhelloana.blog"
            ],
            "published": [
              "2011-12-11T21:57:15+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "London"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1126838936519815168/F4HZ_Fmu.jpg"
            ]
          }
        }
      ],
      "content": [
        "Friends, the current situation may make you to feel productivity shame. I feel that. It sucks. It’s okay to be offline. It’s okay not to work on a side project. It’s okay to turn off the computer after work. You’re not lazy. There’s no expectations. WFH may blur the line."
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-15T23:19:00Z",
  "h": "h-entry",
  "properties": {
    "published": [
      "2020-03-15T23:19:00Z"
    ],
    "repost-of": [
      "https://twitter.com/ohhelloana/status/1239329370856214528"
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1251560411473854464"
    ]
  }
}
