{
  "kind": "likes",
  "slug": "2020/03/dz1yx",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1240985673156513793"
      ],
      "url": [
        "https://twitter.com/CozyBond/status/1240985673156513793"
      ],
      "published": [
        "2020-03-20T12:56:55+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:CozyBond"
            ],
            "numeric-id": [
              "1224077891715190784"
            ],
            "name": [
              "Bond 🍸"
            ],
            "nickname": [
              "CozyBond"
            ],
            "url": [
              "https://twitter.com/CozyBond"
            ],
            "published": [
              "2020-02-02T21:11:57+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1241298028109205504/8TNhy5jS.jpg"
            ]
          }
        }
      ],
      "content": [
        "he ate ALL the jam!"
      ],
      "photo": [
        "https://pbs.twimg.com/media/ETjdzt5XQAACnxc.jpg",
        "https://pbs.twimg.com/media/ETjdzxBWkAEWM4X.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-21T10:21:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @CozyBond's tweet"
    ],
    "like-of": [
      "https://twitter.com/CozyBond/status/1240985673156513793"
    ],
    "published": [
      "2020-03-21T10:21:00Z"
    ],
    "category": [
      "cute"
    ],
    "syndication": [
      "https://twitter.com/CozyBond/status/1240985673156513793"
    ]
  },
  "tags": [
    "cute"
  ]
}
