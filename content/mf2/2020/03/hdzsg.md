{
  "kind": "likes",
  "slug": "2020/03/hdzsg",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1241480434212618240"
      ],
      "url": [
        "https://twitter.com/MrsEmma/status/1241480434212618240"
      ],
      "published": [
        "2020-03-21T21:42:55+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:MrsEmma"
            ],
            "numeric-id": [
              "19500955"
            ],
            "name": [
              "Emma Seward"
            ],
            "nickname": [
              "MrsEmma"
            ],
            "url": [
              "https://twitter.com/MrsEmma",
              "https://www.emmasgarden.co.uk/jewellery"
            ],
            "published": [
              "2009-01-25T19:32:42+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1239225071186653184/gm6PZrtm.jpg"
            ]
          }
        }
      ],
      "content": [
        "Meanwhile... 😂"
      ],
      "photo": [
        "https://pbs.twimg.com/media/ETqf3uQX0AEB7F7.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-21T22:46:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @MrsEmma's tweet"
    ],
    "like-of": [
      "https://twitter.com/MrsEmma/status/1241480434212618240"
    ],
    "published": [
      "2020-03-21T22:46:00Z"
    ],
    "syndication": [
      "https://twitter.com/MrsEmma/status/1241480434212618240"
    ]
  }
}
