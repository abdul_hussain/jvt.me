{
  "kind": "likes",
  "slug": "2020/03/uwrpl",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1240510519422918657"
      ],
      "url": [
        "https://twitter.com/Marcus_Noble_/status/1240510519422918657"
      ],
      "published": [
        "2020-03-19T05:28:50+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/MrAndrew/status/1240429927712858112"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:Marcus_Noble_"
            ],
            "numeric-id": [
              "14732545"
            ],
            "name": [
              "Marcus Noble"
            ],
            "nickname": [
              "Marcus_Noble_"
            ],
            "url": [
              "https://twitter.com/Marcus_Noble_",
              "https://marcusnoble.co.uk"
            ],
            "published": [
              "2008-05-11T09:51:36+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Some container"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/776738772759277569/hfaM5zhA.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Cats are experts at social distancing. We all just need boxes.",
          "html": "Cats are experts at social distancing. We all just need boxes.\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/MrAndrew\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/MrsEmma\"></a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-19T07:50:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @Marcus_Noble_'s tweet"
    ],
    "like-of": [
      "https://twitter.com/Marcus_Noble_/status/1240510519422918657"
    ],
    "published": [
      "2020-03-19T07:50:00Z"
    ],
    "syndication": [
      "https://twitter.com/Marcus_Noble_/status/1240510519422918657"
    ]
  }
}
