{
  "kind": "reposts",
  "slug": "2020/03/2vtdg",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1241107018133442566"
      ],
      "url": [
        "https://twitter.com/HollyHNews/status/1241107018133442566"
      ],
      "published": [
        "2020-03-20T20:59:06+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:HollyHNews"
            ],
            "numeric-id": [
              "25608872"
            ],
            "name": [
              "Holly Hamilton"
            ],
            "nickname": [
              "HollyHNews"
            ],
            "url": [
              "https://twitter.com/HollyHNews",
              "http://www.moneymanagementuk.com/money/holly-hamilton/"
            ],
            "published": [
              "2009-03-21T00:35:44+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Manchester via County Down"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1097973184773664769/YiHHAOJp.jpg"
            ]
          }
        }
      ],
      "location": [
        {
          "type": [
            "h-card",
            "p-location"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:315b740b108481f6"
            ],
            "name": [
              "Manchester, England"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "My sister is my hero. 🦸‍♀️ \nWhile we’re practising “social distancing” in our homes, she’s on a nightshift taking this face on.\nRaise a glass to her and her amazing team tonight please... 👏 👊❤️#NHSheroes #HealthcareHeroes #NHSThankYou",
          "html": "<div style=\"white-space: pre\">My sister is my hero. 🦸‍♀️ \nWhile we’re practising “social distancing” in our homes, she’s on a nightshift taking this face on.\nRaise a glass to her and her amazing team tonight please... 👏 👊❤️<a href=\"https://twitter.com/search?q=%23NHSheroes\">#NHSheroes</a> <a href=\"https://twitter.com/search?q=%23HealthcareHeroes\">#HealthcareHeroes</a> <a href=\"https://twitter.com/search?q=%23NHSThankYou\">#NHSThankYou</a></div>"
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/ETlMQJaWAAEYUF6.jpg",
        "https://pbs.twimg.com/media/ETlMQJYWkAAex2n.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-21T17:46:00Z",
  "h": "h-entry",
  "properties": {
    "published": [
      "2020-03-21T17:46:00Z"
    ],
    "repost-of": [
      "https://twitter.com/HollyHNews/status/1241107018133442566"
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1241422208444116996"
    ]
  }
}
