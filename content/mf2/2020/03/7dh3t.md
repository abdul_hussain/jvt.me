{
  "kind": "likes",
  "slug": "2020/03/7dh3t",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1233997416933871618"
      ],
      "url": [
        "https://twitter.com/hughrawlinson/status/1233997416933871618"
      ],
      "published": [
        "2020-03-01T06:08:05+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:hughrawlinson"
            ],
            "numeric-id": [
              "30527632"
            ],
            "name": [
              "Hugh Rawlinson (he/him)"
            ],
            "nickname": [
              "hughrawlinson"
            ],
            "url": [
              "https://twitter.com/hughrawlinson",
              "https://www.reddit.com/r/popheadscirclejerk/comments/elr3ud/the_gay_web_intern_at_spotify_has_taste/"
            ],
            "published": [
              "2009-04-11T21:25:35+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Boston, MA"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1197152780240261121/-R24iEgS.jpg"
            ]
          }
        }
      ],
      "content": [
        "Devs: please delete your code. Don't leave it commented out, or just in the repo as functions and files and modules that don't get called. Delete it while you know what it was for, so that the next person doesn't have to figure it out. Source control never forgets it."
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-01T07:37:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @hughrawlinson's tweet"
    ],
    "like-of": [
      "https://twitter.com/hughrawlinson/status/1233997416933871618"
    ],
    "published": [
      "2020-03-01T07:37:00Z"
    ],
    "syndication": [
      "https://twitter.com/hughrawlinson/status/1233997416933871618"
    ]
  }
}
