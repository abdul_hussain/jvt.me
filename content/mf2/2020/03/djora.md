{
  "kind": "likes",
  "slug": "2020/03/djora",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1238518684727087104"
      ],
      "url": [
        "https://twitter.com/awhalefact/status/1238518684727087104"
      ],
      "published": [
        "2020-03-13T17:33:59+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/awhalefact/status/1238457522106834944"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:awhalefact"
            ],
            "numeric-id": [
              "1033142532136415232"
            ],
            "name": [
              "whalefact"
            ],
            "nickname": [
              "awhalefact"
            ],
            "url": [
              "https://twitter.com/awhalefact",
              "http://awhalefact.com"
            ],
            "published": [
              "2018-08-25T00:02:29+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "whale merch →"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1212752040088260608/0dHUMBQm.jpg"
            ]
          }
        }
      ],
      "content": [
        "the whales have been washing their hands since birth"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-13T17:44:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @awhalefact's tweet"
    ],
    "like-of": [
      "https://twitter.com/awhalefact/status/1238518684727087104"
    ],
    "published": [
      "2020-03-13T17:44:00Z"
    ],
    "syndication": [
      "https://twitter.com/awhalefact/status/1238518684727087104"
    ]
  }
}
