{
  "kind": "likes",
  "slug": "2020/03/pul7j",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1237401157406019586"
      ],
      "url": [
        "https://twitter.com/MrsEmma/status/1237401157406019586"
      ],
      "published": [
        "2020-03-10T15:33:20+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:MrsEmma"
            ],
            "numeric-id": [
              "19500955"
            ],
            "name": [
              "Emma Seward"
            ],
            "nickname": [
              "MrsEmma"
            ],
            "url": [
              "https://twitter.com/MrsEmma",
              "https://www.emmasgarden.co.uk/jewellery"
            ],
            "published": [
              "2009-01-25T19:32:42+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1229726485923160064/HgP3BpFu.jpg"
            ]
          }
        }
      ],
      "content": [
        "Bottom left is the face of a cat who has realised she's made a terrible mistake."
      ],
      "photo": [
        "https://pbs.twimg.com/media/ESwhxarWkAApxsu.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-10T16:17:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @MrsEmma's tweet"
    ],
    "like-of": [
      "https://twitter.com/MrsEmma/status/1237401157406019586"
    ],
    "published": [
      "2020-03-10T16:17:00Z"
    ],
    "syndication": [
      "https://twitter.com/MrsEmma/status/1237401157406019586"
    ]
  }
}
