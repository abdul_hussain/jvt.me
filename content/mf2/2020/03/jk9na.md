{
  "kind": "likes",
  "slug": "2020/03/jk9na",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1239594863600992257"
      ],
      "url": [
        "https://twitter.com/fionchadd/status/1239594863600992257"
      ],
      "published": [
        "2020-03-16T16:50:20+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:fionchadd"
            ],
            "numeric-id": [
              "19606092"
            ],
            "name": [
              "Hannah"
            ],
            "nickname": [
              "fionchadd"
            ],
            "url": [
              "https://twitter.com/fionchadd",
              "http://makeaspectacle.co.uk"
            ],
            "published": [
              "2009-01-27T18:19:20+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "London, UK"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/740122673062252545/KdQPWKK1.jpg"
            ]
          }
        }
      ],
      "content": [
        "If the toilet paper shortage continues we might actually find out how to use the three seashells."
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-16T16:53:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @fionchadd's tweet"
    ],
    "like-of": [
      "https://twitter.com/fionchadd/status/1239594863600992257"
    ],
    "published": [
      "2020-03-16T16:53:00Z"
    ],
    "syndication": [
      "https://twitter.com/fionchadd/status/1239594863600992257"
    ]
  }
}
