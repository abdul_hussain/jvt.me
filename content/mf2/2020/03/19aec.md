{
  "kind": "rsvps",
  "slug": "2020/03/19aec",
  "client_id": "https://micropublish.net",
  "date": "2020-03-01T08:59:36.992+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP no to https://www.meetup.com/ministry-of-testing-nottingham/events/jgthgrybcfbgb/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/ministry-of-testing-nottingham/events/jgthgrybcfbgb/"
    ],
    "published": [
      "2020-03-01T08:59:36.992+01:00"
    ],
    "rsvp": [
      "no"
    ],
    "syndication": [
      "https://www.meetup.com/ministry-of-testing-nottingham/events/jgthgrybcfbgb/#rsvp-by-https%3A%2F%2Fwww.jvt.me"
    ],
    "event": {
      "location": [
        "Experian - The Sir John Peace Building, Experian Way, Nottingham, United Kingdom"
      ],
      "url": [
        "https://www.meetup.com/ministry-of-testing-nottingham/events/jgthgrybcfbgb/"
      ],
      "name": [
        "#NottsTest - The Humans of Testing with Iain Mc"
      ],
      "start": [
        "2020-03-04T19:00:00Z"
      ],
      "end": [
        "2020-03-04T22:00:00Z"
      ]
    }
  }
}
