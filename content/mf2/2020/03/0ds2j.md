{
  "kind": "replies",
  "slug": "2020/03/0ds2j",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1238573369265659904"
      ],
      "url": [
        "https://twitter.com/mel_codes/status/1238573369265659904"
      ],
      "published": [
        "2020-03-13T21:11:17+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:mel_codes"
            ],
            "numeric-id": [
              "1129526041414918144"
            ],
            "name": [
              "Mel ✌"
            ],
            "nickname": [
              "mel_codes"
            ],
            "url": [
              "https://twitter.com/mel_codes",
              "https://github.com/mel-tse"
            ],
            "published": [
              "2019-05-17T23:16:08+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1141801301233295360/6dQQBSiX.jpg"
            ]
          }
        }
      ],
      "content": [
        "Anyone know how to set the path in terminal to its default one?"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-13T21:25:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/mel_codes/status/1238573369265659904"
    ],
    "in-reply-to": [
      "https://twitter.com/mel_codes/status/1238573369265659904"
    ],
    "published": [
      "2020-03-13T21:25:00Z"
    ],
    "content": [
      {
        "html": "",
        "value": "Do you mean to replace whatever custom changes you've made with the standard one Linux/Mac uses? If so, maybe `source /etc/environment`, but not 100% sure that'll work everywhere"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1238578288211214336"
    ]
  }
}
