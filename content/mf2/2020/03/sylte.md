{
  "kind": "reposts",
  "slug": "2020/03/sylte",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1236725697684688904"
      ],
      "url": [
        "https://twitter.com/exfatalist/status/1236725697684688904"
      ],
      "published": [
        "2020-03-08T18:49:18+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:exfatalist"
            ],
            "numeric-id": [
              "471520440"
            ],
            "name": [
              "» dr mrs the mothman » 🔌🔋"
            ],
            "nickname": [
              "exfatalist"
            ],
            "url": [
              "https://twitter.com/exfatalist",
              "https://curiouscat.me/exfatalist"
            ],
            "published": [
              "2012-01-22T23:53:25+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1190070299477336065/aEQjo-7c.jpg"
            ]
          }
        }
      ],
      "content": [
        "Hey, it's International Women's Day! Ladies, what's the funniest thing you've ever been mansplained to about?"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-09T23:15:00Z",
  "h": "h-entry",
  "properties": {
    "published": [
      "2020-03-09T23:15:00Z"
    ],
    "repost-of": [
      "https://twitter.com/exfatalist/status/1236725697684688904"
    ],
    "category": [
      "diversity-and-inclusion"
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1237156555587489792"
    ]
  },
  "tags": [
    "diversity-and-inclusion"
  ]
}
