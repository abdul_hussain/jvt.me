{
  "kind" : "rsvps",
  "slug" : "2020/03/kbcnc",
  "client_id" : "https://indigenous.realize.be",
  "date" : "2020-03-21T15:11:00Z",
  "h" : "h-entry",
  "properties" : {
    "name" : [ "RSVP yes to https://events.indieweb.org/2020/04/online-homebrew-website-club-nottingham-UpVd9JZeVzx6" ],
    "in-reply-to" : [ "https://events.indieweb.org/2020/04/online-homebrew-website-club-nottingham-UpVd9JZeVzx6" ],
    "published" : [ "2020-03-21T15:11:00Z" ],
    "rsvp" : [ "yes" ],
    "event" : {
      "location" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "name" : [ "Online" ]
        },
        "lang" : "en",
        "value" : "Online"
      } ],
      "url" : [ "https://events.indieweb.org/2020/04/online-homebrew-website-club-nottingham-UpVd9JZeVzx6" ],
      "name" : [ "ONLINE: Homebrew Website Club: Nottingham" ],
      "start" : [ "2020-04-01T17:30:00+01:00" ],
      "end" : [ "2020-04-01T19:30:00+01:00" ]
    }
  }
}
