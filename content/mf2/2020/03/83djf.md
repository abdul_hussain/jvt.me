{
  "kind": "likes",
  "slug": "2020/03/83djf",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1241461968978038786"
      ],
      "url": [
        "https://twitter.com/kvlly/status/1241461968978038786"
      ],
      "published": [
        "2020-03-21T20:29:33+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:kvlly"
            ],
            "numeric-id": [
              "123543103"
            ],
            "name": [
              "Kelly Vaughn 🐞"
            ],
            "nickname": [
              "kvlly"
            ],
            "url": [
              "https://twitter.com/kvlly",
              "http://kellyvaughn.co"
            ],
            "published": [
              "2010-03-16T12:15:39+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Atlanta, GA"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1240319235714408456/EqySP4c8.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Thinking about creating a free course to run through setting up a @Shopify store for the DIY brick and mortar store owners who want to get up and running on their own.\n\nAny recommendations for a course platform? Preferably free since I won't be charging for this?",
          "html": "<div style=\"white-space: pre\">Thinking about creating a free course to run through setting up a <a href=\"https://twitter.com/Shopify\">@Shopify</a> store for the DIY brick and mortar store owners who want to get up and running on their own.\n\nAny recommendations for a course platform? Preferably free since I won't be charging for this?</div>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-22T11:48:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @kvlly's tweet"
    ],
    "like-of": [
      "https://twitter.com/kvlly/status/1241461968978038786"
    ],
    "published": [
      "2020-03-22T11:48:00Z"
    ],
    "syndication": [
      "https://twitter.com/kvlly/status/1241461968978038786"
    ]
  }
}
