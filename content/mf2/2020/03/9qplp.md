{
  "kind": "likes",
  "slug": "2020/03/9qplp",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1241206661114499074"
      ],
      "url": [
        "https://twitter.com/Toaster_Pastry/status/1241206661114499074"
      ],
      "published": [
        "2020-03-21T03:35:03+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:Toaster_Pastry"
            ],
            "numeric-id": [
              "46581007"
            ],
            "name": [
              "Wayne"
            ],
            "nickname": [
              "Toaster_Pastry"
            ],
            "url": [
              "https://twitter.com/Toaster_Pastry"
            ],
            "published": [
              "2009-06-12T04:31:27+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "La Jolla"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/785654786716676096/vzEjpkvq.jpg"
            ]
          }
        }
      ],
      "content": [
        "I attended a Zoom meeting of doctors who, as a subculture, is not used to technology (we are the \"Reply All\" crowd), nor understands the \"MUTE\"  function. Some guy in the middle of the meeting says, \"This guy's a fucking idiot.\" And that ended the meeting."
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-21T11:33:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @Toaster_Pastry's tweet"
    ],
    "like-of": [
      "https://twitter.com/Toaster_Pastry/status/1241206661114499074"
    ],
    "published": [
      "2020-03-21T11:33:00Z"
    ],
    "syndication": [
      "https://twitter.com/Toaster_Pastry/status/1241206661114499074"
    ]
  }
}
