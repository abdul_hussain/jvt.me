{
  "kind": "likes",
  "slug": "2020/03/t6onq",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1237159868303388673"
      ],
      "url": [
        "https://twitter.com/anna_hax/status/1237159868303388673"
      ],
      "video": [
        "https://video.twimg.com/tweet_video/EStGVrgWsAI8udp.mp4"
      ],
      "published": [
        "2020-03-09T23:34:32+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/anna_hax/status/1237156109862961159"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:anna_hax"
            ],
            "numeric-id": [
              "394235377"
            ],
            "name": [
              "Anna ✨🙅‍♀️✨"
            ],
            "nickname": [
              "anna_hax"
            ],
            "url": [
              "https://twitter.com/anna_hax",
              "https://annadodson.co.uk"
            ],
            "published": [
              "2011-10-19T19:37:31+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1134736020669378561/0_EwVzms.jpg"
            ]
          }
        }
      ],
      "content": [
        "And we missed our stop 😅 oopes"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-09T23:39:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @anna_hax's tweet"
    ],
    "like-of": [
      "https://twitter.com/anna_hax/status/1237159868303388673"
    ],
    "published": [
      "2020-03-09T23:39:00Z"
    ],
    "syndication": [
      "https://twitter.com/anna_hax/status/1237159868303388673"
    ]
  }
}
