{
  "kind": "replies",
  "slug": "2020/03/rzusl",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1233907944481271808"
      ],
      "url": [
        "https://twitter.com/maria_fibonacci/status/1233907944481271808"
      ],
      "published": [
        "2020-03-01T00:12:33+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:maria_fibonacci"
            ],
            "numeric-id": [
              "25734165"
            ],
            "name": [
              "Verónica."
            ],
            "nickname": [
              "maria_fibonacci"
            ],
            "url": [
              "https://twitter.com/maria_fibonacci",
              "http://instagram.com/verolopg"
            ],
            "published": [
              "2009-03-21T21:48:47+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Brooklyn, NY"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1222995412895903749/wk1vUmsR.jpg"
            ]
          }
        }
      ],
      "content": [
        "Ok nerds...where do you have your blogs?"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-01T07:42:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/maria_fibonacci/status/1233907944481271808"
    ],
    "in-reply-to": [
      "https://twitter.com/maria_fibonacci/status/1233907944481271808"
    ],
    "published": [
      "2020-03-01T07:42:00Z"
    ],
    "category": [
      "personal-website"
    ],
    "content": [
      {
        "html": "",
        "value": "I'm using Hugo and Netlify, backed with GitLab CI. Because I own the platform that is my site, I can do funky things like reply to this tweet directly from my website!"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1234022410690670592"
    ]
  },
  "tags": [
    "personal-website"
  ]
}
