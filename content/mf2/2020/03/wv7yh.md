{
  "kind": "likes",
  "slug": "2020/03/wv7yh",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1236037209486233603"
      ],
      "url": [
        "https://twitter.com/nbcsnl/status/1236037209486233603"
      ],
      "video": [
        "https://video.twimg.com/amplify_video/1236028036287512578/vid/1280x720/7UIKTDWQgrZG_ZZh.mp4?tag=13"
      ],
      "published": [
        "2020-03-06T21:13:29+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:nbcsnl"
            ],
            "numeric-id": [
              "28221296"
            ],
            "name": [
              "Saturday Night Live - SNL"
            ],
            "nickname": [
              "nbcsnl"
            ],
            "url": [
              "https://twitter.com/nbcsnl",
              "http://nbc.com/snl/"
            ],
            "published": [
              "2009-04-01T23:06:46+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "30 Rock"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1159903342182699009/VOkYedxH.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "This is Daniel Craig. \nAn incredibly serious actor.",
          "html": "<div style=\"white-space: pre\">This is Daniel Craig. \nAn incredibly serious actor.</div>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-08T18:45:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @nbcsnl's tweet"
    ],
    "like-of": [
      "https://twitter.com/nbcsnl/status/1236037209486233603"
    ],
    "published": [
      "2020-03-08T18:45:00Z"
    ],
    "syndication": [
      "https://twitter.com/nbcsnl/status/1236037209486233603"
    ]
  }
}
