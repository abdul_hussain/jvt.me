{
  "kind": "likes",
  "slug": "2020/03/rxxcm",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1238181146044911616"
      ],
      "url": [
        "https://twitter.com/anna_hax/status/1238181146044911616"
      ],
      "published": [
        "2020-03-12T19:12:44+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:anna_hax"
            ],
            "numeric-id": [
              "394235377"
            ],
            "name": [
              "Anna ✨🙅‍♀️✨"
            ],
            "nickname": [
              "anna_hax"
            ],
            "url": [
              "https://twitter.com/anna_hax",
              "https://annadodson.co.uk"
            ],
            "published": [
              "2011-10-19T19:37:31+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1134736020669378561/0_EwVzms.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "About to kick off at #PHPMiNDS here @wearejh! Looking forward to hearing from @akrabat about Slim 4, always a great speaker 🤓🐘 #php",
          "html": "About to kick off at <a href=\"https://twitter.com/search?q=%23PHPMiNDS\">#PHPMiNDS</a> here <a href=\"https://twitter.com/wearejh\">@wearejh</a>! Looking forward to hearing from <a href=\"https://twitter.com/akrabat\">@akrabat</a> about Slim 4, always a great speaker 🤓🐘 <a href=\"https://twitter.com/search?q=%23php\">#php</a>"
        }
      ]
    }
  },
  "client_id": "https://monocle.p3k.io/",
  "date": "2020-03-12T20:55:41.765+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @anna_hax's tweet"
    ],
    "like-of": [
      "https://twitter.com/anna_hax/status/1238181146044911616"
    ],
    "published": [
      "2020-03-12T20:55:41.765+01:00"
    ],
    "syndication": [
      "https://twitter.com/anna_hax/status/1238181146044911616"
    ]
  }
}
