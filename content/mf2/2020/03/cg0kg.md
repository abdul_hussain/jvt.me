{
  "kind": "reposts",
  "slug": "2020/03/cg0kg",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1237156109862961159"
      ],
      "url": [
        "https://twitter.com/anna_hax/status/1237156109862961159"
      ],
      "published": [
        "2020-03-09T23:19:36+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:anna_hax"
            ],
            "numeric-id": [
              "394235377"
            ],
            "name": [
              "Anna ✨🙅‍♀️✨"
            ],
            "nickname": [
              "anna_hax"
            ],
            "url": [
              "https://twitter.com/anna_hax",
              "https://annadodson.co.uk"
            ],
            "published": [
              "2011-10-19T19:37:31+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1134736020669378561/0_EwVzms.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Heading home after some impromptu drinks @BrewDogNotts with some of the @technottingham team, attendees and speakers 🍉 I've had a really great night - thanks all 🤓 #TechNott",
          "html": "Heading home after some impromptu drinks <a href=\"https://twitter.com/BrewDogNotts\">@BrewDogNotts</a> with some of the <a href=\"https://twitter.com/technottingham\">@technottingham</a> team, attendees and speakers 🍉 I've had a really great night - thanks all 🤓 <a href=\"https://twitter.com/search?q=%23TechNott\">#TechNott</a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-09T23:20:00Z",
  "h": "h-entry",
  "properties": {
    "published": [
      "2020-03-09T23:20:00Z"
    ],
    "repost-of": [
      "https://twitter.com/anna_hax/status/1237156109862961159"
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1237157660820152323"
    ]
  }
}
