{
  "kind": "likes",
  "slug": "2020/03/g1rzl",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1237677041010511873"
      ],
      "url": [
        "https://twitter.com/bagwaa/status/1237677041010511873"
      ],
      "published": [
        "2020-03-11T09:49:36+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:bagwaa"
            ],
            "numeric-id": [
              "788508"
            ],
            "name": [
              "Richard Bagshaw"
            ],
            "nickname": [
              "bagwaa"
            ],
            "url": [
              "https://twitter.com/bagwaa",
              "https://www.richardbagshaw.co.uk"
            ],
            "published": [
              "2007-02-22T12:20:09+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, UK"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1234211917843062785/VpvK3YRm.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "If I already have my personal accounts all in @monzo and then move my business account there does that mean #FullFullMonzo ?",
          "html": "If I already have my personal accounts all in <a href=\"https://twitter.com/monzo\">@monzo</a> and then move my business account there does that mean <a href=\"https://twitter.com/search?q=%23FullFullMonzo\">#FullFullMonzo</a> ?"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-11T22:43:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @bagwaa's tweet"
    ],
    "like-of": [
      "https://twitter.com/bagwaa/status/1237677041010511873"
    ],
    "published": [
      "2020-03-11T22:43:00Z"
    ],
    "syndication": [
      "https://twitter.com/bagwaa/status/1237677041010511873"
    ]
  }
}
