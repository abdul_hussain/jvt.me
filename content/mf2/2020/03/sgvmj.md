{
  "kind": "likes",
  "slug": "2020/03/sgvmj",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1235964324516110336"
      ],
      "url": [
        "https://twitter.com/MrsEmma/status/1235964324516110336"
      ],
      "published": [
        "2020-03-06T16:23:52+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/MrsEmma/status/1235964217305464833"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:MrsEmma"
            ],
            "numeric-id": [
              "19500955"
            ],
            "name": [
              "Emma Seward"
            ],
            "nickname": [
              "MrsEmma"
            ],
            "url": [
              "https://twitter.com/MrsEmma",
              "https://www.emmasgarden.co.uk/jewellery"
            ],
            "published": [
              "2009-01-25T19:32:42+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1229726485923160064/HgP3BpFu.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "✨ @JessPWhite Conference speaker AND organiser, always has colourful hair. Addicted to pins, and really wants a cat. Fellow spoonie, she introduced me to the spoon theory and I love her for it.",
          "html": "✨ <a href=\"https://twitter.com/JessPWhite\">@JessPWhite</a> Conference speaker AND organiser, always has colourful hair. Addicted to pins, and really wants a cat. Fellow spoonie, she introduced me to the spoon theory and I love her for it."
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-07T12:13:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @MrsEmma's tweet"
    ],
    "like-of": [
      "https://twitter.com/MrsEmma/status/1235964324516110336"
    ],
    "published": [
      "2020-03-07T12:13:00Z"
    ],
    "syndication": [
      "https://twitter.com/MrsEmma/status/1235964324516110336"
    ]
  }
}
