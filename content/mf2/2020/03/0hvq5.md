{
  "kind": "likes",
  "slug": "2020/03/0hvq5",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1236465665759838208"
      ],
      "url": [
        "https://twitter.com/ThatSaraGoodman/status/1236465665759838208"
      ],
      "published": [
        "2020-03-08T01:36:01+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:ThatSaraGoodman"
            ],
            "numeric-id": [
              "151666167"
            ],
            "name": [
              "Sara Wallace Goodman"
            ],
            "nickname": [
              "ThatSaraGoodman"
            ],
            "url": [
              "https://twitter.com/ThatSaraGoodman",
              "http://faculty.sites.uci.edu/sgoodman/"
            ],
            "published": [
              "2010-06-03T23:40:52+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Irvine, CA"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/931607281849925632/5QcsfdE5.jpg"
            ]
          }
        }
      ],
      "content": [
        "I guess we’re about to find out which meetings could’ve been emails after all..."
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-09T23:41:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @ThatSaraGoodman's tweet"
    ],
    "like-of": [
      "https://twitter.com/ThatSaraGoodman/status/1236465665759838208"
    ],
    "published": [
      "2020-03-09T23:41:00Z"
    ],
    "syndication": [
      "https://twitter.com/ThatSaraGoodman/status/1236465665759838208"
    ]
  }
}
