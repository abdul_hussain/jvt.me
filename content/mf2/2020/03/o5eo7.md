{
  "kind": "likes",
  "slug": "2020/03/o5eo7",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1234424260086636550"
      ],
      "url": [
        "https://twitter.com/rizbizkits/status/1234424260086636550"
      ],
      "published": [
        "2020-03-02T10:24:12+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:rizbizkits"
            ],
            "numeric-id": [
              "800085538954878976"
            ],
            "name": [
              "riz"
            ],
            "nickname": [
              "rizbizkits"
            ],
            "url": [
              "https://twitter.com/rizbizkits",
              "https://www.rizwanakhan.com"
            ],
            "published": [
              "2016-11-19T21:17:12+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/915004957807513609/vN0bht32.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "🚨 Twitterverse! Life update. As the end of Uni is nearing, I've decided to expand my job search. \n\n👩‍💻 I'm looking for full front-end / front-end+design roles, preferably in Nottingham (but open to moving).\n\n📢 Holla at me! DMs open.\n\n💛 RTs appreciated!\n\nrizwanakhan.com/hire-riz",
          "html": "<div style=\"white-space: pre\">🚨 Twitterverse! Life update. As the end of Uni is nearing, I've decided to expand my job search. \n\n👩‍💻 I'm looking for full front-end / front-end+design roles, preferably in Nottingham (but open to moving).\n\n📢 Holla at me! DMs open.\n\n💛 RTs appreciated!\n\n<a href=\"https://rizwanakhan.com/hire-riz\">rizwanakhan.com/hire-riz</a></div>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-03T14:58:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @rizbizkits's tweet"
    ],
    "like-of": [
      "https://twitter.com/rizbizkits/status/1234424260086636550"
    ],
    "published": [
      "2020-03-03T14:58:00Z"
    ],
    "syndication": [
      "https://twitter.com/rizbizkits/status/1234424260086636550"
    ]
  }
}
