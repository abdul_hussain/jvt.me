{
  "kind": "reposts",
  "slug": "2020/03/ubyt5",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1235324524347961345"
      ],
      "url": [
        "https://twitter.com/ohhelloana/status/1235324524347961345"
      ],
      "published": [
        "2020-03-04T22:01:32+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:ohhelloana"
            ],
            "numeric-id": [
              "434457570"
            ],
            "name": [
              "Ana Rodrigues"
            ],
            "nickname": [
              "ohhelloana"
            ],
            "url": [
              "https://twitter.com/ohhelloana",
              "https://ohhelloana.blog"
            ],
            "published": [
              "2011-12-11T21:57:15+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "London"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1126838936519815168/F4HZ_Fmu.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "🗓️ 10 days until @indiewebcamp London! There's still time to register to join us. #indieweb\n\nohhelloana.blog/indiewebcamp-l…",
          "html": "<div style=\"white-space: pre\">🗓️ 10 days until <a href=\"https://twitter.com/indiewebcamp\">@indiewebcamp</a> London! There's still time to register to join us. <a href=\"https://twitter.com/search?q=%23indieweb\">#indieweb</a>\n\n<a href=\"https://ohhelloana.blog/indiewebcamp-london\">ohhelloana.blog/indiewebcamp-l…</a></div>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-04T23:09:00Z",
  "h": "h-entry",
  "properties": {
    "published": [
      "2020-03-04T23:09:00Z"
    ],
    "repost-of": [
      "https://twitter.com/ohhelloana/status/1235324524347961345"
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1235342919063736321"
    ]
  }
}
