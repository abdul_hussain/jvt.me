{
  "kind": "likes",
  "slug": "2020/03/eaepp",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1235649751510437888"
      ],
      "url": [
        "https://twitter.com/chaselyons/status/1235649751510437888"
      ],
      "published": [
        "2020-03-05T19:33:52+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:chaselyons"
            ],
            "numeric-id": [
              "1681067162"
            ],
            "name": [
              "Chase"
            ],
            "nickname": [
              "chaselyons"
            ],
            "url": [
              "https://twitter.com/chaselyons",
              "http://twitch.tv/chaselyons"
            ],
            "published": [
              "2013-08-18T16:06:58+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "satire"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1210264680482181121/XvoHnAKj.jpg"
            ]
          }
        }
      ],
      "content": [
        "do y’all like your PB&J with or without the door hinge"
      ],
      "photo": [
        "https://pbs.twimg.com/media/ESXo5iSXcAEHai9.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-07T00:37:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @chaselyons's tweet"
    ],
    "like-of": [
      "https://twitter.com/chaselyons/status/1235649751510437888"
    ],
    "published": [
      "2020-03-07T00:37:00Z"
    ],
    "syndication": [
      "https://twitter.com/chaselyons/status/1235649751510437888"
    ]
  }
}
