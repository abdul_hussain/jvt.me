{
  "kind": "reads",
  "slug": "2020/03/d32cc",
  "date": "2020-03-23T23:35:00Z",
  "h": "h-entry",
  "properties": {
    "published": [
      "2020-03-23T23:35:00Z"
    ],
    "read-status": [
      "finished"
    ],
    "read-of": [
      {
        "type": [
          "h-cite"
        ],
        "properties": {
          "name": [
            "Star Wars: Thrawn: Treason"
          ],
          "author": [
            "Timothy Zahn"
          ],
          "isbn": [
            "1529124018"
          ]
        }
      }
    ]
  }
}

