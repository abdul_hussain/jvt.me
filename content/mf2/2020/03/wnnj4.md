{
  "kind": "reposts",
  "slug": "2020/03/wnnj4",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1239073331434803201"
      ],
      "url": [
        "https://twitter.com/rondoftw/status/1239073331434803201"
      ],
      "published": [
        "2020-03-15T06:17:57+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:rondoftw"
            ],
            "numeric-id": [
              "3982441"
            ],
            "name": [
              "ronnie chen"
            ],
            "nickname": [
              "rondoftw"
            ],
            "url": [
              "https://twitter.com/rondoftw",
              "http://tinyletter.com/hot-takes-and-hot-cakes"
            ],
            "published": [
              "2007-04-10T04:25:49+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/906210023562293248/OWgN1OPx.jpg"
            ]
          }
        }
      ],
      "content": [
        "The Good Place spent 4 years teaching everyone about ethics and the trolley problem and some of you weren't paying enough attention to give up brunch in order to save the lives of people in your community"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-15T14:34:00Z",
  "h": "h-entry",
  "properties": {
    "published": [
      "2020-03-15T14:34:00Z"
    ],
    "repost-of": [
      "https://twitter.com/rondoftw/status/1239073331434803201"
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1251560403253055488"
    ]
  }
}
