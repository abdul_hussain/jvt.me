{
  "kind": "likes",
  "slug": "2020/03/h0mvl",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1235257627342491648"
      ],
      "url": [
        "https://twitter.com/MrsEmma/status/1235257627342491648"
      ],
      "published": [
        "2020-03-04T17:35:42+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:MrsEmma"
            ],
            "numeric-id": [
              "19500955"
            ],
            "name": [
              "Emma Seward"
            ],
            "nickname": [
              "MrsEmma"
            ],
            "url": [
              "https://twitter.com/MrsEmma",
              "https://www.emmasgarden.co.uk/jewellery"
            ],
            "published": [
              "2009-01-25T19:32:42+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1229726485923160064/HgP3BpFu.jpg"
            ]
          }
        }
      ],
      "content": [
        "Yay for all the Mother's Day promoted tweets cropping up already 💔 Fellow non-mothers for whom it's really bloody hard, I've just adjusted my settings so those two words (and the #) are muted.  Give it a go, hopefully it helps 🤞"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-04T20:50:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @MrsEmma's tweet"
    ],
    "like-of": [
      "https://twitter.com/MrsEmma/status/1235257627342491648"
    ],
    "published": [
      "2020-03-04T20:50:00Z"
    ],
    "syndication": [
      "https://twitter.com/MrsEmma/status/1235257627342491648"
    ]
  }
}
