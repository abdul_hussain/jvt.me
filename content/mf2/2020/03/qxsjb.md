{
  "kind": "likes",
  "slug": "2020/03/qxsjb",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1244302917206708233"
      ],
      "url": [
        "https://twitter.com/HJHaldanePhD/status/1244302917206708233"
      ],
      "published": [
        "2020-03-29T16:38:28+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:HJHaldanePhD"
            ],
            "numeric-id": [
              "1110300998122246148"
            ],
            "name": [
              "Is there an end to Zoom"
            ],
            "nickname": [
              "HJHaldanePhD"
            ],
            "url": [
              "https://twitter.com/HJHaldanePhD"
            ],
            "published": [
              "2019-03-25T22:02:41+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1148175883255058434/9sfuv7b1.jpg"
            ]
          }
        }
      ],
      "content": [
        "FYI: if you're having a committee meeting via Zoom and you use the chat function to privately write to someone, your colleagues may not see it in real time, but it shows up when the chat is downloaded and put in the minutes folder..."
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-30T08:06:00+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @HJHaldanePhD's tweet"
    ],
    "like-of": [
      "https://twitter.com/HJHaldanePhD/status/1244302917206708233"
    ],
    "published": [
      "2020-03-30T08:06:00+01:00"
    ],
    "syndication": [
      "https://twitter.com/HJHaldanePhD/status/1244302917206708233"
    ]
  }
}
