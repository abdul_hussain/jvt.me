{
  "kind": "likes",
  "slug": "2020/03/8e562",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1240743612171857929"
      ],
      "url": [
        "https://twitter.com/8none1/status/1240743612171857929"
      ],
      "published": [
        "2020-03-19T20:55:03+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:8none1"
            ],
            "numeric-id": [
              "38729048"
            ],
            "name": [
              "Will Cooke"
            ],
            "nickname": [
              "8none1"
            ],
            "url": [
              "https://twitter.com/8none1",
              "http://www.whizzy.org"
            ],
            "published": [
              "2009-05-08T19:41:35+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Bedfordshire"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/3115818707/ef997054de583c881d2f31ae32c1b3d2.jpeg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Very real chance that my kids will have completed YouTube by the time they go back to school. #parenting",
          "html": "Very real chance that my kids will have completed YouTube by the time they go back to school. <a href=\"https://twitter.com/search?q=%23parenting\">#parenting</a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-20T13:35:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @8none1's tweet"
    ],
    "like-of": [
      "https://twitter.com/8none1/status/1240743612171857929"
    ],
    "published": [
      "2020-03-20T13:35:00Z"
    ],
    "syndication": [
      "https://twitter.com/8none1/status/1240743612171857929"
    ]
  }
}
