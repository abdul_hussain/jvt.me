{
  "kind": "rsvps",
  "slug": "2020/03/4ax0v",
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-15T23:16:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP no to https://www.meetup.com/ministry-of-testing-nottingham/events/jgthgryccfbfb/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/ministry-of-testing-nottingham/events/jgthgryccfbfb/"
    ],
    "published": [
      "2020-03-15T23:16:00Z"
    ],
    "rsvp": [
      "no"
    ],
    "syndication": [
      "https://www.meetup.com/ministry-of-testing-nottingham/events/jgthgryccfbfb/#rsvp-by-https%3A%2F%2Fwww.jvt.me"
    ],
    "event": {
      "location": [
        "Experian - The Sir John Peace Building, Experian Way, Nottingham, United Kingdom"
      ],
      "url": [
        "https://www.meetup.com/ministry-of-testing-nottingham/events/jgthgryccfbfb/"
      ],
      "name": [
        "#NottsTest - TBA"
      ],
      "start": [
        "2021-03-03T19:00:00Z"
      ],
      "end": [
        "2021-03-03T22:00:00Z"
      ]
    }
  }
}
