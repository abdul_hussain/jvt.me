{
  "kind": "likes",
  "slug": "2020/03/fjoja",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1243605606126272512"
      ],
      "url": [
        "https://twitter.com/halkyardo/status/1243605606126272512"
      ],
      "published": [
        "2020-03-27T18:27:36+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:halkyardo"
            ],
            "numeric-id": [
              "15001249"
            ],
            "name": [
              "Richard"
            ],
            "nickname": [
              "halkyardo"
            ],
            "url": [
              "https://twitter.com/halkyardo",
              "http://instagram.com/halkyardo"
            ],
            "published": [
              "2008-06-04T04:18:20+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Minnesota"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1235630770871558144/pynKpuHp.jpg"
            ]
          }
        }
      ],
      "content": [
        "accidentally called George “Blorg” because I was thinking about how Big and Large he is. don’t think he likes that name."
      ],
      "photo": [
        "https://pbs.twimg.com/media/EUIsr0BXgAAv-9Q.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-28T16:56:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @halkyardo's tweet"
    ],
    "like-of": [
      "https://twitter.com/halkyardo/status/1243605606126272512"
    ],
    "published": [
      "2020-03-28T16:56:00Z"
    ],
    "syndication": [
      "https://twitter.com/halkyardo/status/1243605606126272512"
    ]
  }
}
