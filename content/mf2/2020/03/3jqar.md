{
  "kind": "likes",
  "slug": "2020/03/3jqar",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1241899742264610816"
      ],
      "url": [
        "https://twitter.com/TatianaTMac/status/1241899742264610816"
      ],
      "published": [
        "2020-03-23T01:29:06+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:TatianaTMac"
            ],
            "numeric-id": [
              "1529722572"
            ],
            "name": [
              "Tatiana Mac"
            ],
            "nickname": [
              "TatianaTMac"
            ],
            "url": [
              "https://twitter.com/TatianaTMac",
              "https://www.tatianamac.com"
            ],
            "published": [
              "2013-06-19T04:59:25+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "she/they/hän 💖💛💙"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1214212889122181123/1lCP2C5T.jpg"
            ]
          }
        }
      ],
      "content": [
        "are you supposed to show up to zoom parties late like irl or on time like it's a meeting bc converging life with work tools is v v confusing imho"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-23T07:47:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @TatianaTMac's tweet"
    ],
    "like-of": [
      "https://twitter.com/TatianaTMac/status/1241899742264610816"
    ],
    "published": [
      "2020-03-23T07:47:00Z"
    ],
    "syndication": [
      "https://twitter.com/TatianaTMac/status/1241899742264610816"
    ]
  }
}
