{
  "kind": "likes",
  "slug": "2020/03/s0diu",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1240960836828631042"
      ],
      "url": [
        "https://twitter.com/KyleGeorgeArch/status/1240960836828631042"
      ],
      "published": [
        "2020-03-20T11:18:14+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:KyleGeorgeArch"
            ],
            "numeric-id": [
              "36381627"
            ],
            "name": [
              "Kyle Arch"
            ],
            "nickname": [
              "KyleGeorgeArch"
            ],
            "url": [
              "https://twitter.com/KyleGeorgeArch",
              "http://kylear.ch/"
            ],
            "published": [
              "2009-04-29T15:17:14+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Near the coffee machine"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1190200306874888193/P4NuuNPT.jpg"
            ]
          }
        }
      ],
      "content": [
        "who the fuck decided you can't have trailing commas in JSON"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-21T11:19:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @KyleGeorgeArch's tweet"
    ],
    "like-of": [
      "https://twitter.com/KyleGeorgeArch/status/1240960836828631042"
    ],
    "published": [
      "2020-03-21T11:19:00Z"
    ],
    "syndication": [
      "https://twitter.com/KyleGeorgeArch/status/1240960836828631042"
    ]
  }
}
