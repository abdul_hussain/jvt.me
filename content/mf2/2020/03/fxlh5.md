{
  "kind": "likes",
  "slug": "2020/03/fxlh5",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1237074379428651008"
      ],
      "url": [
        "https://twitter.com/mschoening/status/1237074379428651008"
      ],
      "published": [
        "2020-03-09T17:54:50+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:mschoening"
            ],
            "numeric-id": [
              "14239565"
            ],
            "name": [
              "Max Schoening"
            ],
            "nickname": [
              "mschoening"
            ],
            "url": [
              "https://twitter.com/mschoening",
              "https://max.dev/"
            ],
            "published": [
              "2008-03-27T23:04:59+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "San Francisco"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1030988424239403008/cFqM1tUo.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "✨ Execute any command in a GitHub Issue comment with GitHub Actions and github.com/nwtgck/actions…",
          "html": "✨ Execute any command in a GitHub Issue comment with GitHub Actions and <a href=\"https://github.com/nwtgck/actions-comment-run\">github.com/nwtgck/actions…</a>"
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/ESr4SXvUwAIqLc0.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-10T13:24:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @mschoening's tweet"
    ],
    "like-of": [
      "https://twitter.com/mschoening/status/1237074379428651008"
    ],
    "published": [
      "2020-03-10T13:24:00Z"
    ],
    "syndication": [
      "https://twitter.com/mschoening/status/1237074379428651008"
    ]
  }
}
