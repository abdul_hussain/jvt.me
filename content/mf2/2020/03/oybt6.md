{
  "kind": "likes",
  "slug": "2020/03/oybt6",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1238601672454295557"
      ],
      "url": [
        "https://twitter.com/heylauragao/status/1238601672454295557"
      ],
      "published": [
        "2020-03-13T23:03:45+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:heylauragao"
            ],
            "numeric-id": [
              "2996115143"
            ],
            "name": [
              "Laura Gao ✌️"
            ],
            "nickname": [
              "heylauragao"
            ],
            "url": [
              "https://twitter.com/heylauragao",
              "http://www.lauragao.com"
            ],
            "published": [
              "2015-01-24T16:32:45+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1197550677829881856/j99UekK1.jpg"
            ]
          }
        }
      ],
      "content": [
        "This is the medical team from Wuhan responsible for recovering 50K+ patients in just a few months. They're now risking their lives again to help out Italy. Rag on China all you want for the mishandlings early on but you can't deny that these people are true selfless heroes."
      ],
      "photo": [
        "https://pbs.twimg.com/media/ETBlERuXgAUdHUD.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-14T08:59:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @heylauragao's tweet"
    ],
    "like-of": [
      "https://twitter.com/heylauragao/status/1238601672454295557"
    ],
    "published": [
      "2020-03-14T08:59:00Z"
    ],
    "syndication": [
      "https://twitter.com/heylauragao/status/1238601672454295557"
    ]
  }
}
