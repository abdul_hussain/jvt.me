{
  "kind": "likes",
  "slug": "2020/03/z1urw",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1238415158927454208"
      ],
      "url": [
        "https://twitter.com/ItsProbablyAlex/status/1238415158927454208"
      ],
      "published": [
        "2020-03-13T10:42:37+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:ItsProbablyAlex"
            ],
            "numeric-id": [
              "739875827056312321"
            ],
            "name": [
              "Alex"
            ],
            "nickname": [
              "ItsProbablyAlex"
            ],
            "url": [
              "https://twitter.com/ItsProbablyAlex"
            ],
            "published": [
              "2016-06-06T17:45:18+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1234302729377501185/AeCDIsCG.jpg"
            ]
          }
        }
      ],
      "content": [
        "First attempt at pancakes could have gone worse. 🤷‍♂️"
      ],
      "photo": [
        "https://pbs.twimg.com/media/ES-8AypXgAAuXW8.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-13T12:06:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @ItsProbablyAlex's tweet"
    ],
    "like-of": [
      "https://twitter.com/ItsProbablyAlex/status/1238415158927454208"
    ],
    "published": [
      "2020-03-13T12:06:00Z"
    ],
    "syndication": [
      "https://twitter.com/ItsProbablyAlex/status/1238415158927454208"
    ]
  }
}
