{
  "kind": "rsvps",
  "slug": "2020/03/xabka",
  "client_id": "https://micropublish.net",
  "date": "2020-03-27T17:02:31.312+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP interested to https://www.meetup.com/Chef-Users-UK/events/269691242/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Chef-Users-UK/events/269691242/"
    ],
    "published": [
      "2020-03-27T17:02:31.312+01:00"
    ],
    "rsvp": [
      "interested"
    ],
    "content": [
      {
        "html": "",
        "value": "I'd be interested in this, but unfortunately it's not at the friendliest time, especially with work the next day"
      }
    ],
    "syndication": [
      "https://www.meetup.com/Chef-Users-UK/events/269691242/#rsvp-by-https%3A%2F%2Fwww.jvt.me"
    ],
    "event": {
      "location": [
        "Online"
      ],
      "url": [
        "https://www.meetup.com/Chef-Users-UK/events/269691242/"
      ],
      "name": [
        "Chef Users Global Digital Meetup: Human Free Pipelines with Galen Emery"
      ],
      "start": [
        "2020-03-31T23:00:00+01:00"
      ],
      "end": [
        "2020-04-01T00:30:00+01:00"
      ]
    }
  }
}
