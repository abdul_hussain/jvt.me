{
  "kind": "replies",
  "slug": "2020/03/rxfiz",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1235645746424229891"
      ],
      "url": [
        "https://twitter.com/Beardy/status/1235645746424229891"
      ],
      "video": [
        "https://video.twimg.com/tweet_video/ESXlQGmWsAMQijZ.mp4"
      ],
      "published": [
        "2020-03-05T19:17:57+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:Beardy"
            ],
            "numeric-id": [
              "5513082"
            ],
            "name": [
              "Daniel J Beardsall"
            ],
            "nickname": [
              "Beardy"
            ],
            "url": [
              "https://twitter.com/Beardy"
            ],
            "published": [
              "2007-04-26T01:12:37+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottinghamshire, UK"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/903130775951900672/k0ok2iQG.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "I spy @JamieTanna coding away in an impressively tiny font size on a very high-res screen. Such productivity!",
          "html": "I spy <a href=\"https://twitter.com/JamieTanna\">@JamieTanna</a> coding away in an impressively tiny font size on a very high-res screen. Such productivity!"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-05T19:24:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/Beardy/status/1235645746424229891"
    ],
    "in-reply-to": [
      "https://twitter.com/Beardy/status/1235645746424229891"
    ],
    "published": [
      "2020-03-05T19:24:00Z"
    ],
    "content": [
      {
        "html": "",
        "value": "My go to catchphrase is \"this isn't even my native resolution\" 😂 got to enjoy the screen real estate while my eyes can cope!"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1235649247074299904"
    ]
  }
}
