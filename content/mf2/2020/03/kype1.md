{
  "kind": "likes",
  "slug": "2020/03/kype1",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1244220181750206470"
      ],
      "url": [
        "https://twitter.com/miss_jwo/status/1244220181750206470"
      ],
      "published": [
        "2020-03-29T11:09:42+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:miss_jwo"
            ],
            "numeric-id": [
              "200519952"
            ],
            "name": [
              "Jenny Wong 🐝"
            ],
            "nickname": [
              "miss_jwo"
            ],
            "url": [
              "https://twitter.com/miss_jwo",
              "http://jwong.co.uk"
            ],
            "published": [
              "2010-10-09T14:29:15+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "United Kingdom"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/839845252412358656/PLSY74LG.jpg"
            ]
          }
        }
      ],
      "content": [
        "When you can’t be bothered to wash any glasses. Uni habits it is."
      ],
      "photo": [
        "https://pbs.twimg.com/media/EURbqIzXQAIWBAW.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-29T13:37:00+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @miss_jwo's tweet"
    ],
    "like-of": [
      "https://twitter.com/miss_jwo/status/1244220181750206470"
    ],
    "published": [
      "2020-03-29T13:37:00+01:00"
    ],
    "syndication": [
      "https://twitter.com/miss_jwo/status/1244220181750206470"
    ]
  }
}
