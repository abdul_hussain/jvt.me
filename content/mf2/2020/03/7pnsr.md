{
  "kind": "replies",
  "slug": "2020/03/7pnsr",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1240659336113897472"
      ],
      "url": [
        "https://twitter.com/craigburgess/status/1240659336113897472"
      ],
      "published": [
        "2020-03-19T15:20:10+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:craigburgess"
            ],
            "numeric-id": [
              "14912335"
            ],
            "name": [
              "Craig Burgess"
            ],
            "nickname": [
              "craigburgess"
            ],
            "url": [
              "https://twitter.com/craigburgess",
              "https://www.getdoingthings.com"
            ],
            "published": [
              "2008-05-26T19:26:06+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Barnsley, South Yorkshire, UK"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1162042955974283266/7PD6uyTD.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "I’ve had to cancel #HomebrewWebsiteClub in its physical location @barnsley_dmc on March 25th. I’m now in lockdown, so I won’t be able to make it.\n\nHowever, I want to make it online. Zoom or Hangouts?",
          "html": "<div style=\"white-space: pre\">I’ve had to cancel <a href=\"https://twitter.com/search?q=%23HomebrewWebsiteClub\">#HomebrewWebsiteClub</a> in its physical location <a href=\"https://twitter.com/Barnsley_DMC\">@barnsley_dmc</a> on March 25th. I’m now in lockdown, so I won’t be able to make it.\n\nHowever, I want to make it online. Zoom or Hangouts?</div>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-19T17:41:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/craigburgess/status/1240659336113897472"
    ],
    "in-reply-to": [
      "https://twitter.com/craigburgess/status/1240659336113897472"
    ],
    "published": [
      "2020-03-19T17:41:00Z"
    ],
    "content": [
      {
        "html": "",
        "value": "If you go for Zoom, there are official IndieWeb Zoom accounts that should be usable 👍🏽"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1240696861377224704"
    ]
  }
}
