{
  "kind": "reposts",
  "slug": "2020/03/rbfff",
  "tags": [
    "personal-website",
    "blogging",
    "wit-notts"
  ],
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1244599451471224832"
      ],
      "url": [
        "https://twitter.com/WiT_Notts/status/1244599451471224832"
      ],
      "published": [
        "2020-03-30T12:16:47+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:WiT_Notts"
            ],
            "numeric-id": [
              "4339158441"
            ],
            "name": [
              "Women In Tech, Nottingham 🌈✨"
            ],
            "nickname": [
              "WiT_Notts"
            ],
            "url": [
              "https://twitter.com/WiT_Notts",
              "https://nott.tech/wit"
            ],
            "published": [
              "2015-12-01T11:22:25+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/913380298829844481/YvPcjPlE.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "This week we're running our first remote #WiTNotts and we'd love to see you there!\n\nWebcams at the ready, we've got @paulienuh talking to us about #blogging then we're going to have a little birthday celebration as we turn 4! 🎂🎉🎙️📝🎈 \n\nFull details 👉 nott.tech/wit-april",
          "html": "<div style=\"white-space: pre\">This week we're running our first remote <a href=\"https://twitter.com/search?q=%23WiTNotts\">#WiTNotts</a> and we'd love to see you there!\n\nWebcams at the ready, we've got <a href=\"https://twitter.com/paulienuh\">@paulienuh</a> talking to us about <a href=\"https://twitter.com/search?q=%23blogging\">#blogging</a> then we're going to have a little birthday celebration as we turn 4! 🎂🎉🎙️📝🎈 \n\nFull details 👉 <a href=\"http://nott.tech/wit-april\">nott.tech/wit-april</a></div>"
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/EUW0mk-WAAEjT-G.jpg"
      ]
    }
  },
  "client_id": "https://monocle.p3k.io/",
  "date": "2020-03-30T20:33:13.764+02:00",
  "h": "h-entry",
  "properties": {
    "category": [
      "personal-website",
      "blogging",
      "wit-notts"
    ],
    "published": [
      "2020-03-30T20:33:13.764+02:00"
    ],
    "repost-of": [
      "https://twitter.com/WiT_Notts/status/1244599451471224832"
    ],
    "content": [
      {
        "html": "",
        "value": "It'll be of no surprise to any of you who know me as a <a href=\"/tags/personal-website/\">#PersonalWebsite</a> and <a href=\"/tags/blogging/\">#blogging</a> advocate that I would be very much looking forward to @pawlean.com's talk at <a href=\"/tags/wit-notts/\">#WiTNotts</a> this Thursday! And as it's remote, hopefully more of you can join!"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1244697490047340545"
    ]
  }
}
