{
  "kind": "likes",
  "slug": "2020/03/n12cn",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1238104095237562368"
      ],
      "url": [
        "https://twitter.com/mrjamesob/status/1238104095237562368"
      ],
      "published": [
        "2020-03-12T14:06:33+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:mrjamesob"
            ],
            "numeric-id": [
              "152656121"
            ],
            "name": [
              "James O'Brien"
            ],
            "nickname": [
              "mrjamesob"
            ],
            "url": [
              "https://twitter.com/mrjamesob",
              "http://l-bc.co/2SGnJ7B",
              "http://hive.co.uk/Product/James-",
              "http://LBC.co.uk"
            ],
            "published": [
              "2010-06-06T14:46:00+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "London"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1166349829381005312/rgDurQNe.jpg"
            ]
          }
        }
      ],
      "content": [
        "Of course, it's possible that the government is keeping its cards so close to its chest so that the virus can't get any clues about our negotiating strategy."
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-12T23:26:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @mrjamesob's tweet"
    ],
    "like-of": [
      "https://twitter.com/mrjamesob/status/1238104095237562368"
    ],
    "published": [
      "2020-03-12T23:26:00Z"
    ],
    "syndication": [
      "https://twitter.com/mrjamesob/status/1238104095237562368"
    ]
  }
}
