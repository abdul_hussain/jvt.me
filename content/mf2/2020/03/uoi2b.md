{
  "kind": "likes",
  "slug": "2020/03/uoi2b",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1242849725201756160"
      ],
      "url": [
        "https://twitter.com/Marcus_Noble_/status/1242849725201756160"
      ],
      "published": [
        "2020-03-25T16:24:00+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:Marcus_Noble_"
            ],
            "numeric-id": [
              "14732545"
            ],
            "name": [
              "Marcus Noble"
            ],
            "nickname": [
              "Marcus_Noble_"
            ],
            "url": [
              "https://twitter.com/Marcus_Noble_",
              "https://marcusnoble.co.uk"
            ],
            "published": [
              "2008-05-11T09:51:36+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Some container"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/776738772759277569/hfaM5zhA.jpg"
            ]
          }
        }
      ],
      "content": [
        "I know the weather is nice in the UK and we're all stuck at home but please, before you start blasting music in the garden, remember your neighbours are now having to work from home under stressful conditions. Please don't add to that."
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-25T22:11:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @Marcus_Noble_'s tweet"
    ],
    "like-of": [
      "https://twitter.com/Marcus_Noble_/status/1242849725201756160"
    ],
    "published": [
      "2020-03-25T22:11:00Z"
    ],
    "syndication": [
      "https://twitter.com/Marcus_Noble_/status/1242849725201756160"
    ]
  }
}
