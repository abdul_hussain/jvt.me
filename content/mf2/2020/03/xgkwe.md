{
  "kind": "likes",
  "slug": "2020/03/xgkwe",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1243932495491534848"
      ],
      "url": [
        "https://twitter.com/EmmaManzini/status/1243932495491534848"
      ],
      "published": [
        "2020-03-28T16:06:32+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:EmmaManzini"
            ],
            "numeric-id": [
              "852507406370263042"
            ],
            "name": [
              "Emma Manzini"
            ],
            "nickname": [
              "EmmaManzini"
            ],
            "url": [
              "https://twitter.com/EmmaManzini"
            ],
            "published": [
              "2017-04-13T13:02:59+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/858278181622697984/2fqaUXO9.jpg"
            ]
          }
        }
      ],
      "content": [
        "A gentle reminder that all your panic buying will be going out of date soon. Enjoy your 36 egg omelette, you fat wankers."
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-28T23:56:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @EmmaManzini's tweet"
    ],
    "like-of": [
      "https://twitter.com/EmmaManzini/status/1243932495491534848"
    ],
    "published": [
      "2020-03-28T23:56:00Z"
    ],
    "syndication": [
      "https://twitter.com/EmmaManzini/status/1243932495491534848"
    ]
  }
}
