{
  "kind": "replies",
  "slug": "2020/03/dxtlp",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1237869497488269313"
      ],
      "url": [
        "https://twitter.com/__jcmc__/status/1237869497488269313"
      ],
      "published": [
        "2020-03-11T22:34:21+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:__jcmc__"
            ],
            "numeric-id": [
              "781588467654483968"
            ],
            "name": [
              "Jennifer Mackown"
            ],
            "nickname": [
              "__jcmc__"
            ],
            "url": [
              "https://twitter.com/__jcmc__",
              "http://www.jcmc.xyz"
            ],
            "published": [
              "2016-09-29T20:16:26+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Lincoln"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1137074636213116929/AjJAzHty.png"
            ]
          }
        }
      ],
      "content": [
        "I really want to move completely over to Firefox but it is SO slow on mobile and the desktop version crashes all the time :("
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-12T13:21:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/__jcmc__/status/1237869497488269313"
    ],
    "in-reply-to": [
      "https://twitter.com/__jcmc__/status/1237869497488269313"
    ],
    "published": [
      "2020-03-12T13:21:00Z"
    ],
    "content": [
      {
        "html": "",
        "value": "On mobile too? That's interesting, I've not found it slow since I moved over a while ago, but not got any recent comparison with Chrome so may not be helpful"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1251558730807812101"
    ]
  }
}
