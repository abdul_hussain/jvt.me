{
  "kind": "likes",
  "slug": "2020/03/n6ga2",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1239999986827505665"
      ],
      "url": [
        "https://twitter.com/holly/status/1239999986827505665"
      ],
      "published": [
        "2020-03-17T19:40:09+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:holly"
            ],
            "numeric-id": [
              "7555262"
            ],
            "name": [
              "Holly Brockwell"
            ],
            "nickname": [
              "holly"
            ],
            "url": [
              "https://twitter.com/holly",
              "https://www.instagram.com/hollybrocks/"
            ],
            "published": [
              "2007-07-18T10:27:16+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "From Nottingham, in London"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1237785562100228100/C8wosHKR.jpg"
            ]
          }
        }
      ],
      "content": [
        "I ALSO AM FINDING THIS ISOLATION A CHALLENGE 😬"
      ],
      "photo": [
        "https://pbs.twimg.com/media/ETVdaqZWoAAHj5O.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-17T21:54:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @holly's tweet"
    ],
    "like-of": [
      "https://twitter.com/holly/status/1239999986827505665"
    ],
    "published": [
      "2020-03-17T21:54:00Z"
    ],
    "syndication": [
      "https://twitter.com/holly/status/1239999986827505665"
    ]
  }
}
