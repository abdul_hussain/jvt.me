{
  "kind": "likes",
  "slug": "2020/03/p7t2h",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1235964981067292673"
      ],
      "url": [
        "https://twitter.com/MrsEmma/status/1235964981067292673"
      ],
      "published": [
        "2020-03-06T16:26:29+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/MrsEmma/status/1235964904156409856"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:MrsEmma"
            ],
            "numeric-id": [
              "19500955"
            ],
            "name": [
              "Emma Seward"
            ],
            "nickname": [
              "MrsEmma"
            ],
            "url": [
              "https://twitter.com/MrsEmma",
              "https://www.emmasgarden.co.uk/jewellery"
            ],
            "published": [
              "2009-01-25T19:32:42+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1229726485923160064/HgP3BpFu.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "✨ @short_louise Organises tech events locally and nationally. Produces a podcast, loves a good board game and has the largest collection of anyone I know. Inspiring leader in her field.",
          "html": "✨ <a href=\"https://twitter.com/short_louise\">@short_louise</a> Organises tech events locally and nationally. Produces a podcast, loves a good board game and has the largest collection of anyone I know. Inspiring leader in her field."
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-07T12:13:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @MrsEmma's tweet"
    ],
    "like-of": [
      "https://twitter.com/MrsEmma/status/1235964981067292673"
    ],
    "published": [
      "2020-03-07T12:13:00Z"
    ],
    "syndication": [
      "https://twitter.com/MrsEmma/status/1235964981067292673"
    ]
  }
}
