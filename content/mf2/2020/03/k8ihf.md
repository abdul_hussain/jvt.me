{
  "kind": "replies",
  "slug": "2020/03/k8ihf",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1235032912862384128"
      ],
      "url": [
        "https://twitter.com/markhoppus/status/1235032912862384128"
      ],
      "published": [
        "2020-03-04T02:42:46+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:markhoppus"
            ],
            "numeric-id": [
              "21955058"
            ],
            "name": [
              "Ḿå℟₭"
            ],
            "nickname": [
              "markhoppus"
            ],
            "url": [
              "https://twitter.com/markhoppus",
              "http://simplecreaturesmusic.com"
            ],
            "published": [
              "2009-02-26T01:54:14+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Ravenclaw"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1219468540152963072/5R1WONcM.jpg"
            ]
          }
        }
      ],
      "content": [
        "I need a non-gendered “you guys.” “People” is too formal. “Y’all” isn’t how I speak. “Everyone” doesn’t do it for me. There’s a certain camaraderie and familiarity with “hey you guys” that the others lack. What do we do?"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-04T21:51:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/markhoppus/status/1235032912862384128"
    ],
    "in-reply-to": [
      "https://twitter.com/markhoppus/status/1235032912862384128"
    ],
    "published": [
      "2020-03-04T21:51:00Z"
    ],
    "content": [
      {
        "html": "",
        "value": "https://heyguys.cc/ is a great resource for this, and is a handy link to share others when they're not able to find the right term 👍🏽"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1235323579627118592"
    ]
  }
}
