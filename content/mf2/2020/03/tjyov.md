{
  "kind": "replies",
  "slug": "2020/03/tjyov",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1240569127804252160"
      ],
      "url": [
        "https://twitter.com/popey/status/1240569127804252160"
      ],
      "published": [
        "2020-03-19T09:21:43+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:popey"
            ],
            "numeric-id": [
              "12325402"
            ],
            "name": [
              "Alan Pope 🍺🐧🐱🇬🇧🇪🇺"
            ],
            "nickname": [
              "popey"
            ],
            "url": [
              "https://twitter.com/popey",
              "http://popey.com/"
            ],
            "published": [
              "2008-01-16T17:35:35+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Farnborough"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1197957006486908928/iaSWAJBN.jpg"
            ]
          }
        }
      ],
      "content": [
        "Anyone else get this? In Firefox on Linux. Sometimes when I quit, after re-launching all tabs are white and never load. I have to load in safe mode, disable all extensions, then reload in normal mode and manually re-enable. Happened a few times. Any ideas?"
      ],
      "photo": [
        "https://pbs.twimg.com/media/ETdhcXHXQAAtcBe.png"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-19T10:24:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/popey/status/1240569127804252160"
    ],
    "in-reply-to": [
      "https://twitter.com/popey/status/1240569127804252160"
    ],
    "published": [
      "2020-03-19T10:24:00Z"
    ],
    "category": [
      "firefox"
    ],
    "content": [
      {
        "html": "",
        "value": "Last few days I've had an issue where whenever I start up <a href=\"/tags/firefox/\">#firefox</a> every tab crashes, and I can't load anything, even the settings. The only fix is to disable sandboxing. But that sounds like a slightly different issue to yours 🤔"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1240586226173911041"
    ]
  },
  "tags": [
    "firefox"
  ]
}
