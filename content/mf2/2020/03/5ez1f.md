{
  "kind": "likes",
  "slug": "2020/03/5ez1f",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1238574257988395008"
      ],
      "url": [
        "https://twitter.com/GhibliCricket/status/1238574257988395008"
      ],
      "video": [
        "https://video.twimg.com/tweet_video/ETBMtroWkAAahqs.mp4"
      ],
      "published": [
        "2020-03-13T21:14:49+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/JamieTanna/status/1238571232557236224"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:GhibliCricket"
            ],
            "numeric-id": [
              "343539523"
            ],
            "name": [
              "Simon Hodson"
            ],
            "nickname": [
              "GhibliCricket"
            ],
            "url": [
              "https://twitter.com/GhibliCricket",
              "http://sjhodson.blogspot.co.uk"
            ],
            "published": [
              "2011-07-27T18:36:37+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1205051913898725377/GXAqn_uK.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Sweetcorn niblets tasted like fear.",
          "html": "Sweetcorn niblets tasted like fear.\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-13T21:23:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @GhibliCricket's tweet"
    ],
    "like-of": [
      "https://twitter.com/GhibliCricket/status/1238574257988395008"
    ],
    "published": [
      "2020-03-13T21:23:00Z"
    ],
    "syndication": [
      "https://twitter.com/GhibliCricket/status/1238574257988395008"
    ]
  }
}
