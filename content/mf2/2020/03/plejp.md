{
  "kind": "replies",
  "slug": "2020/03/plejp",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1236052557090312192"
      ],
      "url": [
        "https://twitter.com/ceejbot/status/1236052557090312192"
      ],
      "published": [
        "2020-03-06T22:14:29+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:ceejbot"
            ],
            "numeric-id": [
              "78663"
            ],
            "name": [
              "Ceej is okay if you think differently"
            ],
            "nickname": [
              "ceejbot"
            ],
            "url": [
              "https://twitter.com/ceejbot",
              "http://ceejbot.tumblr.com/"
            ],
            "published": [
              "2006-12-18T23:22:16+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Menlo Park, CA"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/857590885743116288/izm__c9I.jpg"
            ]
          }
        }
      ],
      "content": [
        "I am HERE for your awesome new command-line tool recommendations."
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-07T17:44:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/ceejbot/status/1236052557090312192"
    ],
    "in-reply-to": [
      "https://twitter.com/ceejbot/status/1236052557090312192"
    ],
    "published": [
      "2020-03-07T17:44:00Z"
    ],
    "content": [
      {
        "html": "",
        "value": "Glances (https://www.jvt.me/posts/2017/04/26/glances/) is pretty great for system monitoring"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1236348100224090112"
    ]
  }
}
