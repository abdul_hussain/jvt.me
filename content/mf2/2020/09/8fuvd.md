{
  "date" : "2020-09-26T23:39:00+0100",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/MrsEmma/status/1309985542885511170" ],
    "name" : [ "Like of @MrsEmma's tweet" ],
    "published" : [ "2020-09-26T23:39:00+0100" ],
    "like-of" : [ "https://twitter.com/MrsEmma/status/1309985542885511170" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/8fuvd",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1309985542885511170" ],
      "url" : [ "https://twitter.com/MrsEmma/status/1309985542885511170" ],
      "published" : [ "2020-09-26T22:37:46+00:00" ],
      "in-reply-to" : [ "https://twitter.com/garyshort/status/1309971828476125186" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:MrsEmma" ],
          "numeric-id" : [ "19500955" ],
          "name" : [ "Emma Seward" ],
          "nickname" : [ "MrsEmma" ],
          "url" : [ "https://twitter.com/MrsEmma" ],
          "published" : [ "2009-01-25T19:32:42+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1306627007317504000/K6YXEx2-.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Looks like you just adjusted your language to use a gender neutral term 🤷🏻‍♀️",
        "html" : "Looks like you just adjusted your language to use a gender neutral term 🤷🏻‍♀️\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/garyshort\"></a>\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/marmaladegirl\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
