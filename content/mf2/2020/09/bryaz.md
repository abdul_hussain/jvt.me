{
  "date" : "2020-09-03T22:59:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/thepunningman/status/1301459036911665154" ],
    "name" : [ "Like of @thepunningman's tweet" ],
    "published" : [ "2020-09-03T22:59:00+01:00" ],
    "category" : [ "london" ],
    "like-of" : [ "https://twitter.com/thepunningman/status/1301459036911665154" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/bryaz",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1301459036911665154" ],
      "url" : [ "https://twitter.com/thepunningman/status/1301459036911665154" ],
      "published" : [ "2020-09-03T09:56:28+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:thepunningman" ],
          "numeric-id" : [ "80064003" ],
          "name" : [ "Sean Leahy" ],
          "nickname" : [ "thepunningman" ],
          "url" : [ "https://twitter.com/thepunningman", "https://unbound.com/books/the-monster-cafe" ],
          "published" : [ "2009-10-05T16:28:38+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/925665229920456706/IouKOUHL.jpg" ]
        }
      } ],
      "content" : [ "I thought that tube ad was spot on, personally." ],
      "photo" : [ "https://pbs.twimg.com/media/Eg-193yWoAAH9J0.jpg" ]
    }
  },
  "tags" : [ "london" ],
  "client_id" : "https://indigenous.realize.be"
}
