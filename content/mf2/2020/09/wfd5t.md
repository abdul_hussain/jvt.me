{
  "date" : "2020-09-06T10:16:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/TheCodePixi/status/1302431235868962816" ],
    "name" : [ "Like of @TheCodePixi's tweet" ],
    "published" : [ "2020-09-06T10:16:00+01:00" ],
    "category" : [ "diversity-and-inclusion" ],
    "like-of" : [ "https://twitter.com/TheCodePixi/status/1302431235868962816" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/wfd5t",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1302431235868962816" ],
      "url" : [ "https://twitter.com/TheCodePixi/status/1302431235868962816" ],
      "published" : [ "2020-09-06T02:19:39+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:TheCodePixi" ],
          "numeric-id" : [ "11833602" ],
          "name" : [ "Emily / Pixi 🧚🏻‍♂️ #BLM" ],
          "nickname" : [ "TheCodePixi" ],
          "url" : [ "https://twitter.com/TheCodePixi", "https://thecodepixi.dev/blog", "http://shop.thecodepixi.dev" ],
          "published" : [ "2008-01-04T10:14:42+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "New Jersey / New York  " ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1261307503629013000/bB_aBlgl.jpg" ]
        }
      } ],
      "content" : [ "Abandoning gender roles/expectations (and the concept of gender) is cool as hell. I have a shaved head, painted nails, a salary 3x my male partner's (pre-covid), and my main interests (right now) are needle work, programming, archery, and cooking. Be whatever you wanna do 💕" ]
    }
  },
  "tags" : [ "diversity-and-inclusion" ],
  "client_id" : "https://indigenous.realize.be"
}
