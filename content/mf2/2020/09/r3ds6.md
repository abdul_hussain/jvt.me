{
  "date" : "2020-09-08T07:27:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/custardloaf/status/1303114680840286210" ],
    "name" : [ "Like of @custardloaf's tweet" ],
    "published" : [ "2020-09-08T07:27:00+01:00" ],
    "category" : [ "food" ],
    "like-of" : [ "https://twitter.com/custardloaf/status/1303114680840286210" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/r3ds6",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1303114680840286210" ],
      "url" : [ "https://twitter.com/custardloaf/status/1303114680840286210" ],
      "video" : [ "https://video.twimg.com/ext_tw_video/1303114581472997376/pu/vid/396x702/Jjs2tuv5B6X-7IXK.mp4?tag=10" ],
      "published" : [ "2020-09-07T23:35:25+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:custardloaf" ],
          "numeric-id" : [ "1040135724018819072" ],
          "name" : [ "sydney" ],
          "nickname" : [ "custardloaf" ],
          "url" : [ "https://twitter.com/custardloaf", "http://instagram.com/custardloaf", "http://instagram.com/custard.loaf", "http://last.fm/user/sydney", "http://custardloaf.com" ],
          "published" : [ "2018-09-13T07:10:56+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "seattle" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1295164009075707904/kMG7DFIQ.jpg" ]
        }
      } ],
      "content" : [ "preparing a gourmet charcuterie board for my gamer husband ❤️" ]
    }
  },
  "tags" : [ "food" ],
  "client_id" : "https://indigenous.realize.be"
}
