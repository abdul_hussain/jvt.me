{
  "date" : "2020-09-19T19:26:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/MaartjeME/status/1307228978747449345" ],
    "name" : [ "Like of @MaartjeME's tweet" ],
    "published" : [ "2020-09-19T19:26:00+01:00" ],
    "category" : [ "github", "ssh", "ubuntu" ],
    "like-of" : [ "https://twitter.com/MaartjeME/status/1307228978747449345" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/dn4d4",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1307228978747449345" ],
      "url" : [ "https://twitter.com/MaartjeME/status/1307228978747449345" ],
      "published" : [ "2020-09-19T08:04:10+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:MaartjeME" ],
          "numeric-id" : [ "1483447088" ],
          "name" : [ "Maartje Eyskens 😷" ],
          "nickname" : [ "MaartjeME" ],
          "url" : [ "https://twitter.com/MaartjeME", "https://eyskens.me" ],
          "published" : [ "2013-06-04T22:30:24+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Belgium" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/758623110119755776/SsV19cX0.jpg" ]
        }
      } ],
      "content" : [ "The most under appreciated step of the Ubuntu installer" ],
      "photo" : [ "https://pbs.twimg.com/media/EiQ10sZXkAAKQ-C.jpg" ]
    }
  },
  "tags" : [ "github", "ssh", "ubuntu" ],
  "client_id" : "https://indigenous.realize.be"
}
