{
  "date" : "2020-09-15T08:16:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/technottingham/status/1305562008767602690" ],
    "name" : [ "Like of @technottingham's tweet" ],
    "published" : [ "2020-09-15T08:16:00+01:00" ],
    "category" : [ "diversity-and-inclusion" ],
    "like-of" : [ "https://twitter.com/technottingham/status/1305562008767602690" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/zfcra",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1305562008767602690" ],
      "url" : [ "https://twitter.com/technottingham/status/1305562008767602690" ],
      "published" : [ "2020-09-14T17:40:13+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:technottingham" ],
          "numeric-id" : [ "384492431" ],
          "name" : [ "Tech Nottingham" ],
          "nickname" : [ "technottingham" ],
          "url" : [ "https://twitter.com/technottingham", "http://technottingham.com" ],
          "published" : [ "2011-10-03T19:47:31+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1023974499757293570/ZoPc_QsO.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Closed captions are available at tonight's #TechNott - to enable them click 'Closed Captions' in Zoom",
        "html" : "Closed captions are available at tonight's <a href=\"https://twitter.com/search?q=%23TechNott\">#TechNott</a> - to enable them click 'Closed Captions' in Zoom"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/Eh5JnDEXsAIbXhD.png" ]
    }
  },
  "tags" : [ "diversity-and-inclusion" ],
  "client_id" : "https://indigenous.realize.be"
}
