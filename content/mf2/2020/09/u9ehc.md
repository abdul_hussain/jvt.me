{
  "date" : "2020-09-03T20:53:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/katebcarp/status/1300948270307512320" ],
    "name" : [ "Like of @katebcarp's tweet" ],
    "published" : [ "2020-09-03T20:53:00+01:00" ],
    "like-of" : [ "https://twitter.com/katebcarp/status/1300948270307512320" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/u9ehc",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1300948270307512320" ],
      "url" : [ "https://twitter.com/katebcarp/status/1300948270307512320" ],
      "published" : [ "2020-09-02T00:06:52+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:katebcarp" ],
          "numeric-id" : [ "15756105" ],
          "name" : [ "Kate Carpenter" ],
          "nickname" : [ "katebcarp" ],
          "url" : [ "https://twitter.com/katebcarp", "http://kathrynbcarpenter.com/" ],
          "published" : [ "2008-08-06T22:26:21+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "KCMO / Princeton, NJ" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1116086550469783554/8mBib3-b.jpg" ]
        }
      } ],
      "content" : [ "Having only recently learned that my go-to email sign off, \"Best,\" is considered hostile in some circles, I am facing an existential email crisis." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
