{
  "date" : "2020-09-14T21:43:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/Brunty/status/1305594646622593024" ],
    "name" : [ "Like of @Brunty's tweet" ],
    "published" : [ "2020-09-14T21:43:00+01:00" ],
    "like-of" : [ "https://twitter.com/Brunty/status/1305594646622593024" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/juynh",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1305594646622593024" ],
      "url" : [ "https://twitter.com/Brunty/status/1305594646622593024" ],
      "published" : [ "2020-09-14T19:49:55+00:00" ],
      "in-reply-to" : [ "https://twitter.com/Brunty/status/1305594407429771272" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Brunty" ],
          "numeric-id" : [ "1099517012" ],
          "name" : [ "Matt Brunt" ],
          "nickname" : [ "Brunty" ],
          "url" : [ "https://twitter.com/Brunty", "https://brunty.me/now" ],
          "published" : [ "2013-01-17T23:19:21+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Leicester, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1151043501288824832/TjB9WT83.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "So please enlighten me, where am I proactively encouraged to set my settings? Because from where I’m sitting it looks like I had to go find them.",
        "html" : "So please enlighten me, where am I proactively encouraged to set my settings? Because from where I’m sitting it looks like I had to go find them.\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/Strava\"></a>\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/anna_hax\"></a>\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/pavsmith\"></a>\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/seanmtracey\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
