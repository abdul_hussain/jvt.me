{
  "date" : "2020-09-05T19:48:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/_mikepound/status/1302277403528617985" ],
    "name" : [ "Like of @_mikepound's tweet" ],
    "published" : [ "2020-09-05T19:48:00+01:00" ],
    "like-of" : [ "https://twitter.com/_mikepound/status/1302277403528617985" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/cys7s",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1302277403528617985" ],
      "url" : [ "https://twitter.com/_mikepound/status/1302277403528617985" ],
      "published" : [ "2020-09-05T16:08:22+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:_mikepound" ],
          "numeric-id" : [ "386262125" ],
          "name" : [ "Mike Pound" ],
          "nickname" : [ "_mikepound" ],
          "url" : [ "https://twitter.com/_mikepound", "http://nottingham.ac.uk/cvl" ],
          "published" : [ "2011-10-06T23:20:25+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/731061039945662464/QR2kNfVv.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Just found and ate a chicken nugget my kid had left uneaten. Score!\n\nI now realise she left it because she'd dropped it in the sand box.",
        "html" : "<div style=\"white-space: pre\">Just found and ate a chicken nugget my kid had left uneaten. Score!\n\nI now realise she left it because she'd dropped it in the sand box.</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
