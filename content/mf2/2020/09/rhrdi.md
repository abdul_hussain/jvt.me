{
  "date" : "2020-09-21T19:48:00+0100",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/Rochelle/status/1308087888391749632" ],
    "name" : [ "Like of @Rochelle's tweet" ],
    "published" : [ "2020-09-21T19:48:00+0100" ],
    "category" : [ "ethics" ],
    "like-of" : [ "https://twitter.com/Rochelle/status/1308087888391749632" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/rhrdi",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1308087888391749632" ],
      "url" : [ "https://twitter.com/Rochelle/status/1308087888391749632" ],
      "published" : [ "2020-09-21T16:57:10+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Rochelle" ],
          "numeric-id" : [ "14254846" ],
          "name" : [ "Ro" ],
          "nickname" : [ "Rochelle" ],
          "url" : [ "https://twitter.com/Rochelle" ],
          "published" : [ "2008-03-30T01:59:47+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "The Salish Sea" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1285696710182703106/qFqs_Px4.jpg" ]
        }
      } ],
      "content" : [ "The trolley problem at Facebook." ],
      "photo" : [ "https://pbs.twimg.com/media/EidDAHrVgAASenp.jpg" ]
    }
  },
  "tags" : [ "ethics" ],
  "client_id" : "https://indigenous.realize.be"
}
