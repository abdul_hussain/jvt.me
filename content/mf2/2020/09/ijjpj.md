{
  "date" : "2020-09-08T21:59:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://events.indieweb.org/2020/09/homebrew-website-club-nottingham-hacktoberfest-edition--dTrYgmFEBZff" ],
    "name" : [ "RSVP yes to https://events.indieweb.org/2020/09/homebrew-website-club-nottingham-hacktoberfest-edition--dTrYgmFEBZff" ],
    "published" : [ "2020-09-08T21:59:00+01:00" ],
    "event" : {
      "start" : [ "2020-09-30T17:30:00+01:00" ],
      "name" : [ "Homebrew Website Club: Nottingham (Hacktoberfest edition)" ],
      "end" : [ "2020-09-30T19:30:00+01:00" ],
      "url" : [ "https://events.indieweb.org/2020/09/homebrew-website-club-nottingham-hacktoberfest-edition--dTrYgmFEBZff" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/09/ijjpj",
  "client_id" : "https://indigenous.realize.be"
}
