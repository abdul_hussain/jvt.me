{
  "date" : "2020-09-26T15:53:00+0100",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/lskiuk/status/1309768317385797632" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1309870742142701569" ],
    "name" : [ "Reply to https://twitter.com/lskiuk/status/1309768317385797632" ],
    "published" : [ "2020-09-26T15:53:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "I'm known at work for championing inclusive language, and I call it out regardless of how person saying it identifies. Regardless of how the folks in the group feel at the time, it continues people using it and feeling that its OK. Those people will then use it outside of that circle, and they will inevitably make someone feel unwelcome. I feel its one of those things that, although can be a pain to rewire our brains, does make a difference, and trying to move to a more inclusive place to be is a great end goal"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/09/6lhws",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1309768317385797632" ],
      "url" : [ "https://twitter.com/lskiuk/status/1309768317385797632" ],
      "published" : [ "2020-09-26T08:14:35+00:00" ],
      "in-reply-to" : [ "https://twitter.com/pete_codes/status/1309767145543340032" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:lskiuk" ],
          "numeric-id" : [ "560920130" ],
          "name" : [ "Lski (Lee)" ],
          "nickname" : [ "lskiuk" ],
          "url" : [ "https://twitter.com/lskiuk", "https://www.lski.uk" ],
          "published" : [ "2012-04-23T07:59:01+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Helsinki, Suomi" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1243607828813819907/uPCFkm1q.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I spoke to a recent manager about this after they said it, saying they shouldnt just say the word 'guys'. She sort of agreed but hinted it wasnt that important and she is used to saying it. The other women in the group didnt seem to mind so I didnt press it.",
        "html" : "I spoke to a recent manager about this after they said it, saying they shouldnt just say the word 'guys'. She sort of agreed but hinted it wasnt that important and she is used to saying it. The other women in the group didnt seem to mind so I didnt press it.\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/pete_codes\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
