{
  "date" : "2020-09-14T07:19:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "name" : [ "Like of @holly's tweet" ],
    "published" : [ "2020-09-14T07:19:00+01:00" ],
    "like-of" : [ "https://twitter.com/holly/status/1305317423990661127" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/mkvz8",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1305317423990661127" ],
      "url" : [ "https://twitter.com/holly/status/1305317423990661127" ],
      "published" : [ "2020-09-14T01:28:20+00:00" ],
      "in-reply-to" : [ "https://twitter.com/holly/status/1305272971246923776" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:holly" ],
          "numeric-id" : [ "7555262" ],
          "name" : [ "Holly Brockwell" ],
          "nickname" : [ "holly" ],
          "url" : [ "https://twitter.com/holly", "https://www.instagram.com/hollybrocks/" ],
          "published" : [ "2007-07-18T10:27:16+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "From Nottingham, in London" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1248671410714804225/A_9KJ1y8.jpg" ]
        }
      } ],
      "content" : [ "This has unnerved so many people that I'm genuinely tempted to make it a promoted tweet and scare some randos" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
