{
  "date" : "2020-09-06T22:30:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/tucker_dev/status/1302678301220118528" ],
    "name" : [ "Like of @tucker_dev's tweet" ],
    "published" : [ "2020-09-06T22:30:00+01:00" ],
    "like-of" : [ "https://twitter.com/tucker_dev/status/1302678301220118528" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/bc3s4",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1302678301220118528" ],
      "url" : [ "https://twitter.com/tucker_dev/status/1302678301220118528" ],
      "published" : [ "2020-09-06T18:41:24+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:tucker_dev" ],
          "numeric-id" : [ "1162014237532856320" ],
          "name" : [ "James Tucker" ],
          "nickname" : [ "tucker_dev" ],
          "url" : [ "https://twitter.com/tucker_dev", "http://jamestucker.dev" ],
          "published" : [ "2019-08-15T14:52:38+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "🛶 Minneapolis, MN" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1288973259870998529/Ra3tpR_g.jpg" ]
        }
      } ],
      "content" : [ "If JavaScript developers spent as much time coding as they do tweeting they’d all have multiple revenue streams by now." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
