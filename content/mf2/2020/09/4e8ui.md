{
  "date" : "2020-09-12T17:35:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/deniseyu21/status/1304818129822871552" ],
    "name" : [ "Like of @deniseyu21's tweet" ],
    "published" : [ "2020-09-12T17:35:00+01:00" ],
    "category" : [ "food" ],
    "like-of" : [ "https://twitter.com/deniseyu21/status/1304818129822871552" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/4e8ui",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1304818129822871552" ],
      "url" : [ "https://twitter.com/deniseyu21/status/1304818129822871552" ],
      "published" : [ "2020-09-12T16:24:19+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:deniseyu21" ],
          "numeric-id" : [ "25458567" ],
          "name" : [ "Denise Yu" ],
          "nickname" : [ "deniseyu21" ],
          "url" : [ "https://twitter.com/deniseyu21", "http://deniseyu.io", "http://deniseyu.io/art", "http://tinyletter.com/deniseyu" ],
          "published" : [ "2009-03-20T04:17:20+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Toronto, Ontario" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/471064769097523200/dOghyD0J.jpeg" ]
        }
      } ],
      "content" : [ "Living my best third grader life" ],
      "photo" : [ "https://pbs.twimg.com/media/EhulKTFU8AIQgCM.jpg" ]
    }
  },
  "tags" : [ "food" ],
  "client_id" : "https://indigenous.realize.be"
}
