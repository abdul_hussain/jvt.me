{
  "date" : "2020-09-01T14:44:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "name" : [ "Like of @freezydorito's tweet" ],
    "published" : [ "2020-09-01T14:44:00+01:00" ],
    "category" : [ "gaming" ],
    "like-of" : [ "https://twitter.com/freezydorito/status/1300703597995401217" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/ctfue",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1300703597995401217" ],
      "url" : [ "https://twitter.com/freezydorito/status/1300703597995401217" ],
      "published" : [ "2020-09-01T07:54:38+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:freezydorito" ],
          "numeric-id" : [ "3004020255" ],
          "name" : [ "laura calabazas" ],
          "nickname" : [ "freezydorito" ],
          "url" : [ "https://twitter.com/freezydorito", "http://butts.farm" ],
          "published" : [ "2015-01-29T10:06:54+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "\"shoreditch\" allegedly" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1294254216756834304/EejH9Tpl.jpg" ]
        }
      } ],
      "content" : [ "every time i think im doing some horrible frontend code that ‘works’ i think back to when fallout 3 wanted to animate a train but objects cant move in their engine so they gave an invisible guy a train-shaped-sized hat instead and made him run the tracks. thats the spirit" ]
    }
  },
  "tags" : [ "gaming" ],
  "client_id" : "https://indigenous.realize.be"
}
