{
  "date" : "2020-09-21T17:38:00+0100",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/TobyonTV/status/1307993356941889538" ],
    "name" : [ "Like of @TobyonTV's tweet" ],
    "published" : [ "2020-09-21T17:38:00+0100" ],
    "category" : [ "politics", "coronavirus" ],
    "like-of" : [ "https://twitter.com/TobyonTV/status/1307993356941889538" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/4ntmo",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1307993356941889538" ],
      "url" : [ "https://twitter.com/TobyonTV/status/1307993356941889538" ],
      "video" : [ "https://video.twimg.com/ext_tw_video/1307992887297298433/pu/vid/1280x720/b22q6EzdQEYQSDBC.mp4?tag=10" ],
      "published" : [ "2020-09-21T10:41:32+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:TobyonTV" ],
          "numeric-id" : [ "188495946" ],
          "name" : [ "Toby Earle" ],
          "nickname" : [ "TobyonTV" ],
          "url" : [ "https://twitter.com/TobyonTV", "http://www.tobyontv.com" ],
          "published" : [ "2010-09-08T21:41:29+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1237394303242010626/nmT24FN5.jpg" ]
        }
      } ],
      "content" : [ "Matt Hancock’s furrowed brow as Philip Schofield says thirty kids can’t go & feed ducks but thirty blokes can go out & shoot them" ]
    }
  },
  "tags" : [ "politics", "coronavirus" ],
  "client_id" : "https://indigenous.realize.be"
}
