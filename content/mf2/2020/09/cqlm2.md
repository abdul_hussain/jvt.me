{
  "date" : "2020-09-07T10:56:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/devopsdom/status/1302904507509207060" ],
    "name" : [ "Like of @devopsdom's tweet" ],
    "published" : [ "2020-09-07T10:56:00+01:00" ],
    "like-of" : [ "https://twitter.com/devopsdom/status/1302904507509207060" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/cqlm2",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1302904507509207060" ],
      "url" : [ "https://twitter.com/devopsdom/status/1302904507509207060" ],
      "published" : [ "2020-09-07T09:40:16+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:devopsdom" ],
          "numeric-id" : [ "887257803555000320" ],
          "name" : [ "💎 DevOpsDomi 💀 //#BlackLivesMatter" ],
          "nickname" : [ "devopsdom" ],
          "url" : [ "https://twitter.com/devopsdom" ],
          "published" : [ "2017-07-18T10:28:39+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1302892721452052487/wHdeU4oV.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Time to dig out this gem again - \"The computer industry is the only industry that is more fashion-driven than women’s fashion.\n\"",
        "html" : "<div style=\"white-space: pre\">Time to dig out this gem again - \"The computer industry is the only industry that is more fashion-driven than women’s fashion.\n\"</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
