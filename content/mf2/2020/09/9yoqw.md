{
  "date" : "2020-09-26T22:55:00+0100",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/CarolSaysThings/status/1309949075366842372" ],
    "name" : [ "Like of @CarolSaysThings's tweet" ],
    "published" : [ "2020-09-26T22:55:00+0100" ],
    "like-of" : [ "https://twitter.com/CarolSaysThings/status/1309949075366842372" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/9yoqw",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1309949075366842372" ],
      "url" : [ "https://twitter.com/CarolSaysThings/status/1309949075366842372" ],
      "published" : [ "2020-09-26T20:12:51+00:00" ],
      "in-reply-to" : [ "https://twitter.com/garyshort/status/1309810115403448322" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:CarolSaysThings" ],
          "numeric-id" : [ "36382927" ],
          "name" : [ "Carol 🌻" ],
          "nickname" : [ "CarolSaysThings" ],
          "url" : [ "https://twitter.com/CarolSaysThings", "https://carolgilabert.me/" ],
          "published" : [ "2009-04-29T15:22:13+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "🇧🇷🇪🇸🇬🇧 · Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1305801612359761921/YH1Oo2W0.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Every time this comes up I link to this blog post that covers it quite well, please read it:\n\nxaprb.com/blog/you-guys/",
        "html" : "<div style=\"white-space: pre\">Every time this comes up I link to this blog post that covers it quite well, please read it:\n\n<a href=\"https://www.xaprb.com/blog/you-guys/\">xaprb.com/blog/you-guys/</a></div>\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/garyshort\"></a>\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/pete_codes\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
