{
  "date" : "2020-09-13T14:09:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/qikipedia/status/1305098904585867264" ],
    "name" : [ "Like of @qikipedia's tweet" ],
    "published" : [ "2020-09-13T14:09:00+01:00" ],
    "like-of" : [ "https://twitter.com/qikipedia/status/1305098904585867264" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/f1ej7",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1305098904585867264" ],
      "url" : [ "https://twitter.com/qikipedia/status/1305098904585867264" ],
      "published" : [ "2020-09-13T11:00:01+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:qikipedia" ],
          "numeric-id" : [ "22151193" ],
          "name" : [ "Quite Interesting" ],
          "nickname" : [ "qikipedia" ],
          "url" : [ "https://twitter.com/qikipedia", "http://www.qi.com", "http://instagram.com/theqielves", "http://facebook.com/OfficialQI/", "http://youtube.com/user/TheQIElves" ],
          "published" : [ "2009-02-27T15:36:24+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "QI HQ" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1250059035056971779/mnBGCeFj.jpg" ]
        }
      } ],
      "content" : [ "New evidence suggests that the Dunning-Kruger effect doesn't exist - people who don't know what they're talking about are aware of that fact." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
