{
  "date" : "2020-09-07T22:25:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/PaulDJohnston/status/1303045563982721024" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1303083016231817219" ],
    "name" : [ "Reply to https://twitter.com/PaulDJohnston/status/1303045563982721024" ],
    "published" : [ "2020-09-07T22:25:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "I've not used them before but I've heard great things about <span class=\"h-card\"><a class=\"u-url\" href=\"https://twitter.com/LyricalHost\">@LyricalHost</a></span>"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/09/axslf",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1303045563982721024" ],
      "url" : [ "https://twitter.com/PaulDJohnston/status/1303045563982721024" ],
      "published" : [ "2020-09-07T19:00:46+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:PaulDJohnston" ],
          "numeric-id" : [ "32193" ],
          "name" : [ "Paul Johnston - @home / needing a new hobby" ],
          "nickname" : [ "PaulDJohnston" ],
          "url" : [ "https://twitter.com/PaulDJohnston", "http://medium.com/@PaulDJohnston" ],
          "published" : [ "2006-11-30T11:53:49+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Wolverton, Milton Keynes, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1167486357360586752/hGxBAIVj.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "What's the best (pref UK) WordPress managed hosting?\n\nWe have 2 websites that we've been quoted a *lot* for managed hosting renewal, and I just want to find an alternative. It also seems a long way out of date in terms of WP version. \n\n\"Ours\" as a response will be ignored.",
        "html" : "<div style=\"white-space: pre\">What's the best (pref UK) WordPress managed hosting?\n\nWe have 2 websites that we've been quoted a *lot* for managed hosting renewal, and I just want to find an alternative. It also seems a long way out of date in terms of WP version. \n\n\"Ours\" as a response will be ignored.</div>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
