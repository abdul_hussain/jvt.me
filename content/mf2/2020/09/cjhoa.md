{
  "date" : "2020-09-26T19:59:00+0100",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/MrAndrew/status/1309911045788622849" ],
    "name" : [ "Like of @MrAndrew's tweet" ],
    "published" : [ "2020-09-26T19:59:00+0100" ],
    "like-of" : [ "https://twitter.com/MrAndrew/status/1309911045788622849" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/cjhoa",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1309911045788622849" ],
      "url" : [ "https://twitter.com/MrAndrew/status/1309911045788622849" ],
      "published" : [ "2020-09-26T17:41:44+00:00" ],
      "in-reply-to" : [ "https://twitter.com/garyshort/status/1309907216821084160" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:MrAndrew" ],
          "numeric-id" : [ "9626182" ],
          "name" : [ "Andrew Seward" ],
          "nickname" : [ "MrAndrew" ],
          "url" : [ "https://twitter.com/MrAndrew", "http://www.technottingham.com" ],
          "published" : [ "2007-10-23T15:57:18+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Riddings, Derbyshire" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1307037922571554817/4gtvCAvb.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "How it is acceptable to communicate, particularly when it comes to nouns that refer to groups of people, change over time.\n\nWhat's arbitrary is where you are choosing to dig your heels in.",
        "html" : "<div style=\"white-space: pre\">How it is acceptable to communicate, particularly when it comes to nouns that refer to groups of people, change over time.\n\nWhat's arbitrary is where you are choosing to dig your heels in.</div>\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/garyshort\"></a>\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/pete_codes\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
