{
  "date" : "2020-09-01T12:44:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/tas50/status/1300673605743685633" ],
    "name" : [ "Like of @tas50's tweet" ],
    "published" : [ "2020-09-01T12:44:00+01:00" ],
    "category" : [ "chef", "rubocop" ],
    "like-of" : [ "https://twitter.com/tas50/status/1300673605743685633" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/4p0qu",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1300673605743685633" ],
      "url" : [ "https://twitter.com/tas50/status/1300673605743685633" ],
      "published" : [ "2020-09-01T05:55:27+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:tas50" ],
          "numeric-id" : [ "16125527" ],
          "name" : [ "Tim Smith" ],
          "nickname" : [ "tas50" ],
          "url" : [ "https://twitter.com/tas50", "http://www.therealtimsmith.com" ],
          "published" : [ "2008-09-04T05:13:12+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Portland, OR" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/720006888503402496/oUvXSF8p.jpg" ]
        }
      } ],
      "content" : [ "I'm pretty stoked for this one: The first PR opened by a GitHub App that can be added to your org to automatically autocorrect all your Chef Infra code with each new Cookstyle release. Huge shout out to Xorima for proving the power of the concept:" ],
      "photo" : [ "https://pbs.twimg.com/media/Egzrf1lU8AAlxSF.jpg" ]
    }
  },
  "tags" : [ "chef", "rubocop" ],
  "client_id" : "https://indigenous.realize.be"
}
