{
  "date" : "2020-09-05T09:04:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/iamhirusi/status/1302136189319692290" ],
    "name" : [ "Like of @iamhirusi's tweet" ],
    "published" : [ "2020-09-05T09:04:00+01:00" ],
    "like-of" : [ "https://twitter.com/iamhirusi/status/1302136189319692290" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/l9bdk",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1302136189319692290" ],
      "url" : [ "https://twitter.com/iamhirusi/status/1302136189319692290" ],
      "published" : [ "2020-09-05T06:47:14+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:iamhirusi" ],
          "numeric-id" : [ "1252888172700119041" ],
          "name" : [ "Ru Singh" ],
          "nickname" : [ "iamhirusi" ],
          "url" : [ "https://twitter.com/iamhirusi", "https://rusingh.com" ],
          "published" : [ "2020-04-22T09:14:03+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "India" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1287100701965840385/7LCy7NCC.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "First #IndieWeb RSVP! 🙃 \n\nrusingh.com/micro/rsvps/20…",
        "html" : "<div style=\"white-space: pre\">First <a href=\"https://twitter.com/search?q=%23IndieWeb\">#IndieWeb</a> RSVP! 🙃 \n\n<a href=\"https://rusingh.com/micro/rsvps/2020/09/05/homebrew-india/\">rusingh.com/micro/rsvps/20…</a></div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
