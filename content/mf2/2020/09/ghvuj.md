{
  "date" : "2020-09-13T17:06:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/Julieanne/status/1305086140949704704" ],
    "name" : [ "Like of @Julieanne's tweet" ],
    "published" : [ "2020-09-13T17:06:00+01:00" ],
    "category" : [ "food" ],
    "like-of" : [ "https://twitter.com/Julieanne/status/1305086140949704704" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/ghvuj",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1305086140949704704" ],
      "url" : [ "https://twitter.com/Julieanne/status/1305086140949704704" ],
      "published" : [ "2020-09-13T10:09:17+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Julieanne" ],
          "numeric-id" : [ "1235161" ],
          "name" : [ "Julie Sharp" ],
          "nickname" : [ "Julieanne" ],
          "url" : [ "https://twitter.com/Julieanne" ],
          "published" : [ "2007-03-15T17:15:31+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Brighton" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/725623949317562368/CNHnlSuV.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Happy Birthday @rem 🎉🎂🎁🎈",
        "html" : "Happy Birthday <a href=\"https://twitter.com/rem\">@rem</a> 🎉🎂🎁🎈"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EhyY3NpWAAErjbJ.jpg" ]
    }
  },
  "tags" : [ "food" ],
  "client_id" : "https://indigenous.realize.be"
}
