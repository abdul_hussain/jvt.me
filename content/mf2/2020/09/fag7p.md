{
  "date" : "2020-09-25T23:46:00+0100",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/mipsytipsy/status/1309604622554640384" ],
    "name" : [ "Like of @mipsytipsy's tweet" ],
    "published" : [ "2020-09-25T23:46:00+0100" ],
    "category" : [ "on-call" ],
    "like-of" : [ "https://twitter.com/mipsytipsy/status/1309604622554640384" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/fag7p",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1309604622554640384" ],
      "url" : [ "https://twitter.com/mipsytipsy/status/1309604622554640384" ],
      "published" : [ "2020-09-25T21:24:07+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:mipsytipsy" ],
          "numeric-id" : [ "90488576" ],
          "name" : [ "Charity Majors" ],
          "nickname" : [ "mipsytipsy" ],
          "url" : [ "https://twitter.com/mipsytipsy", "http://charity.wtf" ],
          "published" : [ "2009-11-16T21:28:13+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "San Francisco" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1293091409164001281/_2uTc3m4.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Maybe I need to write a blog post called \"On Call For Managers\". If you're asking engineers to be on call for their code -- and you should -- you owe in return:\n\n- enough time to fix what's broken\n- hands to do the work\n- closely track how often they are interrupted/woken\n- ..etc",
        "html" : "<div style=\"white-space: pre\">Maybe I need to write a blog post called \"On Call For Managers\". If you're asking engineers to be on call for their code -- and you should -- you owe in return:\n\n- enough time to fix what's broken\n- hands to do the work\n- closely track how often they are interrupted/woken\n- ..etc</div>"
      } ]
    }
  },
  "tags" : [ "on-call" ],
  "client_id" : "https://indigenous.realize.be"
}
