{
  "date" : "2020-09-19T14:14:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/arthur_affect/status/1306909606480756736" ],
    "name" : [ "Like of @arthur_affect's tweet" ],
    "published" : [ "2020-09-19T14:14:00+01:00" ],
    "like-of" : [ "https://twitter.com/arthur_affect/status/1306909606480756736" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/ijw8z",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1306909606480756736" ],
      "url" : [ "https://twitter.com/arthur_affect/status/1306909606480756736" ],
      "published" : [ "2020-09-18T10:55:06+00:00" ],
      "in-reply-to" : [ "https://twitter.com/LilyMarsWrites/status/1306901255177015296" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:arthur_affect" ],
          "numeric-id" : [ "65260792" ],
          "name" : [ "Arthur Chu" ],
          "nickname" : [ "arthur_affect" ],
          "url" : [ "https://twitter.com/arthur_affect", "http://arthur-chu.com" ],
          "published" : [ "2009-08-13T03:28:27+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Broadview Heights, Ohio" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1830206330/profile.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Following someone home while they're jogging is only an acceptable way to befriend someone if you are a cat",
        "html" : "Following someone home while they're jogging is only an acceptable way to befriend someone if you are a cat\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/LilyMarsWrites\"></a>\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/TheBrinz\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
