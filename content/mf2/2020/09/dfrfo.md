{
  "date" : "2020-09-06T22:27:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/edent/status/1302670827377360896" ],
    "name" : [ "Like of @edent's tweet" ],
    "published" : [ "2020-09-06T22:27:00+01:00" ],
    "like-of" : [ "https://twitter.com/edent/status/1302670827377360896" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/dfrfo",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1302670827377360896" ],
      "url" : [ "https://twitter.com/edent/status/1302670827377360896" ],
      "published" : [ "2020-09-06T18:11:42+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:edent" ],
          "numeric-id" : [ "14054507" ],
          "name" : [ "Terence Eden" ],
          "nickname" : [ "edent" ],
          "url" : [ "https://twitter.com/edent", "https://shkspr.mobi/blog/", "http://edent.tel" ],
          "published" : [ "2008-02-28T13:10:25+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1228067445153452033/_A8Uq2VY.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I pour the *best* pints.\n#DoYaWantAFlakeInThatLove",
        "html" : "<div style=\"white-space: pre\">I pour the *best* pints.\n<a href=\"https://twitter.com/search?q=%23DoYaWantAFlakeInThatLove\">#DoYaWantAFlakeInThatLove</a></div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EhQELKRXgAEUSmE.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
