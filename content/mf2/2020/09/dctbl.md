{
  "date" : "2020-09-05T19:49:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/shannonmstirone/status/1302289118517370886" ],
    "name" : [ "Like of @shannonmstirone's tweet" ],
    "published" : [ "2020-09-05T19:49:00+01:00" ],
    "like-of" : [ "https://twitter.com/shannonmstirone/status/1302289118517370886" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/dctbl",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1302289118517370886" ],
      "url" : [ "https://twitter.com/shannonmstirone/status/1302289118517370886" ],
      "published" : [ "2020-09-05T16:54:55+00:00" ],
      "in-reply-to" : [ "https://twitter.com/shannonmstirone/status/1302288711472750592" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:shannonmstirone" ],
          "numeric-id" : [ "2707961760" ],
          "name" : [ "Shannon Stirone 💀" ],
          "nickname" : [ "shannonmstirone" ],
          "url" : [ "https://twitter.com/shannonmstirone", "http://www.shannonstirone.com" ],
          "published" : [ "2014-08-05T01:55:07+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Oakland, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1296261763889901568/c0AiyyRL.jpg" ]
        }
      } ],
      "content" : [ "Jupes magnetic field is so big that if we could see it with visible light it would appear about the size of the full moon in the sky. It’s the second largest in the solar system, second only to the sun." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
