{
  "date" : "2020-09-20T15:19:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/_jsimonovski/status/1307542747197239296" ],
    "name" : [ "Like of @_jsimonovski's tweet" ],
    "published" : [ "2020-09-20T15:19:00+01:00" ],
    "category" : [ "twitter", "diversity-and-inclusion" ],
    "like-of" : [ "https://twitter.com/_jsimonovski/status/1307542747197239296" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/x8li6",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1307542747197239296" ],
      "url" : [ "https://twitter.com/_jsimonovski/status/1307542747197239296" ],
      "published" : [ "2020-09-20T04:50:58+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:_jsimonovski" ],
          "numeric-id" : [ "846674811904376832" ],
          "name" : [ "Jordan Simonovski" ],
          "nickname" : [ "_jsimonovski" ],
          "url" : [ "https://twitter.com/_jsimonovski", "https://blog.jordansimonov.ski/" ],
          "published" : [ "2017-03-28T10:46:20+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Sydney, New South Wales" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1305266114667003906/dqzM4CbE.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I wonder if Twitter does this to fictional characters too.\n\n              Lenny                                              Carl",
        "html" : "<div style=\"white-space: pre\">I wonder if Twitter does this to fictional characters too.\n\n              Lenny                                              Carl</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EiVS-PvUMAE6Ap4.jpg", "https://pbs.twimg.com/media/EiVTBEbVgAEElHk.jpg" ]
    }
  },
  "tags" : [ "twitter", "diversity-and-inclusion" ],
  "client_id" : "https://indigenous.realize.be"
}
