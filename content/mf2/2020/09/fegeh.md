{
  "date" : "2020-09-03T18:08:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/alicegoldfuss/status/1301301140315762688" ],
    "name" : [ "Like of @alicegoldfuss's tweet" ],
    "published" : [ "2020-09-03T18:08:00+01:00" ],
    "like-of" : [ "https://twitter.com/alicegoldfuss/status/1301301140315762688" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/fegeh",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1301301140315762688" ],
      "url" : [ "https://twitter.com/alicegoldfuss/status/1301301140315762688" ],
      "published" : [ "2020-09-02T23:29:03+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:alicegoldfuss" ],
          "numeric-id" : [ "163154809" ],
          "name" : [ "bletchley punk is a fullmetal engineer" ],
          "nickname" : [ "alicegoldfuss" ],
          "url" : [ "https://twitter.com/alicegoldfuss", "http://blog.alicegoldfuss.com/", "http://twitch.tv/bletchleypunk" ],
          "published" : [ "2010-07-05T17:51:34+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "/usr/local/sin" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1149452202827759616/1blf87Kn.jpg" ]
        }
      } ],
      "content" : [ "successfully debugged a lock contention, race condition, and invalid memory using only print statements, I have earned a quiet death" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
