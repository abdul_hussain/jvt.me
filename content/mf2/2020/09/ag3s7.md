{
  "date" : "2020-09-25T23:47:00+0100",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/nickijones71/status/1309069203014594560" ],
    "name" : [ "Like of @nickijones71's tweet" ],
    "published" : [ "2020-09-25T23:47:00+0100" ],
    "category" : [ "politics" ],
    "like-of" : [ "https://twitter.com/nickijones71/status/1309069203014594560" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/ag3s7",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1309069203014594560" ],
      "url" : [ "https://twitter.com/nickijones71/status/1309069203014594560" ],
      "published" : [ "2020-09-24T09:56:33+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:nickijones71" ],
          "numeric-id" : [ "741566933263323136" ],
          "name" : [ "Niknax" ],
          "nickname" : [ "nickijones71" ],
          "url" : [ "https://twitter.com/nickijones71" ],
          "published" : [ "2016-06-11T09:45:09+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Winchester, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1270134758777987077/_VOFUu2p.jpg" ]
        }
      } ],
      "content" : [ "My husband’s new t-shirt that I ordered him has arrived. I love it 🤣" ],
      "photo" : [ "https://pbs.twimg.com/media/Eiq_f6YXkAEmYgu.jpg" ]
    }
  },
  "tags" : [ "politics" ],
  "client_id" : "https://indigenous.realize.be"
}
