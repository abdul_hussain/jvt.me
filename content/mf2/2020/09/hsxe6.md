{
  "date" : "2020-09-08T21:59:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://events.indieweb.org/2020/10/homebrew-website-club-nottingham-hacktoberfest-edition--2BrVspWn4jPt" ],
    "name" : [ "RSVP yes to https://events.indieweb.org/2020/10/homebrew-website-club-nottingham-hacktoberfest-edition--2BrVspWn4jPt" ],
    "published" : [ "2020-09-08T21:59:00+01:00" ],
    "event" : {
      "start" : [ "2020-10-28T17:30:00+00:00" ],
      "name" : [ "Homebrew Website Club: Nottingham (Hacktoberfest edition)" ],
      "end" : [ "2020-10-28T19:30:00+00:00" ],
      "url" : [ "https://events.indieweb.org/2020/10/homebrew-website-club-nottingham-hacktoberfest-edition--2BrVspWn4jPt" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/09/hsxe6",
  "client_id" : "https://indigenous.realize.be"
}
