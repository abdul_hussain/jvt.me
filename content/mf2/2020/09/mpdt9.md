{
  "date" : "2020-09-02T22:59:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "name" : [ "Like of @elibelly's tweet" ],
    "published" : [ "2020-09-02T22:59:00+01:00" ],
    "category" : [ "monzo" ],
    "like-of" : [ "https://twitter.com/elibelly/status/1301120530444169217" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/mpdt9",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1301120530444169217" ],
      "url" : [ "https://twitter.com/elibelly/status/1301120530444169217" ],
      "video" : [ "https://video.twimg.com/tweet_video/Eg6CN-YXkAUkkyc.mp4" ],
      "published" : [ "2020-09-02T11:31:22+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:elibelly" ],
          "numeric-id" : [ "19629753" ],
          "name" : [ "eli schutze" ],
          "nickname" : [ "elibelly" ],
          "url" : [ "https://twitter.com/elibelly", "https://monzo.me/eli" ],
          "published" : [ "2009-01-28T01:36:40+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1209626995233099777/pXbuB6mO.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "The new @monzo fees are basically \"you commit to us and we'll commit to you\"\n\nYou want no fees? Make them your main squeeze 💍\n\nYou wanna treat them like a side chick? Gotta pay for the privilege 💅\n\nSeems fair to me.",
        "html" : "<div style=\"white-space: pre\">The new <a href=\"https://twitter.com/monzo\">@monzo</a> fees are basically \"you commit to us and we'll commit to you\"\n\nYou want no fees? Make them your main squeeze 💍\n\nYou wanna treat them like a side chick? Gotta pay for the privilege 💅\n\nSeems fair to me.</div>"
      } ]
    }
  },
  "tags" : [ "monzo" ],
  "client_id" : "https://indigenous.realize.be"
}
