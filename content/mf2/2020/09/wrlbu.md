{
  "date" : "2020-09-26T19:59:00+0100",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/dotDestroyer/status/1309920586123051010" ],
    "name" : [ "Like of @dotDestroyer's tweet" ],
    "published" : [ "2020-09-26T19:59:00+0100" ],
    "category" : [ "guys" ],
    "like-of" : [ "https://twitter.com/dotDestroyer/status/1309920586123051010" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/wrlbu",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1309920586123051010" ],
      "url" : [ "https://twitter.com/dotDestroyer/status/1309920586123051010" ],
      "published" : [ "2020-09-26T18:19:39+00:00" ],
      "in-reply-to" : [ "https://twitter.com/MrAndrew/status/1309917057627160578" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:dotDestroyer" ],
          "numeric-id" : [ "721879454" ],
          "name" : [ "Oliver Tomlinson 🇪🇺" ],
          "nickname" : [ "dotDestroyer" ],
          "url" : [ "https://twitter.com/dotDestroyer" ],
          "published" : [ "2012-07-28T09:46:16+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1038473600070770688/QOnI-FOf.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I’ve recently started substituting “guys” for “folks”. \n\nIt’s not easy to change the habit of a lifetime, but it comes at no cost to me, I’ll keep at it.",
        "html" : "<div style=\"white-space: pre\">I’ve recently started substituting “guys” for “folks”. \n\nIt’s not easy to change the habit of a lifetime, but it comes at no cost to me, I’ll keep at it.</div>\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/MrAndrew\"></a>\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/garyshort\"></a>\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/pete_codes\"></a>"
      } ]
    }
  },
  "tags" : [ "guys" ],
  "client_id" : "https://indigenous.realize.be"
}
