{
  "date" : "2020-09-13T20:31:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/Marcus_Noble_/status/1305221606332149763" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1305228751530332162" ],
    "name" : [ "Reply to https://twitter.com/Marcus_Noble_/status/1305221606332149763" ],
    "published" : [ "2020-09-13T20:31:00+01:00" ],
    "category" : [ "git" ],
    "content" : [ {
      "html" : "",
      "value" : "I don't think so but https://github.com/forgefed/forgefed is a standard that's being worked on to make it possible to federated across providers for this reason"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/09/6tc7a",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1305221606332149763" ],
      "url" : [ "https://twitter.com/Marcus_Noble_/status/1305221606332149763" ],
      "published" : [ "2020-09-13T19:07:35+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Marcus_Noble_" ],
          "numeric-id" : [ "14732545" ],
          "name" : [ "Marcus Noble" ],
          "nickname" : [ "Marcus_Noble_" ],
          "url" : [ "https://twitter.com/Marcus_Noble_", "https://marcusnoble.co.uk" ],
          "published" : [ "2008-05-11T09:51:36+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "PID 1" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/776738772759277569/hfaM5zhA.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Does anyone know of any user-friendly way of doing a git fork / pull request across different git hosts?\ni.e. I want to be able to fork a repo from a Gitea repo into GitHub, do some changes then raise a pull request with those changes.",
        "html" : "<div style=\"white-space: pre\">Does anyone know of any user-friendly way of doing a git fork / pull request across different git hosts?\ni.e. I want to be able to fork a repo from a Gitea repo into GitHub, do some changes then raise a pull request with those changes.</div>"
      } ]
    }
  },
  "tags" : [ "git" ],
  "client_id" : "https://indigenous.realize.be"
}
