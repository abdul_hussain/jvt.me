{
  "date" : "2020-09-26T16:11:00+0100",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/lskiuk/status/1309872908060127232" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1309875916743544832" ],
    "name" : [ "Reply to https://twitter.com/lskiuk/status/1309872908060127232" ],
    "published" : [ "2020-09-26T16:11:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Yeah definitely fair! I know exactly what you mean - and I'm generally going for low hanging fruit 😅"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/09/2yq5k",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1309872908060127232" ],
      "url" : [ "https://twitter.com/lskiuk/status/1309872908060127232" ],
      "published" : [ "2020-09-26T15:10:12+00:00" ],
      "in-reply-to" : [ "https://twitter.com/JamieTanna/status/1309870742142701569" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:lskiuk" ],
          "numeric-id" : [ "560920130" ],
          "name" : [ "Lski (Lee)" ],
          "nickname" : [ "lskiuk" ],
          "url" : [ "https://twitter.com/lskiuk", "https://www.lski.uk" ],
          "published" : [ "2012-04-23T07:59:01+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Helsinki, Suomi" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1243607828813819907/uPCFkm1q.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "There are some things I cant say on this, but think bigger fish to fight at the time, might make sense? I didnt actually give up though, I just didnt push heavily.\n\nIm also rather known as speaking up for things I dont agree with, thats not really my problem :)",
        "html" : "<div style=\"white-space: pre\">There are some things I cant say on this, but think bigger fish to fight at the time, might make sense? I didnt actually give up though, I just didnt push heavily.\n\nIm also rather known as speaking up for things I dont agree with, thats not really my problem :)</div>\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/JamieTanna\"></a>\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/pete_codes\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
