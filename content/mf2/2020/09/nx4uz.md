{
  "date" : "2020-09-13T19:25:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/bitandbang/status/1305025894390665216" ],
    "name" : [ "Like of @bitandbang's tweet" ],
    "published" : [ "2020-09-13T19:25:00+01:00" ],
    "like-of" : [ "https://twitter.com/bitandbang/status/1305025894390665216" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/nx4uz",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1305025894390665216" ],
      "url" : [ "https://twitter.com/bitandbang/status/1305025894390665216" ],
      "published" : [ "2020-09-13T06:09:54+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:bitandbang" ],
          "numeric-id" : [ "622960227" ],
          "name" : [ "Tierney Cyren 🍑" ],
          "nickname" : [ "bitandbang" ],
          "url" : [ "https://twitter.com/bitandbang" ],
          "published" : [ "2012-06-30T14:57:31+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Brooklyn, NY" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1274345571940208642/JFMpVpsW.jpg" ]
        }
      } ],
      "content" : [ "mr. bob really is just so full of himself that he thinks more people would actively go to a conference because he was speaking at it than would entirely avoid it forever knowing he was invited and allowed to speak at it" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
