{
  "date" : "2020-09-10T12:48:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/Brunty/status/1304011243905323008" ],
    "name" : [ "Like of @Brunty's tweet" ],
    "published" : [ "2020-09-10T12:48:00+01:00" ],
    "like-of" : [ "https://twitter.com/Brunty/status/1304011243905323008" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/fsne9",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1304011243905323008" ],
      "url" : [ "https://twitter.com/Brunty/status/1304011243905323008" ],
      "published" : [ "2020-09-10T10:58:02+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Brunty" ],
          "numeric-id" : [ "1099517012" ],
          "name" : [ "Matt Brunt" ],
          "nickname" : [ "Brunty" ],
          "url" : [ "https://twitter.com/Brunty", "https://brunty.me/now" ],
          "published" : [ "2013-01-17T23:19:21+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Leicester, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1151043501288824832/TjB9WT83.jpg" ]
        }
      } ],
      "content" : [ "TFW your code style fixing tools tell you they've fixed -1 errors. ARE YOU INTRODUCING PROBLEMS THERE?!" ],
      "photo" : [ "https://pbs.twimg.com/media/EhjHS60XkAM3GWV.png" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
