{
  "date" : "2020-09-16T22:52:00+0100",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/stephensauceda/status/1306247907981524992" ],
    "name" : [ "Like of @stephensauceda's tweet" ],
    "published" : [ "2020-09-16T22:52:00+0100" ],
    "like-of" : [ "https://twitter.com/stephensauceda/status/1306247907981524992" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/83tbu",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1306247907981524992" ],
      "url" : [ "https://twitter.com/stephensauceda/status/1306247907981524992" ],
      "published" : [ "2020-09-16T15:05:44+00:00" ],
      "in-reply-to" : [ "https://twitter.com/haysstanford/status/1306209477226569729" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:stephensauceda" ],
          "numeric-id" : [ "15521957" ],
          "name" : [ "Stephen" ],
          "nickname" : [ "stephensauceda" ],
          "url" : [ "https://twitter.com/stephensauceda", "https://stephensauceda.com" ],
          "published" : [ "2008-07-21T22:42:39+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "South Carolina" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1282419135205576710/MBz5FNwa.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "What’s stopping you?",
        "html" : "What’s stopping you?\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/haysstanford\"></a>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EiC5i2UWsAAs91c.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
