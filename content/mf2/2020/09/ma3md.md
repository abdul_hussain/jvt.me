{
  "date" : "2020-09-19T17:02:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamieTanna/status/1307351202670223363" ],
    "published" : [ "2020-09-19T17:02:00+01:00" ],
    "repost-of" : [ "https://twitter.com/ellie_made/status/1307311339258425346" ],
    "category" : [ "ethics" ]
  },
  "kind" : "reposts",
  "slug" : "2020/09/ma3md",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1307311339258425346" ],
      "url" : [ "https://twitter.com/ellie_made/status/1307311339258425346" ],
      "published" : [ "2020-09-19T13:31:26+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:ellie_made" ],
          "numeric-id" : [ "3064937201" ],
          "name" : [ "Dr Ellie" ],
          "nickname" : [ "ellie_made" ],
          "url" : [ "https://twitter.com/ellie_made", "http://www.totallyflagulous.com" ],
          "published" : [ "2015-02-28T19:40:23+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "United Kingdom" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1269893622067011584/ZwPxom5z.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Please, teams of tech people who think you're really clever, please before you release a thing can you ask yourselves unbelievably basic questions like:\n- does this work for Black people?\n- would a domestic abuser find this handy?\n- can this be used by a government to oppress?",
        "html" : "<div style=\"white-space: pre\">Please, teams of tech people who think you're really clever, please before you release a thing can you ask yourselves unbelievably basic questions like:\n- does this work for Black people?\n- would a domestic abuser find this handy?\n- can this be used by a government to oppress?</div>"
      } ]
    }
  },
  "tags" : [ "ethics" ],
  "client_id" : "https://indigenous.realize.be"
}
