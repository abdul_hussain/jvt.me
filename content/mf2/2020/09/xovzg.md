{
  "date" : "2020-09-14T07:31:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/mathladyhazel/status/1304671651137835008" ],
    "name" : [ "Like of @mathladyhazel's tweet" ],
    "published" : [ "2020-09-14T07:31:00+01:00" ],
    "like-of" : [ "https://twitter.com/mathladyhazel/status/1304671651137835008" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/xovzg",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1304671651137835008" ],
      "url" : [ "https://twitter.com/mathladyhazel/status/1304671651137835008" ],
      "video" : [ "https://video.twimg.com/tweet_video/Ehsf2KtXsAAIvNV.mp4" ],
      "published" : [ "2020-09-12T06:42:15+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:mathladyhazel" ],
          "numeric-id" : [ "156576788" ],
          "name" : [ "Math Lady Hazel 🇦🇷" ],
          "nickname" : [ "mathladyhazel" ],
          "url" : [ "https://twitter.com/mathladyhazel", "http://abakcus.com" ],
          "published" : [ "2010-06-17T09:00:26+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "New York" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/979823932873129984/9K9CB1B1.jpg" ]
        }
      } ],
      "content" : [ "Where am I gonna use math? 😂" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
