{
  "date" : "2020-09-06T21:17:10.76Z",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://events.indieweb.org/2020/09/homebrew-website-club-nottingham-4BIUc3geqfW0" ],
    "name" : [ "RSVP yes to https://events.indieweb.org/2020/09/homebrew-website-club-nottingham-4BIUc3geqfW0" ],
    "published" : [ "2020-09-06T21:17:10.76Z" ],
    "event" : {
      "start" : [ "2020-09-16T17:30:00+01:00" ],
      "name" : [ "Homebrew Website Club: Nottingham" ],
      "end" : [ "2020-09-16T19:30:00+01:00" ],
      "url" : [ "https://events.indieweb.org/2020/09/homebrew-website-club-nottingham-4BIUc3geqfW0" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/09/npmbg",
  "client_id" : "https://micropublish.net"
}
