{
  "date" : "2020-09-12T22:19:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/kitchens_sync/status/1304638058868875267" ],
    "name" : [ "Like of @kitchens_sync's tweet" ],
    "published" : [ "2020-09-12T22:19:00+01:00" ],
    "like-of" : [ "https://twitter.com/kitchens_sync/status/1304638058868875267" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/yoes0",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1304638058868875267" ],
      "url" : [ "https://twitter.com/kitchens_sync/status/1304638058868875267" ],
      "published" : [ "2020-09-12T04:28:46+00:00" ],
      "in-reply-to" : [ "https://twitter.com/seldo/status/1304637603518361600" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:kitchens_sync" ],
          "numeric-id" : [ "910274060008833024" ],
          "name" : [ "Kev Kitchens 🌈" ],
          "nickname" : [ "kitchens_sync" ],
          "url" : [ "https://twitter.com/kitchens_sync", "http://instagram.com/kev.kitchens/" ],
          "published" : [ "2017-09-19T22:47:02+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Bay Area, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1147504472551301120/ynT5O7pz.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Took me a second for it to click that this wasn’t about multi-factor authentication",
        "html" : "Took me a second for it to click that this wasn’t about multi-factor authentication\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/seldo\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
