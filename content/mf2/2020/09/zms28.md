{
  "date" : "2020-09-05T21:50:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/maggiecodes_/status/1302042477403279360" ],
    "name" : [ "Like of @maggiecodes_'s tweet" ],
    "published" : [ "2020-09-05T21:50:00+01:00" ],
    "category" : [ "recruitment" ],
    "like-of" : [ "https://twitter.com/maggiecodes_/status/1302042477403279360" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/zms28",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1302042477403279360" ],
      "url" : [ "https://twitter.com/maggiecodes_/status/1302042477403279360" ],
      "published" : [ "2020-09-05T00:34:52+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:maggiecodes_" ],
          "numeric-id" : [ "1224745261504835584" ],
          "name" : [ "maggie 👩🏽‍💻🌱" ],
          "nickname" : [ "maggiecodes_" ],
          "url" : [ "https://twitter.com/maggiecodes_", "https://protectblacklives.github.io/go/" ],
          "published" : [ "2020-02-04T17:24:18+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "East Coast" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1284699538272509952/9hucDmKG.jpg" ]
        }
      } ],
      "content" : [ "I just applied to a job by sending a POST request with my deets to their API endpoint 🤯" ]
    }
  },
  "tags" : [ "recruitment" ],
  "client_id" : "https://indigenous.realize.be"
}
