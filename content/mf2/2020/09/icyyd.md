{
  "date" : "2020-09-03T17:06:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/CarolSaysThings/status/1301510149434150912" ],
    "name" : [ "Like of @CarolSaysThings's tweet" ],
    "published" : [ "2020-09-03T17:06:00+01:00" ],
    "like-of" : [ "https://twitter.com/CarolSaysThings/status/1301510149434150912" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/icyyd",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1301510149434150912" ],
      "url" : [ "https://twitter.com/CarolSaysThings/status/1301510149434150912" ],
      "published" : [ "2020-09-03T13:19:35+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:CarolSaysThings" ],
          "numeric-id" : [ "36382927" ],
          "name" : [ "Carol 🌻" ],
          "nickname" : [ "CarolSaysThings" ],
          "url" : [ "https://twitter.com/CarolSaysThings", "https://carolgilabert.me/" ],
          "published" : [ "2009-04-29T15:22:13+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "🇧🇷🇪🇸🇬🇧 · Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1295718060301713408/XcKwL0Z8.jpg" ]
        }
      } ],
      "content" : [ "We just put an offer on a house (!!) and my anxiety and I are already working out what to do when the bank forecloses it 😅🙈" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
