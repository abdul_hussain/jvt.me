{
  "date" : "2020-09-05T21:51:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/gaijin5/status/1302331230680158209" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1302350527028756481" ],
    "name" : [ "Reply to https://twitter.com/gaijin5/status/1302331230680158209" ],
    "published" : [ "2020-09-05T21:51:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "Why bonus points for curl? 🤔"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/09/k5qju",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1302331230680158209" ],
      "url" : [ "https://twitter.com/gaijin5/status/1302331230680158209" ],
      "published" : [ "2020-09-05T19:42:16+00:00" ],
      "in-reply-to" : [ "https://twitter.com/maggiecodes_/status/1302313077036244997" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:gaijin5" ],
          "numeric-id" : [ "18467251" ],
          "name" : [ "Peter Evensen" ],
          "nickname" : [ "gaijin5" ],
          "url" : [ "https://twitter.com/gaijin5", "http://evensen.LutheranMissions.org" ],
          "published" : [ "2008-12-30T02:37:04+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Lomé, Togo, West Africa" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/2491111748/image.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I was going to say, bonus points if you did it with curl",
        "html" : "I was going to say, bonus points if you did it with curl\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/fliplikesuraj\"></a>\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/maggiecodes_\"></a>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
