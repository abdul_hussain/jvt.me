{
  "date" : "2020-09-01T12:46:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/CarolSaysThings/status/1300700333556064256" ],
    "name" : [ "Like of @CarolSaysThings's tweet" ],
    "published" : [ "2020-09-01T12:46:00+01:00" ],
    "category" : [ "coronavirus", "2020" ],
    "like-of" : [ "https://twitter.com/CarolSaysThings/status/1300700333556064256" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/bav7j",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1300700333556064256" ],
      "url" : [ "https://twitter.com/CarolSaysThings/status/1300700333556064256" ],
      "published" : [ "2020-09-01T07:41:40+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:CarolSaysThings" ],
          "numeric-id" : [ "36382927" ],
          "name" : [ "Carol 🌻" ],
          "nickname" : [ "CarolSaysThings" ],
          "url" : [ "https://twitter.com/CarolSaysThings", "https://carolgilabert.me/" ],
          "published" : [ "2009-04-29T15:22:13+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "🇧🇷🇪🇸🇬🇧 · Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1295718060301713408/XcKwL0Z8.jpg" ]
        }
      } ],
      "content" : [ "You don’t say 🙄" ],
      "photo" : [ "https://pbs.twimg.com/media/Eg0EDq_XgAc5dqd.jpg" ]
    }
  },
  "tags" : [ "coronavirus", "2020" ],
  "client_id" : "https://indigenous.realize.be"
}
