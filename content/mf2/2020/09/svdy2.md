{
  "date" : "2020-09-12T10:58:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/scottjehl/status/1304540564067622913" ],
    "name" : [ "Like of @scottjehl's tweet" ],
    "published" : [ "2020-09-12T10:58:00+01:00" ],
    "category" : [ "php" ],
    "like-of" : [ "https://twitter.com/scottjehl/status/1304540564067622913" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/svdy2",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1304540564067622913" ],
      "url" : [ "https://twitter.com/scottjehl/status/1304540564067622913" ],
      "published" : [ "2020-09-11T22:01:22+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:scottjehl" ],
          "numeric-id" : [ "237918766" ],
          "name" : [ "Scott Jehl" ],
          "nickname" : [ "scottjehl" ],
          "url" : [ "https://twitter.com/scottjehl", "http://scottjehl.com", "http://scottjehl.com/lfwp" ],
          "published" : [ "2011-01-13T23:16:47+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "NYC" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1247625703710998529/R4cFcwoC.jpg" ]
        }
      } ],
      "content" : [ "modern web dev is an extreme overreaction to not liking some php" ]
    }
  },
  "tags" : [ "php" ],
  "client_id" : "https://indigenous.realize.be"
}
