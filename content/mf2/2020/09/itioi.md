{
  "date" : "2020-09-13T19:50:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/vlad_mihalcea/status/1305018905069604865" ],
    "name" : [ "Like of @vlad_mihalcea's tweet" ],
    "published" : [ "2020-09-13T19:50:00+01:00" ],
    "category" : [ "interviewing" ],
    "like-of" : [ "https://twitter.com/vlad_mihalcea/status/1305018905069604865" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/itioi",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1305018905069604865" ],
      "url" : [ "https://twitter.com/vlad_mihalcea/status/1305018905069604865" ],
      "published" : [ "2020-09-13T05:42:07+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:vlad_mihalcea" ],
          "numeric-id" : [ "2255336726" ],
          "name" : [ "Vlad Mihalcea" ],
          "nickname" : [ "vlad_mihalcea" ],
          "url" : [ "https://twitter.com/vlad_mihalcea", "https://vladmihalcea.com/", "http://vladmihalcea.com" ],
          "published" : [ "2013-12-20T18:04:04+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Cluj-Napoca, România" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1292369358271000576/z_LLR8AX.jpg" ]
        }
      } ],
      "content" : [ "Another day has passed without having to invert a binary tree or implement a sorting algorithm." ]
    }
  },
  "tags" : [ "interviewing" ],
  "client_id" : "https://indigenous.realize.be"
}
