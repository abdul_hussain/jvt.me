{
  "date" : "2020-09-20T22:43:00+0100",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/MrAndrew/status/1305541625029038082" ],
    "name" : [ "Like of @MrAndrew's tweet" ],
    "published" : [ "2020-09-20T22:43:00+0100" ],
    "category" : [ "diversity-and-inclusion" ],
    "like-of" : [ "https://twitter.com/MrAndrew/status/1305541625029038082" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/lmqjs",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1305541625029038082" ],
      "url" : [ "https://twitter.com/MrAndrew/status/1305541625029038082" ],
      "published" : [ "2020-09-14T16:19:13+00:00" ],
      "in-reply-to" : [ "https://twitter.com/MrAndrew/status/1305536333805477889" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:MrAndrew" ],
          "numeric-id" : [ "9626182" ],
          "name" : [ "Andrew Seward" ],
          "nickname" : [ "MrAndrew" ],
          "url" : [ "https://twitter.com/MrAndrew", "http://www.technottingham.com" ],
          "published" : [ "2007-10-23T15:57:18+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Riddings, Derbyshire" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1307037922571554817/4gtvCAvb.jpg" ]
        }
      } ],
      "content" : [ "This is what happens when the people making these decisions aren't people who have to worry about harassment" ]
    }
  },
  "tags" : [ "diversity-and-inclusion" ],
  "client_id" : "https://indigenous.realize.be"
}
