{
  "date" : "2020-09-22T22:25:00+0100",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/nasamuffin/status/1308147748441481216" ],
    "name" : [ "Like of @nasamuffin's tweet" ],
    "published" : [ "2020-09-22T22:25:00+0100" ],
    "category" : [ "linux", "arch-linux" ],
    "like-of" : [ "https://twitter.com/nasamuffin/status/1308147748441481216" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/lqnap",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1308147748441481216" ],
      "url" : [ "https://twitter.com/nasamuffin/status/1308147748441481216" ],
      "published" : [ "2020-09-21T20:55:02+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:nasamuffin" ],
          "numeric-id" : [ "14398789" ],
          "name" : [ "emily dogmom" ],
          "nickname" : [ "nasamuffin" ],
          "url" : [ "https://twitter.com/nasamuffin" ],
          "published" : [ "2008-04-15T17:24:34+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "San Jose, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/924020478980767744/wEKraX0v.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "me: \"linux [task for automation]\"\ngoogle: \"ubuntu forums? stackoverflow?\"\nme: *sighs*\nme: \"arch [task for automation]\"\ngoogle: \"here's archwiki with exactly what you want in less than 10 lines\"",
        "html" : "<div style=\"white-space: pre\">me: \"linux [task for automation]\"\ngoogle: \"ubuntu forums? stackoverflow?\"\nme: *sighs*\nme: \"arch [task for automation]\"\ngoogle: \"here's archwiki with exactly what you want in less than 10 lines\"</div>"
      } ]
    }
  },
  "tags" : [ "linux", "arch-linux" ],
  "client_id" : "https://indigenous.realize.be"
}
