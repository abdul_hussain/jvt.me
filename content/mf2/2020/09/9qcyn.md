{
  "date" : "2020-09-03T17:30:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/jackyalcine/status/1301424884308828160" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1301559901861408776" ],
    "name" : [ "Reply to https://twitter.com/jackyalcine/status/1301424884308828160" ],
    "published" : [ "2020-09-03T17:30:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "<span class=\"h-card\"><a class=\"u-url\" href=\"https://twitter.com/MrAndrew\">@MrAndrew</a></span> has an awesome tech talk on this!"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/09/9qcyn",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1301424884308828160" ],
      "url" : [ "https://twitter.com/jackyalcine/status/1301424884308828160" ],
      "published" : [ "2020-09-03T07:40:46+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:jackyalcine" ],
          "numeric-id" : [ "1262855987993563136" ],
          "name" : [ "arealnig.ga about that ☭" ],
          "nickname" : [ "jackyalcine" ],
          "url" : [ "https://twitter.com/jackyalcine", "https://v2.jacky.wtf", "http://arealnig.ga" ],
          "published" : [ "2020-05-19T21:22:07+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Chochenyo/Ohlone (Bay Area)" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1298686834092826625/c1vYRpfV.jpg" ]
        }
      } ],
      "content" : [ "Jurassic Park is a story about product management. More people need to study." ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
