{
  "date" : "2020-09-14T07:19:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/holly/status/1305272971246923776" ],
    "name" : [ "Like of @holly's tweet" ],
    "published" : [ "2020-09-14T07:19:00+01:00" ],
    "like-of" : [ "https://twitter.com/holly/status/1305272971246923776" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/njsqp",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1305272971246923776" ],
      "url" : [ "https://twitter.com/holly/status/1305272971246923776" ],
      "published" : [ "2020-09-13T22:31:41+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:holly" ],
          "numeric-id" : [ "7555262" ],
          "name" : [ "Holly Brockwell" ],
          "nickname" : [ "holly" ],
          "url" : [ "https://twitter.com/holly", "https://www.instagram.com/hollybrocks/" ],
          "published" : [ "2007-07-18T10:27:16+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "From Nottingham, in London" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1248671410714804225/A_9KJ1y8.jpg" ]
        }
      } ],
      "content" : [ "Remember, your skeleton is always smiling" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
