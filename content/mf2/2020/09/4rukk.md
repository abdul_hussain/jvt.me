{
  "date" : "2020-09-21T19:40:00+0100",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/jackyef__/status/1308082337658204161" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1308115671209906176" ],
    "name" : [ "Reply to https://twitter.com/jackyef__/status/1308082337658204161" ],
    "published" : [ "2020-09-21T19:40:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "I use <span class=\"h-card\"><a class=\"u-url\" href=\"https://twitter.com/matomo_org\">@matomo_org</a></span> for my own website, self hosted so data is my own, very much recommend!"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/09/4rukk",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1308082337658204161" ],
      "url" : [ "https://twitter.com/jackyef__/status/1308082337658204161" ],
      "published" : [ "2020-09-21T16:35:06+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:jackyef__" ],
          "numeric-id" : [ "1711618165" ],
          "name" : [ "Jacky Efendi" ],
          "nickname" : [ "jackyef__" ],
          "url" : [ "https://twitter.com/jackyef__" ],
          "published" : [ "2013-08-30T02:39:06+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1213806814808043520/F4u7njVE.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Curious, what do people use to measure engagements on their personal site/blog nowadays?\n\nGoogle Analytics/Webmention/other stuffs? Or do you not track at all?",
        "html" : "<div style=\"white-space: pre\">Curious, what do people use to measure engagements on their personal site/blog nowadays?\n\nGoogle Analytics/Webmention/other stuffs? Or do you not track at all?</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
