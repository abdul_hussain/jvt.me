{
  "date" : "2020-09-07T08:14:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/chrismackintosh/status/1302813610985439232" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1302868826397302786" ],
    "name" : [ "Reply to https://twitter.com/chrismackintosh/status/1302813610985439232" ],
    "published" : [ "2020-09-07T08:14:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "☺ Thanks Chris!"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/09/u9gl5",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1302813610985439232" ],
      "url" : [ "https://twitter.com/chrismackintosh/status/1302813610985439232" ],
      "published" : [ "2020-09-07T03:39:04+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:chrismackintosh" ],
          "numeric-id" : [ "15710396" ],
          "name" : [ "Chris Mackintosh" ],
          "nickname" : [ "chrismackintosh" ],
          "url" : [ "https://twitter.com/chrismackintosh", "https://chrismackintosh.work/" ],
          "published" : [ "2008-08-03T13:39:48+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Denver, CO" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1295861556089982976/Tsxf23I2.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "❤️ this site. \n\nJamie Tanna | Software Engineer jvt.me",
        "html" : "<div style=\"white-space: pre\">❤️ this site. \n\nJamie Tanna | Software Engineer <a href=\"https://www.jvt.me/\">jvt.me</a></div>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
