{
  "date" : "2020-09-01T14:44:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/djbaskin/status/1300591320050749440" ],
    "name" : [ "Like of @djbaskin's tweet" ],
    "published" : [ "2020-09-01T14:44:00+01:00" ],
    "like-of" : [ "https://twitter.com/djbaskin/status/1300591320050749440" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/hjt34",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1300591320050749440" ],
      "url" : [ "https://twitter.com/djbaskin/status/1300591320050749440" ],
      "video" : [ "https://video.twimg.com/ext_tw_video/1300590486537658369/pu/vid/720x1280/ZSOtbn1jkxW9yxrI.mp4?tag=10" ],
      "published" : [ "2020-09-01T00:28:29+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:djbaskin" ],
          "numeric-id" : [ "55662470" ],
          "name" : [ "Danielle Baskin" ],
          "nickname" : [ "djbaskin" ],
          "url" : [ "https://twitter.com/djbaskin", "https://www.daniellebaskin.com", "http://dialup.com" ],
          "published" : [ "2009-07-10T21:00:48+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "San Francisco, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1292039027705577472/BiIMhH84.jpg" ]
        }
      } ],
      "content" : [ "Made a special hoodie to wear in public when I don’t feel like participating in a conversation." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
