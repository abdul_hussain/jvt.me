{
  "date" : "2020-09-22T19:52:00+0100",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/sil/status/1308457049462898689" ],
    "name" : [ "Like of @sil's tweet" ],
    "published" : [ "2020-09-22T19:52:00+0100" ],
    "category" : [ "food" ],
    "like-of" : [ "https://twitter.com/sil/status/1308457049462898689" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/w30wk",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1308457049462898689" ],
      "url" : [ "https://twitter.com/sil/status/1308457049462898689" ],
      "published" : [ "2020-09-22T17:24:05+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:sil" ],
          "numeric-id" : [ "1389781" ],
          "name" : [ "Stuart Langridge" ],
          "nickname" : [ "sil" ],
          "url" : [ "https://twitter.com/sil", "https://www.kryogenix.org/" ],
          "published" : [ "2007-03-18T01:52:55+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Birmingham, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/55108762/hackergotchi-simpler.png" ]
        }
      } ],
      "content" : [ "Who doesn't need to make dinner again for two or maybe even three days? This chap. Tray of enchiladas ftw." ],
      "photo" : [ "https://pbs.twimg.com/media/EiiSvPbXsAAcDUF.jpg" ]
    }
  },
  "tags" : [ "food" ],
  "client_id" : "https://indigenous.realize.be"
}
