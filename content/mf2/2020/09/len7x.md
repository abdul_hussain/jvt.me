{
  "date" : "2020-09-22T22:09:00+0100",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JimMFelton/status/1308484068217106433" ],
    "name" : [ "Like of @JimMFelton's tweet" ],
    "published" : [ "2020-09-22T22:09:00+0100" ],
    "category" : [ "coronavirus", "politics" ],
    "like-of" : [ "https://twitter.com/JimMFelton/status/1308484068217106433" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/len7x",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1308484068217106433" ],
      "url" : [ "https://twitter.com/JimMFelton/status/1308484068217106433" ],
      "published" : [ "2020-09-22T19:11:26+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:JimMFelton" ],
          "numeric-id" : [ "2904913023" ],
          "name" : [ "James Felton" ],
          "nickname" : [ "JimMFelton" ],
          "url" : [ "https://twitter.com/JimMFelton", "https://amzn.to/2HHtW08", "http://bit.ly/2mWy7yv", "http://amzn.to/2lev9V0" ],
          "published" : [ "2014-12-04T11:06:45+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/863860130009546754/-2Zr0kqI.jpg" ]
        }
      } ],
      "content" : [ "Boris Johnson blaming rule-breakers. Weird how everyone stopped obeying the rules at the exact same time schools reopened and everyone was paid to eat at Nando’s and told to get back to work or risk getting fired from their jobs they'd been successfully performing from home." ]
    }
  },
  "tags" : [ "coronavirus", "politics" ],
  "client_id" : "https://indigenous.realize.be"
}
