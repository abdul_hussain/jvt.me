{
  "date" : "2020-09-01T08:04:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/iamhirusi/status/1300685388319608835" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1300691926228832258" ],
    "name" : [ "Reply to https://twitter.com/iamhirusi/status/1300685388319608835" ],
    "published" : [ "2020-09-01T08:04:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "I did this with my site with https://www.jvt.me/posts/2018/04/15/caddy-gitlab-review-apps/ and https://www.jvt.me/posts/2017/07/18/gitlab-review-apps-capistrano/ and that was super self hosted 🙃"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/09/vrvju",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1300685388319608835" ],
      "url" : [ "https://twitter.com/iamhirusi/status/1300685388319608835" ],
      "published" : [ "2020-09-01T06:42:16+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:iamhirusi" ],
          "numeric-id" : [ "1252888172700119041" ],
          "name" : [ "Ru Singh" ],
          "nickname" : [ "iamhirusi" ],
          "url" : [ "https://twitter.com/iamhirusi", "https://rusingh.com" ],
          "published" : [ "2020-04-22T09:14:03+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "India" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1287100701965840385/7LCy7NCC.jpg" ]
        }
      } ],
      "content" : [ "Is there a tool that would build a static site *per desired branch* with a unique URL for that branch -- that can also be self hosted?" ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
