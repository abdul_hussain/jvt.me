{
  "date" : "2020-09-22T15:28:00+0100",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/Loftio/status/1308363325361860608" ],
    "name" : [ "Like of @Loftio's tweet" ],
    "published" : [ "2020-09-22T15:28:00+0100" ],
    "category" : [ "battlestations" ],
    "like-of" : [ "https://twitter.com/Loftio/status/1308363325361860608" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/2evjb",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1308363325361860608" ],
      "url" : [ "https://twitter.com/Loftio/status/1308363325361860608" ],
      "published" : [ "2020-09-22T11:11:39+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Loftio" ],
          "numeric-id" : [ "19788371" ],
          "name" : [ "Lex Lofthouse ✨" ],
          "nickname" : [ "Loftio" ],
          "url" : [ "https://twitter.com/Loftio", "http://loftio.co.uk" ],
          "published" : [ "2009-01-30T21:03:02+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1138757469176508417/Hc1FJP7a.png" ]
        }
      } ],
      "content" : [ "So I got some of those strands of artificial ivy that I see on everybody's TikToks at the moment, and I just have to say that it has injected some life into my little desk area ✨" ],
      "photo" : [ "https://pbs.twimg.com/media/Eig9godXkAUpqlz.jpg" ]
    }
  },
  "tags" : [ "battlestations" ],
  "client_id" : "https://indigenous.realize.be"
}
