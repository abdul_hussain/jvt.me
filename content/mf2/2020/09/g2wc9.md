{
  "date" : "2020-09-14T07:21:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamieTanna/status/1305392904324755462" ],
    "published" : [ "2020-09-14T07:21:00+01:00" ],
    "repost-of" : [ "https://twitter.com/OfficialKat/status/1305198707143372806" ]
  },
  "kind" : "reposts",
  "slug" : "2020/09/g2wc9",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1305198707143372806" ],
      "url" : [ "https://twitter.com/OfficialKat/status/1305198707143372806" ],
      "published" : [ "2020-09-13T17:36:35+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:OfficialKat" ],
          "numeric-id" : [ "23544268" ],
          "name" : [ "Kat Dennings" ],
          "nickname" : [ "OfficialKat" ],
          "url" : [ "https://twitter.com/OfficialKat" ],
          "published" : [ "2009-03-10T02:26:45+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1305332692808949760/-7ws1V4G.jpg" ]
        }
      } ],
      "content" : [ "The public respect for Chris Evans’ privacy/feelings is wonderful. Wouldn’t it be nice if it extended to women when this kind of thing happens?" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
