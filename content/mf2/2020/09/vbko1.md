{
  "date" : "2020-09-16T12:37:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/CSMFHT/status/1306180001658626050" ],
    "name" : [ "Like of @CSMFHT's tweet" ],
    "published" : [ "2020-09-16T12:37:00+01:00" ],
    "like-of" : [ "https://twitter.com/CSMFHT/status/1306180001658626050" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/vbko1",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1306180001658626050" ],
      "url" : [ "https://twitter.com/CSMFHT/status/1306180001658626050" ],
      "published" : [ "2020-09-16T10:35:54+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:CSMFHT" ],
          "numeric-id" : [ "1230431996163178497" ],
          "name" : [ "Classical Studies Memes for Hellenistic Teens" ],
          "nickname" : [ "CSMFHT" ],
          "url" : [ "https://twitter.com/CSMFHT", "http://facebook.com/CSMFHT" ],
          "published" : [ "2020-02-20T10:00:46+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "New Zealand" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1230432197326147584/NOEqWdEO.jpg" ]
        }
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EiB7yfmVgAEvBHd.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
