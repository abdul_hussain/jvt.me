{
  "date" : "2020-09-21T07:35:00+0100",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/rizbizkits/status/1307816444084867073" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1307932564640927744" ],
    "name" : [ "Reply to https://twitter.com/rizbizkits/status/1307816444084867073" ],
    "published" : [ "2020-09-21T07:35:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Riz I am so sorry to hear that. Sending love to you all, let us know if there's literally anything we can do to help 🤗"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/09/jab4a",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1307816444084867073" ],
      "url" : [ "https://twitter.com/rizbizkits/status/1307816444084867073" ],
      "published" : [ "2020-09-20T22:58:32+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:rizbizkits" ],
          "numeric-id" : [ "800085538954878976" ],
          "name" : [ "rizwana akmal khan" ],
          "nickname" : [ "rizbizkits" ],
          "url" : [ "https://twitter.com/rizbizkits", "https://www.rizwanakhan.com" ],
          "published" : [ "2016-11-19T21:17:12+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1279903682012774402/G8yaFDCD.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "إِنَّا لِلّهِ وَإِنَّـا إِلَيْهِ رَاجِعونَ\n\nMy beloved father, Mohammad Ahmad Akmal Khan, died on 18/09/20 (Friday). He had cancer, since Jan 2020. I've much to say about how much he meant to me; I pray, with time, I am able to. May Allah grant him a high rank in paradise. Ameen.",
        "html" : "<div style=\"white-space: pre\">إِنَّا لِلّهِ وَإِنَّـا إِلَيْهِ رَاجِعونَ\n\nMy beloved father, Mohammad Ahmad Akmal Khan, died on 18/09/20 (Friday). He had cancer, since Jan 2020. I've much to say about how much he meant to me; I pray, with time, I am able to. May Allah grant him a high rank in paradise. Ameen.</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
