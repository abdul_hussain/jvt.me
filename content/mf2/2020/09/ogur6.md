{
  "date" : "2020-09-17T08:24:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/wongmjane/status/1306393912802639873" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1306495693796843520" ],
    "name" : [ "Reply to https://twitter.com/wongmjane/status/1306393912802639873" ],
    "published" : [ "2020-09-17T08:24:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "If you go the personal website route (which I'd recommend) there's https://brid.gy which let's you tweet from your website so you can still reach your following, as well as ie via RSS"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/09/ogur6",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1306393912802639873" ],
      "url" : [ "https://twitter.com/wongmjane/status/1306393912802639873" ],
      "published" : [ "2020-09-17T00:45:55+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:wongmjane" ],
          "numeric-id" : [ "337119125" ],
          "name" : [ "Jane Manchun Wong" ],
          "nickname" : [ "wongmjane" ],
          "url" : [ "https://twitter.com/wongmjane", "https://wongmjane.com" ],
          "published" : [ "2011-07-17T13:48:53+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1274822387532328960/feySZs0k.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "My Twitter life flashed before my eyes when I thought I’ll be “shadowbanned” forever 😭\n\nAll those tweets, all that audience curated can just go poof for whatever reason\n\nI thought about switching to newsletter to “own my platform”, isn’t that still another form of platform?",
        "html" : "<div style=\"white-space: pre\">My Twitter life flashed before my eyes when I thought I’ll be “shadowbanned” forever 😭\n\nAll those tweets, all that audience curated can just go poof for whatever reason\n\nI thought about switching to newsletter to “own my platform”, isn’t that still another form of platform?</div>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
