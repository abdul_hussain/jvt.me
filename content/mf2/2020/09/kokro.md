{
  "date" : "2020-09-05T08:56:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/272998999/" ],
    "syndication" : [ "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/272998999/#rsvp-by-https%3A%2F%2Fwww.jvt.me%2Fmf2%2F2020%2F09%2Fkokro%2F" ],
    "name" : [ "RSVP yes to https://www.meetup.com/PHPMiNDS-in-Nottingham/events/272998999/" ],
    "published" : [ "2020-09-05T08:56:00+01:00" ],
    "event" : {
      "start" : [ "2020-09-10T18:30:00+01:00" ],
      "name" : [ "How to handle Code Complexity" ],
      "end" : [ "2020-09-10T20:30:00+01:00" ],
      "url" : [ "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/272998999/" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/09/kokro",
  "client_id" : "https://indigenous.realize.be"
}
