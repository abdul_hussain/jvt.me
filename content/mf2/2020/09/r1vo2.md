{
  "date" : "2020-09-26T17:45:00+0100",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/aroradrn/status/1309767296186028032" ],
    "name" : [ "Like of @aroradrn's tweet" ],
    "published" : [ "2020-09-26T17:45:00+0100" ],
    "like-of" : [ "https://twitter.com/aroradrn/status/1309767296186028032" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/r1vo2",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1309767296186028032" ],
      "url" : [ "https://twitter.com/aroradrn/status/1309767296186028032" ],
      "published" : [ "2020-09-26T08:10:32+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:aroradrn" ],
          "numeric-id" : [ "572624518" ],
          "name" : [ "Nitin Arora #WearAMask" ],
          "nickname" : [ "aroradrn" ],
          "url" : [ "https://twitter.com/aroradrn", "http://thosewecarry.com", "http://amzn.to/2QwEqX3" ],
          "published" : [ "2012-05-06T13:19:14+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Birmingham, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1288231951661445125/PM7PvGnJ.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Today I’m going to talk about something uncomfortable \n\nDo you have friends or colleagues who don’t have children? \n\nHere’s some things not to say\n\nThread... \n\n1/n",
        "html" : "<div style=\"white-space: pre\">Today I’m going to talk about something uncomfortable \n\nDo you have friends or colleagues who don’t have children? \n\nHere’s some things not to say\n\nThread... \n\n1/n</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
