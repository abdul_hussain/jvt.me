{
  "date" : "2020-09-26T08:08:00+0100",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/EmilyKager/status/1309643868783083520" ],
    "name" : [ "Like of @EmilyKager's tweet" ],
    "published" : [ "2020-09-26T08:08:00+0100" ],
    "category" : [ "code-review" ],
    "like-of" : [ "https://twitter.com/EmilyKager/status/1309643868783083520" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/h8kj8",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1309643868783083520" ],
      "url" : [ "https://twitter.com/EmilyKager/status/1309643868783083520" ],
      "published" : [ "2020-09-26T00:00:04+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:EmilyKager" ],
          "numeric-id" : [ "2347475803" ],
          "name" : [ "Emily" ],
          "nickname" : [ "EmilyKager" ],
          "url" : [ "https://twitter.com/EmilyKager" ],
          "published" : [ "2014-02-16T21:34:12+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Oakland and SF " ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1296572390252716034/jTxFoueZ.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Gonna miss this kid. @SawyerBlatz 's last review 😭",
        "html" : "Gonna miss this kid. <a href=\"https://twitter.com/SawyerBlatz\">@SawyerBlatz</a> 's last review 😭"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EizKAQeUwAAFyfC.png" ]
    }
  },
  "tags" : [ "code-review" ],
  "client_id" : "https://indigenous.realize.be"
}
