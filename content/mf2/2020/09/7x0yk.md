{
  "date" : "2020-09-22T22:12:00+0100",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamieTanna/status/1308516366333468674" ],
    "published" : [ "2020-09-22T22:12:00+0100" ],
    "category" : [ "satire", "diversity-and-inclusion", "social-media", "ethics" ],
    "repost-of" : [ "https://twitter.com/RachelWenitsky/status/1308479842573332480" ],
    "post-status" : [ "published" ]
  },
  "kind" : "reposts",
  "slug" : "2020/09/7x0yk",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1308479842573332480" ],
      "url" : [ "https://twitter.com/RachelWenitsky/status/1308479842573332480" ],
      "video" : [ "https://video.twimg.com/ext_tw_video/1308479297385041922/pu/vid/468x720/Kk77xw_1ook9fUik.mp4?tag=10" ],
      "published" : [ "2020-09-22T18:54:39+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:RachelWenitsky" ],
          "numeric-id" : [ "465126656" ],
          "name" : [ "Rachel Wenitsky" ],
          "nickname" : [ "RachelWenitsky" ],
          "url" : [ "https://twitter.com/RachelWenitsky" ],
          "published" : [ "2012-01-16T00:34:17+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Brooklyn, NY" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1256594698392788993/V94SM3kz.jpg" ]
        }
      } ],
      "content" : [ "A former tech employee in a documentary about why social media is bad (social media is bad)" ]
    }
  },
  "tags" : [ "satire", "diversity-and-inclusion", "social-media", "ethics" ],
  "client_id" : "https://indigenous.realize.be"
}
