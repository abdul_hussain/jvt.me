{
  "date" : "2020-09-23T12:18:00+0100",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/MrsEmma/status/1308720542950842368" ],
    "name" : [ "Like of @MrsEmma's tweet" ],
    "published" : [ "2020-09-23T12:18:00+0100" ],
    "like-of" : [ "https://twitter.com/MrsEmma/status/1308720542950842368" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/hsdsx",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1308720542950842368" ],
      "url" : [ "https://twitter.com/MrsEmma/status/1308720542950842368" ],
      "published" : [ "2020-09-23T10:51:06+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:MrsEmma" ],
          "numeric-id" : [ "19500955" ],
          "name" : [ "Emma Seward" ],
          "nickname" : [ "MrsEmma" ],
          "url" : [ "https://twitter.com/MrsEmma" ],
          "published" : [ "2009-01-25T19:32:42+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1306627007317504000/K6YXEx2-.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "@anna_hax I knew without reading the message inside that this was from you (and @JamieTanna) 🤣💁🏻‍♀️",
        "html" : "<a href=\"https://twitter.com/anna_hax\">@anna_hax</a> I knew without reading the message inside that this was from you (and <a href=\"https://twitter.com/JamieTanna\">@JamieTanna</a>) 🤣💁🏻‍♀️"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EimCZZwX0AE1VcY.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
