{
  "date" : "2020-09-05T22:56:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/arcaderage/status/1302184450558775298" ],
    "name" : [ "Like of @arcaderage's tweet" ],
    "published" : [ "2020-09-05T22:56:00+01:00" ],
    "category" : [ "2020" ],
    "like-of" : [ "https://twitter.com/arcaderage/status/1302184450558775298" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/byqx2",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1302184450558775298" ],
      "url" : [ "https://twitter.com/arcaderage/status/1302184450558775298" ],
      "published" : [ "2020-09-05T09:59:01+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:arcaderage" ],
          "numeric-id" : [ "84324512" ],
          "name" : [ "Mart Virkus" ],
          "nickname" : [ "arcaderage" ],
          "url" : [ "https://twitter.com/arcaderage", "http://arcaderage.co/" ],
          "published" : [ "2009-10-22T13:29:15+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Tallinn, Estonia" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/701420138952847360/nOKzzC74.png" ]
        }
      } ],
      "content" : [ {
        "value" : "Everybody: 2020 is the worst year\n\nMe: *so far*",
        "html" : "<div style=\"white-space: pre\">Everybody: 2020 is the worst year\n\nMe: *so far*</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EhJJ2mQXgAACOML.jpg" ]
    }
  },
  "tags" : [ "2020" ],
  "client_id" : "https://indigenous.realize.be"
}
