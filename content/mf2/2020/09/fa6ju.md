{
  "date" : "2020-09-25T18:37:00+0100",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/doramilitaru/status/1144237523935354880" ],
    "name" : [ "Like of @doramilitaru's tweet" ],
    "published" : [ "2020-09-25T18:37:00+0100" ],
    "category" : [ "cute" ],
    "like-of" : [ "https://twitter.com/doramilitaru/status/1144237523935354880" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/fa6ju",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1144237523935354880" ],
      "url" : [ "https://twitter.com/doramilitaru/status/1144237523935354880" ],
      "published" : [ "2019-06-27T13:34:19+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:doramilitaru" ],
          "numeric-id" : [ "14115979" ],
          "name" : [ "Dora" ],
          "nickname" : [ "doramilitaru" ],
          "url" : [ "https://twitter.com/doramilitaru", "http://doramilitaru.com" ],
          "published" : [ "2008-03-10T18:03:04+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1120044563463319552/h2QdtAew.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "My parents rescued a kitten and I’m trying to convince them to a) keep him b) name him Ghiță (Romanian folks 👋🏼 do you like this name?) \n#cat",
        "html" : "<div style=\"white-space: pre\">My parents rescued a kitten and I’m trying to convince them to a) keep him b) name him Ghiță (Romanian folks 👋🏼 do you like this name?) \n<a href=\"https://twitter.com/search?q=%23cat\">#cat</a></div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/D-El9ZDXUAAQfSo.jpg" ]
    }
  },
  "tags" : [ "cute" ],
  "client_id" : "https://indigenous.realize.be"
}
