{
  "date" : "2020-09-18T23:29:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/short_louise/status/1307069187274272769" ],
    "name" : [ "Like of @short_louise's tweet" ],
    "published" : [ "2020-09-18T23:29:00+01:00" ],
    "like-of" : [ "https://twitter.com/short_louise/status/1307069187274272769" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/xnmoz",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1307069187274272769" ],
      "url" : [ "https://twitter.com/short_louise/status/1307069187274272769" ],
      "published" : [ "2020-09-18T21:29:13+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:short_louise" ],
          "numeric-id" : [ "276855748" ],
          "name" : [ "Louise Paling" ],
          "nickname" : [ "short_louise" ],
          "url" : [ "https://twitter.com/short_louise" ],
          "published" : [ "2011-04-04T06:40:11+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1204077832483360771/iEm9QMFA.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Just had such a great evening chatting, eating pizza, and playing board games with @marmaladegirl. It's just so nice to actually _see_ friends. Like in the same place as you see. I miss people.",
        "html" : "Just had such a great evening chatting, eating pizza, and playing board games with <a href=\"https://twitter.com/marmaladegirl\">@marmaladegirl</a>. It's just so nice to actually _see_ friends. Like in the same place as you see. I miss people."
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
