{
  "date" : "2020-09-26T17:06:00+0100",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/NoMansSky/status/1309584090908286983" ],
    "name" : [ "Like of @NoMansSky's tweet" ],
    "published" : [ "2020-09-26T17:06:00+0100" ],
    "category" : [ "gaming" ],
    "like-of" : [ "https://twitter.com/NoMansSky/status/1309584090908286983" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/auqp0",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1309584090908286983" ],
      "url" : [ "https://twitter.com/NoMansSky/status/1309584090908286983" ],
      "published" : [ "2020-09-25T20:02:32+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:NoMansSky" ],
          "numeric-id" : [ "2232827756" ],
          "name" : [ "Sean Murray" ],
          "nickname" : [ "NoMansSky" ],
          "url" : [ "https://twitter.com/NoMansSky", "http://www.nomanssky.com", "https://www.instagram.com/nomansskygame/" ],
          "published" : [ "2013-12-06T11:22:44+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Guildford, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/839212372816441344/oIPqe6ss.jpg" ]
        }
      } ],
      "content" : [ "😭" ],
      "photo" : [ "https://pbs.twimg.com/media/EiyTybYWkAUnmy7.jpg" ]
    }
  },
  "tags" : [ "gaming" ],
  "client_id" : "https://indigenous.realize.be"
}
