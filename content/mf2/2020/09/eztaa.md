{
  "date" : "2020-09-27T09:53:00+0100",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/KR1573N/status/1309929711418642434" ],
    "name" : [ "Like of @KR1573N's tweet" ],
    "published" : [ "2020-09-27T09:53:00+0100" ],
    "category" : [ "2020" ],
    "like-of" : [ "https://twitter.com/KR1573N/status/1309929711418642434" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/eztaa",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1309929711418642434" ],
      "url" : [ "https://twitter.com/KR1573N/status/1309929711418642434" ],
      "published" : [ "2020-09-26T18:55:55+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:KR1573N" ],
          "numeric-id" : [ "15012998" ],
          "name" : [ "Kristen Seversky" ],
          "nickname" : [ "KR1573N" ],
          "url" : [ "https://twitter.com/KR1573N", "http://k-studies.com" ],
          "published" : [ "2008-06-05T01:54:15+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Rochester, NY" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1215459478717390848/jAQr4aF9.jpg" ]
        }
      } ],
      "content" : [ "🤷🏻‍♀️ Sometimes all that's left to say can be said on a snarky T-shirt 🤷🏻‍♀️" ],
      "photo" : [ "https://pbs.twimg.com/media/Ei3OIWUXcAEFsGo.jpg" ]
    }
  },
  "tags" : [ "2020" ],
  "client_id" : "https://indigenous.realize.be"
}
