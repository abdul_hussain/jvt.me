{
  "date" : "2020-09-07T04:17:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/bagwaa/status/1302731820371447819" ],
    "name" : [ "Like of @bagwaa's tweet" ],
    "published" : [ "2020-09-07T04:17:00+01:00" ],
    "like-of" : [ "https://twitter.com/bagwaa/status/1302731820371447819" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/aomnr",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1302731820371447819" ],
      "url" : [ "https://twitter.com/bagwaa/status/1302731820371447819" ],
      "published" : [ "2020-09-06T22:14:04+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:bagwaa" ],
          "numeric-id" : [ "788508" ],
          "name" : [ "Richard Bagshaw" ],
          "nickname" : [ "bagwaa" ],
          "url" : [ "https://twitter.com/bagwaa", "https://www.richardbagshaw.co.uk" ],
          "published" : [ "2007-02-22T12:20:09+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1293820616647364609/WUPoTz43.jpg" ]
        }
      } ],
      "content" : [ "Not gonna lie, I enjoy the \"sick emoji\" in my vim config for when my linting is detecting drama ... 🤮" ],
      "photo" : [ "https://pbs.twimg.com/media/EhQ7beAWoAII9xy.png" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
