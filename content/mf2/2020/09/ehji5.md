{
  "date" : "2020-09-13T15:00:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/mjackson/status/1304837624809418752" ],
    "name" : [ "Like of @mjackson's tweet" ],
    "published" : [ "2020-09-13T15:00:00+01:00" ],
    "category" : [ "homebrew" ],
    "like-of" : [ "https://twitter.com/mjackson/status/1304837624809418752" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/ehji5",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1304837624809418752" ],
      "url" : [ "https://twitter.com/mjackson/status/1304837624809418752" ],
      "published" : [ "2020-09-12T17:41:47+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:mjackson" ],
          "numeric-id" : [ "734903" ],
          "name" : [ "Michael Jackson" ],
          "nickname" : [ "mjackson" ],
          "url" : [ "https://twitter.com/mjackson", "https://reacttraining.com" ],
          "published" : [ "2007-01-30T23:35:28+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Carlsbad, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1095819845382299649/zG-2_UHS.jpg" ]
        }
      } ],
      "content" : [ "I've been using Homebrew for macos for what feels like forever now and I still have no clue whatsoever what a \"keg\", \"cask\", or \"cellar\" are." ]
    }
  },
  "tags" : [ "homebrew" ],
  "client_id" : "https://indigenous.realize.be"
}
