{
  "date" : "2020-09-24T23:34:00+0100",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/NadiaWhittomeMP/status/1309066157987639298" ],
    "name" : [ "Like of @NadiaWhittomeMP's tweet" ],
    "published" : [ "2020-09-24T23:34:00+0100" ],
    "category" : [ "politics" ],
    "like-of" : [ "https://twitter.com/NadiaWhittomeMP/status/1309066157987639298" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/1r6ip",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1309066157987639298" ],
      "url" : [ "https://twitter.com/NadiaWhittomeMP/status/1309066157987639298" ],
      "published" : [ "2020-09-24T09:44:27+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:NadiaWhittomeMP" ],
          "numeric-id" : [ "576682647" ],
          "name" : [ "Nadia Whittome MP" ],
          "nickname" : [ "NadiaWhittomeMP" ],
          "url" : [ "https://twitter.com/NadiaWhittomeMP", "http://instagram.com/nadiawhittomemp" ],
          "published" : [ "2012-05-10T21:49:17+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1229722962594553857/js8m1Cn1.jpg" ]
        }
      } ],
      "content" : [ "Thank you all for your messages of support. My statement below:" ],
      "photo" : [ "https://pbs.twimg.com/media/Eiq8OTuX0AEll7Y.jpg", "https://pbs.twimg.com/media/Eiq8SKOX0AQfeqn.jpg", "https://pbs.twimg.com/media/Eiq8VNuWsAgSdHE.jpg" ]
    }
  },
  "tags" : [ "politics" ],
  "client_id" : "https://indigenous.realize.be"
}
