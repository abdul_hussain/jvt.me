{
  "date" : "2020-09-12T21:32:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/MrAndrew/status/1304871779995979777" ],
    "name" : [ "Like of @MrAndrew's tweet" ],
    "published" : [ "2020-09-12T21:32:00+01:00" ],
    "category" : [ "cute" ],
    "like-of" : [ "https://twitter.com/MrAndrew/status/1304871779995979777" ]
  },
  "kind" : "likes",
  "slug" : "2020/09/3pwas",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1304871779995979777" ],
      "url" : [ "https://twitter.com/MrAndrew/status/1304871779995979777" ],
      "published" : [ "2020-09-12T19:57:30+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:MrAndrew" ],
          "numeric-id" : [ "9626182" ],
          "name" : [ "Andrew Seward" ],
          "nickname" : [ "MrAndrew" ],
          "url" : [ "https://twitter.com/MrAndrew", "http://www.technottingham.com" ],
          "published" : [ "2007-10-23T15:57:18+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Riddings, Derbyshire" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1258160750641606657/CR5g9eOt.jpg" ]
        }
      } ],
      "content" : [ "Took a quick break from playing VR and Mollie reminded me that now is sitting down time" ],
      "photo" : [ "https://pbs.twimg.com/media/EhvV93OWkAAT_HL.jpg" ]
    }
  },
  "tags" : [ "cute" ],
  "client_id" : "https://indigenous.realize.be"
}
