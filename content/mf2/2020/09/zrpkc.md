{
  "kind": "reads",
  "slug": "2020/09/zrpkc",
  "date": "2020-09-03T23:33:00Z",
  "h": "h-entry",
  "properties": {
    "published": [
      "2020-09-03T23:33:00Z"
    ],
    "read-status": [
      "finished"
    ],
    "read-of": [
      {
        "type": [
          "h-cite"
        ],
        "properties": {
          "name": [
            "Abaddon's Gate"
          ],
          "author": [
            "James S. A. Corey"
          ],
          "isbn": [
            "1841499935"
          ]
        }
      }
    ]
  }
}


