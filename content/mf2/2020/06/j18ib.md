{
  "date" : "2020-06-20T10:59:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/Smorgasboredom/status/1274124416499187713" ],
    "name" : [ "Like of @Smorgasboredom's tweet" ],
    "published" : [ "2020-06-20T10:59:00+01:00" ],
    "like-of" : [ "https://twitter.com/Smorgasboredom/status/1274124416499187713" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/j18ib",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1274124416499187713" ],
      "url" : [ "https://twitter.com/Smorgasboredom/status/1274124416499187713" ],
      "published" : [ "2020-06-19T23:38:27+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Smorgasboredom" ],
          "numeric-id" : [ "244619289" ],
          "name" : [ "Evan DeSimone" ],
          "nickname" : [ "Smorgasboredom" ],
          "url" : [ "https://twitter.com/Smorgasboredom", "https://evandesimone.substack.com/subscribe" ],
          "published" : [ "2011-01-29T18:46:36+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Brooklyn, NY" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1269612639765872642/Og7bvVyB.jpg" ]
        }
      } ],
      "content" : [ "Every time we’re forced to talk about Joe Rogan I am reminded of my best and most immutable axiom: Nothing that only men like is cool." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
