{
  "date" : "2020-06-16T12:19:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/ParodyHancock/status/1272838281303130113" ],
    "name" : [ "Like of @ParodyHancock's tweet" ],
    "published" : [ "2020-06-16T12:19:00+01:00" ],
    "like-of" : [ "https://twitter.com/ParodyHancock/status/1272838281303130113" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/j37gw",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1272838281303130113" ],
      "url" : [ "https://twitter.com/ParodyHancock/status/1272838281303130113" ],
      "published" : [ "2020-06-16T10:27:49+00:00" ],
      "in-reply-to" : [ "https://twitter.com/howardthedolph1/status/1272837889680949250" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:ParodyHancock" ],
          "numeric-id" : [ "1245982102883053569" ],
          "name" : [ "Parody Matt Hancock" ],
          "nickname" : [ "ParodyHancock" ],
          "url" : [ "https://twitter.com/ParodyHancock" ],
          "published" : [ "2020-04-03T07:51:11+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Barnard Castle" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1245982797619027968/xo0cK5kD.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "The most difficult part is trying to keep ahead of the absurdity.",
        "html" : "The most difficult part is trying to keep ahead of the absurdity.\n<a class=\"u-mention\" href=\"https://twitter.com/EmmaKennedy\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/howardthedolph1\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
