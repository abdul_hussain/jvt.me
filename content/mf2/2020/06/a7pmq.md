{
  "date" : "2020-06-28T22:01:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/Brunty/status/1277327292046495745" ],
    "name" : [ "Like of @Brunty's tweet" ],
    "published" : [ "2020-06-28T22:01:00+01:00" ],
    "category" : [ "food" ],
    "like-of" : [ "https://twitter.com/Brunty/status/1277327292046495745" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/a7pmq",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1277327292046495745" ],
      "url" : [ "https://twitter.com/Brunty/status/1277327292046495745" ],
      "published" : [ "2020-06-28T19:45:32+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Brunty" ],
          "numeric-id" : [ "1099517012" ],
          "name" : [ "Matt Brunt" ],
          "nickname" : [ "Brunty" ],
          "url" : [ "https://twitter.com/Brunty", "https://brunty.me/now" ],
          "published" : [ "2013-01-17T23:19:21+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Leicester, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1151043501288824832/TjB9WT83.jpg" ]
        }
      } ],
      "content" : [ "Uni-cone ice cream! Nom nom." ],
      "photo" : [ "https://pbs.twimg.com/media/Ebn6ZJvWoAE_Kyo.jpg" ]
    }
  },
  "tags" : [ "food" ],
  "client_id" : "https://indigenous.realize.be"
}
