{
  "date" : "2020-06-24T23:03:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamesSACorey/status/1275905666259902464" ],
    "name" : [ "Like of @JamesSACorey's tweet" ],
    "published" : [ "2020-06-24T23:03:00+01:00" ],
    "like-of" : [ "https://twitter.com/JamesSACorey/status/1275905666259902464" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/h3fgm",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1275905666259902464" ],
      "url" : [ "https://twitter.com/JamesSACorey/status/1275905666259902464" ],
      "published" : [ "2020-06-24T21:36:30+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:JamesSACorey" ],
          "numeric-id" : [ "550270216" ],
          "name" : [ "James S.A. Corey" ],
          "nickname" : [ "JamesSACorey" ],
          "url" : [ "https://twitter.com/JamesSACorey", "http://www.the-expanse.com" ],
          "published" : [ "2012-04-10T16:59:23+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Rocinante" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1029097103333543936/H4TP9cxr.jpg" ]
        }
      } ],
      "content" : [ "Yes.  I am aware of the thing.  I have sent along all of the information I have about the thing to the people who have the power to make decisions.  I can not comment on the thing until they've had time to do their due diligence and release an official response.  Thank you." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
