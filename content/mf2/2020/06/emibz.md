{
  "date" : "2020-06-27T15:41:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [  ],
    "name" : [ "Like of @Brunty's tweet" ],
    "published" : [ "2020-06-27T15:41:00+01:00" ],
    "like-of" : [ "https://twitter.com/Brunty/status/1276876612705955841" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/emibz",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1276876612705955841" ],
      "url" : [ "https://twitter.com/Brunty/status/1276876612705955841" ],
      "published" : [ "2020-06-27T13:54:42+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Brunty" ],
          "numeric-id" : [ "1099517012" ],
          "name" : [ "Matt Brunt" ],
          "nickname" : [ "Brunty" ],
          "url" : [ "https://twitter.com/Brunty", "https://brunty.me/now" ],
          "published" : [ "2013-01-17T23:19:21+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Leicester, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1151043501288824832/TjB9WT83.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "A while back I tweeted about things advertising as \"military grade\" when that's stupid.\n\nI've just found the best one...\n\nMILITARY GRADE CHICKEN COOP DOOR.",
        "html" : "<div style=\"white-space: pre\">A while back I tweeted about things advertising as \"military grade\" when that's stupid.\n\nI've just found the best one...\n\nMILITARY GRADE CHICKEN COOP DOOR.</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EbhgeZ2WoAA8sQe.png" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
