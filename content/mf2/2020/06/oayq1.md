{
  "date" : "2020-06-28T16:04:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/_elletownsend/status/1277237500071870464" ],
    "name" : [ "Like of @_elletownsend's tweet" ],
    "published" : [ "2020-06-28T16:04:00+01:00" ],
    "category" : [ "blogging" ],
    "like-of" : [ "https://twitter.com/_elletownsend/status/1277237500071870464" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/oayq1",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1277237500071870464" ],
      "url" : [ "https://twitter.com/_elletownsend/status/1277237500071870464" ],
      "published" : [ "2020-06-28T13:48:44+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:_elletownsend" ],
          "numeric-id" : [ "4308006796" ],
          "name" : [ "Elle Townsend🌱" ],
          "nickname" : [ "_elletownsend" ],
          "url" : [ "https://twitter.com/_elletownsend", "http://www.elletownsend.co.uk" ],
          "published" : [ "2015-11-28T14:28:32+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "England, United Kingdom" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1237757944089006086/67Kjy-LA.jpg" ]
        }
      } ],
      "content" : [ "I'm kind of floored... today I had TWO medium publications ask if I would like to submit my work to their publications 🤯 I know its not a huge deal but It definitely helps with the imposter syndrome" ]
    }
  },
  "tags" : [ "blogging" ],
  "client_id" : "https://indigenous.realize.be"
}
