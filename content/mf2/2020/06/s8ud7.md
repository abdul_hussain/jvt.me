{
  "date" : "2020-06-27T10:18:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [  ],
    "name" : [ "Like of @moriahmaney's tweet" ],
    "published" : [ "2020-06-27T10:18:00+01:00" ],
    "category" : [ "battlestations" ],
    "like-of" : [ "https://twitter.com/moriahmaney/status/1276717724555718656" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/s8ud7",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1276717724555718656" ],
      "url" : [ "https://twitter.com/moriahmaney/status/1276717724555718656" ],
      "published" : [ "2020-06-27T03:23:20+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:moriahmaney" ],
          "numeric-id" : [ "928949262" ],
          "name" : [ "Moriah Maney" ],
          "nickname" : [ "moriahmaney" ],
          "url" : [ "https://twitter.com/moriahmaney", "http://moriah.dev" ],
          "published" : [ "2012-11-06T03:59:00+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1181365917546143744/YBbgKfy8.jpg" ]
        }
      } ],
      "content" : [ "five years and a ✨glow up✨" ],
      "photo" : [ "https://pbs.twimg.com/media/EbfP_vLXQAA9SEN.jpg", "https://pbs.twimg.com/media/EbfQAECXYAQ342S.jpg" ]
    }
  },
  "tags" : [ "battlestations" ],
  "client_id" : "https://indigenous.realize.be"
}
