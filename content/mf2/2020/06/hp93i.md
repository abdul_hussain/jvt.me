{
  "date" : "2020-06-20T11:22:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/mijustin/status/1274120480329396225" ],
    "name" : [ "Like of @mijustin's tweet" ],
    "published" : [ "2020-06-20T11:22:00+01:00" ],
    "like-of" : [ "https://twitter.com/mijustin/status/1274120480329396225" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/hp93i",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1274120480329396225" ],
      "url" : [ "https://twitter.com/mijustin/status/1274120480329396225" ],
      "published" : [ "2020-06-19T23:22:49+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:mijustin" ],
          "numeric-id" : [ "17046072" ],
          "name" : [ "Justin Jackson" ],
          "nickname" : [ "mijustin" ],
          "url" : [ "https://twitter.com/mijustin", "https://justinjackson.ca" ],
          "published" : [ "2008-10-29T17:28:50+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Vernon, BC" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/932726689485828097/n86GsuLG.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "All you bootstrappers saying \"fuck Basecamp, they should have known about Apple's draconian policies before they built Hey:\"\n\nRemind me to not fight for you the next time Google, Amazon, Apple, Shopify clamps down, and hurts your business.\n\nThe cynicism and spite here is 🤯🙄",
        "html" : "<div style=\"white-space: pre\">All you bootstrappers saying \"fuck Basecamp, they should have known about Apple's draconian policies before they built Hey:\"\n\nRemind me to not fight for you the next time Google, Amazon, Apple, Shopify clamps down, and hurts your business.\n\nThe cynicism and spite here is 🤯🙄</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
