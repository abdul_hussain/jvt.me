{
  "date" : "2020-06-19T09:58:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamieTanna/status/1273904605051662337" ],
    "published" : [ "2020-06-19T09:58:00+01:00" ],
    "repost-of" : [ "https://twitter.com/WiT_Notts/status/1273886716366073856" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "I had such a great time at this last year (https://www.jvt.me/posts/2019/07/01/dddem-speaker-workshop/), and would very thoroughly recommend going!"
    } ]
  },
  "kind" : "reposts",
  "slug" : "2020/06/eescx",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1273886716366073856" ],
      "url" : [ "https://twitter.com/WiT_Notts/status/1273886716366073856" ],
      "video" : [ "https://video.twimg.com/tweet_video/Ea3BNkGWAAYCFV1.mp4" ],
      "published" : [ "2020-06-19T07:53:55+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:WiT_Notts" ],
          "numeric-id" : [ "4339158441" ],
          "name" : [ "Women In Tech, Nottingham 🌈✨" ],
          "nickname" : [ "WiT_Notts" ],
          "url" : [ "https://twitter.com/WiT_Notts", "https://nott.tech/wit" ],
          "published" : [ "2015-12-01T11:22:25+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/913380298829844481/YvPcjPlE.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "📢 Last chance! If you'd like to attend the awesome @dddeastmidlands Speaker Workshop on 11th July, apply TODAY👇\n\nnott.tech/wit-ddd-worksh…\n\nThank you to everyone who has applied, we'll be emailing you over the weekend to let you know if you've got a ticket 🤞🧡 #dddem #WiTNotts",
        "html" : "<div style=\"white-space: pre\">📢 Last chance! If you'd like to attend the awesome <a href=\"https://twitter.com/dddeastmidlands\">@dddeastmidlands</a> Speaker Workshop on 11th July, apply TODAY👇\n\n<a href=\"http://nott.tech/wit-ddd-workshop\">nott.tech/wit-ddd-worksh…</a>\n\nThank you to everyone who has applied, we'll be emailing you over the weekend to let you know if you've got a ticket 🤞🧡 <a href=\"https://twitter.com/search?q=%23dddem\">#dddem</a> <a href=\"https://twitter.com/search?q=%23WiTNotts\">#WiTNotts</a></div>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
