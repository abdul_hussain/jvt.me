{
  "date" : "2020-06-27T14:29:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/ChrisAldrich/status/1276760708730908672" ],
    "name" : [ "Like of @ChrisAldrich's tweet" ],
    "published" : [ "2020-06-27T14:29:00+01:00" ],
    "category" : [ "indiewebcamp" ],
    "like-of" : [ "https://twitter.com/ChrisAldrich/status/1276760708730908672" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/wqjbo",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1276760708730908672" ],
      "url" : [ "https://twitter.com/ChrisAldrich/status/1276760708730908672" ],
      "published" : [ "2020-06-27T06:14:08+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:ChrisAldrich" ],
          "numeric-id" : [ "13645402" ],
          "name" : [ "Chris Aldrich" ],
          "nickname" : [ "ChrisAldrich" ],
          "url" : [ "https://twitter.com/ChrisAldrich", "https://www.boffosocko.com", "https://wiki.chrisaldrich.net/Main_Page" ],
          "published" : [ "2008-02-18T22:31:54+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Los Angeles, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/890299009381916672/CafdvvxN.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I had a great time tonight with an awesome crowd of creatives at the online Zoom pre-party for IndieWebCamp West. If tonight's turn out is any indicator, we're going to have a lot of fun and learn a lot this weekend.\n#IndieWeb\nboffosocko.com/2020/06/26/557…",
        "html" : "<div style=\"white-space: pre\">I had a great time tonight with an awesome crowd of creatives at the online Zoom pre-party for IndieWebCamp West. If tonight's turn out is any indicator, we're going to have a lot of fun and learn a lot this weekend.\n<a href=\"https://twitter.com/search?q=%23IndieWeb\">#IndieWeb</a>\n<a href=\"https://boffosocko.com/2020/06/26/55772733/\">boffosocko.com/2020/06/26/557…</a></div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EbftmLGUEAAHSI9.jpg" ]
    }
  },
  "tags" : [ "indiewebcamp" ],
  "client_id" : "https://indigenous.realize.be"
}
