{
  "date" : "2020-06-26T15:12:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/Loftio/status/1276503370044448769" ],
    "name" : [ "Like of @Loftio's tweet" ],
    "published" : [ "2020-06-26T15:12:00+01:00" ],
    "like-of" : [ "https://twitter.com/Loftio/status/1276503370044448769" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/zdskt",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1276503370044448769" ],
      "url" : [ "https://twitter.com/Loftio/status/1276503370044448769" ],
      "published" : [ "2020-06-26T13:11:34+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Loftio" ],
          "numeric-id" : [ "19788371" ],
          "name" : [ "Lex Lofthouse ✨" ],
          "nickname" : [ "Loftio" ],
          "url" : [ "https://twitter.com/Loftio", "http://loftio.co.uk" ],
          "published" : [ "2009-01-30T21:03:02+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1138757469176508417/Hc1FJP7a.png" ]
        }
      } ],
      "content" : [ {
        "value" : "I appreciate the sentiment behind the NHS clap, but I absolutely despite the ritual of it when the staff aren't seeing pay rises, or when student nurses' contracts got cancelled early. It's just a farce. \n\nbbc.co.uk/news/uk-531774…",
        "html" : "<div style=\"white-space: pre\">I appreciate the sentiment behind the NHS clap, but I absolutely despite the ritual of it when the staff aren't seeing pay rises, or when student nurses' contracts got cancelled early. It's just a farce. \n\n<a href=\"https://www.bbc.co.uk/news/uk-53177483\">bbc.co.uk/news/uk-531774…</a></div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
