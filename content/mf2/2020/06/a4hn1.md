{
  "date" : "2020-06-26T14:16:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/HanaMichels/status/1276191856443834368" ],
    "name" : [ "Like of @HanaMichels's tweet" ],
    "published" : [ "2020-06-26T14:16:00+01:00" ],
    "like-of" : [ "https://twitter.com/HanaMichels/status/1276191856443834368" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/a4hn1",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1276191856443834368" ],
      "url" : [ "https://twitter.com/HanaMichels/status/1276191856443834368" ],
      "published" : [ "2020-06-25T16:33:43+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:HanaMichels" ],
          "numeric-id" : [ "542729893" ],
          "name" : [ "Hana Michels" ],
          "nickname" : [ "HanaMichels" ],
          "url" : [ "https://twitter.com/HanaMichels" ],
          "published" : [ "2012-04-01T17:56:09+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Los Angeles, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1151316203748515840/bY-UqNMh.jpg" ]
        }
      } ],
      "content" : [ "The word \"muse\" was invented to not pay women for ideas right?" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
