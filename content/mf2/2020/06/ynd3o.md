{
  "date" : "2020-06-13T17:39:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/StevenPears/status/1271826211401015299" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1271846333121548295" ],
    "name" : [ "Reply to https://twitter.com/StevenPears/status/1271826211401015299" ],
    "published" : [ "2020-06-13T17:39:00+01:00" ],
    "category" : [ "artemis-fowl" ],
    "content" : [ {
      "html" : "",
      "value" : "I wasn't happy with how rushed and different the siege of Fowl Manor was, and it felt like lots they were doing was setting up a sequel, than investing in the first film"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/06/ynd3o",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1271826211401015299" ],
      "url" : [ "https://twitter.com/StevenPears/status/1271826211401015299" ],
      "published" : [ "2020-06-13T15:26:12+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:StevenPears" ],
          "numeric-id" : [ "404697859" ],
          "name" : [ "Steven Pears" ],
          "nickname" : [ "StevenPears" ],
          "url" : [ "https://twitter.com/StevenPears", "https://github.com/stoiveyp" ],
          "published" : [ "2011-11-04T08:54:04+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1210474450824642560/lmh3mZjU.jpg" ]
        }
      } ],
      "location" : [ {
        "type" : [ "h-card", "p-location" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:7d7bdec12d2549d4" ],
          "name" : [ "Nottingham, England" ]
        }
      } ],
      "content" : [ {
        "value" : "Just finished watching Artemis Fowl on Disney+\n\nI loved the books, so it was disappointing. But if it's the pilot to a TV show - I'd probably watch the show 🤷‍♂️",
        "html" : "<div style=\"white-space: pre\">Just finished watching Artemis Fowl on Disney+\n\nI loved the books, so it was disappointing. But if it's the pilot to a TV show - I'd probably watch the show 🤷‍♂️</div>"
      } ]
    }
  },
  "tags" : [ "artemis-fowl" ],
  "client_id" : "https://indigenous.realize.be"
}
