{
  "date" : "2020-06-27T13:54:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/tpope/status/1276657238476759042" ],
    "name" : [ "Like of @tpope's tweet" ],
    "published" : [ "2020-06-27T13:54:00+01:00" ],
    "like-of" : [ "https://twitter.com/tpope/status/1276657238476759042" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/dcei9",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1276657238476759042" ],
      "url" : [ "https://twitter.com/tpope/status/1276657238476759042" ],
      "published" : [ "2020-06-26T23:22:59+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:tpope" ],
          "numeric-id" : [ "8000842" ],
          "name" : [ "Tim Pope" ],
          "nickname" : [ "tpope" ],
          "url" : [ "https://twitter.com/tpope", "https://tpo.pe" ],
          "published" : [ "2007-08-06T20:05:32+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Brooklyn" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1248728796565516288/zFSsiDD_.jpg" ]
        }
      } ],
      "content" : [ "not using vim is a form of privilege, and it's one we don't talk about nearly enough." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
