{
  "date" : "2020-06-24T12:26:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamieTanna/status/1275753182551998464" ],
    "published" : [ "2020-06-24T12:26:00+01:00" ],
    "repost-of" : [ "https://twitter.com/jennschiffer/status/1275585747588976645" ],
    "category" : [ "diversity-and-inclusion", "github" ]
  },
  "kind" : "reposts",
  "slug" : "2020/06/jjbgw",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1275585747588976645" ],
      "url" : [ "https://twitter.com/jennschiffer/status/1275585747588976645" ],
      "published" : [ "2020-06-24T00:25:16+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:jennschiffer" ],
          "numeric-id" : [ "12524622" ],
          "name" : [ "jenn" ],
          "nickname" : [ "jennschiffer" ],
          "url" : [ "https://twitter.com/jennschiffer", "https://jennmoney.biz", "http://make8bitart.com", "http://livelaugh.blog" ],
          "published" : [ "2008-01-22T05:42:15+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "jersey city, jenn@dotbiz.info" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1275591382976126976/-3Hgsfo0.jpg" ]
        }
      } ],
      "content" : [ "breaking: github to rename default “master” branch to “ICE”" ]
    }
  },
  "tags" : [ "diversity-and-inclusion", "github" ],
  "client_id" : "https://indigenous.realize.be"
}
