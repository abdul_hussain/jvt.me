{
  "kind": "reads",
  "slug": "2020/06/ghatp",
  "date": "2020-06-30T11:45:00Z",
  "h": "h-entry",
  "properties": {
    "published": [
      "2020-06-30T11:45:00Z"
    ],
    "read-status": [
      "finished"
    ],
    "read-of": [
      {
        "type": [
          "h-cite"
        ],
        "properties": {
          "name": [
            "Caliban's War"
          ],
          "author": [
            "James S. A. Corey"
          ],
          "isbn": [
            "1841499900"
          ]
        }
      }
    ]
  }
}

