{
  "date" : "2020-06-19T18:18:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/DavidAns/status/1273818522108321793" ],
    "name" : [ "Like of @DavidAns's tweet" ],
    "published" : [ "2020-06-19T18:18:00+01:00" ],
    "like-of" : [ "https://twitter.com/DavidAns/status/1273818522108321793" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/hj9ib",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1273818522108321793" ],
      "url" : [ "https://twitter.com/DavidAns/status/1273818522108321793" ],
      "published" : [ "2020-06-19T03:22:56+00:00" ],
      "in-reply-to" : [ "https://twitter.com/ericlaw/status/1273818099427487744" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:DavidAns" ],
          "numeric-id" : [ "82213118" ],
          "name" : [ "David Anson" ],
          "nickname" : [ "DavidAns" ],
          "url" : [ "https://twitter.com/DavidAns", "https://dlaa.me/" ],
          "published" : [ "2009-10-13T22:47:27+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Redmond, WA, USA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/542908940996976640/u8e9eg8u.jpeg" ]
        }
      } ],
      "content" : [ {
        "value" : "Congrats, you do now!",
        "html" : "Congrats, you do now!\n<a class=\"u-mention\" href=\"https://twitter.com/ericlaw\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
