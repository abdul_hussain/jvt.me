{
  "date" : "2020-06-29T08:28:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamesSACorey/status/1277336533876404224" ],
    "name" : [ "Like of @JamesSACorey's tweet" ],
    "published" : [ "2020-06-29T08:28:00+01:00" ],
    "like-of" : [ "https://twitter.com/JamesSACorey/status/1277336533876404224" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/titva",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1277336533876404224" ],
      "url" : [ "https://twitter.com/JamesSACorey/status/1277336533876404224" ],
      "published" : [ "2020-06-28T20:22:16+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:JamesSACorey" ],
          "numeric-id" : [ "550270216" ],
          "name" : [ "James S.A. Corey" ],
          "nickname" : [ "JamesSACorey" ],
          "url" : [ "https://twitter.com/JamesSACorey", "http://www.the-expanse.com" ],
          "published" : [ "2012-04-10T16:59:23+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Rocinante" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1029097103333543936/H4TP9cxr.jpg" ]
        }
      } ],
      "content" : [ "If you want insider details on what's going on in my life, just call me and ask. If you don't even have my phone number, then you're probably not someone who should be making assertions about what I'm up to, sa sa que?" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
