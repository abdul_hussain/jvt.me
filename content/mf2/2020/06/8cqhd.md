{
  "date" : "2020-06-30T23:53:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/LandTanin/status/1278033276435869699" ],
    "name" : [ "Like of @LandTanin's tweet" ],
    "published" : [ "2020-06-30T23:53:00+01:00" ],
    "like-of" : [ "https://twitter.com/LandTanin/status/1278033276435869699" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/8cqhd",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1278033276435869699" ],
      "url" : [ "https://twitter.com/LandTanin/status/1278033276435869699" ],
      "published" : [ "2020-06-30T18:30:52+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:LandTanin" ],
          "numeric-id" : [ "2211089017" ],
          "name" : [ "Tanin" ],
          "nickname" : [ "LandTanin" ],
          "url" : [ "https://twitter.com/LandTanin", "http://tanin.dev" ],
          "published" : [ "2013-11-23T17:56:53+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "England, United Kingdom" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1224019457984671745/RSEUuc3o.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Really pleased my article about Stub vs Mock in #iOS unit testing has finally been published on @CapitalOneTech #medium 🤓🎉\nmedium.com/capital-one-te…",
        "html" : "<div style=\"white-space: pre\">Really pleased my article about Stub vs Mock in <a href=\"https://twitter.com/search?q=%23iOS\">#iOS</a> unit testing has finally been published on <a href=\"https://twitter.com/CapitalOneTech\">@CapitalOneTech</a> <a href=\"https://twitter.com/search?q=%23medium\">#medium</a> 🤓🎉\n<a href=\"https://medium.com/capital-one-tech/using-stub-vs-mock-in-ios-unit-testing-634ec4cc6a10\">medium.com/capital-one-te…</a></div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
