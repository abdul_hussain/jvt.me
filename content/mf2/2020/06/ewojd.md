{
  "date" : "2020-06-04T14:15:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/BarclaysUKHelp/status/1268502581497692161" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1268533138990522368" ],
    "name" : [ "Reply to https://twitter.com/BarclaysUKHelp/status/1268502581497692161" ],
    "published" : [ "2020-06-04T14:15:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "No, there was no indication anything went wrong, and when redirected back, nothing said whether the transaction had passed or failed"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/06/ewojd",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1268502581497692161" ],
      "url" : [ "https://twitter.com/BarclaysUKHelp/status/1268502581497692161" ],
      "published" : [ "2020-06-04T11:19:17+00:00" ],
      "in-reply-to" : [ "https://twitter.com/JamieTanna/status/1266715350311329792" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:BarclaysUKHelp" ],
          "numeric-id" : [ "3046525515" ],
          "name" : [ "Barclays UK Help" ],
          "nickname" : [ "BarclaysUKHelp" ],
          "url" : [ "https://twitter.com/BarclaysUKHelp", "https://barc.ly/SocialMediaTerms" ],
          "published" : [ "2015-02-20T11:58:21+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1194874837489065984/xgFOjcfy.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Hi there, thanks for reaching out to us. I appreciate your patience. 👍 I'm really sorry that you were facing a few issues! We do regularly test our online banking through multiple browsers to ensure that it does function correctly. Did an error code or message pop up? Jack",
        "html" : "Hi there, thanks for reaching out to us. I appreciate your patience. 👍 I'm really sorry that you were facing a few issues! We do regularly test our online banking through multiple browsers to ensure that it does function correctly. Did an error code or message pop up? Jack\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
