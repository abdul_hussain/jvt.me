{
  "date" : "2020-06-12T11:42:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/justindross/status/1271110694977409025" ],
    "name" : [ "Like of @justindross's tweet" ],
    "published" : [ "2020-06-12T11:42:00+01:00" ],
    "like-of" : [ "https://twitter.com/justindross/status/1271110694977409025" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/hxifz",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1271110694977409025" ],
      "url" : [ "https://twitter.com/justindross/status/1271110694977409025" ],
      "published" : [ "2020-06-11T16:03:00+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:justindross" ],
          "numeric-id" : [ "8360172" ],
          "name" : [ "JD Ross" ],
          "nickname" : [ "justindross" ],
          "url" : [ "https://twitter.com/justindross" ],
          "published" : [ "2007-08-22T15:07:17+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "San Francisco" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1256393882335502342/nrGZWkit.jpg" ]
        }
      } ],
      "content" : [ "5 years ago today, the greatest headline ever written as Jack took over from Dick C at Twitter" ],
      "photo" : [ "https://pbs.twimg.com/media/EaPkcIDVcAAW0Z6.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
