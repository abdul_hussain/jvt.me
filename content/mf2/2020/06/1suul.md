{
  "date" : "2020-06-21T15:06:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/Brunty/status/1274703376660475905" ],
    "name" : [ "Like of @Brunty's tweet" ],
    "published" : [ "2020-06-21T15:06:00+01:00" ],
    "category" : [ "food" ],
    "like-of" : [ "https://twitter.com/Brunty/status/1274703376660475905" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/1suul",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1274703376660475905" ],
      "url" : [ "https://twitter.com/Brunty/status/1274703376660475905" ],
      "published" : [ "2020-06-21T13:59:02+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Brunty" ],
          "numeric-id" : [ "1099517012" ],
          "name" : [ "Matt Brunt" ],
          "nickname" : [ "Brunty" ],
          "url" : [ "https://twitter.com/Brunty", "https://brunty.me/now" ],
          "published" : [ "2013-01-17T23:19:21+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Leicester, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1151043501288824832/TjB9WT83.jpg" ]
        }
      } ],
      "content" : [ "Chocolate chip Nutella cookie dough pies." ],
      "photo" : [ "https://pbs.twimg.com/media/EbCn9L7XQAYVu-n.jpg" ]
    }
  },
  "tags" : [ "food" ],
  "client_id" : "https://indigenous.realize.be"
}
