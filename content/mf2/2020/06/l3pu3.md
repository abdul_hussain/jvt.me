{
  "date" : "2020-06-26T23:12:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/katie_fenn/status/1276631314616258561" ],
    "name" : [ "Like of @katie_fenn's tweet" ],
    "published" : [ "2020-06-26T23:12:00+01:00" ],
    "like-of" : [ "https://twitter.com/katie_fenn/status/1276631314616258561" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/l3pu3",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1276631314616258561" ],
      "url" : [ "https://twitter.com/katie_fenn/status/1276631314616258561" ],
      "published" : [ "2020-06-26T21:39:58+00:00" ],
      "in-reply-to" : [ "https://twitter.com/katie_fenn/status/1276630953562120194" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:katie_fenn" ],
          "numeric-id" : [ "14979616" ],
          "name" : [ "Katie Fenn" ],
          "nickname" : [ "katie_fenn" ],
          "url" : [ "https://twitter.com/katie_fenn" ],
          "published" : [ "2008-06-02T12:02:44+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Sheffield" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1094575150517891072/sGJJqyzP.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "But it also left me not being able to have children.\n\nAnd that’s *not* a fucking privilege.",
        "html" : "<div style=\"white-space: pre\">But it also left me not being able to have children.\n\nAnd that’s *not* a fucking privilege.</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
