{
  "date" : "2020-06-14T16:19:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/paulienuh/status/1272167805857792001" ],
    "name" : [ "Like of @paulienuh's tweet" ],
    "published" : [ "2020-06-14T16:19:00+01:00" ],
    "category" : [ "rss" ],
    "like-of" : [ "https://twitter.com/paulienuh/status/1272167805857792001" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/fevtg",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1272167805857792001" ],
      "url" : [ "https://twitter.com/paulienuh/status/1272167805857792001" ],
      "published" : [ "2020-06-14T14:03:35+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:paulienuh" ],
          "numeric-id" : [ "230185453" ],
          "name" : [ "Pauline P. Narvas" ],
          "nickname" : [ "paulienuh" ],
          "url" : [ "https://twitter.com/paulienuh", "https://pawlean.com" ],
          "published" : [ "2010-12-24T14:46:00+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Leeds, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1265398650395639813/PfESApun.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "A few people have asked me about my RSS feed not working, I think the issue has been fixed now! If it doesn't - let me know 😊\n\nAdd the link below to your RSS feed woo!\npawlean.com/feed/",
        "html" : "<div style=\"white-space: pre\">A few people have asked me about my RSS feed not working, I think the issue has been fixed now! If it doesn't - let me know 😊\n\nAdd the link below to your RSS feed woo!\n<a href=\"https://pawlean.com/feed/\">pawlean.com/feed/</a></div>"
      } ]
    }
  },
  "tags" : [ "rss" ],
  "client_id" : "https://indigenous.realize.be"
}
