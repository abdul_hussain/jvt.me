{
  "date" : "2020-06-27T12:08:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamieTanna/status/1276835728408928256" ],
    "published" : [ "2020-06-27T12:08:00+01:00" ],
    "repost-of" : [ "https://twitter.com/MemphisQuinn/status/1276662502596775936" ],
    "category" : [ "diversity-and-inclusion" ]
  },
  "kind" : "reposts",
  "slug" : "2020/06/h06ny",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1276662502596775936" ],
      "url" : [ "https://twitter.com/MemphisQuinn/status/1276662502596775936" ],
      "published" : [ "2020-06-26T23:43:54+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:MemphisQuinn" ],
          "numeric-id" : [ "436968221" ],
          "name" : [ "BlackLivesMatter✊🏿Professional Comic Illustrator" ],
          "nickname" : [ "MemphisQuinn" ],
          "url" : [ "https://twitter.com/MemphisQuinn", "http://www.operative.net" ],
          "published" : [ "2011-12-14T20:10:57+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Orange Mound, Memphis" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1222540274137620480/2Tiixpo0.jpg" ]
        }
      } ],
      "content" : [ "Yup" ],
      "photo" : [ "https://pbs.twimg.com/media/EbedxyTXYAQKQwq.jpg" ]
    }
  },
  "tags" : [ "diversity-and-inclusion" ],
  "client_id" : "https://indigenous.realize.be"
}
