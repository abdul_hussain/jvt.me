{
  "date" : "2020-06-15T19:27:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/_CalvinAllen/status/1272595525276258304" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1272620823866740738" ],
    "name" : [ "Reply to https://twitter.com/_CalvinAllen/status/1272595525276258304" ],
    "published" : [ "2020-06-15T19:27:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "Netlify is definitely what you want - I've only recently been going over the free tier limits 🙌🏼"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/06/8gsut",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1272595525276258304" ],
      "url" : [ "https://twitter.com/_CalvinAllen/status/1272595525276258304" ],
      "published" : [ "2020-06-15T18:23:11+00:00" ],
      "in-reply-to" : [ "https://twitter.com/LeeEnglestone/status/1272595016200925185" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:_CalvinAllen" ],
          "numeric-id" : [ "897106747512508417" ],
          "name" : [ "Calvin A. Allen" ],
          "nickname" : [ "_CalvinAllen" ],
          "url" : [ "https://twitter.com/_CalvinAllen", "https://www.calvinallen.net" ],
          "published" : [ "2017-08-14T14:44:50+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "USA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1129754481665691654/f-SppwzS.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "A static site on @Netlify",
        "html" : "A static site on <a href=\"https://twitter.com/Netlify\">@Netlify</a>\n<a class=\"u-mention\" href=\"https://twitter.com/LeeEnglestone\"></a>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
