{
  "date" : "2020-06-28T12:27:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/codinghorror/status/1276934067015974912" ],
    "name" : [ "Like of @codinghorror's tweet" ],
    "published" : [ "2020-06-28T12:27:00+01:00" ],
    "category" : [ "vim" ],
    "like-of" : [ "https://twitter.com/codinghorror/status/1276934067015974912" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/1ilsh",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1276934067015974912" ],
      "url" : [ "https://twitter.com/codinghorror/status/1276934067015974912" ],
      "published" : [ "2020-06-27T17:43:00+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:codinghorror" ],
          "numeric-id" : [ "5637652" ],
          "name" : [ "Jeff Atwood" ],
          "nickname" : [ "codinghorror" ],
          "url" : [ "https://twitter.com/codinghorror", "http://blog.codinghorror.com", "https://stackoverflow.com", "https://discourse.org" ],
          "published" : [ "2007-04-29T20:50:37+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Bay Area, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1246568020991864832/hh4FVqbl.jpg" ]
        }
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EbiUw3TUcAESLZf.jpg" ]
    }
  },
  "tags" : [ "vim" ],
  "client_id" : "https://indigenous.realize.be"
}
