{
  "date" : "2020-06-18T22:21:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/monicalent/status/1273645614190989312" ],
    "name" : [ "Like of @monicalent's tweet" ],
    "published" : [ "2020-06-18T22:21:00+01:00" ],
    "category" : [ "postman" ],
    "like-of" : [ "https://twitter.com/monicalent/status/1273645614190989312" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/u4u6o",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1273645614190989312" ],
      "url" : [ "https://twitter.com/monicalent/status/1273645614190989312" ],
      "published" : [ "2020-06-18T15:55:52+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:monicalent" ],
          "numeric-id" : [ "1158897613" ],
          "name" : [ "Monica Lent" ],
          "nickname" : [ "monicalent" ],
          "url" : [ "https://twitter.com/monicalent", "https://monicalent.com", "http://affilimate.com", "http://bloggingfordevs.com" ],
          "published" : [ "2013-02-08T02:35:11+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Berlin" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1188792506319884289/ScFibdDk.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Here I was thinking that postman is just fancy curl. techcrunch.com/2020/06/11/api…",
        "html" : "Here I was thinking that postman is just fancy curl. <a href=\"https://techcrunch.com/2020/06/11/api-platform-postman-nabs-150m-series-c-on-2b-valuation/\">techcrunch.com/2020/06/11/api…</a>"
      } ]
    }
  },
  "tags" : [ "postman" ],
  "client_id" : "https://indigenous.realize.be"
}
