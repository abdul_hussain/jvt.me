{
  "date" : "2020-06-25T18:00:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/mathowie/status/1275919100187774976" ],
    "name" : [ "Like of @mathowie's tweet" ],
    "published" : [ "2020-06-25T18:00:00+01:00" ],
    "like-of" : [ "https://twitter.com/mathowie/status/1275919100187774976" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/yc7ei",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1275919100187774976" ],
      "url" : [ "https://twitter.com/mathowie/status/1275919100187774976" ],
      "published" : [ "2020-06-24T22:29:53+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:mathowie" ],
          "numeric-id" : [ "761975" ],
          "name" : [ "Matt Haughey 😷" ],
          "nickname" : [ "mathowie" ],
          "url" : [ "https://twitter.com/mathowie", "http://a.wholelottanothing.org" ],
          "published" : [ "2007-02-10T03:17:49+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Oregon, USA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1053138151210799105/8O9EGGDO.jpg" ]
        }
      } ],
      "content" : [ "lol seeing guys that spent the last twenty years prepping their bunker to survive indefinitely in a nuclear winter are giving up after wearing a thin cloth mask for three weeks." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
