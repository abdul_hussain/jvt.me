{
  "date" : "2020-06-16T17:32:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/edent/status/1272811707359006721" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1272932280420438016" ],
    "name" : [ "Reply to https://twitter.com/edent/status/1272811707359006721" ],
    "published" : [ "2020-06-16T17:32:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "Reminds me of when I used to root my phone and had CyanogenMod's PrivacyGuard which once told me Uber Eats was trying to access my microphone when I'd opened the app, not actually doing anything else 🤔"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/06/m2lwo",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1272811707359006721" ],
      "url" : [ "https://twitter.com/edent/status/1272811707359006721" ],
      "published" : [ "2020-06-16T08:42:13+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:edent" ],
          "numeric-id" : [ "14054507" ],
          "name" : [ "Terence Eden" ],
          "nickname" : [ "edent" ],
          "url" : [ "https://twitter.com/edent", "https://shkspr.mobi/blog/", "https://edent.tel" ],
          "published" : [ "2008-02-28T13:10:25+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1228067445153452033/_A8Uq2VY.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "The new Android 10 permissions manager is great.\nWhy is @signalapp getting my location in the background?",
        "html" : "<div style=\"white-space: pre\">The new Android 10 permissions manager is great.\nWhy is <a href=\"https://twitter.com/signalapp\">@signalapp</a> getting my location in the background?</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/Eanvf6HWkAA7wbM.jpg" ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
