{
  "date" : "2020-06-27T10:21:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/tesseralis/status/1276660501292806144" ],
    "name" : [ "Like of @tesseralis's tweet" ],
    "published" : [ "2020-06-27T10:21:00+01:00" ],
    "like-of" : [ "https://twitter.com/tesseralis/status/1276660501292806144" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/xze0l",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1276660501292806144" ],
      "url" : [ "https://twitter.com/tesseralis/status/1276660501292806144" ],
      "published" : [ "2020-06-26T23:35:57+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:tesseralis" ],
          "numeric-id" : [ "471768380" ],
          "name" : [ "Nat Alison 🍑" ],
          "nickname" : [ "tesseralis" ],
          "url" : [ "https://twitter.com/tesseralis", "http://www.tessera.li", "http://Ko-fi.com/tesseralis" ],
          "published" : [ "2012-01-23T07:18:57+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Koda Island" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1231468297649979392/t_7jReNz.jpg" ]
        }
      } ],
      "content" : [ "Being the founder and CTO of a tech company which exploited its marginalized employees only for it to crash and burn and be sold to Github is privilege that we don't talk about nearly enough 😔" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
