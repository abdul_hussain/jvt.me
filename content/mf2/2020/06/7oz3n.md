{
  "date" : "2020-06-28T13:06:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/ethomson/status/1277197924859084802" ],
    "name" : [ "Like of @ethomson's tweet" ],
    "published" : [ "2020-06-28T13:06:00+01:00" ],
    "category" : [ "git" ],
    "like-of" : [ "https://twitter.com/ethomson/status/1277197924859084802" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/7oz3n",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1277197924859084802" ],
      "url" : [ "https://twitter.com/ethomson/status/1277197924859084802" ],
      "published" : [ "2020-06-28T11:11:29+00:00" ],
      "in-reply-to" : [ "https://twitter.com/hmemcpy/status/1277194673577234432" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:ethomson" ],
          "numeric-id" : [ "14297174" ],
          "name" : [ "Edward Thomson" ],
          "nickname" : [ "ethomson" ],
          "url" : [ "https://twitter.com/ethomson", "https://edwardthomson.com/" ],
          "published" : [ "2008-04-03T21:58:27+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Cambridge, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/502942125101568000/a1KejPfj.jpeg" ]
        }
      } ],
      "content" : [ {
        "value" : "Check in a `.gitattributes`.  Ignore `core.autocrlf`. edwardthomson.com/blog/git_for_w…",
        "html" : "Check in a `.gitattributes`.  Ignore `core.autocrlf`. <a href=\"https://www.edwardthomson.com/blog/git_for_windows_line_endings.html\">edwardthomson.com/blog/git_for_w…</a>\n<a class=\"u-mention\" href=\"https://twitter.com/hmemcpy\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/pati_gallardo\"></a>"
      } ]
    }
  },
  "tags" : [ "git" ],
  "client_id" : "https://indigenous.realize.be"
}
