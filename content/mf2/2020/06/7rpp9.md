{
  "date" : "2020-06-06T00:26:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/sebs_tweets/status/1268523788838285314" ],
    "name" : [ "Like of @sebs_tweets's tweet" ],
    "published" : [ "2020-06-06T00:26:00+01:00" ],
    "like-of" : [ "https://twitter.com/sebs_tweets/status/1268523788838285314" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/7rpp9",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1268523788838285314" ],
      "url" : [ "https://twitter.com/sebs_tweets/status/1268523788838285314" ],
      "published" : [ "2020-06-04T12:43:33+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:sebs_tweets" ],
          "numeric-id" : [ "1017903220977537024" ],
          "name" : [ "Sebastian David Lees" ],
          "nickname" : [ "sebs_tweets" ],
          "url" : [ "https://twitter.com/sebs_tweets", "https://datablast.com" ],
          "published" : [ "2018-07-13T22:46:54+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1216106879845326850/1DNksofu.jpg" ]
        }
      } ],
      "content" : [ "The greatest opening to a textbook I have ever seen..." ],
      "photo" : [ "https://pbs.twimg.com/media/EZqzqReXgAQgRZh.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
