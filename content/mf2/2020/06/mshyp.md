{
  "date" : "2020-06-28T13:18:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/ktguen/status/1276694142341402626" ],
    "name" : [ "Like of @ktguen's tweet" ],
    "published" : [ "2020-06-28T13:18:00+01:00" ],
    "category" : [ "coronavirus" ],
    "like-of" : [ "https://twitter.com/ktguen/status/1276694142341402626" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/mshyp",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1276694142341402626" ],
      "url" : [ "https://twitter.com/ktguen/status/1276694142341402626" ],
      "published" : [ "2020-06-27T01:49:38+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:ktguen" ],
          "numeric-id" : [ "46018794" ],
          "name" : [ "Katie Guenther" ],
          "nickname" : [ "ktguen" ],
          "url" : [ "https://twitter.com/ktguen" ],
          "published" : [ "2009-06-10T02:49:07+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Somewhere on I-77" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1189692556520677376/z8BdPcn_.jpg" ]
        }
      } ],
      "content" : [ "My dad is a Kellogg’s employee and his PPE just came in....." ],
      "photo" : [ "https://pbs.twimg.com/media/Ebe6fOHXkAEQbd-.jpg", "https://pbs.twimg.com/media/Ebe6fOHXYAMcHkj.jpg", "https://pbs.twimg.com/media/Ebe6fOIWAAIHovc.jpg", "https://pbs.twimg.com/media/Ebe6fOHWoAMBJfj.jpg" ]
    }
  },
  "tags" : [ "coronavirus" ],
  "client_id" : "https://indigenous.realize.be"
}
