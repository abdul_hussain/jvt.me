{
  "date" : "2020-06-28T23:06:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [  ],
    "published" : [ "2020-06-28T23:06:00+01:00" ],
    "repost-of" : [ "https://twitter.com/d1rtydan/status/1277081198624337920" ],
    "category" : [ "tiktok" ]
  },
  "kind" : "reposts",
  "slug" : "2020/06/svytv",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1277081198624337920" ],
      "url" : [ "https://twitter.com/d1rtydan/status/1277081198624337920" ],
      "published" : [ "2020-06-28T03:27:39+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:d1rtydan" ],
          "numeric-id" : [ "217246038" ],
          "name" : [ "Dan Okopnyi 🇺🇦" ],
          "nickname" : [ "d1rtydan" ],
          "url" : [ "https://twitter.com/d1rtydan", "https://linktr.ee/dokopnyi" ],
          "published" : [ "2010-11-19T00:05:44+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "United States" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1261315411141750785/HljZu0eh.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "A guy on reddit reversed engineered #TikTok\n\nHere’s what he found on the data it collects on you\n\nIt’s far worse than just stealing what’s on your clipboard:",
        "html" : "<div style=\"white-space: pre\">A guy on reddit reversed engineered <a href=\"https://twitter.com/search?q=%23TikTok\">#TikTok</a>\n\nHere’s what he found on the data it collects on you\n\nIt’s far worse than just stealing what’s on your clipboard:</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EbkalGXVcAIOoni.jpg", "https://pbs.twimg.com/media/EbkalGmUEAAg5lz.jpg" ]
    }
  },
  "tags" : [ "tiktok" ],
  "client_id" : "https://indigenous.realize.be"
}
