{
  "date" : "2020-06-02T07:29:18.238Z",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamieTanna/status/1267721029960503298" ],
    "published" : [ "2020-06-02T07:29:18.238Z" ],
    "repost-of" : [ "https://twitter.com/WiT_Notts/status/1267717197604032512" ]
  },
  "kind" : "reposts",
  "slug" : "2020/06/qioz8",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1267717197604032512" ],
      "url" : [ "https://twitter.com/WiT_Notts/status/1267717197604032512" ],
      "published" : [ "2020-06-02T07:18:27+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:WiT_Notts" ],
          "numeric-id" : [ "4339158441" ],
          "name" : [ "Women In Tech, Nottingham 🌈✨" ],
          "nickname" : [ "WiT_Notts" ],
          "url" : [ "https://twitter.com/WiT_Notts", "https://nott.tech/wit" ],
          "published" : [ "2015-12-01T11:22:25+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/913380298829844481/YvPcjPlE.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Our thoughts and hearts go out to all those that are protesting across the world right now, it is more important than ever that we stand together to end the ignorance and violence. Love you all #BlackLivesMatter",
        "html" : "Our thoughts and hearts go out to all those that are protesting across the world right now, it is more important than ever that we stand together to end the ignorance and violence. Love you all <a href=\"https://twitter.com/search?q=%23BlackLivesMatter\">#BlackLivesMatter</a>"
      } ]
    }
  },
  "client_id" : "https://micropublish.net"
}
