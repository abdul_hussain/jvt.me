{
  "date" : "2020-06-28T17:09:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamieTanna/status/1277274308063236100" ],
    "published" : [ "2020-06-28T17:09:00+01:00" ],
    "repost-of" : [ "https://twitter.com/mattstratton/status/1277033459920355328" ],
    "category" : [ "coronavirus" ]
  },
  "kind" : "reposts",
  "slug" : "2020/06/hrlsg",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1277033459920355328" ],
      "url" : [ "https://twitter.com/mattstratton/status/1277033459920355328" ],
      "published" : [ "2020-06-28T00:17:57+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:mattstratton" ],
          "numeric-id" : [ "46113" ],
          "name" : [ "Matt Stratton" ],
          "nickname" : [ "mattstratton" ],
          "url" : [ "https://twitter.com/mattstratton", "http://mattstratton.com" ],
          "published" : [ "2006-12-06T18:20:17+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Chicago, IL" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1255702214686973957/PWZ34zLA.jpg" ]
        }
      } ],
      "content" : [ "Are you wearing a mask in public?" ]
    }
  },
  "tags" : [ "coronavirus" ],
  "client_id" : "https://indigenous.realize.be"
}
