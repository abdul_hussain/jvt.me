{
  "date" : "2020-06-21T18:31:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/271420212/" ],
    "syndication" : [ "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/271420212/#rsvp-by-https%3A%2F%2Fwww.jvt.me%2Fmf2%2F2020%2F06%2Fphbhc%2F" ],
    "name" : [ "RSVP yes to https://www.meetup.com/PHPMiNDS-in-Nottingham/events/271420212/" ],
    "published" : [ "2020-06-21T18:31:00+01:00" ],
    "event" : {
      "start" : [ "2020-07-09T18:30:00+01:00" ],
      "name" : [ "An Introduction to Kubernetes by Marcus Noble" ],
      "end" : [ "2020-07-09T20:30:00+01:00" ],
      "location" : [ "Online" ],
      "url" : [ "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/271420212/" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/06/phbhc",
  "client_id" : "https://indigenous.realize.be"
}
