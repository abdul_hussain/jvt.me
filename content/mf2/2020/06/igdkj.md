{
  "date" : "2020-06-28T13:02:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/glennzw/status/1277168117442064384" ],
    "name" : [ "Like of @glennzw's tweet" ],
    "published" : [ "2020-06-28T13:02:00+01:00" ],
    "like-of" : [ "https://twitter.com/glennzw/status/1277168117442064384" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/igdkj",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1277168117442064384" ],
      "url" : [ "https://twitter.com/glennzw/status/1277168117442064384" ],
      "published" : [ "2020-06-28T09:13:02+00:00" ],
      "in-reply-to" : [ "https://twitter.com/_liclac/status/1277161065764315136" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:glennzw" ],
          "numeric-id" : [ "21854112" ],
          "name" : [ "Glenn Wilkinson 🇿🇼" ],
          "nickname" : [ "glennzw" ],
          "url" : [ "https://twitter.com/glennzw", "http://github.com/glennzw" ],
          "published" : [ "2009-02-25T10:46:17+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "glennzw at protonmail dot com" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/624265041722871813/g33jFUgF.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Alternatively; if you can’t join them, beat them. 🥊",
        "html" : "Alternatively; if you can’t join them, beat them. 🥊\n<a class=\"u-mention\" href=\"https://twitter.com/_liclac\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/kieranmch\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
