{
  "date" : "2020-06-28T11:35:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.meetup.com/Nottingham-IoT-Meetup/events/271579036/" ],
    "syndication" : [ "https://brid.gy/publish/meetup" ],
    "name" : [ "RSVP yes to https://www.meetup.com/Nottingham-IoT-Meetup/events/271579036/" ],
    "published" : [ "2020-06-28T11:35:00+01:00" ],
    "event" : {
      "start" : [ "2020-07-16T18:30:00+01:00" ],
      "name" : [ "[Online] Derek Roffesoft - Tesla Coils + Eleanor Tang - The Rise of Smart Bins" ],
      "end" : [ "2020-07-16T21:30:00+01:00" ],
      "location" : [ "Online" ],
      "url" : [ "https://www.meetup.com/Nottingham-IoT-Meetup/events/271579036/" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/06/mi7nr",
  "client_id" : "https://indigenous.realize.be"
}
