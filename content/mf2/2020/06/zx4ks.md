{
  "date" : "2020-06-25T18:00:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/WhitStillman/status/1275898543027507201" ],
    "name" : [ "Like of @WhitStillman's tweet" ],
    "published" : [ "2020-06-25T18:00:00+01:00" ],
    "like-of" : [ "https://twitter.com/WhitStillman/status/1275898543027507201" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/zx4ks",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1275898543027507201" ],
      "url" : [ "https://twitter.com/WhitStillman/status/1275898543027507201" ],
      "published" : [ "2020-06-24T21:08:12+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:WhitStillman" ],
          "numeric-id" : [ "148055299" ],
          "name" : [ "Whit Stillman" ],
          "nickname" : [ "WhitStillman" ],
          "url" : [ "https://twitter.com/WhitStillman", "https://www.amazon.com/Love-Friendship-Austens-Entirely-Vindicated/dp/0316294128" ],
          "published" : [ "2010-05-25T18:32:34+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "USA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/743692418554355712/cmraKxm1.jpg" ]
        }
      } ],
      "content" : [ "I'd been fearing just this." ],
      "photo" : [ "https://pbs.twimg.com/media/EbTm7j5XgAAykSS.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
