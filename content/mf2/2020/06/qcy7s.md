{
  "date" : "2020-06-27T13:55:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/danhett/status/1276837180124598275" ],
    "name" : [ "Like of @danhett's tweet" ],
    "published" : [ "2020-06-27T13:55:00+01:00" ],
    "like-of" : [ "https://twitter.com/danhett/status/1276837180124598275" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/qcy7s",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1276837180124598275" ],
      "url" : [ "https://twitter.com/danhett/status/1276837180124598275" ],
      "published" : [ "2020-06-27T11:18:00+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:danhett" ],
          "numeric-id" : [ "46346652" ],
          "name" : [ "dan hett" ],
          "nickname" : [ "danhett" ],
          "url" : [ "https://twitter.com/danhett", "http://danhett.com", "http://mmbcreative.com/clients/dan-he" ],
          "published" : [ "2009-06-11T08:36:26+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "MCR / New Mills" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1230967123943591936/fCQ3cQj6.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "my seven year old announced he's made a comic, starring me! I'm so proud. \n\nahh you know what, never mind",
        "html" : "<div style=\"white-space: pre\">my seven year old announced he's made a comic, starring me! I'm so proud. \n\nahh you know what, never mind</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/Ebg8oN_XQAAFRSR.jpg", "https://pbs.twimg.com/media/Ebg8o0CWkAEiGGX.jpg", "https://pbs.twimg.com/media/Ebg8pRCWsAIs5rs.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
