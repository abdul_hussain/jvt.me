{
  "date" : "2020-06-26T14:23:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/kendalmintcode/status/1276486696775925760" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1276507866959052800" ],
    "name" : [ "Reply to https://twitter.com/kendalmintcode/status/1276486696775925760" ],
    "published" : [ "2020-06-26T14:23:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "This is what I do too! I tag them under my site as https://www.jvt.me/tags/blogumentation/ and am always super happy to see how much organic search traffic I get - it means I'm (hopefully) helping others"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/06/2mhf0",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1276486696775925760" ],
      "url" : [ "https://twitter.com/kendalmintcode/status/1276486696775925760" ],
      "published" : [ "2020-06-26T12:05:19+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:kendalmintcode" ],
          "numeric-id" : [ "997813603867774976" ],
          "name" : [ "Rob Kendal {{☕}}" ],
          "nickname" : [ "kendalmintcode" ],
          "url" : [ "https://twitter.com/kendalmintcode", "https://robkendal.co.uk" ],
          "published" : [ "2018-05-19T12:17:56+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Yorkshire, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1271883694085734402/YI-Zglwa.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "‘Provide the solution you wish you’d found when you were searching’. 🤯\n\nMy approach to blog writing about technical solutions. The rest of the time I just think about providing my perspective on a topic. Might help someone see a different path or it might start a discussion",
        "html" : "<div style=\"white-space: pre\">‘Provide the solution you wish you’d found when you were searching’. 🤯\n\nMy approach to blog writing about technical solutions. The rest of the time I just think about providing my perspective on a topic. Might help someone see a different path or it might start a discussion</div>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
