{
  "date" : "2020-06-25T16:57:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/TheDailyShow/status/1276172320965636099" ],
    "name" : [ "Like of @TheDailyShow's tweet" ],
    "published" : [ "2020-06-25T16:57:00+01:00" ],
    "like-of" : [ "https://twitter.com/TheDailyShow/status/1276172320965636099" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/joncg",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1276172320965636099" ],
      "url" : [ "https://twitter.com/TheDailyShow/status/1276172320965636099" ],
      "published" : [ "2020-06-25T15:16:06+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:TheDailyShow" ],
          "numeric-id" : [ "158414847" ],
          "name" : [ "The Daily Show" ],
          "nickname" : [ "TheDailyShow" ],
          "url" : [ "https://twitter.com/TheDailyShow", "http://thedailyshow.com", "http://dailyshow.com/donate" ],
          "published" : [ "2010-06-22T16:41:05+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1246210124252958721/9sPxtCEL.jpg" ]
        }
      } ],
      "content" : [ "We did it, America. We flattened the curve." ],
      "photo" : [ "https://pbs.twimg.com/media/EbXf4xWXYAAHaYq.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
