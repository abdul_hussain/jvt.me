{
  "date" : "2020-06-18T13:14:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/rafrasenberg/status/1273525861128470528" ],
    "name" : [ "Like of @rafrasenberg's tweet" ],
    "published" : [ "2020-06-18T13:14:00+01:00" ],
    "like-of" : [ "https://twitter.com/rafrasenberg/status/1273525861128470528" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/rtp57",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1273525861128470528" ],
      "url" : [ "https://twitter.com/rafrasenberg/status/1273525861128470528" ],
      "published" : [ "2020-06-18T08:00:00+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:rafrasenberg" ],
          "numeric-id" : [ "1070248144858963969" ],
          "name" : [ "Raf Rasenberg" ],
          "nickname" : [ "rafrasenberg" ],
          "url" : [ "https://twitter.com/rafrasenberg", "https://youtube.com/rafrasenberg" ],
          "published" : [ "2018-12-05T09:26:56+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Code & Lifestyle videos 👉" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1263763108302139393/XNllStIX.jpg" ]
        }
      } ],
      "content" : [ "Developers nowadays when they need to set-up a simple blog or landing page" ],
      "photo" : [ "https://pbs.twimg.com/media/EaxzRSEXkAEJKyS.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
