{
  "date" : "2020-06-08T23:58:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/technottingham/status/1270083570103734274" ],
    "name" : [ "Like of @technottingham's tweet" ],
    "published" : [ "2020-06-08T23:58:00+01:00" ],
    "like-of" : [ "https://twitter.com/technottingham/status/1270083570103734274" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/yzloa",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1270083570103734274" ],
      "url" : [ "https://twitter.com/technottingham/status/1270083570103734274" ],
      "published" : [ "2020-06-08T20:01:34+00:00" ],
      "in-reply-to" : [ "https://twitter.com/CrdlPls/status/1270078554693414913" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:technottingham" ],
          "numeric-id" : [ "384492431" ],
          "name" : [ "Tech Nottingham" ],
          "nickname" : [ "technottingham" ],
          "url" : [ "https://twitter.com/technottingham", "http://technottingham.com" ],
          "published" : [ "2011-10-03T19:47:31+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1023974499757293570/ZoPc_QsO.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "#TechNott - YOU'LL NEVER LEAVE",
        "html" : "<a href=\"https://twitter.com/search?q=%23TechNott\">#TechNott</a> - YOU'LL NEVER LEAVE\n<a class=\"u-mention\" href=\"https://twitter.com/Codling\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/CrdlPls\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/chatterboxCoder\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
