{
  "date" : "2020-06-17T11:22:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/EmilyKager/status/1273092804458287104" ],
    "name" : [ "Like of @EmilyKager's tweet" ],
    "published" : [ "2020-06-17T11:22:00+01:00" ],
    "category" : [ "satire" ],
    "like-of" : [ "https://twitter.com/EmilyKager/status/1273092804458287104" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/aux5l",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1273092804458287104" ],
      "url" : [ "https://twitter.com/EmilyKager/status/1273092804458287104" ],
      "published" : [ "2020-06-17T03:19:12+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:EmilyKager" ],
          "numeric-id" : [ "2347475803" ],
          "name" : [ "Emily Kager" ],
          "nickname" : [ "EmilyKager" ],
          "url" : [ "https://twitter.com/EmilyKager", "https://www.emilykager.com/" ],
          "published" : [ "2014-02-16T21:34:12+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Oakland and SF " ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1260968235408605184/XezL8Z3r.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "👋 Super excited to announce NAMASTE! It's like email, if email aligned your chakras! \n- It's not a typo! Your inbox is now \"Ombox\", the mind, body, and spirit of your internet experience.\n- No more \"mark unread\" just mark to \"meditate on\"\n- Reach \"nirvana\" with always inbox 0™️",
        "html" : "<div style=\"white-space: pre\">👋 Super excited to announce NAMASTE! It's like email, if email aligned your chakras! \n- It's not a typo! Your inbox is now \"Ombox\", the mind, body, and spirit of your internet experience.\n- No more \"mark unread\" just mark to \"meditate on\"\n- Reach \"nirvana\" with always inbox 0™️</div>"
      } ]
    }
  },
  "tags" : [ "satire" ],
  "client_id" : "https://indigenous.realize.be"
}
