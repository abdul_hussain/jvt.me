{
  "date" : "2020-06-02T20:35:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/technottingham/status/1267863487016992768" ],
    "name" : [ "Like of @technottingham's tweet" ],
    "published" : [ "2020-06-02T20:35:00+01:00" ],
    "like-of" : [ "https://twitter.com/technottingham/status/1267863487016992768" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/ab97e",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1267863487016992768" ],
      "url" : [ "https://twitter.com/technottingham/status/1267863487016992768" ],
      "published" : [ "2020-06-02T16:59:45+00:00" ],
      "in-reply-to" : [ "https://twitter.com/technottingham/status/1267863485221928960" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:technottingham" ],
          "numeric-id" : [ "384492431" ],
          "name" : [ "Tech Nottingham" ],
          "nickname" : [ "technottingham" ],
          "url" : [ "https://twitter.com/technottingham", "http://technottingham.com" ],
          "published" : [ "2011-10-03T19:47:31+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1023974499757293570/ZoPc_QsO.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Tech Nottingham is a community that stands for and enforces the principles of tolerance, equality, justice and peace.\nWe have made donations to @bailproject as well as @NCSCounselling who run Nottingham's Black, Asian and Minority Ethnic Counselling Hub \n\n#BlackLivesMatter",
        "html" : "<div style=\"white-space: pre\">Tech Nottingham is a community that stands for and enforces the principles of tolerance, equality, justice and peace.\nWe have made donations to <a href=\"https://twitter.com/bailproject\">@bailproject</a> as well as <a href=\"https://twitter.com/NCSCounselling\">@NCSCounselling</a> who run Nottingham's Black, Asian and Minority Ethnic Counselling Hub \n\n<a href=\"https://twitter.com/search?q=%23BlackLivesMatter\">#BlackLivesMatter</a></div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
