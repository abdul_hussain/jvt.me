{
  "date" : "2020-06-03T23:14:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamieTanna/status/1268306695073280011" ],
    "published" : [ "2020-06-03T23:14:00+01:00" ],
    "repost-of" : [ "https://twitter.com/AkilahObviously/status/1267687440292081664" ]
  },
  "kind" : "reposts",
  "slug" : "2020/06/o8i0g",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1267687440292081664" ],
      "url" : [ "https://twitter.com/AkilahObviously/status/1267687440292081664" ],
      "published" : [ "2020-06-02T05:20:12+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:AkilahObviously" ],
          "numeric-id" : [ "61371461" ],
          "name" : [ "Akilah Hughes" ],
          "nickname" : [ "AkilahObviously" ],
          "url" : [ "https://twitter.com/AkilahObviously", "https://linktr.ee/akilahh" ],
          "published" : [ "2009-07-30T02:54:28+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Los Angeles, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1137040870488858628/ekFKy1Rt.jpg" ]
        }
      } ],
      "content" : [ "If you want to play piano, but you’re bad at playing piano, you practice and you get better.  Don’t be the kind of white person who doesn’t post out of fear of fucking up. If you want to support black people, but you’re bad at supporting black people, practice and get better." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
