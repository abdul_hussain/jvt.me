{
  "date" : "2020-06-23T19:15:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamieTanna/status/1275493752111579136" ],
    "published" : [ "2020-06-23T19:15:00+01:00" ],
    "repost-of" : [ "https://twitter.com/H_DeQuincey/status/1275006884219621377" ],
    "category" : [ "remote-work" ]
  },
  "kind" : "reposts",
  "slug" : "2020/06/ievqt",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1275006884219621377" ],
      "url" : [ "https://twitter.com/H_DeQuincey/status/1275006884219621377" ],
      "published" : [ "2020-06-22T10:05:04+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:H_DeQuincey" ],
          "numeric-id" : [ "978680692665126912" ],
          "name" : [ "Heather De-Quincey" ],
          "nickname" : [ "H_DeQuincey" ],
          "url" : [ "https://twitter.com/H_DeQuincey" ],
          "published" : [ "2018-03-27T17:10:34+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Swansea, Wales" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1252194655464312833/1OzT9tup.jpg" ]
        }
      } ],
      "content" : [ "I think we need to stop calling it 'working from home' and start calling it 'living at work'" ]
    }
  },
  "tags" : [ "remote-work" ],
  "client_id" : "https://indigenous.realize.be"
}
