{
  "date" : "2020-06-12T18:46:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JennyENicholson/status/1271267475963838467" ],
    "name" : [ "Like of @JennyENicholson's tweet" ],
    "published" : [ "2020-06-12T18:46:00+01:00" ],
    "like-of" : [ "https://twitter.com/JennyENicholson/status/1271267475963838467" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/vccyp",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1271267475963838467" ],
      "url" : [ "https://twitter.com/JennyENicholson/status/1271267475963838467" ],
      "published" : [ "2020-06-12T02:25:59+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:JennyENicholson" ],
          "numeric-id" : [ "1700725878" ],
          "name" : [ "Jenny Nicholson" ],
          "nickname" : [ "JennyENicholson" ],
          "url" : [ "https://twitter.com/JennyENicholson", "https://www.jennywebsite.com/" ],
          "published" : [ "2013-08-26T02:46:59+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Los Angeles, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1264756616131297280/QDOFjqAJ.jpg" ]
        }
      } ],
      "content" : [ "We're gonna have to retire the expression \"avoid it like the plague\" because it turns out humans do not do that" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
