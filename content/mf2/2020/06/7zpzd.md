{
  "date" : "2020-06-21T10:27:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/svershbow/status/1274352466017103873" ],
    "name" : [ "Like of @svershbow's tweet" ],
    "published" : [ "2020-06-21T10:27:00+01:00" ],
    "like-of" : [ "https://twitter.com/svershbow/status/1274352466017103873" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/7zpzd",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1274352466017103873" ],
      "url" : [ "https://twitter.com/svershbow/status/1274352466017103873" ],
      "published" : [ "2020-06-20T14:44:38+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:svershbow" ],
          "numeric-id" : [ "459815426" ],
          "name" : [ "Sophie Vershbow" ],
          "nickname" : [ "svershbow" ],
          "url" : [ "https://twitter.com/svershbow", "https://www.sophievershbow.com/" ],
          "published" : [ "2012-01-10T02:44:48+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "New York, NY" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1269015440799514624/ZG1pDl9z.jpg" ]
        }
      } ],
      "content" : [ "Weird that I haven’t bought an avocado toast in more than three months and I still don’t have the down payment for a house." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
