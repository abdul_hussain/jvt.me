{
  "date" : "2020-06-20T12:08:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/joeyalison/status/1274160714890964992" ],
    "name" : [ "Like of @joeyalison's tweet" ],
    "published" : [ "2020-06-20T12:08:00+01:00" ],
    "like-of" : [ "https://twitter.com/joeyalison/status/1274160714890964992" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/egx6q",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1274160714890964992" ],
      "url" : [ "https://twitter.com/joeyalison/status/1274160714890964992" ],
      "published" : [ "2020-06-20T02:02:41+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:joeyalison" ],
          "numeric-id" : [ "312107565" ],
          "name" : [ "joey alison sayers" ],
          "nickname" : [ "joeyalison" ],
          "url" : [ "https://twitter.com/joeyalison", "http://www.jsayers.com" ],
          "published" : [ "2011-06-06T16:01:28+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Oakland, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1207517539770613761/tFgZ2oC9.jpg" ]
        }
      } ],
      "content" : [ "Let's play!" ],
      "photo" : [ "https://pbs.twimg.com/media/Ea66UchVAAAxNJ3.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
