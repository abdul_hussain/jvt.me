{
  "date" : "2020-06-27T00:19:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://events.indieweb.org/2020/06/indiewebcamp-west-2020-ZB8zoAAu6sdN", "https://indieweb.org/2020/West" ],
    "name" : [ "RSVP maybe to https://events.indieweb.org/2020/06/indiewebcamp-west-2020-ZB8zoAAu6sdN" ],
    "published" : [ "2020-06-27T00:19:00+01:00" ],
    "category" : [ ],
    "event" : {
      "start" : [ "2020-06-27" ],
      "name" : [ "IndieWebCamp West 2020" ],
      "end" : [ "2020-06-28" ],
      "url" : [ "https://events.indieweb.org/2020/06/indiewebcamp-west-2020-ZB8zoAAu6sdN", "https://indieweb.org/2020/West" ]
    },
    "rsvp" : [ "maybe" ],
    "content" : [ {
      "html" : "",
      "value" : "I'll try and attend what I can - but it may not be the best timezone-wise!"
    } ]
  },
  "kind" : "rsvps",
  "slug" : "2020/06/xxssb",
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
