{
  "date" : "2020-06-26T18:36:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/Loftio/status/1276551993906135040" ],
    "name" : [ "Like of @Loftio's tweet" ],
    "published" : [ "2020-06-26T18:36:00+01:00" ],
    "like-of" : [ "https://twitter.com/Loftio/status/1276551993906135040" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/dpoos",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1276551993906135040" ],
      "url" : [ "https://twitter.com/Loftio/status/1276551993906135040" ],
      "published" : [ "2020-06-26T16:24:47+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Loftio" ],
          "numeric-id" : [ "19788371" ],
          "name" : [ "Lex Lofthouse ✨" ],
          "nickname" : [ "Loftio" ],
          "url" : [ "https://twitter.com/Loftio", "http://loftio.co.uk" ],
          "published" : [ "2009-01-30T21:03:02+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1138757469176508417/Hc1FJP7a.png" ]
        }
      } ],
      "content" : [ {
        "value" : "@ tech & design twitter today\n👁👄👁",
        "html" : "<div style=\"white-space: pre\">@ tech &amp; design twitter today\n👁👄👁</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/Ebc5IX5WkAst0D_.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
