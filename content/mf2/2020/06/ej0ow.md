{
  "date" : "2020-06-25T19:43:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/freezydorito/status/1276224356549525504" ],
    "name" : [ "Like of @freezydorito's tweet" ],
    "published" : [ "2020-06-25T19:43:00+01:00" ],
    "like-of" : [ "https://twitter.com/freezydorito/status/1276224356549525504" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/ej0ow",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1276224356549525504" ],
      "url" : [ "https://twitter.com/freezydorito/status/1276224356549525504" ],
      "published" : [ "2020-06-25T18:42:52+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:freezydorito" ],
          "numeric-id" : [ "3004020255" ],
          "name" : [ "laura whatevers" ],
          "nickname" : [ "freezydorito" ],
          "url" : [ "https://twitter.com/freezydorito", "http://butts.farm" ],
          "published" : [ "2015-01-29T10:06:54+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "boxpark shoreditch’s ruins" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1274297202446405632/v0Y1DHR_.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "[rfc] [discussion] code proposal for a plan to refactor state management app-wide [+4,171 / -2,917]\n✅ Changes approved by 12 people 💬 2 Comments \n\n[nit] make the unread message indicator blue instead of red [+3 / -3]\n🛑 Changes Requested  💬 12,719 Comments",
        "html" : "<div style=\"white-space: pre\">[rfc] [discussion] code proposal for a plan to refactor state management app-wide [+4,171 / -2,917]\n✅ Changes approved by 12 people 💬 2 Comments \n\n[nit] make the unread message indicator blue instead of red [+3 / -3]\n🛑 Changes Requested  💬 12,719 Comments</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
