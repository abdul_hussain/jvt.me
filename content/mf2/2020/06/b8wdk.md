{
  "date" : "2020-06-21T19:00:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JacobJanerka/status/1274665569023750144" ],
    "name" : [ "Like of @JacobJanerka's tweet" ],
    "published" : [ "2020-06-21T19:00:00+01:00" ],
    "like-of" : [ "https://twitter.com/JacobJanerka/status/1274665569023750144" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/b8wdk",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1274665569023750144" ],
      "url" : [ "https://twitter.com/JacobJanerka/status/1274665569023750144" ],
      "video" : [ "https://video.twimg.com/tweet_video/EbCFK53UEAAMDZr.mp4" ],
      "published" : [ "2020-06-21T11:28:48+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:JacobJanerka" ],
          "numeric-id" : [ "518495199" ],
          "name" : [ "Jacob Janerka" ],
          "nickname" : [ "JacobJanerka" ],
          "url" : [ "https://twitter.com/JacobJanerka", "http://dungeonexperience.com/" ],
          "published" : [ "2012-03-08T13:30:38+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Australia" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1251146382431272960/nSr8yUet.jpg" ]
        }
      } ],
      "content" : [ "My new game pitch: It's a regular FPS, but all the guns are floppy." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
