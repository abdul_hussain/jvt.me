{
  "date" : "2020-06-10T17:54:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/MrsEmma/status/1270749460268793856" ],
    "name" : [ "Like of @MrsEmma's tweet" ],
    "published" : [ "2020-06-10T17:54:00+01:00" ],
    "like-of" : [ "https://twitter.com/MrsEmma/status/1270749460268793856" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/l2aax",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1270749460268793856" ],
      "url" : [ "https://twitter.com/MrsEmma/status/1270749460268793856" ],
      "published" : [ "2020-06-10T16:07:35+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:MrsEmma" ],
          "numeric-id" : [ "19500955" ],
          "name" : [ "Emma Seward" ],
          "nickname" : [ "MrsEmma" ],
          "url" : [ "https://twitter.com/MrsEmma" ],
          "published" : [ "2009-01-25T19:32:42+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1270492094495371264/P86JBCzO.jpg" ]
        }
      } ],
      "content" : [ "Just spoken to a psychologist about starting some therapy as soon as face to face appointments are available again, and I feel like a huge weight has lifted. To have someone say 'yes, that all sounds *really* hard, it's no wonder you're struggling' is reassuring and validating." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
