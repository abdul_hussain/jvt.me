{
  "date" : "2020-06-24T16:09:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/JackEllis/status/1275803121856339968" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1275810464577003524" ],
    "name" : [ "Reply to https://twitter.com/JackEllis/status/1275803121856339968" ],
    "published" : [ "2020-06-24T16:09:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "Nice, glad to hear. Fathom seems pretty nice, I do think I should give it a go"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/06/3y0kp",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1275803121856339968" ],
      "url" : [ "https://twitter.com/JackEllis/status/1275803121856339968" ],
      "published" : [ "2020-06-24T14:49:02+00:00" ],
      "in-reply-to" : [ "https://twitter.com/JamieTanna/status/1275801888861151232" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:JackEllis" ],
          "numeric-id" : [ "14964243" ],
          "name" : [ "Jack Ellis" ],
          "nickname" : [ "JackEllis" ],
          "url" : [ "https://twitter.com/JackEllis", "https://jackellis.me" ],
          "published" : [ "2008-05-31T16:19:32+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Canada / United Kingdom" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/989296839194173440/ZRsVtbV5.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Yup. All aggregated, no user-specific tracking ;)",
        "html" : "Yup. All aggregated, no user-specific tracking ;)\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
