{
  "date" : "2020-06-23T21:25:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/jackyalcine/status/1275513063186108416" ],
    "name" : [ "Like of @jackyalcine's tweet" ],
    "published" : [ "2020-06-23T21:25:00+01:00" ],
    "category" : [ "github" ],
    "like-of" : [ "https://twitter.com/jackyalcine/status/1275513063186108416" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/pcusx",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1275513063186108416" ],
      "url" : [ "https://twitter.com/jackyalcine/status/1275513063186108416" ],
      "published" : [ "2020-06-23T19:36:26+00:00" ],
      "in-reply-to" : [ "https://twitter.com/jackyalcine/status/1275510080910143489" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:jackyalcine" ],
          "numeric-id" : [ "1262855987993563136" ],
          "name" : [ "jacky. 📟🇭🇹✊🏾" ],
          "nickname" : [ "jackyalcine" ],
          "url" : [ "https://twitter.com/jackyalcine", "https://v2.jacky.wtf", "http://jacky.wtf" ],
          "published" : [ "2020-05-19T21:22:07+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Chochenyo/Ohlone (Bay Area)" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1274459690819182592/z28afA3r.jpg" ]
        }
      } ],
      "content" : [ "[GitHub has lock-in like none other but no one will comfortably admit that because of the \"\"\"social\"\"\" pressures]" ]
    }
  },
  "tags" : [ "github" ],
  "client_id" : "https://indigenous.realize.be"
}
