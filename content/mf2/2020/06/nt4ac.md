{
  "date" : "2020-06-12T17:20:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamieTanna/status/1271488783565959168" ],
    "published" : [ "2020-06-12T17:20:00+01:00" ],
    "repost-of" : [ "https://twitter.com/PFNotts/status/1271476102221815810" ],
    "category" : [ "pfnotts" ],
    "content" : [ {
      "html" : "",
      "value" : "<a href=\"/tags/pfnotts/\">#PFNotts</a> is an awesome initiative run by some awesome people - y'all should help make it better with continued support 🙌🏼"
    } ]
  },
  "kind" : "reposts",
  "slug" : "2020/06/nt4ac",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1271476102221815810" ],
      "url" : [ "https://twitter.com/PFNotts/status/1271476102221815810" ],
      "published" : [ "2020-06-12T16:15:00+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:PFNotts" ],
          "numeric-id" : [ "1075373010515824640" ],
          "name" : [ "ProjectFunction" ],
          "nickname" : [ "PFNotts" ],
          "url" : [ "https://twitter.com/PFNotts", "https://projectfunction.io/" ],
          "published" : [ "2018-12-19T12:51:19+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1267932076608835586/vww7O5vB.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "💙 Oh, we're beyond astonished by @JamieTanna for becoming our 1st VIP Patron! 🤩 This has truly just made our day.\n\nJoin Jamie in support of ProjectFunction, here: patreon.com/ProjectFunction 🐳",
        "html" : "<div style=\"white-space: pre\">💙 Oh, we're beyond astonished by <a href=\"https://twitter.com/JamieTanna\">@JamieTanna</a> for becoming our 1st VIP Patron! 🤩 This has truly just made our day.\n\nJoin Jamie in support of ProjectFunction, here: <a href=\"https://patreon.com/ProjectFunction\">patreon.com/ProjectFunction</a> 🐳</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EaUVc03X0AEXfO5.jpg" ]
    }
  },
  "tags" : [ "pfnotts" ],
  "client_id" : "https://indigenous.realize.be"
}
