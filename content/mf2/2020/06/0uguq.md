{
  "date" : "2020-06-30T23:55:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/juliaferraioli/status/1278037065754488832" ],
    "name" : [ "Like of @juliaferraioli's tweet" ],
    "published" : [ "2020-06-30T23:55:00+01:00" ],
    "like-of" : [ "https://twitter.com/juliaferraioli/status/1278037065754488832" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/0uguq",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1278037065754488832" ],
      "url" : [ "https://twitter.com/juliaferraioli/status/1278037065754488832" ],
      "published" : [ "2020-06-30T18:45:55+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:juliaferraioli" ],
          "numeric-id" : [ "16597587" ],
          "name" : [ "julia ferraioli" ],
          "nickname" : [ "juliaferraioli" ],
          "url" : [ "https://twitter.com/juliaferraioli", "https://juliaferraioli.com/blog" ],
          "published" : [ "2008-10-05T01:43:10+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Seattle, WA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1023607536530989056/9Euw7-kF.jpg" ]
        }
      } ],
      "content" : [ "Is it unprofessional to add an eyeroll emoji to a colleague's message when they're being unnecessarily pedantic? Asking for a friend." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
