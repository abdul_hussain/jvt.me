{
  "date" : "2020-06-04T21:53:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/eodsgn/status/1268623628272193536" ],
    "name" : [ "Like of @eodsgn's tweet" ],
    "published" : [ "2020-06-04T21:53:00+01:00" ],
    "like-of" : [ "https://twitter.com/eodsgn/status/1268623628272193536" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/6h2ry",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1268623628272193536" ],
      "url" : [ "https://twitter.com/eodsgn/status/1268623628272193536" ],
      "published" : [ "2020-06-04T19:20:17+00:00" ],
      "in-reply-to" : [ "https://twitter.com/a66ot/status/1268590150205964288" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:eodsgn" ],
          "numeric-id" : [ "264223385" ],
          "name" : [ "Eduardo Kerr–Oliveira" ],
          "nickname" : [ "eodsgn" ],
          "url" : [ "https://twitter.com/eodsgn", "https://www.behance.net/eodsgn" ],
          "published" : [ "2011-03-11T15:06:48+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1267930829881315329/_LwtcJdg.jpg" ]
        }
      } ],
      "location" : [ {
        "type" : [ "h-card", "p-location" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:2a3f152d1ac5044a" ],
          "name" : [ "Hackney, London" ]
        }
      } ],
      "content" : [ {
        "value" : "Not surprised: wired.co.uk/article/revolu…",
        "html" : "Not surprised: <a href=\"https://www.wired.co.uk/article/revolut-trade-unions-labour-fintech-politics-storonsky\">wired.co.uk/article/revolu…</a>\n<a class=\"u-mention\" href=\"https://twitter.com/TechCrunch\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/a66ot\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/sohear\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
