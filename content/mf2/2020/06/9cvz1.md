{
  "date" : "2020-06-08T23:25:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [  ],
    "name" : [ "Like of @TashJNorris's tweet" ],
    "published" : [ "2020-06-08T23:25:00+01:00" ],
    "category" : [ "diversity-and-inclusion" ],
    "like-of" : [ "https://twitter.com/TashJNorris/status/1270104829604626432" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/9cvz1",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1270104829604626432" ],
      "url" : [ "https://twitter.com/TashJNorris/status/1270104829604626432" ],
      "published" : [ "2020-06-08T21:26:03+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:TashJNorris" ],
          "numeric-id" : [ "19965271" ],
          "name" : [ "Tash Norris" ],
          "nickname" : [ "TashJNorris" ],
          "url" : [ "https://twitter.com/TashJNorris", "https://medium.com/@tashjnorris" ],
          "published" : [ "2009-02-03T11:25:57+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1084014099510558720/_2lwD_ZS.jpg" ]
        }
      } ],
      "content" : [ "Sigh." ],
      "photo" : [ "https://pbs.twimg.com/media/EaBRmw8XQAA9TKt.jpg" ]
    }
  },
  "tags" : [ "diversity-and-inclusion" ],
  "client_id" : "https://indigenous.realize.be"
}
