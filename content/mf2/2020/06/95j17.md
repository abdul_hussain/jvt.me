{
  "date" : "2020-06-14T18:47:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/djbaskin/status/1271965100727865344" ],
    "name" : [ "Like of @djbaskin's tweet" ],
    "published" : [ "2020-06-14T18:47:00+01:00" ],
    "like-of" : [ "https://twitter.com/djbaskin/status/1271965100727865344" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/95j17",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1271965100727865344" ],
      "url" : [ "https://twitter.com/djbaskin/status/1271965100727865344" ],
      "published" : [ "2020-06-14T00:38:06+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:djbaskin" ],
          "numeric-id" : [ "55662470" ],
          "name" : [ "Danielle Baskin" ],
          "nickname" : [ "djbaskin" ],
          "url" : [ "https://twitter.com/djbaskin", "https://www.daniellebaskin.com", "http://dialup.com" ],
          "published" : [ "2009-07-10T21:00:48+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "San Francisco, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1219233434322628608/ZD1qvkE-.jpg" ]
        }
      } ],
      "content" : [ "To make sure nobody shares a photo of you, put a GettyImages® watermark on your face mask." ],
      "photo" : [ "https://pbs.twimg.com/media/EabqiLBUEAE_tS4.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
