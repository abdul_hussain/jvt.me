{
  "date" : "2020-06-12T23:04:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/miss_jwo/status/1271545324218322945" ],
    "name" : [ "Like of @miss_jwo's tweet" ],
    "published" : [ "2020-06-12T23:04:00+01:00" ],
    "category" : [ "php", "wordpress" ],
    "like-of" : [ "https://twitter.com/miss_jwo/status/1271545324218322945" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/idakn",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1271545324218322945" ],
      "url" : [ "https://twitter.com/miss_jwo/status/1271545324218322945" ],
      "published" : [ "2020-06-12T20:50:04+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:miss_jwo" ],
          "numeric-id" : [ "200519952" ],
          "name" : [ "Jenny Wong 🐝" ],
          "nickname" : [ "miss_jwo" ],
          "url" : [ "https://twitter.com/miss_jwo", "http://jwong.co.uk" ],
          "published" : [ "2010-10-09T14:29:15+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "United Kingdom" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/839845252412358656/PLSY74LG.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Dear every #PHP developer who has complained to me about #WordPress being on old versions of PHP can now buy me and the team a fucking drink. \n\nIn covid terms - come to our meetups and chat about PHP - People wanna learn! \n\nDon't give me the travel excuse. We went digital!",
        "html" : "<div style=\"white-space: pre\">Dear every <a href=\"https://twitter.com/search?q=%23PHP\">#PHP</a> developer who has complained to me about <a href=\"https://twitter.com/search?q=%23WordPress\">#WordPress</a> being on old versions of PHP can now buy me and the team a fucking drink. \n\nIn covid terms - come to our meetups and chat about PHP - People wanna learn! \n\nDon't give me the travel excuse. We went digital!</div>"
      } ]
    }
  },
  "tags" : [ "php", "wordpress" ],
  "client_id" : "https://indigenous.realize.be"
}
