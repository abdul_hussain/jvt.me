{
  "date" : "2020-06-30T23:54:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/reillyusa/status/1277870083457593346" ],
    "name" : [ "Like of @reillyusa's tweet" ],
    "published" : [ "2020-06-30T23:54:00+01:00" ],
    "like-of" : [ "https://twitter.com/reillyusa/status/1277870083457593346" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/xpbzz",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1277870083457593346" ],
      "url" : [ "https://twitter.com/reillyusa/status/1277870083457593346" ],
      "published" : [ "2020-06-30T07:42:24+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:reillyusa" ],
          "numeric-id" : [ "15128930" ],
          "name" : [ "Christian Reilly" ],
          "nickname" : [ "reillyusa" ],
          "url" : [ "https://twitter.com/reillyusa", "https://theloosecouple.wordpress.com" ],
          "published" : [ "2008-06-15T23:35:18+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "UK. USA. Airports of the world" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1264129890519068672/-FGuWmKa.jpg" ]
        }
      } ],
      "content" : [ "It was only a matter of time." ],
      "photo" : [ "https://pbs.twimg.com/media/EbvoEOLWsAAqE80.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
