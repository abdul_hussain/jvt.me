{
  "date" : "2020-06-20T11:26:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/MrMichaelSpicer/status/1274241202230046720" ],
    "name" : [ "Like of @MrMichaelSpicer's tweet" ],
    "published" : [ "2020-06-20T11:26:00+01:00" ],
    "category" : [ "politics" ],
    "like-of" : [ "https://twitter.com/MrMichaelSpicer/status/1274241202230046720" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/gr9wn",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1274241202230046720" ],
      "url" : [ "https://twitter.com/MrMichaelSpicer/status/1274241202230046720" ],
      "video" : [ "https://video.twimg.com/ext_tw_video/1274240311938646017/pu/vid/1280x720/17lXPKQLxtFA1qfe.mp4?tag=10" ],
      "published" : [ "2020-06-20T07:22:31+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:MrMichaelSpicer" ],
          "numeric-id" : [ "93222172" ],
          "name" : [ "Michael Spicer" ],
          "nickname" : [ "MrMichaelSpicer" ],
          "url" : [ "https://twitter.com/MrMichaelSpicer", "http://www.michaelspicer.co.uk" ],
          "published" : [ "2009-11-28T17:19:58+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Kent, London" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1016265693048655872/SNWKkMs4.jpg" ]
        }
      } ],
      "content" : [ "the room next door - Dominic Raab and Taking a Knee" ]
    }
  },
  "tags" : [ "politics" ],
  "client_id" : "https://indigenous.realize.be"
}
