{
  "date" : "2020-06-25T16:47:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/rwdrich/status/1276158457423552514" ],
    "name" : [ "Like of @rwdrich's tweet" ],
    "published" : [ "2020-06-25T16:47:00+01:00" ],
    "like-of" : [ "https://twitter.com/rwdrich/status/1276158457423552514" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/jwmmw",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1276158457423552514" ],
      "url" : [ "https://twitter.com/rwdrich/status/1276158457423552514" ],
      "published" : [ "2020-06-25T14:21:00+00:00" ],
      "in-reply-to" : [ "https://twitter.com/rwdrich/status/1275799764727222278" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:rwdrich" ],
          "numeric-id" : [ "107548386" ],
          "name" : [ "Richard Davies" ],
          "nickname" : [ "rwdrich" ],
          "url" : [ "https://twitter.com/rwdrich", "http://rwdrich.co.uk" ],
          "published" : [ "2010-01-22T23:14:30+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Cambridge, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/917481919872491521/BwypzjEE.jpg" ]
        }
      } ],
      "content" : [ "I miss these days. Pushing 29 now... really modelling the waterfall methodology down my back right now." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
