{
  "date" : "2020-06-30T22:21:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/MrsEmma/status/1278056205366239235" ],
    "name" : [ "Like of @MrsEmma's tweet" ],
    "published" : [ "2020-06-30T22:21:00+01:00" ],
    "like-of" : [ "https://twitter.com/MrsEmma/status/1278056205366239235" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/2rold",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1278056205366239235" ],
      "url" : [ "https://twitter.com/MrsEmma/status/1278056205366239235" ],
      "video" : [ "https://video.twimg.com/tweet_video/EbyRVrtWAAI4rhc.mp4" ],
      "published" : [ "2020-06-30T20:01:59+00:00" ],
      "in-reply-to" : [ "https://twitter.com/MrsEmma/status/1278042318180884485" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:MrsEmma" ],
          "numeric-id" : [ "19500955" ],
          "name" : [ "Emma Seward" ],
          "nickname" : [ "MrsEmma" ],
          "url" : [ "https://twitter.com/MrsEmma" ],
          "published" : [ "2009-01-25T19:32:42+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1270492094495371264/P86JBCzO.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "And @CarolSaysThings's slides are 💯😍😍😍\n\n(And used one of my fave gifs)",
        "html" : "<div style=\"white-space: pre\">And <a href=\"https://twitter.com/CarolSaysThings\">@CarolSaysThings</a>'s slides are 💯😍😍😍\n\n(And used one of my fave gifs)</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
