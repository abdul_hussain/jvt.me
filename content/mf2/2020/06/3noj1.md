{
  "date" : "2020-06-12T07:56:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/NathOnSecurity/status/1271276609199321088" ],
    "name" : [ "Like of @NathOnSecurity's tweet" ],
    "published" : [ "2020-06-12T07:56:00+01:00" ],
    "like-of" : [ "https://twitter.com/NathOnSecurity/status/1271276609199321088" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/3noj1",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1271276609199321088" ],
      "url" : [ "https://twitter.com/NathOnSecurity/status/1271276609199321088" ],
      "published" : [ "2020-06-12T03:02:17+00:00" ],
      "in-reply-to" : [ "https://twitter.com/IanColdwater/status/1271274095456456705" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:NathOnSecurity" ],
          "numeric-id" : [ "2788383706" ],
          "name" : [ "Nathan 🏳️‍🌈" ],
          "nickname" : [ "NathOnSecurity" ],
          "url" : [ "https://twitter.com/NathOnSecurity", "https://nathan.sx" ],
          "published" : [ "2014-09-28T02:19:24+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Not So Great Britain" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1257701188704878593/O4lzfuho.jpg" ]
        }
      } ],
      "content" : [ {
        "html" : "\n<a class=\"u-mention\" href=\"https://twitter.com/IanColdwater\"></a>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EaR7VbeUcAIj2KR.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
