{
  "date" : "2020-06-30T13:58:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/CarolSaysThings/status/1277941613852008449" ],
    "name" : [ "Like of @CarolSaysThings's tweet" ],
    "published" : [ "2020-06-30T13:58:00+01:00" ],
    "category" : [ "diversity-and-inclusion" ],
    "like-of" : [ "https://twitter.com/CarolSaysThings/status/1277941613852008449" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/5qglq",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1277941613852008449" ],
      "url" : [ "https://twitter.com/CarolSaysThings/status/1277941613852008449" ],
      "published" : [ "2020-06-30T12:26:38+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:CarolSaysThings" ],
          "numeric-id" : [ "36382927" ],
          "name" : [ "Carol 😅" ],
          "nickname" : [ "CarolSaysThings" ],
          "url" : [ "https://twitter.com/CarolSaysThings", "https://carolgilabert.me/" ],
          "published" : [ "2009-04-29T15:22:13+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "🇧🇷🇪🇸🇬🇧 · Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1238515159594917889/C5994QPa.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Hey peeps! I'll be @DevopsNotts later talking (briefly) about my experiences at work and if/how they might differ from yours. See you later, maybe? 👀\n\nmeetup.com/DevOps-Notts/e…",
        "html" : "<div style=\"white-space: pre\">Hey peeps! I'll be <a href=\"https://twitter.com/DevopsNotts\">@DevopsNotts</a> later talking (briefly) about my experiences at work and if/how they might differ from yours. See you later, maybe? 👀\n\n<a href=\"https://www.meetup.com/DevOps-Notts/events/270888441/\">meetup.com/DevOps-Notts/e…</a></div>"
      } ]
    }
  },
  "tags" : [ "diversity-and-inclusion" ],
  "client_id" : "https://indigenous.realize.be"
}
