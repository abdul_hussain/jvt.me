{
  "date" : "2020-06-14T09:53:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/iamhirusi/status/1272044061344452608" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1272090776781160448" ],
    "name" : [ "Reply to https://twitter.com/iamhirusi/status/1272044061344452608" ],
    "published" : [ "2020-06-14T09:53:00+01:00" ],
    "category" : [ "indieweb" ],
    "content" : [ {
      "html" : "",
      "value" : "What in particular are you finding difficult? I'm running a Hugo site and have quite a few <a href=\"/tags/indieweb/\">#IndieWeb</a> integrations"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/06/e4shg",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1272044061344452608" ],
      "url" : [ "https://twitter.com/iamhirusi/status/1272044061344452608" ],
      "published" : [ "2020-06-14T05:51:52+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:iamhirusi" ],
          "numeric-id" : [ "1252888172700119041" ],
          "name" : [ "Ru Singh" ],
          "nickname" : [ "iamhirusi" ],
          "url" : [ "https://twitter.com/iamhirusi", "https://rusingh.com" ],
          "published" : [ "2020-04-22T09:14:03+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "India" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1258445925145591814/b0T5aa6E.png" ]
        }
      } ],
      "content" : [ {
        "value" : "Not quite happy with the #IndieWeb in the context of #static sites. Well, more than IndieWeb itself, it's the state of tooling around it. Working on adding a #Thoughts category to my blog but faced with bigger structural changes than I'd like.",
        "html" : "Not quite happy with the <a href=\"https://twitter.com/search?q=%23IndieWeb\">#IndieWeb</a> in the context of <a href=\"https://twitter.com/search?q=%23static\">#static</a> sites. Well, more than IndieWeb itself, it's the state of tooling around it. Working on adding a <a href=\"https://twitter.com/search?q=%23Thoughts\">#Thoughts</a> category to my blog but faced with bigger structural changes than I'd like."
      } ]
    }
  },
  "tags" : [ "indieweb" ],
  "client_id" : "https://indigenous.realize.be"
}
