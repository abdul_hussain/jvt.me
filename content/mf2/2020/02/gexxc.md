{
  "kind": "replies",
  "slug": "2020/02/gexxc",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1228995531143335936"
      ],
      "url": [
        "https://twitter.com/m_ott/status/1228995531143335936"
      ],
      "published": [
        "2020-02-16T10:52:22+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:m_ott"
            ],
            "numeric-id": [
              "58448784"
            ],
            "name": [
              "Matthias Ott"
            ],
            "nickname": [
              "m_ott"
            ],
            "url": [
              "https://twitter.com/m_ott"
            ],
            "published": [
              "2009-07-20T11:18:01+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Stuttgart, Germany"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1217370913487892485/oN9cGCLI.jpg"
            ]
          }
        }
      ],
      "content": [
        "One thing that I still struggle with when writing posts for my personal site is to decide whether a piece is a “note” or an “article”. I made the distinction to free me from the pressure to always put out polished articles and instead write down my thoughts more quickly."
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-16T11:10:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/m_ott/status/1228995531143335936"
    ],
    "in-reply-to": [
      "https://twitter.com/m_ott/status/1228995531143335936"
    ],
    "published": [
      "2020-02-16T11:10:00Z"
    ],
    "content": [
      {
        "html": "",
        "value": "I think of notes in terms of https://indieweb.org/post-type-discovery and them being short form content similar to a tweet (but without a size limit) without a title"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1229001130627260417"
    ]
  }
}
