{
  "kind": "likes",
  "slug": "2020/02/dzics",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1225949531512197126"
      ],
      "url": [
        "https://twitter.com/IanColdwater/status/1225949531512197126"
      ],
      "published": [
        "2020-02-08T01:08:40+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:IanColdwater"
            ],
            "numeric-id": [
              "711367366756720640"
            ],
            "name": [
              "Ian Coldwater"
            ],
            "nickname": [
              "IanColdwater"
            ],
            "url": [
              "https://twitter.com/IanColdwater",
              "https://pronoun.is/they"
            ],
            "published": [
              "2016-03-20T01:42:51+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Minnesota"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1223394146498568197/FEePrJxI.jpg"
            ]
          }
        }
      ],
      "content": [
        "We are all made of stars, but your RBAC shouldn't be"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-08T10:18:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @IanColdwater's tweet"
    ],
    "like-of": [
      "https://twitter.com/IanColdwater/status/1225949531512197126"
    ],
    "published": [
      "2020-02-08T10:18:00Z"
    ],
    "syndication": [
      "https://twitter.com/IanColdwater/status/1225949531512197126"
    ]
  }
}
