{
  "kind": "likes",
  "slug": "2020/02/48dxi",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1232932533093924864"
      ],
      "url": [
        "https://twitter.com/WiT_Notts/status/1232932533093924864"
      ],
      "published": [
        "2020-02-27T07:36:37+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:WiT_Notts"
            ],
            "numeric-id": [
              "4339158441"
            ],
            "name": [
              "Women In Tech, Nottingham 🌈✨"
            ],
            "nickname": [
              "WiT_Notts"
            ],
            "url": [
              "https://twitter.com/WiT_Notts",
              "https://nott.tech/wit"
            ],
            "published": [
              "2015-12-01T11:22:25+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/913380298829844481/YvPcjPlE.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "📢One week today!!!\n\nJoin us 5th March @Antenna_UK 6.30pm for\n\n⚡️⚡️ LIGHTNING TALKS ⚡️⚡️\n\nThe audience take over with 5 minute talks on any subject of their choosing 🎉✨\n\nNo need to sign up, just come along!\n👉 nott.tech/wit-march",
          "html": "<div style=\"white-space: pre\">📢One week today!!!\n\nJoin us 5th March <a href=\"https://twitter.com/Antenna_UK\">@Antenna_UK</a> 6.30pm for\n\n⚡️⚡️ LIGHTNING TALKS ⚡️⚡️\n\nThe audience take over with 5 minute talks on any subject of their choosing 🎉✨\n\nNo need to sign up, just come along!\n👉 <a href=\"http://nott.tech/wit-march\">nott.tech/wit-march</a></div>"
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/ERxBFMNXYAERNUJ.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-27T07:44:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @WiT_Notts's tweet"
    ],
    "like-of": [
      "https://twitter.com/WiT_Notts/status/1232932533093924864"
    ],
    "published": [
      "2020-02-27T07:44:00Z"
    ],
    "syndication": [
      "https://twitter.com/WiT_Notts/status/1232932533093924864"
    ]
  }
}
