{
  "kind": "likes",
  "slug": "2020/02/z0wm1",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1233271588012740608"
      ],
      "url": [
        "https://twitter.com/jerebloom/status/1233271588012740608"
      ],
      "published": [
        "2020-02-28T06:03:54+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/Humphreytalks/status/1233184797507256320"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:jerebloom"
            ],
            "numeric-id": [
              "35963335"
            ],
            "name": [
              "he"
            ],
            "nickname": [
              "jerebloom"
            ],
            "url": [
              "https://twitter.com/jerebloom"
            ],
            "published": [
              "2009-04-28T02:47:54+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1232526906395750400/dCNa3kCJ.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Thanks! What’s crazy is that if you made $10,000 an hour and work 40 hours per week, it would take 6000 years to make that much money.",
          "html": "Thanks! What’s crazy is that if you made $10,000 an hour and work 40 hours per week, it would take 6000 years to make that much money.\n<a class=\"u-mention\" href=\"https://twitter.com/Humphreytalks\"></a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-28T17:40:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @jerebloom's tweet"
    ],
    "like-of": [
      "https://twitter.com/jerebloom/status/1233271588012740608"
    ],
    "published": [
      "2020-02-28T17:40:00Z"
    ],
    "syndication": [
      "https://twitter.com/jerebloom/status/1233271588012740608"
    ]
  }
}
