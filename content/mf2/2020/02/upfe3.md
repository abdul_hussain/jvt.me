{
  "kind": "likes",
  "slug": "2020/02/upfe3",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1227285174011998208"
      ],
      "url": [
        "https://twitter.com/ShotsOfNotts/status/1227285174011998208"
      ],
      "video": [
        "https://video.twimg.com/ext_tw_video/1199863622454763520/pu/vid/640x640/GYpe9bjSrvz2uZ2j.mp4?tag=10"
      ],
      "published": [
        "2020-02-11T17:36:01+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:ShotsOfNotts"
            ],
            "numeric-id": [
              "960165273990041601"
            ],
            "name": [
              "Shots Of Notts 📷"
            ],
            "nickname": [
              "ShotsOfNotts"
            ],
            "url": [
              "https://twitter.com/ShotsOfNotts"
            ],
            "published": [
              "2018-02-04T14:56:55+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, Notts."
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1075404412523372546/p1oeYo9w.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Gridlock in Nottingham... #CliftonBridge #A52 \n",
          "html": "<div style=\"white-space: pre\">Gridlock in Nottingham... <a href=\"https://twitter.com/search?q=%23CliftonBridge\">#CliftonBridge</a> <a href=\"https://twitter.com/search?q=%23A52\">#A52</a> \n</div>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-13T07:24:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @ShotsOfNotts's tweet"
    ],
    "like-of": [
      "https://twitter.com/ShotsOfNotts/status/1227285174011998208"
    ],
    "published": [
      "2020-02-13T07:24:00Z"
    ],
    "syndication": [
      "https://twitter.com/ShotsOfNotts/status/1227285174011998208"
    ]
  }
}
