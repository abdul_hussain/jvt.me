{
  "kind": "likes",
  "slug": "2020/02/crd2p",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1232966838738399235"
      ],
      "url": [
        "https://twitter.com/__jcmc__/status/1232966838738399235"
      ],
      "published": [
        "2020-02-27T09:52:56+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:__jcmc__"
            ],
            "numeric-id": [
              "781588467654483968"
            ],
            "name": [
              "Jennifer Mackown"
            ],
            "nickname": [
              "__jcmc__"
            ],
            "url": [
              "https://twitter.com/__jcmc__",
              "http://www.jcmc.xyz"
            ],
            "published": [
              "2016-09-29T20:16:26+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Lincoln"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1137074636213116929/AjJAzHty.png"
            ]
          }
        }
      ],
      "content": [
        "Hoping today goes a bit better"
      ],
      "photo": [
        "https://pbs.twimg.com/media/ERxgtzMWAAEb70C.png"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-27T10:27:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @__jcmc__'s tweet"
    ],
    "like-of": [
      "https://twitter.com/__jcmc__/status/1232966838738399235"
    ],
    "published": [
      "2020-02-27T10:27:00Z"
    ],
    "syndication": [
      "https://twitter.com/__jcmc__/status/1232966838738399235"
    ]
  }
}
