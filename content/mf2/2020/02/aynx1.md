{
  "kind": "likes",
  "slug": "2020/02/aynx1",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1231008674090344449"
      ],
      "url": [
        "https://twitter.com/indygreg/status/1231008674090344449"
      ],
      "published": [
        "2020-02-22T00:11:53+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:indygreg"
            ],
            "numeric-id": [
              "15626745"
            ],
            "name": [
              "indygreg"
            ],
            "nickname": [
              "indygreg"
            ],
            "url": [
              "https://twitter.com/indygreg",
              "https://gregoryszorc.com"
            ],
            "published": [
              "2008-07-28T02:24:02+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "San Francisco, CA"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/577168676/me05.jpg"
            ]
          }
        }
      ],
      "content": [
        "A hallmark of modern CI is spending 10 minutes to build a Docker image to run a process for 5s."
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-23T11:27:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @indygreg's tweet"
    ],
    "like-of": [
      "https://twitter.com/indygreg/status/1231008674090344449"
    ],
    "published": [
      "2020-02-23T11:27:00Z"
    ],
    "syndication": [
      "https://twitter.com/indygreg/status/1231008674090344449"
    ]
  }
}
