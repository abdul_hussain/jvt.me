{
  "kind": "likes",
  "slug": "2020/02/nskxn",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1231884407507542017"
      ],
      "url": [
        "https://twitter.com/NetflixIndia/status/1231884407507542017"
      ],
      "published": [
        "2020-02-24T10:11:44+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:NetflixIndia"
            ],
            "numeric-id": [
              "4502936240"
            ],
            "name": [
              "Netflix India"
            ],
            "nickname": [
              "NetflixIndia"
            ],
            "url": [
              "https://twitter.com/NetflixIndia"
            ],
            "published": [
              "2015-12-09T02:40:41+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1072124554808422401/s8K84jEW.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "A wise detective once discovered this emoji:\n\n🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒",
          "html": "<div style=\"white-space: pre\">A wise detective once discovered this emoji:\n\n🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒🆒</div>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-24T19:51:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @NetflixIndia's tweet"
    ],
    "like-of": [
      "https://twitter.com/NetflixIndia/status/1231884407507542017"
    ],
    "published": [
      "2020-02-24T19:51:00Z"
    ],
    "syndication": [
      "https://twitter.com/NetflixIndia/status/1231884407507542017"
    ]
  }
}
