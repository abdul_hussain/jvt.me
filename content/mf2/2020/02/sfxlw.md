{
  "kind": "likes",
  "slug": "2020/02/sfxlw",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1232384355072233472"
      ],
      "url": [
        "https://twitter.com/deniseyu21/status/1232384355072233472"
      ],
      "published": [
        "2020-02-25T19:18:21+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:deniseyu21"
            ],
            "numeric-id": [
              "25458567"
            ],
            "name": [
              "Denise Yu"
            ],
            "nickname": [
              "deniseyu21"
            ],
            "url": [
              "https://twitter.com/deniseyu21",
              "http://deniseyu.io",
              "http://deniseyu.io/art"
            ],
            "published": [
              "2009-03-20T04:17:20+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Toronto, Ontario"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/471064769097523200/dOghyD0J.jpeg"
            ]
          }
        }
      ],
      "content": [
        "the \"HA\" in HAproxy doesn't stand for anything, you're actually just meant to giggle out loud every time you talk about it"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-25T20:19:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @deniseyu21's tweet"
    ],
    "like-of": [
      "https://twitter.com/deniseyu21/status/1232384355072233472"
    ],
    "published": [
      "2020-02-25T20:19:00Z"
    ],
    "syndication": [
      "https://twitter.com/deniseyu21/status/1232384355072233472"
    ]
  }
}
