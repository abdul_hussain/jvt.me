{
  "kind": "likes",
  "slug": "2020/02/d67qp",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1233084492840980481"
      ],
      "url": [
        "https://twitter.com/QuinnyPig/status/1233084492840980481"
      ],
      "published": [
        "2020-02-27T17:40:27+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:QuinnyPig"
            ],
            "numeric-id": [
              "97114171"
            ],
            "name": [
              "Corey Quinn"
            ],
            "nickname": [
              "QuinnyPig"
            ],
            "url": [
              "https://twitter.com/QuinnyPig",
              "http://www.duckbillgroup.com",
              "http://lastweekinaws.com",
              "http://screaminginthecloud.com"
            ],
            "published": [
              "2009-12-16T02:19:14+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "San Francisco, CA"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1093667107278147584/rKaSDcaB.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Sometimes when I can’t figure out how to deploy an @awscloud service, I restrict an IAM user to just that service and check the keys in publicly just to see how the attacker configures it.",
          "html": "Sometimes when I can’t figure out how to deploy an <a href=\"https://twitter.com/awscloud\">@awscloud</a> service, I restrict an IAM user to just that service and check the keys in publicly just to see how the attacker configures it."
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-27T19:18:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @QuinnyPig's tweet"
    ],
    "like-of": [
      "https://twitter.com/QuinnyPig/status/1233084492840980481"
    ],
    "published": [
      "2020-02-27T19:18:00Z"
    ],
    "syndication": [
      "https://twitter.com/QuinnyPig/status/1233084492840980481"
    ]
  }
}
