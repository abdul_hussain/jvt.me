{
  "kind": "likes",
  "slug": "2020/02/c0tpi",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1231147451832307712"
      ],
      "url": [
        "https://twitter.com/pearlylondon/status/1231147451832307712"
      ],
      "published": [
        "2020-02-22T09:23:20+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:pearlylondon"
            ],
            "numeric-id": [
              "749252172492185600"
            ],
            "name": [
              "𝐏𝐞𝐚𝐫𝐥𝐲"
            ],
            "nickname": [
              "pearlylondon"
            ],
            "url": [
              "https://twitter.com/pearlylondon"
            ],
            "published": [
              "2016-07-02T14:43:33+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Bedlam Asylum."
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1223725901801762818/GwuqIY6o.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Whoever nicknamed the ulnar nerve 'funny bone' obviously never twatted theirs on a radiator. \n\n'Tourette's bone' is far more appropriate.",
          "html": "<div style=\"white-space: pre\">Whoever nicknamed the ulnar nerve 'funny bone' obviously never twatted theirs on a radiator. \n\n'Tourette's bone' is far more appropriate.</div>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-22T21:51:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @pearlylondon's tweet"
    ],
    "like-of": [
      "https://twitter.com/pearlylondon/status/1231147451832307712"
    ],
    "published": [
      "2020-02-22T21:51:00Z"
    ],
    "syndication": [
      "https://twitter.com/pearlylondon/status/1231147451832307712"
    ]
  }
}
