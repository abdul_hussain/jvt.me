{
  "kind": "likes",
  "slug": "2020/02/r5vlx",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1225457179102195714"
      ],
      "url": [
        "https://twitter.com/hankchizljaw/status/1225457179102195714"
      ],
      "published": [
        "2020-02-06T16:32:14+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:hankchizljaw"
            ],
            "numeric-id": [
              "98734097"
            ],
            "name": [
              "Andy Bell"
            ],
            "nickname": [
              "hankchizljaw"
            ],
            "url": [
              "https://twitter.com/hankchizljaw",
              "https://hankchizljaw.com"
            ],
            "published": [
              "2009-12-22T22:28:06+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Cheltenham, UK"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1219208545138290689/XoucaVnN.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "I'm seeing very few blog posts on Medium or DEV and a lot more on personal sites and that is great and it makes me happy.\n\nSend me your RSS feeds.\n\nmicroblog.hankchizljaw.com/1581006620/",
          "html": "<div style=\"white-space: pre\">I'm seeing very few blog posts on Medium or DEV and a lot more on personal sites and that is great and it makes me happy.\n\nSend me your RSS feeds.\n\n<a href=\"https://microblog.hankchizljaw.com/1581006620/\">microblog.hankchizljaw.com/1581006620/</a></div>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-11T07:58:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @hankchizljaw's tweet"
    ],
    "like-of": [
      "https://twitter.com/hankchizljaw/status/1225457179102195714"
    ],
    "published": [
      "2020-02-11T07:58:00Z"
    ],
    "syndication": [
      "https://twitter.com/hankchizljaw/status/1225457179102195714"
    ]
  }
}
