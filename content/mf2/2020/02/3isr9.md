{
  "kind": "replies",
  "slug": "2020/02/3isr9",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1226144161608265728"
      ],
      "url": [
        "https://twitter.com/roytang/status/1226144161608265728"
      ],
      "published": [
        "2020-02-08T14:02:03+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/JamieTanna/status/1225915761450999810"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:roytang"
            ],
            "numeric-id": [
              "15879320"
            ],
            "name": [
              "Roy Tang"
            ],
            "nickname": [
              "roytang"
            ],
            "url": [
              "https://twitter.com/roytang",
              "https://roytang.net/"
            ],
            "published": [
              "2008-08-17T03:41:45+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Philippines"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1052748331519225857/SYa3OZmz.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Hello! Not sure if you're aware, but Inoreader dislikes your RSS feed apparently for having too many items.",
          "html": "Hello! Not sure if you're aware, but Inoreader dislikes your RSS feed apparently for having too many items.\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>"
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/EQQjnUQU0AA6W7x.png"
      ]
    }
  },
  "client_id": "https://monocle.p3k.io/",
  "date": "2020-02-08T15:50:49.092+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/roytang/status/1226144161608265728"
    ],
    "in-reply-to": [
      "https://twitter.com/roytang/status/1226144161608265728"
    ],
    "published": [
      "2020-02-08T15:50:49.092+01:00"
    ],
    "content": [
      {
        "html": "",
        "value": "Sorry to hear that - I think part of it is due to my feed including most things on my site, which I need to fix at some point - if you want to just subscribe to my blog posts, https://www.jvt.me/posts/feed.xml should do it for now!"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1226157460773359616"
    ]
  }
}
