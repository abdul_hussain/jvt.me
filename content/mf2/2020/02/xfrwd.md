{
  "kind": "replies",
  "slug": "2020/02/xfrwd",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1224038155701100545"
      ],
      "url": [
        "https://twitter.com/Cambridgeport90/status/1224038155701100545"
      ],
      "published": [
        "2020-02-02T18:33:32+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/JamieTanna/status/1224037846203424770"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:Cambridgeport90"
            ],
            "numeric-id": [
              "47860297"
            ],
            "name": [
              "Katherine Moss"
            ],
            "nickname": [
              "Cambridgeport90"
            ],
            "url": [
              "https://twitter.com/Cambridgeport90",
              "http://cambridgeport90.net"
            ],
            "published": [
              "2009-06-17T04:52:23+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Wilmington, MA"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/442822138806812672/NiWMJ1Y4.jpeg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "How does it work for some like myself using Wordpress, though? If using something like Quill to post, then, do I just paste the result of Granary's translation in the URL field?",
          "html": "How does it work for some like myself using Wordpress, though? If using something like Quill to post, then, do I just paste the result of Granary's translation in the URL field?\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-02T18:39:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/Cambridgeport90/status/1224038155701100545"
    ],
    "in-reply-to": [
      "https://twitter.com/Cambridgeport90/status/1224038155701100545"
    ],
    "published": [
      "2020-02-02T18:39:00Z"
    ],
    "content": [
      {
        "html": "",
        "value": "I don't think so no. I've done this on my Micropub side, so it stores it in the same content format as the actual post. I guess we'd likely do something similar, or when loading the page pulling that data from Granary?"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1224040684287598593"
    ]
  }
}
