{
  "kind": "replies",
  "slug": "2020/02/se1uy",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1225500647665127425"
      ],
      "url": [
        "https://twitter.com/CarolSaysThings/status/1225500647665127425"
      ],
      "video": [
        "https://video.twimg.com/tweet_video/EQHaVuoXUAEOdr-.mp4"
      ],
      "published": [
        "2020-02-06T19:24:57+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/MrAndrew/status/1225496730659172353"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:CarolSaysThings"
            ],
            "numeric-id": [
              "36382927"
            ],
            "name": [
              "Carol ⚡️"
            ],
            "nickname": [
              "CarolSaysThings"
            ],
            "url": [
              "https://twitter.com/CarolSaysThings",
              "https://carolgilabert.me/"
            ],
            "published": [
              "2009-04-29T15:22:13+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "🇧🇷🇪🇸🇬🇧 · Nottingham"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1218543968000139267/GEWFadbR.jpg"
            ]
          }
        }
      ],
      "location": [
        {
          "type": [
            "h-card",
            "p-location"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:7d7bdec12d2549d4"
            ],
            "name": [
              "Nottingham, England"
            ]
          }
        }
      ],
      "content": [
        {
          "html": "\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/MrAndrew\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/anna_hax\"></a>"
        }
      ]
    }
  },
  "client_id": "https://micropublish.net",
  "date": "2020-02-06T20:33:26.948+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/CarolSaysThings/status/1225500647665127425"
    ],
    "in-reply-to": [
      "https://twitter.com/CarolSaysThings/status/1225500647665127425"
    ],
    "published": [
      "2020-02-06T20:33:26.948+01:00"
    ],
    "content": [
      {
        "html": "",
        "value": "On the plus side, you found an edge case for me! Should've tested it better 😬"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1225503888226254848"
    ]
  }
}
