{
  "kind": "likes",
  "slug": "2020/02/4fwjg",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1224793942190780416"
      ],
      "url": [
        "https://twitter.com/TylerDinucci/status/1224793942190780416"
      ],
      "published": [
        "2020-02-04T20:36:46+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:TylerDinucci"
            ],
            "numeric-id": [
              "20714338"
            ],
            "name": [
              "Tyler Dinucci"
            ],
            "nickname": [
              "TylerDinucci"
            ],
            "url": [
              "https://twitter.com/TylerDinucci"
            ],
            "published": [
              "2009-02-12T20:30:41+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "America!"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1136693439951544320/2TTKgcWY.jpg"
            ]
          }
        }
      ],
      "content": [
        "Hey guys, was let go from my job today 😟. If anyone has any leads, please let me know. I’ve attached my resume. References upon request!"
      ],
      "photo": [
        "https://pbs.twimg.com/media/EP9XhdvU0AAxKy9.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-05T22:11:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @TylerDinucci's tweet"
    ],
    "like-of": [
      "https://twitter.com/TylerDinucci/status/1224793942190780416"
    ],
    "published": [
      "2020-02-05T22:11:00Z"
    ],
    "syndication": [
      "https://twitter.com/TylerDinucci/status/1224793942190780416"
    ]
  }
}
