{
  "kind": "replies",
  "slug": "2020/02/5itka",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1227640122243502080"
      ],
      "url": [
        "https://twitter.com/fragmad/status/1227640122243502080"
      ],
      "published": [
        "2020-02-12T17:06:28+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/CarolSaysThings/status/1227639588325351425"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:fragmad"
            ],
            "numeric-id": [
              "7396842"
            ],
            "name": [
              "Will Ellwood"
            ],
            "nickname": [
              "fragmad"
            ],
            "url": [
              "https://twitter.com/fragmad",
              "http://www.will-ellwood.com/"
            ],
            "published": [
              "2007-07-11T08:57:29+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Bloody England."
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/922398676441616384/ER8vV1Bf.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Clearly @JamieTanna doesn't use Arch  properly. It isn't in his Twitter bio.",
          "html": "Clearly @JamieTanna doesn't use Arch  properly. It isn't in his Twitter bio.\n<a class=\"u-mention\" href=\"https://twitter.com/CarolSaysThings\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/anna_hax\"></a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-12T17:53:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/fragmad/status/1227640122243502080"
    ],
    "in-reply-to": [
      "https://twitter.com/fragmad/status/1227640122243502080"
    ],
    "published": [
      "2020-02-12T17:53:00Z"
    ],
    "content": [
      {
        "html": "",
        "value": "But it is on my website, such is more important https://www.jvt.me/now/ 😉 "
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1227653308292182018"
    ]
  }
}
