{
  "kind": "likes",
  "slug": "2020/02/p585j",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1226969936226062338"
      ],
      "url": [
        "https://twitter.com/technottingham/status/1226969936226062338"
      ],
      "video": [
        "https://video.twimg.com/tweet_video/EQcSpjpWsAARBXJ.mp4"
      ],
      "published": [
        "2020-02-10T20:43:23+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/JamieTanna/status/1226967847726919680"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:technottingham"
            ],
            "numeric-id": [
              "384492431"
            ],
            "name": [
              "Tech Nottingham"
            ],
            "nickname": [
              "technottingham"
            ],
            "url": [
              "https://twitter.com/technottingham",
              "http://technottingham.com"
            ],
            "published": [
              "2011-10-03T19:47:31+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1023974499757293570/ZoPc_QsO.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "html": "\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-10T20:43:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @technottingham's tweet"
    ],
    "like-of": [
      "https://twitter.com/technottingham/status/1226969936226062338"
    ],
    "published": [
      "2020-02-10T20:43:00Z"
    ],
    "syndication": [
      "https://twitter.com/technottingham/status/1226969936226062338"
    ]
  }
}
