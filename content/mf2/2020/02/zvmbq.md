{
  "kind": "replies",
  "slug": "2020/02/zvmbq",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1229465579393822720"
      ],
      "url": [
        "https://twitter.com/rwdrich/status/1229465579393822720"
      ],
      "published": [
        "2020-02-17T18:00:11+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:rwdrich"
            ],
            "numeric-id": [
              "107548386"
            ],
            "name": [
              "Richard Davies"
            ],
            "nickname": [
              "rwdrich"
            ],
            "url": [
              "https://twitter.com/rwdrich",
              "http://rwdrich.co.uk"
            ],
            "published": [
              "2010-01-22T23:14:30+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Cambridge, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/917481919872491521/BwypzjEE.jpg"
            ]
          }
        }
      ],
      "content": [
        "Finally got round to updating to ubuntu 19.10 on my desktop, and now it wont boot 😪"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-18T07:20:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/rwdrich/status/1229465579393822720"
    ],
    "in-reply-to": [
      "https://twitter.com/rwdrich/status/1229465579393822720"
    ],
    "published": [
      "2020-02-18T07:20:00Z"
    ],
    "content": [
      {
        "html": "",
        "value": "Should've bought a mac 🤷🏽‍♂️"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1229668272666267648"
    ]
  }
}
