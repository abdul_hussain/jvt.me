{
  "kind": "replies",
  "slug": "2020/02/i0chp",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1229702932989386752"
      ],
      "url": [
        "https://twitter.com/rwdrich/status/1229702932989386752"
      ],
      "published": [
        "2020-02-18T09:43:20+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/JamieTanna/status/1229668272666267648"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:rwdrich"
            ],
            "numeric-id": [
              "107548386"
            ],
            "name": [
              "Richard Davies"
            ],
            "nickname": [
              "rwdrich"
            ],
            "url": [
              "https://twitter.com/rwdrich",
              "http://rwdrich.co.uk"
            ],
            "published": [
              "2010-01-22T23:14:30+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Cambridge, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/917481919872491521/BwypzjEE.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "I miss you <3",
          "html": "I miss you &lt;3\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-18T12:13:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/rwdrich/status/1229702932989386752"
    ],
    "in-reply-to": [
      "https://twitter.com/rwdrich/status/1229702932989386752"
    ],
    "published": [
      "2020-02-18T12:13:00Z"
    ],
    "content": [
      {
        "html": "",
        "value": "Miss you too dude! You should just move back to Notts 😝 "
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1229742104227782658"
    ]
  }
}
