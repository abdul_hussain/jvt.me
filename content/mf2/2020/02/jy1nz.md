{
  "kind": "likes",
  "slug": "2020/02/jy1nz",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1228265282101284865"
      ],
      "url": [
        "https://twitter.com/manylittlewords/status/1228265282101284865"
      ],
      "published": [
        "2020-02-14T10:30:37+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/manylittlewords/status/1228265280423546881"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:manylittlewords"
            ],
            "numeric-id": [
              "333383613"
            ],
            "name": [
              "Gemma Amor"
            ],
            "nickname": [
              "manylittlewords"
            ],
            "url": [
              "https://twitter.com/manylittlewords",
              "https://www.amazon.com/gp/aw/d/1690854154/ref=tmm_pap_title_0?ie=UTF8&qid=1569016531&sr=8-1"
            ],
            "published": [
              "2011-07-11T13:13:00+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Bristol"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1209879541956710400/wKrLgLFw.jpg"
            ]
          }
        }
      ],
      "content": [
        "A ‘cheeky nandos’ is sadly still a thing"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-15T11:37:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @manylittlewords's tweet"
    ],
    "like-of": [
      "https://twitter.com/manylittlewords/status/1228265282101284865"
    ],
    "published": [
      "2020-02-15T11:37:00Z"
    ],
    "syndication": [
      "https://twitter.com/manylittlewords/status/1228265282101284865"
    ]
  }
}
