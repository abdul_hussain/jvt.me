{
  "kind": "likes",
  "slug": "2020/02/xb875",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1231303622790656001"
      ],
      "url": [
        "https://twitter.com/MLHacks/status/1231303622790656001"
      ],
      "published": [
        "2020-02-22T19:43:54+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:MLHacks"
            ],
            "numeric-id": [
              "1674737485"
            ],
            "name": [
              "Major League Hacking"
            ],
            "nickname": [
              "MLHacks"
            ],
            "url": [
              "https://twitter.com/MLHacks",
              "https://mlh.io"
            ],
            "published": [
              "2013-08-16T03:42:11+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "New York, NY"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1184141979493568515/NMa0vlIb.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Want an exclusive MLH 2020 season t-shirt provided by @CapitalOne? We are giving them out @calvinhacks so make sure to grab one before they run out! #CalvinHacks",
          "html": "Want an exclusive MLH 2020 season t-shirt provided by <a href=\"https://twitter.com/CapitalOne\">@CapitalOne</a>? We are giving them out <a href=\"https://twitter.com/calvinhacks\">@calvinhacks</a> so make sure to grab one before they run out! <a href=\"https://twitter.com/search?q=%23CalvinHacks\">#CalvinHacks</a>"
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/ERZ4HqCXsAEta6V.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-22T19:48:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @MLHacks's tweet"
    ],
    "like-of": [
      "https://twitter.com/MLHacks/status/1231303622790656001"
    ],
    "published": [
      "2020-02-22T19:48:00Z"
    ],
    "syndication": [
      "https://twitter.com/MLHacks/status/1231303622790656001"
    ]
  }
}
