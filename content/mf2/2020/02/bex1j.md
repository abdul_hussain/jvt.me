{
  "kind": "reposts",
  "slug": "2020/02/bex1j",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1229074066369327107"
      ],
      "url": [
        "https://twitter.com/MayorofLondon/status/1229074066369327107"
      ],
      "published": [
        "2020-02-16T16:04:27+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:MayorofLondon"
            ],
            "numeric-id": [
              "14700117"
            ],
            "name": [
              "Mayor of London"
            ],
            "nickname": [
              "MayorofLondon"
            ],
            "url": [
              "https://twitter.com/MayorofLondon",
              "http://www.london.gov.uk/"
            ],
            "published": [
              "2008-05-08T13:23:35+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "London"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1194227613352562688/aXbKb3pE.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Trans women are women.\nTrans men are men.\nNon-binary people are non-binary.\nAll gender identities are valid.\n \n#LGBTHistoryMonth",
          "html": "<div style=\"white-space: pre\">Trans women are women.\nTrans men are men.\nNon-binary people are non-binary.\nAll gender identities are valid.\n \n<a href=\"https://twitter.com/search?q=%23LGBTHistoryMonth\">#LGBTHistoryMonth</a></div>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-17T17:47:00Z",
  "h": "h-entry",
  "properties": {
    "published": [
      "2020-02-17T17:47:00Z"
    ],
    "repost-of": [
      "https://twitter.com/MayorofLondon/status/1229074066369327107"
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1229463633752739843"
    ]
  }
}
