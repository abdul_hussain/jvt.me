{
  "kind": "likes",
  "slug": "2020/02/sqkqp",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1231934107052961792"
      ],
      "url": [
        "https://twitter.com/jonathansfrakes/status/1231934107052961792"
      ],
      "published": [
        "2020-02-24T13:29:14+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:jonathansfrakes"
            ],
            "numeric-id": [
              "325832193"
            ],
            "name": [
              "Jonathan Frakes"
            ],
            "nickname": [
              "jonathansfrakes"
            ],
            "url": [
              "https://twitter.com/jonathansfrakes"
            ],
            "published": [
              "2011-06-28T23:12:18+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1147829969558618112/kw17a9g9.png"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "That’s a wrap 🖖🏾 congrats to my family ⁦@startrekcbs⁩ #Disco",
          "html": "That’s a wrap 🖖🏾 congrats to my family ⁦<a href=\"https://twitter.com/startrekcbs\">@startrekcbs</a>⁩ <a href=\"https://twitter.com/search?q=%23Disco\">#Disco</a>"
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/ERi1ih0UEAAhVPG.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-25T08:58:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @jonathansfrakes's tweet"
    ],
    "like-of": [
      "https://twitter.com/jonathansfrakes/status/1231934107052961792"
    ],
    "published": [
      "2020-02-25T08:58:00Z"
    ],
    "syndication": [
      "https://twitter.com/jonathansfrakes/status/1231934107052961792"
    ]
  }
}
