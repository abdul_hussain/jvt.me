{
  "kind": "likes",
  "slug": "2020/02/qeljw",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1228758330769903618"
      ],
      "url": [
        "https://twitter.com/ScribblingOn/status/1228758330769903618"
      ],
      "published": [
        "2020-02-15T19:09:49+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:ScribblingOn"
            ],
            "numeric-id": [
              "291009942"
            ],
            "name": [
              "Shubheksha ✨"
            ],
            "nickname": [
              "ScribblingOn"
            ],
            "url": [
              "https://twitter.com/ScribblingOn",
              "https://www.shubheksha.com"
            ],
            "published": [
              "2011-05-01T10:03:57+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "A bubble in England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1131333207780220928/Hpejkti5.jpg"
            ]
          }
        }
      ],
      "content": [
        "Nothing in life prepares you to navigate the AWS console. Nothing."
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-15T19:47:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @ScribblingOn's tweet"
    ],
    "like-of": [
      "https://twitter.com/ScribblingOn/status/1228758330769903618"
    ],
    "published": [
      "2020-02-15T19:47:00Z"
    ],
    "syndication": [
      "https://twitter.com/ScribblingOn/status/1228758330769903618"
    ]
  }
}
