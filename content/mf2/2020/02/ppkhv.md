{
  "kind": "reposts",
  "slug": "2020/02/ppkhv",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1228319821047246848"
      ],
      "url": [
        "https://twitter.com/venikunche/status/1228319821047246848"
      ],
      "published": [
        "2020-02-14T14:07:21+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:venikunche"
            ],
            "numeric-id": [
              "297102985"
            ],
            "name": [
              "Veni Kunche"
            ],
            "nickname": [
              "venikunche"
            ],
            "url": [
              "https://twitter.com/venikunche",
              "http://veni.dev"
            ],
            "published": [
              "2011-05-11T23:07:54+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Washington, DC"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1192278417934303232/WH8LwUm3.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "I'm going to be interviewed by a student for her entrepreneurship class.\n\nShe messaged me late last night letting me know that she is a Muslim & that she wears a Niqab and asked if I was ok w/ that.\n\nI can't imagine how the world treats her..that she had to ask this. 💔",
          "html": "<div style=\"white-space: pre\">I'm going to be interviewed by a student for her entrepreneurship class.\n\nShe messaged me late last night letting me know that she is a Muslim &amp; that she wears a Niqab and asked if I was ok w/ that.\n\nI can't imagine how the world treats her..that she had to ask this. 💔</div>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-14T17:17:00Z",
  "h": "h-entry",
  "properties": {
    "published": [
      "2020-02-14T17:17:00Z"
    ],
    "repost-of": [
      "https://twitter.com/venikunche/status/1228319821047246848"
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1228368851060035585"
    ]
  }
}
