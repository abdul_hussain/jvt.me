{
  "kind": "likes",
  "slug": "2020/02/hdfff",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1231602318732386305"
      ],
      "url": [
        "https://twitter.com/CarolSaysThings/status/1231602318732386305"
      ],
      "published": [
        "2020-02-23T15:30:49+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:CarolSaysThings"
            ],
            "numeric-id": [
              "36382927"
            ],
            "name": [
              "Carol 🤸‍♀️"
            ],
            "nickname": [
              "CarolSaysThings"
            ],
            "url": [
              "https://twitter.com/CarolSaysThings",
              "https://carolgilabert.me/"
            ],
            "published": [
              "2009-04-29T15:22:13+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "🇧🇷🇪🇸🇬🇧 · Nottingham"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1218543968000139267/GEWFadbR.jpg"
            ]
          }
        }
      ],
      "location": [
        {
          "type": [
            "h-card",
            "p-location"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:7d7bdec12d2549d4"
            ],
            "name": [
              "Nottingham, England"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Finally sorted my financial life out a bit and I’m going #FullMonzo 😎",
          "html": "Finally sorted my financial life out a bit and I’m going <a href=\"https://twitter.com/search?q=%23FullMonzo\">#FullMonzo</a> 😎"
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/EReHx0MWsAAv7mF.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-23T17:22:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @CarolSaysThings's tweet"
    ],
    "like-of": [
      "https://twitter.com/CarolSaysThings/status/1231602318732386305"
    ],
    "published": [
      "2020-02-23T17:22:00Z"
    ],
    "syndication": [
      "https://twitter.com/CarolSaysThings/status/1231602318732386305"
    ]
  }
}
