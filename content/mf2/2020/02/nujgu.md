{
  "kind": "likes",
  "slug": "2020/02/nujgu",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1226942020171763717"
      ],
      "url": [
        "https://twitter.com/anna_hax/status/1226942020171763717"
      ],
      "published": [
        "2020-02-10T18:52:27+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:anna_hax"
            ],
            "numeric-id": [
              "394235377"
            ],
            "name": [
              "Anna ✨🙅‍♀️✨"
            ],
            "nickname": [
              "anna_hax"
            ],
            "url": [
              "https://twitter.com/anna_hax",
              "https://annadodson.co.uk"
            ],
            "published": [
              "2011-10-19T19:37:31+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1134736020669378561/0_EwVzms.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "We made it!! So happy to see people here tonight #TechNott 🙌\n❄️☃️🚊🚗🔥",
          "html": "<div style=\"white-space: pre\">We made it!! So happy to see people here tonight <a href=\"https://twitter.com/search?q=%23TechNott\">#TechNott</a> 🙌\n❄️☃️🚊🚗🔥</div>"
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/EQb5QbrXUAAonVv.jpg"
      ]
    }
  },
  "client_id": "https://micropublish.net",
  "date": "2020-02-10T19:53:00.022+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @anna_hax's tweet"
    ],
    "like-of": [
      "https://twitter.com/anna_hax/status/1226942020171763717"
    ],
    "published": [
      "2020-02-10T19:53:00.022+01:00"
    ],
    "syndication": [
      "https://twitter.com/anna_hax/status/1226942020171763717"
    ]
  }
}
