{
  "kind": "likes",
  "slug": "2020/02/l2hao",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1231717943760781315"
      ],
      "url": [
        "https://twitter.com/hankchizljaw/status/1231717943760781315"
      ],
      "published": [
        "2020-02-23T23:10:16+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:hankchizljaw"
            ],
            "numeric-id": [
              "98734097"
            ],
            "name": [
              "Andy Bell"
            ],
            "nickname": [
              "hankchizljaw"
            ],
            "url": [
              "https://twitter.com/hankchizljaw",
              "https://hankchizljaw.com"
            ],
            "published": [
              "2009-12-22T22:28:06+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Cheltenham, UK"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1219208545138290689/XoucaVnN.jpg"
            ]
          }
        }
      ],
      "location": [
        {
          "type": [
            "h-card",
            "p-location"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:12cd7ae15ec9a177"
            ],
            "name": [
              "Greet, England"
            ]
          }
        }
      ],
      "content": [
        "a well actua11y is when someone replies to a solid tweet, telling you off about a minor accessibility issue with it"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-24T07:34:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @hankchizljaw's tweet"
    ],
    "like-of": [
      "https://twitter.com/hankchizljaw/status/1231717943760781315"
    ],
    "published": [
      "2020-02-24T07:34:00Z"
    ],
    "syndication": [
      "https://twitter.com/hankchizljaw/status/1231717943760781315"
    ]
  }
}
