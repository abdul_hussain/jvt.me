{
  "kind": "likes",
  "slug": "2020/02/xotwl",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1226950983156207620"
      ],
      "url": [
        "https://twitter.com/short_louise/status/1226950983156207620"
      ],
      "published": [
        "2020-02-10T19:28:04+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/MrsEmma/status/1226950465239371778"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:short_louise"
            ],
            "numeric-id": [
              "276855748"
            ],
            "name": [
              "Louise Paling"
            ],
            "nickname": [
              "short_louise"
            ],
            "url": [
              "https://twitter.com/short_louise"
            ],
            "published": [
              "2011-04-04T06:40:11+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1204077832483360771/iEm9QMFA.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "I'm thinking the first item would be a thorough review of the CoC 😆",
          "html": "I'm thinking the first item would be a thorough review of the CoC 😆\n<a class=\"u-mention\" href=\"https://twitter.com/MrsEmma\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/StevenPears\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/slsmithwell\"></a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-10T20:14:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @short_louise's tweet"
    ],
    "like-of": [
      "https://twitter.com/short_louise/status/1226950983156207620"
    ],
    "published": [
      "2020-02-10T20:14:00Z"
    ],
    "syndication": [
      "https://twitter.com/short_louise/status/1226950983156207620"
    ]
  }
}
