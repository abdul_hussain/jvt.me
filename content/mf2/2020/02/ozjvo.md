{
  "kind": "rsvps",
  "slug": "2020/02/ozjvo",
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-17T07:08:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP no to https://www.meetup.com/dotnetnotts/events/268477161/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/dotnetnotts/events/268477161/"
    ],
    "published": [
      "2020-02-17T07:08:00Z"
    ],
    "rsvp": [
      "no"
    ],
    "syndication": [
      "https://www.meetup.com/dotnetnotts/events/268477161/#rsvp-by-https%3A%2F%2Fwww.jvt.me"
    ],
    "event": {
      "location": [
        "BJSS, 16 King St, Nottingham, United Kingdom"
      ],
      "url": [
        "https://www.meetup.com/dotnetnotts/events/268477161/"
      ],
      "name": [
        "Kevin Smith - Flexible Mongo DB & Liam Gulliver - Containers in Azure DevOps"
      ],
      "start": [
        "2020-02-24T19:00:00Z"
      ],
      "end": [
        "2020-02-24T21:30:00Z"
      ]
    }
  }
}
