{
  "kind": "replies",
  "slug": "2020/02/r6tfr",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1225134964787662851"
      ],
      "url": [
        "https://twitter.com/rothgar/status/1225134964787662851"
      ],
      "published": [
        "2020-02-05T19:11:52+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:rothgar"
            ],
            "numeric-id": [
              "14361975"
            ],
            "name": [
              "Justin Garrison"
            ],
            "nickname": [
              "rothgar"
            ],
            "url": [
              "https://twitter.com/rothgar",
              "https://justingarrison.com",
              "http://cnibook.info",
              "http://imdb.to/2FikCiY",
              "http://bit.ly/2tJM3yt"
            ],
            "published": [
              "2008-04-11T15:59:21+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "LAland"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/938285374824214528/IfkIWpni.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "I'm torn if I should stick with Ubuntu or go back to Fedora on my new laptop.\n\nI want to like elementary but I don't like some of the defaults and haven't figure out how to change them (e.g. disable dock, change keyboard shortcuts/launcher)",
          "html": "<div style=\"white-space: pre\">I'm torn if I should stick with Ubuntu or go back to Fedora on my new laptop.\n\nI want to like elementary but I don't like some of the defaults and haven't figure out how to change them (e.g. disable dock, change keyboard shortcuts/launcher)</div>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-05T20:22:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/rothgar/status/1225134964787662851"
    ],
    "in-reply-to": [
      "https://twitter.com/rothgar/status/1225134964787662851"
    ],
    "published": [
      "2020-02-05T20:22:00Z"
    ],
    "content": [
      {
        "html": "",
        "value": "Can you not just swap out the Desktop Environment you're using without swapping distro? "
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1225153797791604737"
    ]
  }
}
