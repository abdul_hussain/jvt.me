{
  "kind": "likes",
  "slug": "2020/02/zl9ak",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1223923837047386112"
      ],
      "url": [
        "https://twitter.com/fosdem/status/1223923837047386112"
      ],
      "published": [
        "2020-02-02T10:59:16+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:fosdem"
            ],
            "numeric-id": [
              "10680142"
            ],
            "name": [
              "FOSDEM"
            ],
            "nickname": [
              "fosdem"
            ],
            "url": [
              "https://twitter.com/fosdem",
              "http://fosdem.org/"
            ],
            "published": [
              "2007-11-28T15:03:49+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Europe"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/817394480517185538/shrAijm8.jpg"
            ]
          }
        }
      ],
      "content": [
        "Want to know if a room is full?"
      ],
      "photo": [
        "https://pbs.twimg.com/media/EPxAPlfX0AEJNDV.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-02T11:38:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @fosdem's tweet"
    ],
    "like-of": [
      "https://twitter.com/fosdem/status/1223923837047386112"
    ],
    "published": [
      "2020-02-02T11:38:00Z"
    ],
    "syndication": [
      "https://twitter.com/fosdem/status/1223923837047386112"
    ]
  }
}
