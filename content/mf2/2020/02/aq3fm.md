{
  "date" : "2020-02-25T08:20:27+0000",
  "deleted" : true,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://devopsdays.org/events/2020-london/" ],
    "name" : [ "RSVP yes to https://devopsdays.org/events/2020-london/" ],
    "published" : [ "2020-02-25T08:20:27+0000" ],
    "event" : {
      "location" : [ "QEII Centre, Broad Sanctuary, Westminster " ],
      "url" : [ "https://devopsdays.org/events/2020-london/" ],
      "name" : [ "DevOpsDays London 2020" ],
      "start" : [ "2020-09-24T09:00:00Z" ],
      "end" : [ "2020-09-25T18:00:00Z" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/02/aq3fm"
}
