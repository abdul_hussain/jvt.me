{
  "kind": "rsvps",
  "slug": "2020/02/zg8ef",
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-27T16:54:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP no to https://www.meetup.com/digitallincoln/events/268750051/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/digitallincoln/events/268750051/"
    ],
    "published": [
      "2020-02-27T16:54:00Z"
    ],
    "rsvp": [
      "no"
    ],
    "syndication": [
      "https://www.meetup.com/digitallincoln/events/268750051/#rsvp-by-https%3A%2F%2Fwww.jvt.me"
    ],
    "event": {
      "location": [
        "Mosaic, Thomas Parker House, Lincoln, United Kingdom"
      ],
      "url": [
        "https://www.meetup.com/digitallincoln/events/268750051/"
      ],
      "name": [
        "Automating UI testing with Tom Austin"
      ],
      "start": [
        "2020-03-31T18:30:00+01:00"
      ],
      "end": [
        "2020-03-31T20:30:00+01:00"
      ]
    }
  }
}
