{
  "kind": "likes",
  "slug": "2020/02/luclp",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1226946487252328449"
      ],
      "url": [
        "https://twitter.com/anna_hax/status/1226946487252328449"
      ],
      "published": [
        "2020-02-10T19:10:12+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/MrsEmma/status/1226946297724313603"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:anna_hax"
            ],
            "numeric-id": [
              "394235377"
            ],
            "name": [
              "Anna ✨🙅‍♀️✨"
            ],
            "nickname": [
              "anna_hax"
            ],
            "url": [
              "https://twitter.com/anna_hax",
              "https://annadodson.co.uk"
            ],
            "published": [
              "2011-10-19T19:37:31+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1134736020669378561/0_EwVzms.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "I think he was considering a shorter transition time too 😅",
          "html": "I think he was considering a shorter transition time too 😅\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/MrAndrew\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/MrsEmma\"></a>"
        }
      ]
    }
  },
  "client_id": "https://micropublish.net",
  "date": "2020-02-10T20:43:56.809+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @anna_hax's tweet"
    ],
    "like-of": [
      "https://twitter.com/anna_hax/status/1226946487252328449"
    ],
    "published": [
      "2020-02-10T20:43:56.809+01:00"
    ],
    "syndication": [
      "https://twitter.com/anna_hax/status/1226946487252328449"
    ]
  }
}
