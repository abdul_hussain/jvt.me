{
  "kind": "likes",
  "slug": "2020/02/q7b7k",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1226971514794397696"
      ],
      "url": [
        "https://twitter.com/edent/status/1226971514794397696"
      ],
      "published": [
        "2020-02-10T20:49:39+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:edent"
            ],
            "numeric-id": [
              "14054507"
            ],
            "name": [
              "Terence Eden"
            ],
            "nickname": [
              "edent"
            ],
            "url": [
              "https://twitter.com/edent",
              "https://shkspr.mobi/blog/",
              "https://edent.tel"
            ],
            "published": [
              "2008-02-28T13:10:25+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "London, UK"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/785312424031293440/ilDkJR5W.jpg"
            ]
          }
        }
      ],
      "content": [
        "The first two words don't match the second two words."
      ],
      "photo": [
        "https://pbs.twimg.com/media/EQcUFj4WoAEcJzi.jpg"
      ]
    }
  },
  "client_id": "https://micropublish.net",
  "date": "2020-02-10T21:54:12.979+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @edent's tweet"
    ],
    "like-of": [
      "https://twitter.com/edent/status/1226971514794397696"
    ],
    "published": [
      "2020-02-10T21:54:12.979+01:00"
    ],
    "syndication": [
      "https://twitter.com/edent/status/1226971514794397696"
    ]
  }
}
