{
  "kind": "likes",
  "slug": "2020/02/c6fvd",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1232431717501128704"
      ],
      "url": [
        "https://twitter.com/Nick_Craver/status/1232431717501128704"
      ],
      "published": [
        "2020-02-25T22:26:33+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:Nick_Craver"
            ],
            "numeric-id": [
              "95030839"
            ],
            "name": [
              "Nick Craver"
            ],
            "nickname": [
              "Nick_Craver"
            ],
            "url": [
              "https://twitter.com/Nick_Craver",
              "https://nickcraver.com"
            ],
            "published": [
              "2009-12-06T16:53:08+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "North Carolina"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/623686606159872000/twD9NrLc.jpg"
            ]
          }
        }
      ],
      "content": [
        "Finding out the combination of which episodes of which shows from what seasons are available via which subscriptions to whatever services is beyond exhausting. Piracy rising dramatically right now shouldn’t be a surprise to anyone who’s actually tried to buy content legitimately."
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-26T18:39:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @Nick_Craver's tweet"
    ],
    "like-of": [
      "https://twitter.com/Nick_Craver/status/1232431717501128704"
    ],
    "published": [
      "2020-02-26T18:39:00Z"
    ],
    "syndication": [
      "https://twitter.com/Nick_Craver/status/1232431717501128704"
    ]
  }
}
