{
  "kind": "likes",
  "slug": "2020/02/vkabk",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1231182951980961792"
      ],
      "url": [
        "https://twitter.com/Doctor_Gamma/status/1231182951980961792"
      ],
      "published": [
        "2020-02-22T11:44:24+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:Doctor_Gamma"
            ],
            "numeric-id": [
              "745225318244425728"
            ],
            "name": [
              "Doctor Gamma"
            ],
            "nickname": [
              "Doctor_Gamma"
            ],
            "url": [
              "https://twitter.com/Doctor_Gamma",
              "http://youtube.com/DoctorGamma"
            ],
            "published": [
              "2016-06-21T12:02:16+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Lisbon, Portugal"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/897895242011217920/lrbDc6JM.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Rest in peace, Jens Knudsen. (1942-2020)\n\nThank you for inventing the LEGO figure.",
          "html": "<div style=\"white-space: pre\">Rest in peace, Jens Knudsen. (1942-2020)\n\nThank you for inventing the LEGO figure.</div>"
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/ERYKEkzXYAAvCXC.jpg",
        "https://pbs.twimg.com/media/ERYKNZpX0AYM6la.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-23T21:37:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @Doctor_Gamma's tweet"
    ],
    "like-of": [
      "https://twitter.com/Doctor_Gamma/status/1231182951980961792"
    ],
    "published": [
      "2020-02-23T21:37:00Z"
    ],
    "syndication": [
      "https://twitter.com/Doctor_Gamma/status/1231182951980961792"
    ]
  }
}
