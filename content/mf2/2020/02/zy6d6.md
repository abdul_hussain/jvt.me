{
  "kind": "replies",
  "slug": "2020/02/zy6d6",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1230241191288926208"
      ],
      "url": [
        "https://twitter.com/tripofmice/status/1230241191288926208"
      ],
      "published": [
        "2020-02-19T21:22:11+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/robaeprice/status/1230241044517638147"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:tripofmice"
            ],
            "numeric-id": [
              "1304991110"
            ],
            "name": [
              "Mouse Reeve"
            ],
            "nickname": [
              "tripofmice"
            ],
            "url": [
              "https://twitter.com/tripofmice",
              "https://www.mousereeve.com"
            ],
            "published": [
              "2013-03-26T17:27:45+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "San Francisco, CA"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1192267157482037248/-1uCPrsR.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "yep, that's the position I'm in with good reads for sure",
          "html": "yep, that's the position I'm in with good reads for sure\n<a class=\"u-mention\" href=\"https://twitter.com/robaeprice\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/xor\"></a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-24T22:50:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/tripofmice/status/1230241191288926208"
    ],
    "in-reply-to": [
      "https://twitter.com/tripofmice/status/1230241191288926208"
    ],
    "published": [
      "2020-02-24T22:50:00Z"
    ],
    "content": [
      {
        "html": "",
        "value": "You may want to check out <span class=\"h-card\"><a class=\"u-url\" href=\"https://twitter.com/edent\">@edent</a></span>'s blog post https://shkspr.mobi/blog/2019/12/add-review-to-goodreads-from-schema-markup/ for how to pull the data from the Goodreads API"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1232076463932919808"
    ]
  }
}
