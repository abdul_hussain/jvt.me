{
  "kind": "replies",
  "slug": "2020/02/mi0pp",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1229444982500741120"
      ],
      "url": [
        "https://twitter.com/rwdrich/status/1229444982500741120"
      ],
      "published": [
        "2020-02-17T16:38:20+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:rwdrich"
            ],
            "numeric-id": [
              "107548386"
            ],
            "name": [
              "Richard Davies"
            ],
            "nickname": [
              "rwdrich"
            ],
            "url": [
              "https://twitter.com/rwdrich",
              "http://rwdrich.co.uk"
            ],
            "published": [
              "2010-01-22T23:14:30+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Cambridge, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/917481919872491521/BwypzjEE.jpg"
            ]
          }
        }
      ],
      "content": [
        "In a foul mood, and about to go join stationary traffic that'll turn my 20 minute commute into at least an hour. Joy."
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-17T17:43:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/rwdrich/status/1229444982500741120"
    ],
    "in-reply-to": [
      "https://twitter.com/rwdrich/status/1229444982500741120"
    ],
    "published": [
      "2020-02-17T17:43:00Z"
    ],
    "content": [
      {
        "html": "",
        "value": "🤗 "
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1229462785949683714"
    ]
  }
}
