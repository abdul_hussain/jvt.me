{
  "kind": "likes",
  "slug": "2020/02/ev3ru",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1228452792630730758"
      ],
      "url": [
        "https://twitter.com/oihamza/status/1228452792630730758"
      ],
      "published": [
        "2020-02-14T22:55:43+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:oihamza"
            ],
            "numeric-id": [
              "176425492"
            ],
            "name": [
              "Hamza"
            ],
            "nickname": [
              "oihamza"
            ],
            "url": [
              "https://twitter.com/oihamza"
            ],
            "published": [
              "2010-08-09T14:16:11+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "New York, NY"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1210960440294952961/UcSPSmQ8.jpg"
            ]
          }
        }
      ],
      "content": [
        "Two factor authentication but for texting your ex"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-15T16:18:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @oihamza's tweet"
    ],
    "like-of": [
      "https://twitter.com/oihamza/status/1228452792630730758"
    ],
    "published": [
      "2020-02-15T16:18:00Z"
    ],
    "syndication": [
      "https://twitter.com/oihamza/status/1228452792630730758"
    ]
  }
}
