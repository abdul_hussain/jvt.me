{
  "kind": "likes",
  "slug": "2020/02/8bkhm",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1233687091416125440"
      ],
      "url": [
        "https://twitter.com/edent/status/1233687091416125440"
      ],
      "published": [
        "2020-02-29T09:34:58+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:edent"
            ],
            "numeric-id": [
              "14054507"
            ],
            "name": [
              "Terence Eden"
            ],
            "nickname": [
              "edent"
            ],
            "url": [
              "https://twitter.com/edent",
              "https://shkspr.mobi/blog/",
              "https://edent.tel"
            ],
            "published": [
              "2008-02-28T13:10:25+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "London, UK"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1228067445153452033/_A8Uq2VY.jpg"
            ]
          }
        }
      ],
      "content": [
        "How is my blog still getting hits from... Google+?!?"
      ],
      "photo": [
        "https://pbs.twimg.com/media/ER7vrM2WkAAYK04.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-29T12:19:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @edent's tweet"
    ],
    "like-of": [
      "https://twitter.com/edent/status/1233687091416125440"
    ],
    "published": [
      "2020-02-29T12:19:00Z"
    ],
    "syndication": [
      "https://twitter.com/edent/status/1233687091416125440"
    ]
  }
}
