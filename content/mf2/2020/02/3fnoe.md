{
  "kind": "replies",
  "slug": "2020/02/3fnoe",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1227671859476549632"
      ],
      "url": [
        "https://twitter.com/hawx/status/1227671859476549632"
      ],
      "published": [
        "2020-02-12T19:12:34+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:hawx"
            ],
            "numeric-id": [
              "17422610"
            ],
            "name": [
              "Josh"
            ],
            "nickname": [
              "hawx"
            ],
            "url": [
              "https://twitter.com/hawx",
              "https://hawx.me"
            ],
            "published": [
              "2008-11-16T14:10:09+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1137377659611176961/o2b408SS.png"
            ]
          }
        }
      ],
      "content": [
        "Hopefully+I+can+now+post+from+my+phone%2C+using+the+HTTP+Shortcuts+app+because+I+can%27t+get+the+login+button+of+indigenous+to+work.+"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-13T20:00:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/hawx/status/1227671859476549632"
    ],
    "in-reply-to": [
      "https://twitter.com/hawx/status/1227671859476549632"
    ],
    "published": [
      "2020-02-13T20:00:00Z"
    ],
    "content": [
      {
        "html": "",
        "value": "What's up with Indigenous? I'd be happy to give a hand if I can 🙃 "
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1228047629524885516"
    ]
  }
}
