{
  "kind" : "rsvps",
  "slug" : "2020/02/gbexh",
  "client_id" : "https://indigenous.realize.be",
  "date" : "2020-02-11T07:36:00Z",
  "h" : "h-entry",
  "properties" : {
    "name" : [ "RSVP yes to https://events.indieweb.org/2020/03/homebrew-website-club-nottingham-anniversary-edition--Rcujt5SykHv1" ],
    "in-reply-to" : [ "https://events.indieweb.org/2020/03/homebrew-website-club-nottingham-anniversary-edition--Rcujt5SykHv1" ],
    "published" : [ "2020-02-11T07:36:00Z" ],
    "rsvp" : [ "yes" ],
    "event" : {
      "location" : {
        "type" : [ "h-card" ],
        "properties" : {
          "name" : [ "Ludorati Café" ],
          "street-address" : [ "72 Maid Marian Way" ],
          "region" : [ "England" ],
          "country-name" : [ "United Kingdom" ]
        },
        "lang" : "en",
        "value" : "Ludorati Café"
      },
      "url" : [ "https://events.indieweb.org/2020/03/homebrew-website-club-nottingham-anniversary-edition--Rcujt5SykHv1" ],
      "name" : [ "Homebrew Website Club: Nottingham (Anniversary edition)" ],
      "start" : [ "2020-03-18T17:30:00+00:00" ],
      "end" : [ "2020-03-18T19:30:00+00:00" ]
    }
  }
}
