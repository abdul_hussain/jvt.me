{
  "kind": "replies",
  "slug": "2020/02/igcmg",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1229745875972431875"
      ],
      "url": [
        "https://twitter.com/jesslynnrose/status/1229745875972431875"
      ],
      "published": [
        "2020-02-18T12:33:59+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:jesslynnrose"
            ],
            "numeric-id": [
              "1530096708"
            ],
            "name": [
              "Jessica Rose"
            ],
            "nickname": [
              "jesslynnrose"
            ],
            "url": [
              "https://twitter.com/jesslynnrose",
              "http://jessica.tech"
            ],
            "published": [
              "2013-06-19T07:56:27+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Birmingham/どこでも"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1186288133144502272/wRTm_JRi.jpg"
            ]
          }
        }
      ],
      "content": [
        "Who do we like for business bank accounts in the UK rn?"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-18T16:33:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/jesslynnrose/status/1229745875972431875"
    ],
    "in-reply-to": [
      "https://twitter.com/jesslynnrose/status/1229745875972431875"
    ],
    "published": [
      "2020-02-18T16:33:00Z"
    ],
    "content": [
      {
        "html": "",
        "value": "I've been seeing some heavy advertising for Tide's business banking so I guess it's worked cause I'm talking about it, but no clue if it's any good 🤷🏽‍♂️ "
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1251558325763878915"
    ]
  }
}
