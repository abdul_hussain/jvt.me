{
  "kind": "likes",
  "slug": "2020/02/jo5x5",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1225559554345357312"
      ],
      "url": [
        "https://twitter.com/CarolSaysThings/status/1225559554345357312"
      ],
      "published": [
        "2020-02-06T23:19:02+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:CarolSaysThings"
            ],
            "numeric-id": [
              "36382927"
            ],
            "name": [
              "Carol ⚡️"
            ],
            "nickname": [
              "CarolSaysThings"
            ],
            "url": [
              "https://twitter.com/CarolSaysThings",
              "https://carolgilabert.me/"
            ],
            "published": [
              "2009-04-29T15:22:13+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "🇧🇷🇪🇸🇬🇧 · Nottingham"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1218543968000139267/GEWFadbR.jpg"
            ]
          }
        }
      ],
      "location": [
        {
          "type": [
            "h-card",
            "p-location"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:7d7bdec12d2549d4"
            ],
            "name": [
              "Nottingham, England"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "sometimes ✨ falls out of my recently used emoji, and it’s a wake up call for me tbh.\n\nlike, what kinda life am I leading?",
          "html": "<div style=\"white-space: pre\">sometimes ✨ falls out of my recently used emoji, and it’s a wake up call for me tbh.\n\nlike, what kinda life am I leading?</div>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-07T07:48:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @CarolSaysThings's tweet"
    ],
    "like-of": [
      "https://twitter.com/CarolSaysThings/status/1225559554345357312"
    ],
    "published": [
      "2020-02-07T07:48:00Z"
    ],
    "syndication": [
      "https://twitter.com/CarolSaysThings/status/1225559554345357312"
    ]
  }
}
