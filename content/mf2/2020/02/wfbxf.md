{
  "kind": "likes",
  "slug": "2020/02/wfbxf",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1226964616548405248"
      ],
      "url": [
        "https://twitter.com/hankchizljaw/status/1226964616548405248"
      ],
      "published": [
        "2020-02-10T20:22:15+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:hankchizljaw"
            ],
            "numeric-id": [
              "98734097"
            ],
            "name": [
              "Andy Bell"
            ],
            "nickname": [
              "hankchizljaw"
            ],
            "url": [
              "https://twitter.com/hankchizljaw",
              "https://hankchizljaw.com"
            ],
            "published": [
              "2009-12-22T22:28:06+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Cheltenham, UK"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1219208545138290689/XoucaVnN.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "📣 Got reply guys in your mentions? Not sure what to do about it?\n\nHow about you give them a sarcastic pat on the back with a trophy?\n\nIntroducing, \"Thanks for the reply, Guy\": a customisable page to show your reply guys  that you care about their work 🏆\n\nthanksforthereply.com",
          "html": "<div style=\"white-space: pre\">📣 Got reply guys in your mentions? Not sure what to do about it?\n\nHow about you give them a sarcastic pat on the back with a trophy?\n\nIntroducing, \"Thanks for the reply, Guy\": a customisable page to show your reply guys  that you care about their work 🏆\n\n<a href=\"https://thanksforthereply.com/\">thanksforthereply.com</a></div>"
        }
      ]
    }
  },
  "client_id": "https://micropublish.net",
  "date": "2020-02-10T21:44:57.611+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @hankchizljaw's tweet"
    ],
    "like-of": [
      "https://twitter.com/hankchizljaw/status/1226964616548405248"
    ],
    "published": [
      "2020-02-10T21:44:57.611+01:00"
    ],
    "syndication": [
      "https://twitter.com/hankchizljaw/status/1226964616548405248"
    ]
  }
}
