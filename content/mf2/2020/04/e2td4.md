{
  "date": "2020-04-17T01:02:00+01:00",
  "deleted": false,
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/contodonetflix/status/1250861258317783040"
    ],
    "name": [
      "Like of @contodonetflix's tweet"
    ],
    "published": [
      "2020-04-17T01:02:00+01:00"
    ],
    "like-of": [
      "https://twitter.com/contodonetflix/status/1250861258317783040"
    ]
  },
  "kind": "likes",
  "slug": "2020/04/e2td4",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1250861258317783040"
      ],
      "url": [
        "https://twitter.com/contodonetflix/status/1250861258317783040"
      ],
      "published": [
        "2020-04-16T18:58:58+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:contodonetflix"
            ],
            "numeric-id": [
              "1202673282945695749"
            ],
            "name": [
              "Con Todo"
            ],
            "nickname": [
              "contodonetflix"
            ],
            "url": [
              "https://twitter.com/contodonetflix",
              "http://smarturl.it/contodonetflix"
            ],
            "published": [
              "2019-12-05T19:37:34+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "California, USA"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1218338227628597250/6xkXJGYQ.jpg"
            ]
          }
        }
      ],
      "content": [
        "add \"pero like\" to any famous movie line - I'll go first: \"pero like, I see dead people.\""
      ]
    }
  },
  "client_id": "https://indigenous.realize.be"
}
