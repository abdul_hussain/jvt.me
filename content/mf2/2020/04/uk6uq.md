{
  "kind": "likes",
  "slug": "2020/04/uk6uq",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1245772578398011393"
      ],
      "url": [
        "https://twitter.com/JessPWhite/status/1245772578398011393"
      ],
      "video": [
        "https://video.twimg.com/tweet_video/EUnfjYIWsAccmph.mp4"
      ],
      "published": [
        "2020-04-02T17:58:22+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:JessPWhite"
            ],
            "numeric-id": [
              "3021209961"
            ],
            "name": [
              "Jessica White 🦎"
            ],
            "nickname": [
              "JessPWhite"
            ],
            "url": [
              "https://twitter.com/JessPWhite",
              "https://jesswhite.co.uk"
            ],
            "published": [
              "2015-02-06T10:02:36+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1186021421350375424/lYEBBAc4.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "So far virtual #WiTNotts feels very much like the physical one apart from with better backgrounds and it's running on time🤣 Great job so far crew!",
          "html": "So far virtual <a href=\"https://twitter.com/search?q=%23WiTNotts\">#WiTNotts</a> feels very much like the physical one apart from with better backgrounds and it's running on time🤣 Great job so far crew!"
        }
      ]
    }
  },
  "client_id": "https://micropublish.net",
  "date": "2020-04-02T20:36:28.026+02:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @JessPWhite's tweet"
    ],
    "like-of": [
      "https://twitter.com/JessPWhite/status/1245772578398011393"
    ],
    "published": [
      "2020-04-02T20:36:28.026+02:00"
    ],
    "syndication": [
      "https://twitter.com/JessPWhite/status/1245772578398011393"
    ]
  }
}
