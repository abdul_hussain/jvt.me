{
  "date" : "2020-04-22T23:25:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/Julie_Gund/status/1253047861526196224" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1254059100503244802" ],
    "name" : [ "Reply to https://twitter.com/Julie_Gund/status/1253047861526196224" ],
    "published" : [ "2020-04-22T23:25:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "I'm really sorry to hear this - I had a kidney stent put in a couple of years ago, but I was quite fortunate that it was only ever so mild discomfort, although my body at the time was recovering from my ruptured appendix so maybe that distracted me? What have you been given for the pain?"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/04/p5hir",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1253047861526196224" ],
      "url" : [ "https://twitter.com/Julie_Gund/status/1253047861526196224" ],
      "published" : [ "2020-04-22T19:47:45+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Julie_Gund" ],
          "numeric-id" : [ "3122157632" ],
          "name" : [ "Julie Gunderson" ],
          "nickname" : [ "Julie_Gund" ],
          "url" : [ "https://twitter.com/Julie_Gund" ],
          "published" : [ "2015-04-01T00:01:23+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1222653812059426816/uwZviYGs.jpg" ]
        }
      } ],
      "location" : [ {
        "type" : [ "h-card", "p-location" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:4723507d8ce23a60" ],
          "name" : [ "Idaho, USA" ]
        }
      } ],
      "content" : [ "If anyone out there has had a kidney stent and has ANY ADVICE on how to deal with the endless pain, please message me, I have 10 more days until it comes out and am not handling it well. Thanks in advance! (I have prescriptions for pain but they barely make a dent)" ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
