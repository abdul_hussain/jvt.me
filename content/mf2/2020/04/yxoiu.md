{
  "date" : "2020-04-25T22:32:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/engineeringvids/status/1254075759980773378" ],
    "name" : [ "Like of @engineeringvids's tweet" ],
    "published" : [ "2020-04-25T22:32:00+01:00" ],
    "like-of" : [ "https://twitter.com/engineeringvids/status/1254075759980773378" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/yxoiu",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1254075759980773378" ],
      "url" : [ "https://twitter.com/engineeringvids/status/1254075759980773378" ],
      "video" : [ "https://video.twimg.com/tweet_video/CoZ1YPtWYAAgxrK.mp4" ],
      "published" : [ "2020-04-25T15:52:15+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:engineeringvids" ],
          "numeric-id" : [ "1149159932777730048" ],
          "name" : [ "Engineering" ],
          "nickname" : [ "engineeringvids" ],
          "url" : [ "https://twitter.com/engineeringvids" ],
          "published" : [ "2019-07-11T03:34:13+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1153902592763678722/vMAqm8lK.jpg" ]
        }
      } ],
      "content" : [ "This is how camera lenses change the shape of your face " ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
