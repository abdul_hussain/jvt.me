{
  "date" : "2020-04-28T12:23:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/jevakallio/status/1254823758784856065" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1255110233015422976" ],
    "name" : [ "Reply to https://twitter.com/jevakallio/status/1254823758784856065" ],
    "published" : [ "2020-04-28T12:23:00+01:00" ],
    "category" : [ "personal-website", "indieweb" ],
    "content" : [ {
      "html" : "",
      "value" : "My <a href=\"/tags/personal-website/\">#PersonalWebsite</a> is https://www.jvt.me and as I'm very into owning my own data and being part of the <a href=\"/tags/indieweb/\">#IndieWeb</a>, I'm replying to this tweet from my website!\r\n\r\nSite itself is Hugo and hosted on Netlify"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/04/g03ju",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1254823758784856065" ],
      "url" : [ "https://twitter.com/jevakallio/status/1254823758784856065" ],
      "published" : [ "2020-04-27T17:24:32+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:jevakallio" ],
          "numeric-id" : [ "15930040" ],
          "name" : [ "Jani Eväkallio" ],
          "nickname" : [ "jevakallio" ],
          "url" : [ "https://twitter.com/jevakallio", "https://github.com/jevakallio" ],
          "published" : [ "2008-08-21T11:16:59+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1216111940625485826/JzDs-ms6.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "HI! Do you have a self-hosted personal website/blog? If yes drop a link in replies! 🙏\n\nI'm also interested what are your main problems in hosting your own website? Effort? Cost? Tech? Getting engagement?",
        "html" : "<div style=\"white-space: pre\">HI! Do you have a self-hosted personal website/blog? If yes drop a link in replies! 🙏\n\nI'm also interested what are your main problems in hosting your own website? Effort? Cost? Tech? Getting engagement?</div>"
      } ]
    }
  },
  "tags" : [ "personal-website", "indieweb" ],
  "client_id" : "https://indigenous.realize.be"
}
