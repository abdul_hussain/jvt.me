{
  "date" : "2020-04-24T07:46:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/krisajenkins/status/1253294613663711235" ],
    "name" : [ "Like of @krisajenkins's tweet" ],
    "published" : [ "2020-04-24T07:46:00+01:00" ],
    "category" : [ "agile" ],
    "like-of" : [ "https://twitter.com/krisajenkins/status/1253294613663711235" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/endfy",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1253294613663711235" ],
      "url" : [ "https://twitter.com/krisajenkins/status/1253294613663711235" ],
      "published" : [ "2020-04-23T12:08:15+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:krisajenkins" ],
          "numeric-id" : [ "18505708" ],
          "name" : [ "Kris Jenkins" ],
          "nickname" : [ "krisajenkins" ],
          "url" : [ "https://twitter.com/krisajenkins" ],
          "published" : [ "2008-12-31T13:10:40+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/983356928586854400/bNw6aMkM.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Q. What are Story Points?\nA. Story Points are a way of making developers deliver faster by asking them for a random number, and then saying, “That seems a little high,” until they say a smaller number.",
        "html" : "<div style=\"white-space: pre\">Q. What are Story Points?\nA. Story Points are a way of making developers deliver faster by asking them for a random number, and then saying, “That seems a little high,” until they say a smaller number.</div>"
      } ]
    }
  },
  "tags" : [ "agile" ],
  "client_id" : "https://indigenous.realize.be"
}
