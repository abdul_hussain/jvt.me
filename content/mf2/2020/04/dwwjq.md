{
  "date" : "2020-04-25T15:38:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/edent/status/1254015002383712258" ],
    "name" : [ "Like of @edent's tweet" ],
    "published" : [ "2020-04-25T15:38:00+01:00" ],
    "like-of" : [ "https://twitter.com/edent/status/1254015002383712258" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/dwwjq",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1254015002383712258" ],
      "url" : [ "https://twitter.com/edent/status/1254015002383712258" ],
      "published" : [ "2020-04-25T11:50:49+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:edent" ],
          "numeric-id" : [ "14054507" ],
          "name" : [ "Terence Eden" ],
          "nickname" : [ "edent" ],
          "url" : [ "https://twitter.com/edent", "https://shkspr.mobi/blog/", "https://edent.tel" ],
          "published" : [ "2008-02-28T13:10:25+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1228067445153452033/_A8Uq2VY.jpg" ]
        }
      } ],
      "content" : [ "Just tried to tab-complete my password." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
