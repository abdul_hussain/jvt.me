{
  "date" : "2020-04-28T21:26:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/EmmaKennedy/status/1255053722515902464" ],
    "name" : [ "Like of @EmmaKennedy's tweet" ],
    "published" : [ "2020-04-28T21:26:00+01:00" ],
    "like-of" : [ "https://twitter.com/EmmaKennedy/status/1255053722515902464" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/l2dei",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1255053722515902464" ],
      "url" : [ "https://twitter.com/EmmaKennedy/status/1255053722515902464" ],
      "published" : [ "2020-04-28T08:38:19+00:00" ],
      "in-reply-to" : [ "https://twitter.com/SoozUK/status/1255053121782456320" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:EmmaKennedy" ],
          "numeric-id" : [ "23217907" ],
          "name" : [ "Emma Kennedy" ],
          "nickname" : [ "EmmaKennedy" ],
          "url" : [ "https://twitter.com/EmmaKennedy", "http://www.emmakennedy.net" ],
          "published" : [ "2009-03-07T18:16:08+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "london" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1112314598479286272/3BI5lnqR.png" ]
        }
      } ],
      "content" : [ {
        "value" : "I sometimes wonder how the journalist who broke that story now feels. Changed the course of that election. The butterfly wing that has changed our nation beyond all recognition.",
        "html" : "I sometimes wonder how the journalist who broke that story now feels. Changed the course of that election. The butterfly wing that has changed our nation beyond all recognition.\n<a class=\"u-mention\" href=\"https://twitter.com/SoozUK\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
