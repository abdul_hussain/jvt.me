{
  "date" : "2020-04-12T19:38:42+0100",
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.facebook.com/events/241227923666873/" ],
    "name" : [ "RSVP yes to https://www.facebook.com/events/241227923666873/" ],
    "published" : [ "2020-04-12T19:38:42+0100" ],
    "event" : {
      "url" : [ "https://www.facebook.com/events/241227923666873/" ],
      "name" : [ "The Big Quiz for Cancer Research UK - Live!" ],
      "start" : [ "2020-04-12T20:00:00+0100" ],
      "end" : [ "2020-04-12T21:00:00+0100" ],
      "location" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "name" : [ "Online" ]
        },
        "lang" : "en",
        "value" : "Online"
      } ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/04/rvrhz"
}
