{
  "date": "2020-04-12T14:27:00+01:00",
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/JamieTanna/status/1249329503928795140"
    ],
    "in-reply-to": [
      "https://twitter.com/PandelisZ/status/1249326198146072578"
    ],
    "name": [
      "Reply to https://twitter.com/PandelisZ/status/1249326198146072578"
    ],
    "published": [
      "2020-04-12T14:27:00+01:00"
    ],
    "content": [
      {
        "html": "",
        "value": "I use https://hetzner.cloud for my personal servers which have some in Germany but looks like not in Frankfurt unfortunately"
      }
    ]
  },
  "kind": "replies",
  "slug": "2020/04/1mnke",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1249326198146072578"
      ],
      "url": [
        "https://twitter.com/PandelisZ/status/1249326198146072578"
      ],
      "published": [
        "2020-04-12T13:19:11+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:PandelisZ"
            ],
            "numeric-id": [
              "2837561099"
            ],
            "name": [
              "‏Pα̹̖̺̃̊ͩ̑̽ͥν͍̯̳̳τ̗̘̺̥̦͊̉̃ͥͫͅε̝͗̎͗ͮλ̒̓̿͆is🇪🇺"
            ],
            "nickname": [
              "PandelisZ"
            ],
            "url": [
              "https://twitter.com/PandelisZ",
              "https://pa.ndel.is/"
            ],
            "published": [
              "2014-10-19T20:06:41+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Beerlin"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1090981237722877952/m04GP5lj.jpg"
            ]
          }
        }
      ],
      "content": [
        "Has anyone ever run a VPS server in Berlin? I want to run a gaming server but I can't find a VPS service. All the good ones are in the Netherlands or Frankfurt"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be"
}
