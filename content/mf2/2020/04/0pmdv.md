{
  "date": "2020-04-18T13:10:49.797+02:00",
  "deleted": false,
  "h": "h-entry",
  "properties": {
    "in-reply-to": [
      "https://twitter.com/JamieTanna/status/1251229692121812996"
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1251469307302854657"
    ],
    "name": [
      "Reply to https://twitter.com/JamieTanna/status/1251229692121812996"
    ],
    "published": [
      "2020-04-18T13:10:49.797+02:00"
    ],
    "category": [

    ],
    "content": [
      {
        "html": "",
        "value": "Turns out it was due to a mistake on my part for not reading the docs properly for https://telegraph.p3k.io/ - this time it should work!"
      }
    ]
  },
  "kind": "replies",
  "slug": "2020/04/0pmdv",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1251229692121812996"
      ],
      "url": [
        "https://twitter.com/JamieTanna/status/1251229692121812996"
      ],
      "published": [
        "2020-04-17T19:22:59+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/JamieTanna/status/1251227486870011906"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:JamieTanna"
            ],
            "numeric-id": [
              "2292531098"
            ],
            "name": [
              "Jamie Tanna | www.jvt.me"
            ],
            "nickname": [
              "JamieTanna"
            ],
            "url": [
              "https://twitter.com/JamieTanna",
              "https://www.jvt.me"
            ],
            "published": [
              "2014-01-15T10:55:11+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1207672129308778496/4erVfT-i.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "And even higher timeouts... (jvt.me/mf2/2020/04/ur…)",
          "html": "And even higher timeouts... (<a href=\"https://www.jvt.me/mf2/2020/04/ur1ib/\">jvt.me/mf2/2020/04/ur…</a>)\n<a class=\"u-mention\" href=\"https://twitter.com/CarolSaysThings\"></a>"
        }
      ]
    }
  },
  "tags": [

  ],
  "client_id": "https://micropublish.net"
}
