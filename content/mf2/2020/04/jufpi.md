{
  "date": "2020-04-11T15:59:00+01:00",
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/JamieTanna/status/1248990470308540416"
    ],
    "in-reply-to": [
      "https://twitter.com/Sp4ghettiCode/status/1248973061044539394"
    ],
    "name": [
      "Reply to https://twitter.com/Sp4ghettiCode/status/1248973061044539394"
    ],
    "published": [
      "2020-04-11T15:59:00+01:00"
    ],
    "content": [
      {
        "html": "",
        "value": "Sending hugs 🤗 can't imagine having to go through this alone - I'm lucky to be living with @annadodson.co.uk, and that both of us are quite introverted and enjoy being at home. Hopefully it won't be long until you can see them"
      }
    ]
  },
  "kind": "replies",
  "slug": "2020/04/jufpi",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1248973061044539394"
      ],
      "url": [
        "https://twitter.com/Sp4ghettiCode/status/1248973061044539394"
      ],
      "published": [
        "2020-04-11T13:55:57+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:Sp4ghettiCode"
            ],
            "numeric-id": [
              "991238672812052480"
            ],
            "name": [
              "Ed George"
            ],
            "nickname": [
              "Sp4ghettiCode"
            ],
            "url": [
              "https://twitter.com/Sp4ghettiCode",
              "http://ed-george.github.io"
            ],
            "published": [
              "2018-05-01T08:51:30+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "London, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1224696422353817602/Szvtaq2I.jpg"
            ]
          }
        }
      ],
      "location": [
        {
          "type": [
            "h-card",
            "p-location"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:5461b744712914a7"
            ],
            "name": [
              "Aylesbury, England"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "I think today has been the toughest day of isolation for me. I am now close to a month without seeing my finacee or family.\n\nIf anyone else is isolating alone, I feel you!",
          "html": "<div style=\"white-space: pre\">I think today has been the toughest day of isolation for me. I am now close to a month without seeing my finacee or family.\n\nIf anyone else is isolating alone, I feel you!</div>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be"
}
