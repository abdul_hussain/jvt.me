{
  "date": "2020-04-10T10:21:00+01:00",
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/_elletownsend/status/1248320335268720641"
    ],
    "name": [
      "Like of @_elletownsend's tweet"
    ],
    "like-of": [
      "https://twitter.com/_elletownsend/status/1248320335268720641"
    ],
    "published": [
      "2020-04-10T10:21:00+01:00"
    ]
  },
  "kind": "likes",
  "slug": "2020/04/76uz0",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1248320335268720641"
      ],
      "url": [
        "https://twitter.com/_elletownsend/status/1248320335268720641"
      ],
      "published": [
        "2020-04-09T18:42:15+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:_elletownsend"
            ],
            "numeric-id": [
              "4308006796"
            ],
            "name": [
              "Elle Townsend✨"
            ],
            "nickname": [
              "_elletownsend"
            ],
            "url": [
              "https://twitter.com/_elletownsend",
              "http://www.elletownsend.co.uk"
            ],
            "published": [
              "2015-11-28T14:28:32+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1237757944089006086/67Kjy-LA.jpg"
            ]
          }
        }
      ],
      "content": [
        "Today I turned 21 - yes I am wearing a badge even if I spent the whole day at home 🎂"
      ],
      "photo": [
        "https://pbs.twimg.com/media/EVLstcZUMAA4FHW.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be"
}
