{
  "date": "2020-04-11T09:10:00+01:00",
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/JamieTanna/status/1248887274382729216"
    ],
    "in-reply-to": [
      "https://twitter.com/monotron_/status/1248747725576908801"
    ],
    "name": [
      "Reply to https://twitter.com/monotron_/status/1248747725576908801"
    ],
    "published": [
      "2020-04-11T09:10:00+01:00"
    ],
    "content": [
      {
        "html": "",
        "value": "Happy birthday 🎂 🎉"
      }
    ]
  },
  "kind": "replies",
  "slug": "2020/04/64j1d",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1248747725576908801"
      ],
      "url": [
        "https://twitter.com/monotron_/status/1248747725576908801"
      ],
      "published": [
        "2020-04-10T23:00:33+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:monotron_"
            ],
            "numeric-id": [
              "3399167086"
            ],
            "name": [
              "Toby"
            ],
            "nickname": [
              "monotron_"
            ],
            "url": [
              "https://twitter.com/monotron_",
              "https://github.com/monotron"
            ],
            "published": [
              "2015-08-01T19:13:59+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "United Kingdom"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1235696023093329920/4HP8BiRi.jpg"
            ]
          }
        }
      ],
      "content": [
        "It's my birthday 🥳"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be"
}
