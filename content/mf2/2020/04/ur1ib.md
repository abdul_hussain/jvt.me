{
  "date": "2020-04-17T21:18:54.739+02:00",
  "deleted": false,
  "h": "h-entry",
  "properties": {
    "in-reply-to": [
      "https://twitter.com/JamieTanna/status/1251227486870011906"
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1251229692121812996"
    ],
    "name": [
      "Reply to https://twitter.com/JamieTanna/status/1251227486870011906"
    ],
    "published": [
      "2020-04-17T21:18:54.739+02:00"
    ],
    "category": [

    ],
    "content": [
      {
        "html": "",
        "value": "And even higher timeouts..."
      }
    ]
  },
  "kind": "replies",
  "slug": "2020/04/ur1ib",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1251227486870011906"
      ],
      "url": [
        "https://twitter.com/JamieTanna/status/1251227486870011906"
      ],
      "published": [
        "2020-04-17T19:14:14+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/CarolSaysThings/status/1251201184645427208"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:JamieTanna"
            ],
            "numeric-id": [
              "2292531098"
            ],
            "name": [
              "Jamie Tanna | www.jvt.me"
            ],
            "nickname": [
              "JamieTanna"
            ],
            "url": [
              "https://twitter.com/JamieTanna",
              "https://www.jvt.me"
            ],
            "published": [
              "2014-01-15T10:55:11+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1207672129308778496/4erVfT-i.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Needed to up a few timeouts, 🤞 this time! (jvt.me/mf2/2020/04/ad…)",
          "html": "Needed to up a few timeouts, 🤞 this time! (<a href=\"https://www.jvt.me/mf2/2020/04/adoij/\">jvt.me/mf2/2020/04/ad…</a>)\n<a class=\"u-mention\" href=\"https://twitter.com/CarolSaysThings\"></a>"
        }
      ]
    }
  },
  "tags": [

  ],
  "client_id": "https://micropublish.net"
}
