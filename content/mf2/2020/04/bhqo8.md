{
  "date": "2020-04-11T19:05:00+01:00",
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/pascaldoesgo/status/1249033946400251909"
    ],
    "name": [
      "Like of @pascaldoesgo's tweet"
    ],
    "like-of": [
      "https://twitter.com/pascaldoesgo/status/1249033946400251909"
    ],
    "published": [
      "2020-04-11T19:05:00+01:00"
    ]
  },
  "kind": "likes",
  "slug": "2020/04/bhqo8",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1249033946400251909"
      ],
      "url": [
        "https://twitter.com/pascaldoesgo/status/1249033946400251909"
      ],
      "published": [
        "2020-04-11T17:57:53+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/JamieTanna/status/1249033619072520198"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:pascaldoesgo"
            ],
            "numeric-id": [
              "1003529600901767168"
            ],
            "name": [
              "Pascal Dennerly"
            ],
            "nickname": [
              "pascaldoesgo"
            ],
            "url": [
              "https://twitter.com/pascaldoesgo"
            ],
            "published": [
              "2018-06-04T06:51:16+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1199757075561222147/eYBkMUrZ.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Waaaaay ahead of you.",
          "html": "Waaaaay ahead of you.\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be"
}
