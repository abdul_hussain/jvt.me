{
  "date" : "2020-04-22T19:21:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/Alain_Tolhurst/status/1252987811751436293" ],
    "name" : [ "Like of @Alain_Tolhurst's tweet" ],
    "published" : [ "2020-04-22T19:21:00+01:00" ],
    "like-of" : [ "https://twitter.com/Alain_Tolhurst/status/1252987811751436293" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/q7he2",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1252987811751436293" ],
      "url" : [ "https://twitter.com/Alain_Tolhurst/status/1252987811751436293" ],
      "video" : [ "https://video.twimg.com/ext_tw_video/1252987659418566658/pu/vid/1280x720/rBEhBLZ5CuR6nzkx.mp4?tag=10" ],
      "published" : [ "2020-04-22T15:49:08+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Alain_Tolhurst" ],
          "numeric-id" : [ "29801423" ],
          "name" : [ "Alain Tolhurst" ],
          "nickname" : [ "Alain_Tolhurst" ],
          "url" : [ "https://twitter.com/Alain_Tolhurst" ],
          "published" : [ "2009-04-08T19:46:26+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Westminster" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1217449402597629953/IUAzeqlJ.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "We have the first great Parliamentary Zoom disaster, courtesy of @vaughangething in the Welsh Assembly",
        "html" : "We have the first great Parliamentary Zoom disaster, courtesy of <a href=\"https://twitter.com/vaughangething\">@vaughangething</a> in the Welsh Assembly"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
