{
  "date" : "2020-04-28T20:26:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/kvlly/status/1255173275090640897" ],
    "name" : [ "Like of @kvlly's tweet" ],
    "published" : [ "2020-04-28T20:26:00+01:00" ],
    "category" : [ "automation" ],
    "like-of" : [ "https://twitter.com/kvlly/status/1255173275090640897" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/oofwk",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1255173275090640897" ],
      "url" : [ "https://twitter.com/kvlly/status/1255173275090640897" ],
      "published" : [ "2020-04-28T16:33:23+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:kvlly" ],
          "numeric-id" : [ "123543103" ],
          "name" : [ "Kelly Vaughn 🐞" ],
          "nickname" : [ "kvlly" ],
          "url" : [ "https://twitter.com/kvlly", "http://kellyvaughn.co" ],
          "published" : [ "2010-03-16T12:15:39+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Atlanta, GA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1240319235714408456/EqySP4c8.jpg" ]
        }
      } ],
      "content" : [ "Why spend 8 minutes doing a one-time task manually when you can spend 2 hours attempting to automate it?" ]
    }
  },
  "tags" : [ "automation" ],
  "client_id" : "https://indigenous.realize.be"
}
