{
  "date" : "2020-04-29T22:59:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/edent/status/1255420517697404928" ],
    "name" : [ "Like of @edent's tweet" ],
    "published" : [ "2020-04-29T22:59:00+01:00" ],
    "like-of" : [ "https://twitter.com/edent/status/1255420517697404928" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/bkcso",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1255420517697404928" ],
      "url" : [ "https://twitter.com/edent/status/1255420517697404928" ],
      "published" : [ "2020-04-29T08:55:50+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:edent" ],
          "numeric-id" : [ "14054507" ],
          "name" : [ "Terence Eden" ],
          "nickname" : [ "edent" ],
          "url" : [ "https://twitter.com/edent", "https://shkspr.mobi/blog/", "https://edent.tel" ],
          "published" : [ "2008-02-28T13:10:25+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1228067445153452033/_A8Uq2VY.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "There is something immensely satisfying about finding a bug in software, reading the source code, submitting a patch, and having it accepted.\nAll in a few hours.\n(and, yes, I'm slightly gutted that it wasn't #1138!)",
        "html" : "<div style=\"white-space: pre\">There is something immensely satisfying about finding a bug in software, reading the source code, submitting a patch, and having it accepted.\nAll in a few hours.\n(and, yes, I'm slightly gutted that it wasn't #1138!)</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EWwlvb9XsAEFfNB.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
