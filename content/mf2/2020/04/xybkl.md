{
  "kind": "replies",
  "slug": "2020/04/xybkl",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1245242122808123392"
      ],
      "url": [
        "https://twitter.com/_am1t/status/1245242122808123392"
      ],
      "published": [
        "2020-04-01T06:50:32+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/JamieTanna/status/1245239651985772544"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:_am1t"
            ],
            "numeric-id": [
              "9333372"
            ],
            "name": [
              "Amit Gawande"
            ],
            "nickname": [
              "_am1t"
            ],
            "url": [
              "https://twitter.com/_am1t",
              "https://www.amitgawande.com"
            ],
            "published": [
              "2007-10-09T18:00:31+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Pune"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1014380627158761472/II4EAplk.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Ok sure, no worries. I wasn't aware if this was open or closed. Had reached out to Aaron first - thought online events were open for all. Again, no problem at all. Will try to join London event.",
          "html": "Ok sure, no worries. I wasn't aware if this was open or closed. Had reached out to Aaron first - thought online events were open for all. Again, no problem at all. Will try to join London event.\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-04-01T08:02:00+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/_am1t/status/1245242122808123392"
    ],
    "in-reply-to": [
      "https://twitter.com/_am1t/status/1245242122808123392"
    ],
    "published": [
      "2020-04-01T08:02:00+01:00"
    ],
    "content": [
      {
        "html": "",
        "value": "They usually are, yes - given there's been a lot of zoombombing recently I want to get a few things in place before it's fully open for remote - sorry, and hope it'll be available for next time!"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1245246553071267841"
    ]
  }
}
