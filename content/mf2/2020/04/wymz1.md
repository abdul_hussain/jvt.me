{
  "date": "2020-04-06T22:04:00+01:00",
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/anna_hax/status/1247136528553381889"
    ],
    "name": [
      "Like of @anna_hax's tweet"
    ],
    "like-of": [
      "https://twitter.com/anna_hax/status/1247136528553381889"
    ],
    "published": [
      "2020-04-06T22:04:00+01:00"
    ]
  },
  "kind": "likes",
  "slug": "2020/04/wymz1",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1247136528553381889"
      ],
      "url": [
        "https://twitter.com/anna_hax/status/1247136528553381889"
      ],
      "published": [
        "2020-04-06T12:18:13+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:anna_hax"
            ],
            "numeric-id": [
              "394235377"
            ],
            "name": [
              "Anna 🏠"
            ],
            "nickname": [
              "anna_hax"
            ],
            "url": [
              "https://twitter.com/anna_hax",
              "https://annadodson.co.uk"
            ],
            "published": [
              "2011-10-19T19:37:31+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1134736020669378561/0_EwVzms.jpg"
            ]
          }
        }
      ],
      "content": [
        "Pool sliders and thermal socks. Yes, 100% yes 🔥"
      ],
      "photo": [
        "https://pbs.twimg.com/media/EU64DObX0AEXG6Z.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be"
}
