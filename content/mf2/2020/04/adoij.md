{
  "date": "2020-04-17T21:10:05.374+02:00",
  "deleted": false,
  "h": "h-entry",
  "properties": {
    "in-reply-to": [
      "https://twitter.com/CarolSaysThings/status/1251201184645427208"
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1251227486870011906"
    ],
    "name": [
      "Reply to https://twitter.com/CarolSaysThings/status/1251201184645427208"
    ],
    "published": [
      "2020-04-17T21:10:05.374+02:00"
    ],
    "category": [

    ],
    "content": [
      {
        "html": "",
        "value": "Needed to up a few timeouts, 🤞 this time!"
      }
    ]
  },
  "kind": "replies",
  "slug": "2020/04/adoij",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1251201184645427208"
      ],
      "url": [
        "https://twitter.com/CarolSaysThings/status/1251201184645427208"
      ],
      "published": [
        "2020-04-17T17:29:43+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/JamieTanna/status/1251200698274009094"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:CarolSaysThings"
            ],
            "numeric-id": [
              "36382927"
            ],
            "name": [
              "Carol ⚡️"
            ],
            "nickname": [
              "CarolSaysThings"
            ],
            "url": [
              "https://twitter.com/CarolSaysThings",
              "https://carolgilabert.me/"
            ],
            "published": [
              "2009-04-29T15:22:13+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "🇧🇷🇪🇸🇬🇧 · Nottingham"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1238515159594917889/C5994QPa.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "🤞",
          "html": "🤞\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>"
        }
      ]
    }
  },
  "tags": [

  ],
  "client_id": "https://micropublish.net"
}
