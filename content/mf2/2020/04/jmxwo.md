{
  "date" : "2020-04-27T14:01:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/mschoening/status/1254445765092507648" ],
    "name" : [ "Like of @mschoening's tweet" ],
    "published" : [ "2020-04-27T14:01:00+01:00" ],
    "like-of" : [ "https://twitter.com/mschoening/status/1254445765092507648" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/jmxwo",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1254445765092507648" ],
      "url" : [ "https://twitter.com/mschoening/status/1254445765092507648" ],
      "published" : [ "2020-04-26T16:22:31+00:00" ],
      "in-reply-to" : [ "https://twitter.com/kneath/status/1254445189554270209" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:mschoening" ],
          "numeric-id" : [ "14239565" ],
          "name" : [ "Max Schoening" ],
          "nickname" : [ "mschoening" ],
          "url" : [ "https://twitter.com/mschoening", "https://max.dev/" ],
          "published" : [ "2008-03-27T23:04:59+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "San Francisco" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1254549113103761411/s1QFYG_8.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "A fidget spinner for enterprise customers? Sounds useful.",
        "html" : "A fidget spinner for enterprise customers? Sounds useful.\n<a class=\"u-mention\" href=\"https://twitter.com/kneath\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
