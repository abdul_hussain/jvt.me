{
  "date" : "2020-04-29T13:46:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/DevopsNotts/status/1255469196886126592" ],
    "name" : [ "Like of @DevopsNotts's tweet" ],
    "published" : [ "2020-04-29T13:46:00+01:00" ],
    "like-of" : [ "https://twitter.com/DevopsNotts/status/1255469196886126592" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/gi7vv",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1255469196886126592" ],
      "url" : [ "https://twitter.com/DevopsNotts/status/1255469196886126592" ],
      "published" : [ "2020-04-29T12:09:16+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:DevopsNotts" ],
          "numeric-id" : [ "1141066068032741376" ],
          "name" : [ "DevOps Notts" ],
          "nickname" : [ "DevopsNotts" ],
          "url" : [ "https://twitter.com/DevopsNotts" ],
          "published" : [ "2019-06-18T19:32:05+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1148214665882849281/l7RvVFSV.png" ]
        }
      } ],
      "content" : [ {
        "value" : "April 2020's winner of the @jetbrains license for @DevopsNotts is @JamieTanna!\n\nCongratulations @JamieTanna! A license will be making it's way to your DMs soon!",
        "html" : "<div style=\"white-space: pre\">April 2020's winner of the <a href=\"https://twitter.com/jetbrains\">@jetbrains</a> license for <a href=\"https://twitter.com/DevopsNotts\">@DevopsNotts</a> is <a href=\"https://twitter.com/JamieTanna\">@JamieTanna</a>!\n\nCongratulations @JamieTanna! A license will be making it's way to your DMs soon!</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EWxSfmrXYAI6zuQ.png" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
