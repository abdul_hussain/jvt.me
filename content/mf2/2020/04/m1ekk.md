{
  "date": "2020-04-10T10:41:00+01:00",
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/ZiziFothSi/status/1248370470111645698"
    ],
    "name": [
      "Like of @ZiziFothSi's tweet"
    ],
    "like-of": [
      "https://twitter.com/ZiziFothSi/status/1248370470111645698"
    ],
    "published": [
      "2020-04-10T10:41:00+01:00"
    ],
    "category": [
      "cute"
    ]
  },
  "tags": [
    "cute"
  ],
  "kind": "likes",
  "slug": "2020/04/m1ekk",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1248370470111645698"
      ],
      "url": [
        "https://twitter.com/ZiziFothSi/status/1248370470111645698"
      ],
      "published": [
        "2020-04-09T22:01:28+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:ZiziFothSi"
            ],
            "numeric-id": [
              "79504275"
            ],
            "name": [
              "Katie"
            ],
            "nickname": [
              "ZiziFothSi"
            ],
            "url": [
              "https://twitter.com/ZiziFothSi",
              "http://katharinegrayart.com"
            ],
            "published": [
              "2009-10-03T16:30:51+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "London"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1232322920392904704/D5u2nKD9.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Him scronch \n\nHim stremch",
          "html": "<div style=\"white-space: pre\">Him scronch \n\nHim stremch</div>"
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/EVMaTbaXQAMjMba.jpg",
        "https://pbs.twimg.com/media/EVMaTelU4AIZxR_.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be"
}
