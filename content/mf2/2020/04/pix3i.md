{
  "kind": "replies",
  "slug": "2020/04/pix3i",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1245204273563979779"
      ],
      "url": [
        "https://twitter.com/_am1t/status/1245204273563979779"
      ],
      "published": [
        "2020-04-01T04:20:08+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/JamieTanna/status/1245094024819740673"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:_am1t"
            ],
            "numeric-id": [
              "9333372"
            ],
            "name": [
              "Amit Gawande"
            ],
            "nickname": [
              "_am1t"
            ],
            "url": [
              "https://twitter.com/_am1t",
              "https://www.amitgawande.com"
            ],
            "published": [
              "2007-10-09T18:00:31+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Pune"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1014380627158761472/II4EAplk.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Just curious, is it only for Nottingham folks? Or people from other places are welcome too?",
          "html": "Just curious, is it only for Nottingham folks? Or people from other places are welcome too?\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-04-01T07:35:00+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/_am1t/status/1245204273563979779"
    ],
    "in-reply-to": [
      "https://twitter.com/_am1t/status/1245204273563979779"
    ],
    "published": [
      "2020-04-01T07:35:00+01:00"
    ],
    "content": [
      {
        "html": "",
        "value": "Hey Amit I meant to reach out - I think for this one I'll probably keep it as just for Nottingham, but I know the London event https://events.indieweb.org/2020/04/online-homebrew-website-club-europe-london-2Kc9aetW589f is open"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1245239651985772544"
    ]
  }
}
