{
  "date" : "2020-04-21T16:16:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/rwdrich/status/1252552877228396544" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1252618842142097408" ],
    "name" : [ "Reply to https://twitter.com/rwdrich/status/1252552877228396544" ],
    "published" : [ "2020-04-21T16:16:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "I had some spaghetti that came out of the tin in that shape the other day and @annadodson.co.uk was having none of it"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/04/2mvwx",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1252552877228396544" ],
      "url" : [ "https://twitter.com/rwdrich/status/1252552877228396544" ],
      "published" : [ "2020-04-21T11:00:51+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:rwdrich" ],
          "numeric-id" : [ "107548386" ],
          "name" : [ "Richard Davies" ],
          "nickname" : [ "rwdrich" ],
          "url" : [ "https://twitter.com/rwdrich", "http://rwdrich.co.uk" ],
          "published" : [ "2010-01-22T23:14:30+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Cambridge, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/917481919872491521/BwypzjEE.jpg" ]
        }
      } ],
      "content" : [ "It's always so appealing when soup comes out in the shape of the carton/tin ..." ],
      "photo" : [ "https://pbs.twimg.com/media/EWH2MoLXkAMjNv4.jpg" ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
