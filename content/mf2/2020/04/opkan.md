{
  "date": "2020-04-15T12:42:00+01:00",
  "deleted": false,
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/ThisGeekTweets/status/1250373902649393153"
    ],
    "name": [
      "Like of @ThisGeekTweets's tweet"
    ],
    "published": [
      "2020-04-15T12:42:00+01:00"
    ],
    "like-of": [
      "https://twitter.com/ThisGeekTweets/status/1250373902649393153"
    ]
  },
  "kind": "likes",
  "slug": "2020/04/opkan",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1250373902649393153"
      ],
      "url": [
        "https://twitter.com/ThisGeekTweets/status/1250373902649393153"
      ],
      "published": [
        "2020-04-15T10:42:23+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:ThisGeekTweets"
            ],
            "numeric-id": [
              "1183869302"
            ],
            "name": [
              "Gary Williams"
            ],
            "nickname": [
              "ThisGeekTweets"
            ],
            "url": [
              "https://twitter.com/ThisGeekTweets",
              "http://www.thisgeektweets.com"
            ],
            "published": [
              "2013-02-15T21:42:16+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Norwich, Norfolk"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/816747717372297218/k_zgDdTK.jpg"
            ]
          }
        }
      ],
      "content": [
        "Great message of the day today."
      ],
      "photo": [
        "https://pbs.twimg.com/media/EVo4X9jXgAA2kOi.png"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be"
}
