{
  "date" : "2020-04-29T12:10:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [  ],
    "name" : [ "Like of @andyRoidO's tweet" ],
    "published" : [ "2020-04-29T12:10:00+01:00" ],
    "like-of" : [ "https://twitter.com/andyRoidO/status/1254521447655014401" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/lnqmt",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1254521447655014401" ],
      "url" : [ "https://twitter.com/andyRoidO/status/1254521447655014401" ],
      "published" : [ "2020-04-26T21:23:15+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:andyRoidO" ],
          "numeric-id" : [ "3587979616" ],
          "name" : [ "Andy Obuoforibo" ],
          "nickname" : [ "andyRoidO" ],
          "url" : [ "https://twitter.com/andyRoidO", "http://www.andyobuoforibo.com" ],
          "published" : [ "2015-09-08T13:16:22+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Lagos, Nigeria" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1171405456063025159/uZa42AJX.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "PUT THAT SHIT BACK!!\n\nTHIS IS NOT THE YEAR!",
        "html" : "<div style=\"white-space: pre\">PUT THAT SHIT BACK!!\n\nTHIS IS NOT THE YEAR!</div>"
      } ]
    },
    "children" : [ {
      "type" : [ "u-quotation-of", "h-cite" ],
      "properties" : {
        "uid" : [ "tag:twitter.com:1254358546545704960" ],
        "url" : [ "https://twitter.com/Museum_archive/status/1254358546545704960" ],
        "published" : [ "2020-04-26T10:35:57+00:00" ],
        "author" : [ {
          "type" : [ "h-card" ],
          "properties" : {
            "uid" : [ "tag:twitter.com:Museum_archive" ],
            "numeric-id" : [ "1196339698895147009" ],
            "name" : [ "Museum Archive" ],
            "nickname" : [ "Museum_archive" ],
            "url" : [ "https://twitter.com/Museum_archive" ],
            "published" : [ "2019-11-18T08:10:01+00:00" ],
            "photo" : [ "https://pbs.twimg.com/profile_images/1249220712461733888/3Z1i_y0-.jpg" ]
          }
        } ],
        "content" : [ "More than 20 sealed coffins discovered near Luxor Egypt" ],
        "photo" : [ "https://pbs.twimg.com/media/EWhgYs3UwAYcBLe.jpg" ]
      }
    } ]
  },
  "client_id" : "https://indigenous.realize.be"
}
