{
  "date" : "2020-04-26T15:27:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/MrsEmma/status/1254179903077048326" ],
    "name" : [ "Like of @MrsEmma's tweet" ],
    "published" : [ "2020-04-26T15:27:00+01:00" ],
    "like-of" : [ "https://twitter.com/MrsEmma/status/1254179903077048326" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/xicam",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1254179903077048326" ],
      "url" : [ "https://twitter.com/MrsEmma/status/1254179903077048326" ],
      "video" : [ "https://video.twimg.com/tweet_video/EWe9-EeWsAAuIeu.mp4" ],
      "published" : [ "2020-04-25T22:46:05+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:MrsEmma" ],
          "numeric-id" : [ "19500955" ],
          "name" : [ "Emma Seward" ],
          "nickname" : [ "MrsEmma" ],
          "url" : [ "https://twitter.com/MrsEmma" ],
          "published" : [ "2009-01-25T19:32:42+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1247624012190158852/rhR91wjJ.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Missing friends so much 😔 Zoom isn't the same - I want to hug everyone and touch their faces.\n\n(Like that time I stroked @LittleHelli's face in the night when I was half asleep. Because that wasn't weird.\n\n\"That's your nose.\"\nYes.\n\"Sorry.\"\n\nGood times.)",
        "html" : "<div style=\"white-space: pre\">Missing friends so much 😔 Zoom isn't the same - I want to hug everyone and touch their faces.\n\n(Like that time I stroked <a href=\"https://twitter.com/LittleHelli\">@LittleHelli</a>'s face in the night when I was half asleep. Because that wasn't weird.\n\n\"That's your nose.\"\nYes.\n\"Sorry.\"\n\nGood times.)</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
