{
  "date" : "2020-04-29T12:12:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/__apf__/status/1255280553156382720" ],
    "name" : [ "Like of @__apf__'s tweet" ],
    "published" : [ "2020-04-29T12:12:00+01:00" ],
    "like-of" : [ "https://twitter.com/__apf__/status/1255280553156382720" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/c00rv",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1255280553156382720" ],
      "url" : [ "https://twitter.com/__apf__/status/1255280553156382720" ],
      "published" : [ "2020-04-28T23:39:40+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:__apf__" ],
          "numeric-id" : [ "533539874" ],
          "name" : [ "Adrienne Porter Felt" ],
          "nickname" : [ "__apf__" ],
          "url" : [ "https://twitter.com/__apf__", "https://www.adrienneporterfelt.com" ],
          "published" : [ "2012-03-22T21:42:54+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Mountain View, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1179506821817610240/ydOqD2Pv.jpg" ]
        }
      } ],
      "content" : [ "I learned today that my child can add numbers up to 10... as long as you phrase it in terms of cookies. \"If you have 2 cookies and I give you 4 more, how many cookies do you have?\" Subtraction does not work because he gets mad about losing cookies." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
