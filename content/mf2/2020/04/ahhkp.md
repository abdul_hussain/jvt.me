{
  "date" : "2020-04-19T12:24:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/cr3/status/1251508803499065345" ],
    "name" : [ "Like of @cr3's tweet" ],
    "published" : [ "2020-04-19T12:24:00+01:00" ],
    "like-of" : [ "https://twitter.com/cr3/status/1251508803499065345" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/ahhkp",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1251508803499065345" ],
      "url" : [ "https://twitter.com/cr3/status/1251508803499065345" ],
      "published" : [ "2020-04-18T13:52:05+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:cr3" ],
          "numeric-id" : [ "7163332" ],
          "name" : [ "Paul Curry" ],
          "nickname" : [ "cr3" ],
          "url" : [ "https://twitter.com/cr3", "https://www.cr3ative.co.uk" ],
          "published" : [ "2007-06-30T00:12:32+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Kent / London" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/876714936122126336/0bZUQUeS.jpg" ]
        }
      } ],
      "content" : [ "Bought a breathalyser and the first sentence of the manual is DON’T TRY TO SET A HIGH SCORE" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
