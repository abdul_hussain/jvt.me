{
  "date": "2020-04-11T19:05:00+01:00",
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/JamieTanna/status/1249037335871860738"
    ],
    "in-reply-to": [
      "https://twitter.com/Marcus_Noble_/status/1249033872731488256"
    ],
    "name": [
      "Reply to https://twitter.com/Marcus_Noble_/status/1249033872731488256"
    ],
    "published": [
      "2020-04-11T19:05:00+01:00"
    ],
    "content": [
      {
        "html": "",
        "value": "Not that I'm aware of, sorry!"
      }
    ]
  },
  "kind": "replies",
  "slug": "2020/04/im8zo",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1249033872731488256"
      ],
      "url": [
        "https://twitter.com/Marcus_Noble_/status/1249033872731488256"
      ],
      "published": [
        "2020-04-11T17:57:35+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/JamieTanna/status/1249033508003229697"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:Marcus_Noble_"
            ],
            "numeric-id": [
              "14732545"
            ],
            "name": [
              "Marcus Noble"
            ],
            "nickname": [
              "Marcus_Noble_"
            ],
            "url": [
              "https://twitter.com/Marcus_Noble_",
              "https://marcusnoble.co.uk"
            ],
            "published": [
              "2008-05-11T09:51:36+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Some container"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/776738772759277569/hfaM5zhA.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "They're in my list to check out. Only need a two instance setup for now. Gonna do done price comparisons later I think.\n\nDo you know if they do load balancers?",
          "html": "<div style=\"white-space: pre\">They're in my list to check out. Only need a two instance setup for now. Gonna do done price comparisons later I think.\n\nDo you know if they do load balancers?</div>\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be"
}
