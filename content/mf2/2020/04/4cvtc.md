{
  "date" : "2020-04-26T22:45:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.meetup.com/DevOps-Notts/events/267477505/" ],
    "syndication" : [ "https://www.meetup.com/DevOps-Notts/events/267477505/#rsvp-by-https%3A%2F%2Fwww.jvt.me%2Fmf2%2F2020%2F04%2F4cvtc%2F" ],
    "name" : [ "RSVP yes to https://www.meetup.com/DevOps-Notts/events/267477505/" ],
    "published" : [ "2020-04-26T22:45:00+01:00" ],
    "event" : {
      "start" : [ "2020-04-28T18:30:00+01:00" ],
      "name" : [ "DevOps Notts - April 2020" ],
      "end" : [ "2020-04-28T21:30:00+01:00" ],
      "location" : [ "Online" ],
      "url" : [ "https://www.meetup.com/DevOps-Notts/events/267477505/" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/04/4cvtc",
  "client_id" : "https://indigenous.realize.be"
}
