{
  "kind": "likes",
  "slug": "2020/04/a3ppy",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1246064620307124224"
      ],
      "url": [
        "https://twitter.com/whoisgraham/status/1246064620307124224"
      ],
      "published": [
        "2020-04-03T13:18:50+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/WiT_Notts/status/1245799382915710976"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:whoisgraham"
            ],
            "numeric-id": [
              "133487462"
            ],
            "name": [
              "Graham : A Star Wars Story"
            ],
            "nickname": [
              "whoisgraham"
            ],
            "url": [
              "https://twitter.com/whoisgraham"
            ],
            "published": [
              "2010-04-15T23:03:34+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/2907221110/712dfcecc7fd591915d1355bdbe4adcd.jpeg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Playing Where's Waldo? with the tech community 👀 @JamieTanna",
          "html": "Playing Where's Waldo? with the tech community 👀 <a href=\"https://twitter.com/JamieTanna\">@JamieTanna</a>\n<a class=\"u-mention\" href=\"https://twitter.com/WiT_Notts\"></a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-04-03T14:44:00+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @whoisgraham's tweet"
    ],
    "like-of": [
      "https://twitter.com/whoisgraham/status/1246064620307124224"
    ],
    "published": [
      "2020-04-03T14:44:00+01:00"
    ],
    "syndication": [
      "https://twitter.com/whoisgraham/status/1246064620307124224"
    ]
  }
}
