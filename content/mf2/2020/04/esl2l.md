{
  "date": "2020-04-18T14:31:04.736+02:00",
  "deleted": false,
  "h": "h-entry",
  "properties": {
    "in-reply-to": [
      "https://twitter.com/JamieTanna/status/1251200698274009094"
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1251489411843399680"
    ],
    "name": [
      "Reply to https://twitter.com/JamieTanna/status/1251200698274009094"
    ],
    "published": [
      "2020-04-18T14:31:04.736+02:00"
    ],
    "category": [

    ],
    "content": [
      {
        "html": "",
        "value": "Code is merged, bugs fixed in both Micropub and post-deploy, so this should work!"
      }
    ]
  },
  "kind": "replies",
  "slug": "2020/04/esl2l",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1251200698274009094"
      ],
      "url": [
        "https://twitter.com/JamieTanna/status/1251200698274009094"
      ],
      "published": [
        "2020-04-17T17:27:47+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:JamieTanna"
            ],
            "numeric-id": [
              "2292531098"
            ],
            "name": [
              "Jamie Tanna | www.jvt.me"
            ],
            "nickname": [
              "JamieTanna"
            ],
            "url": [
              "https://twitter.com/JamieTanna",
              "https://www.jvt.me"
            ],
            "published": [
              "2014-01-15T10:55:11+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1207672129308778496/4erVfT-i.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "One more time, with the right token... (jvt.me/mf2/2020/04/jo…)",
          "html": "One more time, with the right token... (<a href=\"https://www.jvt.me/mf2/2020/04/joym8/\">jvt.me/mf2/2020/04/jo…</a>)"
        }
      ]
    }
  },
  "tags": [

  ],
  "client_id": "https://micropublish.net"
}
