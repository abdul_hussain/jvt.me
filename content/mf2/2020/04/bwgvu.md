{
  "date": "2020-04-16T18:48:00+01:00",
  "deleted": false,
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/alindeman/status/1250478857318268935"
    ],
    "name": [
      "Like of @alindeman's tweet"
    ],
    "published": [
      "2020-04-16T18:48:00+01:00"
    ],
    "like-of": [
      "https://twitter.com/alindeman/status/1250478857318268935"
    ]
  },
  "kind": "likes",
  "slug": "2020/04/bwgvu",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1250478857318268935"
      ],
      "url": [
        "https://twitter.com/alindeman/status/1250478857318268935"
      ],
      "published": [
        "2020-04-15T17:39:27+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:alindeman"
            ],
            "numeric-id": [
              "13235612"
            ],
            "name": [
              "Andy Lindeman"
            ],
            "nickname": [
              "alindeman"
            ],
            "url": [
              "https://twitter.com/alindeman",
              "http://andylindeman.com"
            ],
            "published": [
              "2008-02-08T05:13:54+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Atlanta, GA, USA"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/790184084425347072/Tslq3rBo.jpg"
            ]
          }
        }
      ],
      "content": [
        "OH: \"DNS is just naming things and cache invalidation, how could it end up being complicated?\""
      ]
    }
  },
  "client_id": "https://indigenous.realize.be"
}
