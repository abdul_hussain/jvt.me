{
  "date": "2020-04-17T14:02:00+01:00",
  "deleted": false,
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/shaunnorris/status/1251038955014782976"
    ],
    "name": [
      "Like of @shaunnorris's tweet"
    ],
    "published": [
      "2020-04-17T14:02:00+01:00"
    ],
    "like-of": [
      "https://twitter.com/shaunnorris/status/1251038955014782976"
    ]
  },
  "kind": "likes",
  "slug": "2020/04/om1kq",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1251038955014782976"
      ],
      "url": [
        "https://twitter.com/shaunnorris/status/1251038955014782976"
      ],
      "published": [
        "2020-04-17T06:45:04+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:shaunnorris"
            ],
            "numeric-id": [
              "11297362"
            ],
            "name": [
              "Shaun Norris"
            ],
            "nickname": [
              "shaunnorris"
            ],
            "url": [
              "https://twitter.com/shaunnorris",
              "http://pivotal.io"
            ],
            "published": [
              "2007-12-18T15:28:23+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Singapore"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1211905288560709634/CPMK7gPy.jpg"
            ]
          }
        }
      ],
      "location": [
        {
          "type": [
            "h-card",
            "p-location"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:6635b2fcebd13c64"
            ],
            "name": [
              "East Region, Singapore"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Just turned down a speaking panel engagement as there were no women on the planned panel. Took opportunity to recommend several women more qualified than me from my network #justsaynotomanels",
          "html": "Just turned down a speaking panel engagement as there were no women on the planned panel. Took opportunity to recommend several women more qualified than me from my network <a href=\"https://twitter.com/search?q=%23justsaynotomanels\">#justsaynotomanels</a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be"
}
