{
  "date" : "2020-04-27T18:31:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/bfishbfish/status/1254567969545469954" ],
    "name" : [ "Like of @bfishbfish's tweet" ],
    "published" : [ "2020-04-27T18:31:00+01:00" ],
    "like-of" : [ "https://twitter.com/bfishbfish/status/1254567969545469954" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/jllnk",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1254567969545469954" ],
      "url" : [ "https://twitter.com/bfishbfish/status/1254567969545469954" ],
      "published" : [ "2020-04-27T00:28:07+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:bfishbfish" ],
          "numeric-id" : [ "37613042" ],
          "name" : [ "Rebecca Fishbein" ],
          "nickname" : [ "bfishbfish" ],
          "url" : [ "https://twitter.com/bfishbfish", "http://rebeccafishbein.com" ],
          "published" : [ "2009-05-04T06:57:11+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Brooklyn, NY" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1220711949727674371/PT3jx-wS.png" ]
        }
      } ],
      "content" : [ "Ah yes, my guest room" ],
      "photo" : [ "https://pbs.twimg.com/media/EWke1dzWsAA0N5X.png" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
