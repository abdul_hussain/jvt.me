{
  "date": "2020-04-13T10:09:00+01:00",
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/JamieTanna/status/1249626915478134787"
    ],
    "in-reply-to": [
      "https://twitter.com/Marcus_Noble_/status/1249289428872433664"
    ],
    "name": [
      "Reply to https://twitter.com/Marcus_Noble_/status/1249289428872433664"
    ],
    "published": [
      "2020-04-13T10:09:00+01:00"
    ],
    "content": [
      {
        "html": "",
        "value": "Oh no! @annadodson.co.uk was looking to get one of her own, so is a bit disappointed by the news. How come? Is it that just the island won't transfer, or nothing at all?"
      }
    ]
  },
  "kind": "replies",
  "slug": "2020/04/f9xzi",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1249289428872433664"
      ],
      "url": [
        "https://twitter.com/Marcus_Noble_/status/1249289428872433664"
      ],
      "published": [
        "2020-04-12T10:53:05+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:Marcus_Noble_"
            ],
            "numeric-id": [
              "14732545"
            ],
            "name": [
              "Marcus Noble"
            ],
            "nickname": [
              "Marcus_Noble_"
            ],
            "url": [
              "https://twitter.com/Marcus_Noble_",
              "https://marcusnoble.co.uk"
            ],
            "published": [
              "2008-05-11T09:51:36+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Some container"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/776738772759277569/hfaM5zhA.jpg"
            ]
          }
        }
      ],
      "content": [
        "Just learnt you can't transfer animal crossing saves to a new switch. ☹️ Maybe not gonna bother getting a switch lite just yet."
      ]
    }
  },
  "client_id": "https://indigenous.realize.be"
}
