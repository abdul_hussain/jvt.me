{
  "date": "2020-04-16T19:50:00+01:00",
  "deleted": false,
  "h": "h-entry",
  "properties": {
    "in-reply-to": [
      "https://twitter.com/edent/status/1250858869573771264"
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1250860423433379840"
    ],
    "name": [
      "Reply to https://twitter.com/edent/status/1250858869573771264"
    ],
    "published": [
      "2020-04-16T19:50:00+01:00"
    ],
    "category": [

    ],
    "content": [
      {
        "html": "",
        "value": "Yes, I think I saw this when I first had the post up, I'd tried to resolve it but it broke on other platforms 😅 I think maybe Slack? But if you've got any thoughts on the right way to fix that'd be appreciated!"
      }
    ]
  },
  "kind": "replies",
  "slug": "2020/04/mxvx7",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1250858869573771264"
      ],
      "url": [
        "https://twitter.com/edent/status/1250858869573771264"
      ],
      "published": [
        "2020-04-16T18:49:28+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/JamieTanna/status/1250842766558597120"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:edent"
            ],
            "numeric-id": [
              "14054507"
            ],
            "name": [
              "Terence Eden"
            ],
            "nickname": [
              "edent"
            ],
            "url": [
              "https://twitter.com/edent",
              "https://shkspr.mobi/blog/",
              "https://edent.tel"
            ],
            "published": [
              "2008-02-28T13:10:25+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "London, UK"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1228067445153452033/_A8Uq2VY.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Looks like a problem with your OGP metatags.",
          "html": "Looks like a problem with your OGP metatags.\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/kevinmarks\"></a>"
        }
      ]
    }
  },
  "tags": [

  ],
  "client_id": "https://indigenous.realize.be"
}
