{
  "date": "2020-04-13T11:59:00+01:00",
  "deleted": false,
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/anna_hax/status/1249599392564359172"
    ],
    "name": [
      "Like of @anna_hax's tweet"
    ],
    "like-of": [
      "https://twitter.com/anna_hax/status/1249599392564359172"
    ],
    "published": [
      "2020-04-13T11:59:00+01:00"
    ]
  },
  "kind": "likes",
  "slug": "2020/04/6jzfv",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1249599392564359172"
      ],
      "url": [
        "https://twitter.com/anna_hax/status/1249599392564359172"
      ],
      "video": [
        "https://video.twimg.com/tweet_video/EVd4AxCXQAAUgHn.mp4"
      ],
      "published": [
        "2020-04-13T07:24:46+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:anna_hax"
            ],
            "numeric-id": [
              "394235377"
            ],
            "name": [
              "Anna 🏠"
            ],
            "nickname": [
              "anna_hax"
            ],
            "url": [
              "https://twitter.com/anna_hax",
              "https://annadodson.co.uk"
            ],
            "published": [
              "2011-10-19T19:37:31+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1134736020669378561/0_EwVzms.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Me jumping on the #AnimalCrossingNewHorizons bandwagon this weekend",
          "html": "Me jumping on the <a href=\"https://twitter.com/search?q=%23AnimalCrossingNewHorizons\">#AnimalCrossingNewHorizons</a> bandwagon this weekend"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be"
}
