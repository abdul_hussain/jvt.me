{
  "kind": "likes",
  "slug": "2020/04/mefku",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1245795799998611456"
      ],
      "url": [
        "https://twitter.com/paulienuh/status/1245795799998611456"
      ],
      "published": [
        "2020-04-02T19:30:39+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/JamieTanna/status/1245791797059694593"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:paulienuh"
            ],
            "numeric-id": [
              "230185453"
            ],
            "name": [
              "Pauline is at home 🧚🏻"
            ],
            "nickname": [
              "paulienuh"
            ],
            "url": [
              "https://twitter.com/paulienuh",
              "https://pawlean.com"
            ],
            "published": [
              "2010-12-24T14:46:00+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Leeds, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1241319221344325632/mJF_s5Oi.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Thanks for all your comments Jamie! Means a lot from a blogger like yourself ☺️",
          "html": "Thanks for all your comments Jamie! Means a lot from a blogger like yourself ☺️\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>"
        }
      ]
    }
  },
  "client_id": "https://micropublish.net",
  "date": "2020-04-02T21:31:10.444+02:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @paulienuh's tweet"
    ],
    "like-of": [
      "https://twitter.com/paulienuh/status/1245795799998611456"
    ],
    "published": [
      "2020-04-02T21:31:10.444+02:00"
    ],
    "syndication": [
      "https://twitter.com/paulienuh/status/1245795799998611456"
    ]
  }
}
