{
  "kind": "likes",
  "slug": "2020/04/smkh5",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1246446014879412224"
      ],
      "url": [
        "https://twitter.com/MrAndrew/status/1246446014879412224"
      ],
      "published": [
        "2020-04-04T14:34:22+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:MrAndrew"
            ],
            "numeric-id": [
              "9626182"
            ],
            "name": [
              "Andrew Seward"
            ],
            "nickname": [
              "MrAndrew"
            ],
            "url": [
              "https://twitter.com/MrAndrew",
              "http://www.technottingham.com"
            ],
            "published": [
              "2007-10-23T15:57:18+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Riddings, Derbyshire"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1246456234452140032/romkM0EV.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "For reasons involving boredom and loneliness, I am live on Twitch right now! Join me for a tour of my Minecraft airport :) twitch.tv/mrandrewplayin…",
          "html": "For reasons involving boredom and loneliness, I am live on Twitch right now! Join me for a tour of my Minecraft airport :) <a href=\"https://www.twitch.tv/mrandrewplaying/\">twitch.tv/mrandrewplayin…</a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-04-04T17:29:00+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @MrAndrew's tweet"
    ],
    "like-of": [
      "https://twitter.com/MrAndrew/status/1246446014879412224"
    ],
    "published": [
      "2020-04-04T17:29:00+01:00"
    ],
    "syndication": [
      "https://twitter.com/MrAndrew/status/1246446014879412224"
    ]
  }
}
