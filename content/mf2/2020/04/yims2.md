{
  "date": "2020-04-13T23:42:00+01:00",
  "deleted": false,
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/quarantinerules/status/1244028548295397377"
    ],
    "name": [
      "Like of @quarantinerules's tweet"
    ],
    "published": [
      "2020-04-13T23:42:00+01:00"
    ],
    "like-of": [
      "https://twitter.com/quarantinerules/status/1244028548295397377"
    ]
  },
  "kind": "likes",
  "slug": "2020/04/yims2",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1244028548295397377"
      ],
      "url": [
        "https://twitter.com/quarantinerules/status/1244028548295397377"
      ],
      "published": [
        "2020-03-28T22:28:13+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:quarantinerules"
            ],
            "numeric-id": [
              "1244027753550282754"
            ],
            "name": [
              "Social Distancing Rules"
            ],
            "nickname": [
              "quarantinerules"
            ],
            "url": [
              "https://twitter.com/quarantinerules"
            ],
            "published": [
              "2020-03-28T22:25:16+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1244028515638534147/7ZIhek1r.jpg"
            ]
          }
        }
      ],
      "content": [
        "New social distancing rules: you may go on walks with loved ones but they must remain 6 feet behind you and if you turn around to look at them, they will stay in the underworld forever"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be"
}
