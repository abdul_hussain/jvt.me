{
  "kind": "likes",
  "slug": "2020/04/ok7xk",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1245827502657818624"
      ],
      "url": [
        "https://twitter.com/dresserman/status/1245827502657818624"
      ],
      "published": [
        "2020-04-02T21:36:37+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:dresserman"
            ],
            "numeric-id": [
              "124122203"
            ],
            "name": [
              "Steve Dresser"
            ],
            "nickname": [
              "dresserman"
            ],
            "url": [
              "https://twitter.com/dresserman",
              "http://www.groceryinsight.com"
            ],
            "published": [
              "2010-03-18T09:45:54+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Keighley / Train / Car / Plane"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1215546009306259456/9V0NMufl.jpg"
            ]
          }
        }
      ],
      "content": [
        "My word. Ocado with perhaps the fact of 2020 so far."
      ],
      "photo": [
        "https://pbs.twimg.com/media/EUoRgNWWoAAzgrP.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-04-04T14:33:00+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @dresserman's tweet"
    ],
    "like-of": [
      "https://twitter.com/dresserman/status/1245827502657818624"
    ],
    "published": [
      "2020-04-04T14:33:00+01:00"
    ],
    "syndication": [
      "https://twitter.com/dresserman/status/1245827502657818624"
    ]
  }
}
