{
  "kind": "likes",
  "slug": "2020/04/wgqri",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1246185079094693888"
      ],
      "url": [
        "https://twitter.com/theninjagreg/status/1246185079094693888"
      ],
      "published": [
        "2020-04-03T21:17:30+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:theninjagreg"
            ],
            "numeric-id": [
              "182900961"
            ],
            "name": [
              "Gggreggg Sinclair"
            ],
            "nickname": [
              "theninjagreg"
            ],
            "url": [
              "https://twitter.com/theninjagreg",
              "http://www.hockeystats.ca"
            ],
            "published": [
              "2010-08-25T17:36:28+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "The Land of Always Winter"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1058960674292850688/dTEwtpAL.jpg"
            ]
          }
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/EUtWtvkU4AEzVo0.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-04-04T22:13:00+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @theninjagreg's tweet"
    ],
    "like-of": [
      "https://twitter.com/theninjagreg/status/1246185079094693888"
    ],
    "published": [
      "2020-04-04T22:13:00+01:00"
    ],
    "syndication": [
      "https://twitter.com/theninjagreg/status/1246185079094693888"
    ]
  }
}
