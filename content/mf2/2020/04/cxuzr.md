{
  "date" : "2020-04-25T19:22:51.677+02:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/Marcus_Noble_/status/1254093650579120128" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1254100790807011330" ],
    "name" : [ "Reply to https://twitter.com/Marcus_Noble_/status/1254093650579120128" ],
    "published" : [ "2020-04-25T19:22:51.677+02:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "That being said, it looks like the Docker example shows that the Alpine package contains `nextcloudcmd`, so you may be able to pull it out of https://pkgs.alpinelinux.org/package/edge/community/x86_64/nextcloud-client (or if you'd like I can extract it and send it over?)"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/04/cxuzr",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1254093650579120128" ],
      "url" : [ "https://twitter.com/Marcus_Noble_/status/1254093650579120128" ],
      "published" : [ "2020-04-25T17:03:20+00:00" ],
      "in-reply-to" : [ "https://twitter.com/edent/status/1254093369824993281" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Marcus_Noble_" ],
          "numeric-id" : [ "14732545" ],
          "name" : [ "Marcus Noble" ],
          "nickname" : [ "Marcus_Noble_" ],
          "url" : [ "https://twitter.com/Marcus_Noble_", "https://marcusnoble.co.uk" ],
          "published" : [ "2008-05-11T09:51:36+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "PID 1" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/776738772759277569/hfaM5zhA.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I can't help then 😞 Sorry",
        "html" : "I can't help then 😞 Sorry\n<a class=\"u-mention\" href=\"https://twitter.com/edent\"></a>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://micropublish.net"
}
