{
  "date" : "2020-04-27T14:01:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/kneath/status/1254445189554270209" ],
    "name" : [ "Like of @kneath's tweet" ],
    "published" : [ "2020-04-27T14:01:00+01:00" ],
    "like-of" : [ "https://twitter.com/kneath/status/1254445189554270209" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/wod38",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1254445189554270209" ],
      "url" : [ "https://twitter.com/kneath/status/1254445189554270209" ],
      "published" : [ "2020-04-26T16:20:14+00:00" ],
      "in-reply-to" : [ "https://twitter.com/mschoening/status/1254437013782007813" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:kneath" ],
          "numeric-id" : [ "638323" ],
          "name" : [ "Kyle Neath" ],
          "nickname" : [ "kneath" ],
          "url" : [ "https://twitter.com/kneath" ],
          "published" : [ "2007-01-15T20:15:11+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "South Lake Tahoe, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/792066899668455425/Slz8XV0J.jpg" ]
        }
      } ],
      "content" : [ {
        "html" : "\n<a class=\"u-mention\" href=\"https://twitter.com/mschoening\"></a>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EWivIOdUcAMHeYi.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
