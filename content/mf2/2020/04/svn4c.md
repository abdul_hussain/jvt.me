{
  "date" : "2020-04-27T09:08:31.089+02:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/i/status/1254056540958535682" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1254670009420431360" ],
    "name" : [ "Reply to https://twitter.com/i/status/1254056540958535682" ],
    "published" : [ "2020-04-27T09:08:31.089+02:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "Thansk - I have a `tweet.js` which is 2020-2018. I'll try and re-fetch using the steps in https://help.twitter.com/en/managing-your-account/how-to-download-your-twitter-archive and see what I get"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/04/svn4c",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1254056540958535682" ],
      "url" : [ "https://twitter.com/JamieTanna/status/1254056540958535682" ],
      "published" : [ "2020-04-25T14:35:53+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:JamieTanna" ],
          "numeric-id" : [ "2292531098" ],
          "name" : [ "Jamie Tanna | www.jvt.me" ],
          "nickname" : [ "JamieTanna" ],
          "url" : [ "https://twitter.com/JamieTanna", "https://www.jvt.me" ],
          "published" : [ "2014-01-15T10:55:11+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1207672129308778496/4erVfT-i.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Does anyone know the best way to export all your tweets from #Twitter? The data export I requested from them only has tweets until 2018 but my account is a few years older than that (jvt.me/mf2/2020/04/fz…)",
        "html" : "Does anyone know the best way to export all your tweets from <a href=\"https://twitter.com/search?q=%23Twitter\">#Twitter</a>? The data export I requested from them only has tweets until 2018 but my account is a few years older than that (<a href=\"https://www.jvt.me/mf2/2020/04/fzxlm/\">jvt.me/mf2/2020/04/fz…</a>)"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://micropublish.net"
}
