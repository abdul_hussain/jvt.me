{
  "date": "2020-04-13T22:50:00+01:00",
  "deleted": false,
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/MrAndrew/status/1249790216866496513"
    ],
    "name": [
      "Like of @MrAndrew's tweet"
    ],
    "published": [
      "2020-04-13T22:50:00+01:00"
    ],
    "like-of": [
      "https://twitter.com/MrAndrew/status/1249790216866496513"
    ]
  },
  "kind": "likes",
  "slug": "2020/04/cpyl8",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1249790216866496513"
      ],
      "url": [
        "https://twitter.com/MrAndrew/status/1249790216866496513"
      ],
      "published": [
        "2020-04-13T20:03:02+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/MrsEmma/status/1249787037626089473"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:MrAndrew"
            ],
            "numeric-id": [
              "9626182"
            ],
            "name": [
              "Andrew Seward"
            ],
            "nickname": [
              "MrAndrew"
            ],
            "url": [
              "https://twitter.com/MrAndrew",
              "http://www.technottingham.com"
            ],
            "published": [
              "2007-10-23T15:57:18+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Riddings, Derbyshire"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1246456234452140032/romkM0EV.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "For god's sake don't eat it",
          "html": "For god's sake don't eat it\n<a class=\"u-mention\" href=\"https://twitter.com/MrsEmma\"></a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be"
}
