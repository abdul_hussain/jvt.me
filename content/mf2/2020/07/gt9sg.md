{
  "date" : "2020-07-11T00:31:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/pete_codes/status/1281356805424193536" ],
    "name" : [ "Like of @pete_codes's tweet" ],
    "published" : [ "2020-07-11T00:31:00+01:00" ],
    "like-of" : [ "https://twitter.com/pete_codes/status/1281356805424193536" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/gt9sg",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1281356805424193536" ],
      "url" : [ "https://twitter.com/pete_codes/status/1281356805424193536" ],
      "published" : [ "2020-07-09T22:37:23+00:00" ],
      "in-reply-to" : [ "https://twitter.com/JamieTanna/status/1281166228845928449" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:pete_codes" ],
          "numeric-id" : [ "15156749" ],
          "name" : [ "Pete Gallagher - Azure #MVPBuzz" ],
          "nickname" : [ "pete_codes" ],
          "url" : [ "https://twitter.com/pete_codes", "https://www.petecodes.co.uk" ],
          "published" : [ "2008-06-18T11:20:03+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1005728851882921985/r3eA9KtW.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Wahoo!!! Congrats!",
        "html" : "Wahoo!!! Congrats!\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
