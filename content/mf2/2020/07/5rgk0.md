{
  "date" : "2020-07-24T17:56:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/nex3/status/1286426239335424000" ],
    "name" : [ "Like of @nex3's tweet" ],
    "published" : [ "2020-07-24T17:56:00+01:00" ],
    "like-of" : [ "https://twitter.com/nex3/status/1286426239335424000" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/5rgk0",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1286426239335424000" ],
      "url" : [ "https://twitter.com/nex3/status/1286426239335424000" ],
      "published" : [ "2020-07-23T22:21:30+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:nex3" ],
          "numeric-id" : [ "12973272" ],
          "name" : [ "Natal1312" ],
          "nickname" : [ "nex3" ],
          "url" : [ "https://twitter.com/nex3", "http://nex3.tumblr.com" ],
          "published" : [ "2008-02-02T07:19:15+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Seattle" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1219112995776655360/ccsFSQHH.jpg" ]
        }
      } ],
      "content" : [ "being a senior software engineer means being able to architect an entire end-to-end application in your head but having to Google how to check if a JS array contains an element because you can only remember how to do it in three other languages" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
