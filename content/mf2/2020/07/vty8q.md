{
  "date" : "2020-07-07T01:10:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/doniamae/status/1280228248929140737" ],
    "name" : [ "Like of @doniamae's tweet" ],
    "published" : [ "2020-07-07T01:10:00+01:00" ],
    "like-of" : [ "https://twitter.com/doniamae/status/1280228248929140737" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/vty8q",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1280228248929140737" ],
      "url" : [ "https://twitter.com/doniamae/status/1280228248929140737" ],
      "published" : [ "2020-07-06T19:52:54+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:doniamae" ],
          "numeric-id" : [ "63779496" ],
          "name" : [ "Donia (Lasagna)" ],
          "nickname" : [ "doniamae" ],
          "url" : [ "https://twitter.com/doniamae" ],
          "published" : [ "2009-08-07T17:24:55+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Illinois, USA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1062146121928241153/JLrXmdXE.jpg" ]
        }
      } ],
      "content" : [ "We're not canceling, we're just cleaning up some bad code." ],
      "photo" : [ "https://pbs.twimg.com/media/EcRIvXRWsAEKFhB.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
