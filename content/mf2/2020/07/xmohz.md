{
  "date" : "2020-07-07T10:29:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/DevRelCallum/status/1280433154805960705" ],
    "name" : [ "Like of @DevRelCallum's tweet" ],
    "published" : [ "2020-07-07T10:29:00+01:00" ],
    "category" : [ "diversity-and-inclusion" ],
    "like-of" : [ "https://twitter.com/DevRelCallum/status/1280433154805960705" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/xmohz",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1280433154805960705" ],
      "url" : [ "https://twitter.com/DevRelCallum/status/1280433154805960705" ],
      "published" : [ "2020-07-07T09:27:08+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:DevRelCallum" ],
          "numeric-id" : [ "2380186378" ],
          "name" : [ "Callum Uwunderwood" ],
          "nickname" : [ "DevRelCallum" ],
          "url" : [ "https://twitter.com/DevRelCallum", "http://robotteddy.org" ],
          "published" : [ "2014-03-03T19:48:30+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Private: @notdevrelcallum" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1231539930914004992/zqVKUD0w.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Googline \"Joules kids wellies\" gives you:\n\nGirls Wellies - Unicorn and Floral\nBoys Wellies - Dinosaur and Shark \n\nAs a parent who's trying not to force their kids down the \"traditional\" gendered clothing path, this is so disappointing to see @Joulesclothing :(",
        "html" : "<div style=\"white-space: pre\">Googline \"Joules kids wellies\" gives you:\n\nGirls Wellies - Unicorn and Floral\nBoys Wellies - Dinosaur and Shark \n\nAs a parent who's trying not to force their kids down the \"traditional\" gendered clothing path, this is so disappointing to see <a href=\"https://twitter.com/Joulesclothing\">@Joulesclothing</a> :(</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EcUDKnVX0AAaZW9.png" ]
    }
  },
  "tags" : [ "diversity-and-inclusion" ],
  "client_id" : "https://indigenous.realize.be"
}
