{
  "date" : "2020-07-28T23:13:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/steveklabnik/status/1288231694701604870" ],
    "name" : [ "Like of @steveklabnik's tweet" ],
    "published" : [ "2020-07-28T23:13:00+01:00" ],
    "like-of" : [ "https://twitter.com/steveklabnik/status/1288231694701604870" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/qfwge",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1288231694701604870" ],
      "url" : [ "https://twitter.com/steveklabnik/status/1288231694701604870" ],
      "published" : [ "2020-07-28T21:55:44+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:steveklabnik" ],
          "numeric-id" : [ "22386062" ],
          "name" : [ "steveklabnik" ],
          "nickname" : [ "steveklabnik" ],
          "url" : [ "https://twitter.com/steveklabnik", "https://www.steveklabnik.com" ],
          "published" : [ "2009-03-01T18:00:55+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Austin, TX" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1233862690172477440/-0B5TBY7.jpg" ]
        }
      } ],
      "content" : [ "The \"CAP Theorem\" refers to a theory some developers have about if a distributed system would wear a hat, and if so, what kind of hat would it wear" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
