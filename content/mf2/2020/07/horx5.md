{
  "date" : "2020-07-28T22:53:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/haysstanford/status/1287760626270507008" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1288232789414903809" ],
    "name" : [ "Reply to https://twitter.com/haysstanford/status/1287760626270507008" ],
    "published" : [ "2020-07-28T22:53:00+01:00" ],
    "category" : [ "gitlab" ],
    "content" : [ {
      "html" : "",
      "value" : "I'm wearing a GitLab hoodie that I bought with my own money, so... 🦊"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/07/horx5",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1287760626270507008" ],
      "url" : [ "https://twitter.com/haysstanford/status/1287760626270507008" ],
      "published" : [ "2020-07-27T14:43:53+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:haysstanford" ],
          "numeric-id" : [ "828753839427878913" ],
          "name" : [ "Hays Stanford 🏜️" ],
          "nickname" : [ "haysstanford" ],
          "url" : [ "https://twitter.com/haysstanford", "https://www.haysstanford.com/course/" ],
          "published" : [ "2017-02-06T23:54:47+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Austin, TX" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1269455747311718401/ML0EoGvk.jpg" ]
        }
      } ],
      "content" : [ "Does anyone not use GitHub as their primary code repo?" ]
    }
  },
  "tags" : [ "gitlab" ],
  "client_id" : "https://indigenous.realize.be"
}
