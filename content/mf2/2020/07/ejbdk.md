{
  "date" : "2020-07-01T19:55:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/edent/status/1278351420924866561" ],
    "name" : [ "Like of @edent's tweet" ],
    "published" : [ "2020-07-01T19:55:00+01:00" ],
    "category" : [ "automation" ],
    "like-of" : [ "https://twitter.com/edent/status/1278351420924866561" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/ejbdk",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1278351420924866561" ],
      "url" : [ "https://twitter.com/edent/status/1278351420924866561" ],
      "published" : [ "2020-07-01T15:35:04+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:edent" ],
          "numeric-id" : [ "14054507" ],
          "name" : [ "Terence Eden" ],
          "nickname" : [ "edent" ],
          "url" : [ "https://twitter.com/edent", "https://shkspr.mobi/blog/", "https://edent.tel" ],
          "published" : [ "2008-02-28T13:10:25+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1228067445153452033/_A8Uq2VY.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I need to manually alter about 80 blog posts.\nI reckon it'll take me at least 2 hours.\n⋯ OR ⋯\nI could spend ~4 hours trying to write a script to automate it.\n\nChoices, choices!",
        "html" : "<div style=\"white-space: pre\">I need to manually alter about 80 blog posts.\nI reckon it'll take me at least 2 hours.\n⋯ OR ⋯\nI could spend ~4 hours trying to write a script to automate it.\n\nChoices, choices!</div>"
      } ]
    }
  },
  "tags" : [ "automation" ],
  "client_id" : "https://indigenous.realize.be"
}
