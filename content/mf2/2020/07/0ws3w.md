{
  "date" : "2020-07-31T16:16:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/PeterSciretta/status/1289090955220402176" ],
    "name" : [ "Like of @PeterSciretta's tweet" ],
    "published" : [ "2020-07-31T16:16:00+01:00" ],
    "category" : [ "star-wars" ],
    "like-of" : [ "https://twitter.com/PeterSciretta/status/1289090955220402176" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/0ws3w",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1289090955220402176" ],
      "url" : [ "https://twitter.com/PeterSciretta/status/1289090955220402176" ],
      "video" : [ "https://video.twimg.com/ext_tw_video/1289065635683524609/pu/vid/576x1024/jWkkC-yUHPnpnMzS.mp4?tag=10" ],
      "published" : [ "2020-07-31T06:50:08+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:PeterSciretta" ],
          "numeric-id" : [ "1195470651877814275" ],
          "name" : [ "Peter Sciretta loves Star Wars" ],
          "nickname" : [ "PeterSciretta" ],
          "url" : [ "https://twitter.com/PeterSciretta", "https://youtube.com/ordinaryadventures" ],
          "published" : [ "2019-11-15T22:36:51+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Galaxys Edge" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1195470961060896768/tLuSrtY7.jpg" ]
        }
      } ],
      "content" : [ "🤯 " ]
    }
  },
  "tags" : [ "star-wars" ],
  "client_id" : "https://indigenous.realize.be"
}
