{
  "date" : "2020-07-09T22:23:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "name" : [ "Like of @midgetjems's tweet" ],
    "published" : [ "2020-07-09T22:23:00+01:00" ],
    "like-of" : [ "https://twitter.com/midgetjems/status/1281128600301907969" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/pmcud",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1281128600301907969" ],
      "url" : [ "https://twitter.com/midgetjems/status/1281128600301907969" ],
      "published" : [ "2020-07-09T07:30:35+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:midgetjems" ],
          "numeric-id" : [ "422079748" ],
          "name" : [ "jemma" ],
          "nickname" : [ "midgetjems" ],
          "url" : [ "https://twitter.com/midgetjems" ],
          "published" : [ "2011-11-26T19:45:03+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "she/her " ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1069659109459410944/Ebim1wKv.jpg" ]
        }
      } ],
      "content" : [ "Not one for bragging but feeling v proud to say that as a northern, working class, state school educated, first generation student from a single parent household I've got a FIRST CLASS DEGREE FROM OXFORD!!" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
