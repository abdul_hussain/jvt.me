{
  "date" : "2020-07-20T20:19:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/DannyDeraney/status/1284541789727162369" ],
    "name" : [ "Like of @DannyDeraney's tweet" ],
    "published" : [ "2020-07-20T20:19:00+01:00" ],
    "category" : [ "cute" ],
    "like-of" : [ "https://twitter.com/DannyDeraney/status/1284541789727162369" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/xwds3",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1284541789727162369" ],
      "url" : [ "https://twitter.com/DannyDeraney/status/1284541789727162369" ],
      "video" : [ "https://video.twimg.com/ext_tw_video/1284541745175248896/pu/vid/720x1280/BsJ-eMtu91UjeTuI.mp4?tag=10" ],
      "published" : [ "2020-07-18T17:33:22+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:DannyDeraney" ],
          "numeric-id" : [ "18309553" ],
          "name" : [ "Danny Deraney" ],
          "nickname" : [ "DannyDeraney" ],
          "url" : [ "https://twitter.com/DannyDeraney", "http://deraneypr.com" ],
          "published" : [ "2008-12-22T16:08:35+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "LA-NYC" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/996218052453679104/22bk_l-8.jpg" ]
        }
      } ],
      "content" : [ "Because you want to watch a little bunny rabbit standing and enjoying a snack." ]
    }
  },
  "tags" : [ "cute" ],
  "client_id" : "https://indigenous.realize.be"
}
