{
  "date" : "2020-07-09T17:20:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/helpermethod/status/1281259889852112896" ],
    "name" : [ "Like of @helpermethod's tweet" ],
    "published" : [ "2020-07-09T17:20:00+01:00" ],
    "like-of" : [ "https://twitter.com/helpermethod/status/1281259889852112896" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/iqsvi",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1281259889852112896" ],
      "url" : [ "https://twitter.com/helpermethod/status/1281259889852112896" ],
      "published" : [ "2020-07-09T16:12:17+00:00" ],
      "in-reply-to" : [ "https://twitter.com/davefarley77/status/1281257303149182977" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:helpermethod" ],
          "numeric-id" : [ "359526554" ],
          "name" : [ "Oliver Weiler" ],
          "nickname" : [ "helpermethod" ],
          "url" : [ "https://twitter.com/helpermethod", "https://github.com/helpermethod" ],
          "published" : [ "2011-08-21T18:54:36+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Köln, Deutschland" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/2301187038/f20qbnpgsl8cy4fy0b2r.jpeg" ]
        }
      } ],
      "content" : [ {
        "value" : "DevOps Team",
        "html" : "DevOps Team\n<a class=\"u-mention\" href=\"https://twitter.com/davefarley77\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
