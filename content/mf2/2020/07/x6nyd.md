{
  "date" : "2020-07-23T11:56:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/MozDevNet/status/1286210718619099137" ],
    "name" : [ "Like of @MozDevNet's tweet" ],
    "published" : [ "2020-07-23T11:56:00+01:00" ],
    "like-of" : [ "https://twitter.com/MozDevNet/status/1286210718619099137" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/x6nyd",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1286210718619099137" ],
      "url" : [ "https://twitter.com/MozDevNet/status/1286210718619099137" ],
      "published" : [ "2020-07-23T08:05:06+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:MozDevNet" ],
          "numeric-id" : [ "1317739632" ],
          "name" : [ "MDN Web Docs" ],
          "nickname" : [ "MozDevNet" ],
          "url" : [ "https://twitter.com/MozDevNet", "http://developer.mozilla.org", "https://www.mozilla.org/en-US/newsletter/developer/" ],
          "published" : [ "2013-03-31T05:09:49+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Worldwide" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1282703476682883077/ZU83BqsS.png" ]
        }
      } ],
      "content" : [ {
        "value" : "Today is the BIG DAY. We're 15 years young! 🥳\n\nFrom humble beginnings, rising out of the ashes of Netscape DevEdge, we have grown to be one of the best-respected web platform documentation sites out there. #MDNTurns15 hacks.mozilla.org/2020/07/mdn-we…",
        "html" : "<div style=\"white-space: pre\">Today is the BIG DAY. We're 15 years young! 🥳\n\nFrom humble beginnings, rising out of the ashes of Netscape DevEdge, we have grown to be one of the best-respected web platform documentation sites out there. <a href=\"https://twitter.com/search?q=%23MDNTurns15\">#MDNTurns15</a> <a href=\"https://hacks.mozilla.org/2020/07/mdn-web-docs-15-years-young/\">hacks.mozilla.org/2020/07/mdn-we…</a></div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
