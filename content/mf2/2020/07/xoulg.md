{
  "date" : "2020-07-08T22:12:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/ianmiell/status/1280971142539415553" ],
    "name" : [ "Like of @ianmiell's tweet" ],
    "published" : [ "2020-07-08T22:12:00+01:00" ],
    "like-of" : [ "https://twitter.com/ianmiell/status/1280971142539415553" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/xoulg",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1280971142539415553" ],
      "url" : [ "https://twitter.com/ianmiell/status/1280971142539415553" ],
      "published" : [ "2020-07-08T21:04:54+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:ianmiell" ],
          "numeric-id" : [ "58017706" ],
          "name" : [ "Ian Miell" ],
          "nickname" : [ "ianmiell" ],
          "url" : [ "https://twitter.com/ianmiell", "http://zwischenzugs.com", "http://bit.ly/35X34Ew" ],
          "published" : [ "2009-07-18T19:59:32+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1120618937253793793/xb6_9WMl.jpg" ]
        }
      } ],
      "content" : [ "Are there any techie 'reaction' videos out there? No-one checking out the kubernetes codebase and live-streaming their response as they try and grok the codebase? Tearing their hair out at trying to install Spinnaker? Would this be entertainment or some kind of torture to watch?" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
