{
  "date" : "2020-07-01T07:36:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/javagrifter/status/1278122818035687424" ],
    "name" : [ "Like of @javagrifter's tweet" ],
    "published" : [ "2020-07-01T07:36:00+01:00" ],
    "like-of" : [ "https://twitter.com/javagrifter/status/1278122818035687424" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/wp1tn",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1278122818035687424" ],
      "url" : [ "https://twitter.com/javagrifter/status/1278122818035687424" ],
      "published" : [ "2020-07-01T00:26:40+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:javagrifter" ],
          "numeric-id" : [ "1278118393036627968" ],
          "name" : [ "JavaGrifter" ],
          "nickname" : [ "javagrifter" ],
          "url" : [ "https://twitter.com/javagrifter" ],
          "published" : [ "2020-07-01T00:09:16+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1278118746138304512/rt6LwJ7P.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Before coding, I had nothing. Just my rich parents and rich wife. \n\nIt just shows, as long as you love to code, you can become someone.",
        "html" : "<div style=\"white-space: pre\">Before coding, I had nothing. Just my rich parents and rich wife. \n\nIt just shows, as long as you love to code, you can become someone.</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
