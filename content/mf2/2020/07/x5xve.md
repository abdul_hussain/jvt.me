{
  "date" : "2020-07-17T18:02:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/__jcmc__/status/1284143411390492672" ],
    "name" : [ "Like of @__jcmc__'s tweet" ],
    "published" : [ "2020-07-17T18:02:00+01:00" ],
    "like-of" : [ "https://twitter.com/__jcmc__/status/1284143411390492672" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/x5xve",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1284143411390492672" ],
      "url" : [ "https://twitter.com/__jcmc__/status/1284143411390492672" ],
      "published" : [ "2020-07-17T15:10:22+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:__jcmc__" ],
          "numeric-id" : [ "781588467654483968" ],
          "name" : [ "Jennifer Mackown" ],
          "nickname" : [ "__jcmc__" ],
          "url" : [ "https://twitter.com/__jcmc__", "http://www.jcmc.xyz" ],
          "published" : [ "2016-09-29T20:16:26+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Lincoln" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1137074636213116929/AjJAzHty.png" ]
        }
      } ],
      "content" : [ "Don't think I've ever had a press release about something I've done at work before. Kinda cool." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
