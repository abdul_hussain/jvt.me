{
  "date" : "2020-07-09T19:49:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/Marcus_Noble_/status/1281297929311903744" ],
    "name" : [ "Like of @Marcus_Noble_'s tweet" ],
    "published" : [ "2020-07-09T19:49:00+01:00" ],
    "category" : [ "phpminds", "kubernetes" ],
    "like-of" : [ "https://twitter.com/Marcus_Noble_/status/1281297929311903744" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/uvr5i",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1281297929311903744" ],
      "url" : [ "https://twitter.com/Marcus_Noble_/status/1281297929311903744" ],
      "published" : [ "2020-07-09T18:43:26+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Marcus_Noble_" ],
          "numeric-id" : [ "14732545" ],
          "name" : [ "Marcus Noble" ],
          "nickname" : [ "Marcus_Noble_" ],
          "url" : [ "https://twitter.com/Marcus_Noble_", "https://marcusnoble.co.uk" ],
          "published" : [ "2008-05-11T09:51:36+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "PID 1" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/776738772759277569/hfaM5zhA.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Thanks so much for having me @PHPMinds ! I had a great time!\n\nIf anyone wants the slides you can find them here: noti.st/averagemarcus/…\n\nFeel free to DM if you have any questions.",
        "html" : "<div style=\"white-space: pre\">Thanks so much for having me <a href=\"https://twitter.com/PHPMinds\">@PHPMinds</a> ! I had a great time!\n\nIf anyone wants the slides you can find them here: <a href=\"https://noti.st/averagemarcus/yPQIgN/an-introduction-to-kubernetes\">noti.st/averagemarcus/…</a>\n\nFeel free to DM if you have any questions.</div>"
      } ]
    }
  },
  "tags" : [ "phpminds", "kubernetes" ],
  "client_id" : "https://indigenous.realize.be"
}
