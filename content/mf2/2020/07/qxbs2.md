{
  "date" : "2020-07-11T11:25:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/mr_goodwin/status/1281877862799290369" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1281899278101872640" ],
    "name" : [ "Reply to https://twitter.com/mr_goodwin/status/1281877862799290369" ],
    "published" : [ "2020-07-11T11:25:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "Even aside from this I do check pretty compulsively, as there's always a lot of background noise from us as we have mechanical keyboards - but yeah it's always a risk!"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/07/qxbs2",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1281877862799290369" ],
      "url" : [ "https://twitter.com/mr_goodwin/status/1281877862799290369" ],
      "published" : [ "2020-07-11T09:07:53+00:00" ],
      "in-reply-to" : [ "https://twitter.com/JamieTanna/status/1281671060765671426" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:mr_goodwin" ],
          "numeric-id" : [ "1481941" ],
          "name" : [ "Mark Goodwin" ],
          "nickname" : [ "mr_goodwin" ],
          "url" : [ "https://twitter.com/mr_goodwin" ],
          "published" : [ "2007-03-19T08:27:18+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottinghamshire, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/756072798603411457/SIyD6Fnf.jpg" ]
        }
      } ],
      "location" : [ {
        "type" : [ "h-card", "p-location" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:72c99e5173ba679c" ],
          "name" : [ "Gotham, England" ]
        }
      } ],
      "content" : [ {
        "value" : "Better have your \"mute\" game down. I remember a conf years ago when a speaker went to the loo after a talk still mic'd...",
        "html" : "Better have your \"mute\" game down. I remember a conf years ago when a speaker went to the loo after a talk still mic'd...\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
