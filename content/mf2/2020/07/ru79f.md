{
  "date" : "2020-07-01T07:42:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/bestofnextdoor/status/1278173029101522944" ],
    "name" : [ "Like of @bestofnextdoor's tweet" ],
    "published" : [ "2020-07-01T07:42:00+01:00" ],
    "like-of" : [ "https://twitter.com/bestofnextdoor/status/1278173029101522944" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/ru79f",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1278173029101522944" ],
      "url" : [ "https://twitter.com/bestofnextdoor/status/1278173029101522944" ],
      "published" : [ "2020-07-01T03:46:12+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:bestofnextdoor" ],
          "numeric-id" : [ "924836857988313088" ],
          "name" : [ "Best of Nextdoor" ],
          "nickname" : [ "bestofnextdoor" ],
          "url" : [ "https://twitter.com/bestofnextdoor", "https://bit.ly/NextdoorPetition" ],
          "published" : [ "2017-10-30T03:14:24+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "not affiliated w/nextdoor obvi" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1210768910103937027/JghrHXH2.jpg" ]
        }
      } ],
      "location" : [ {
        "type" : [ "h-card", "p-location" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:5a110d312052166f" ],
          "name" : [ "San Francisco, CA" ]
        }
      } ],
      "content" : [ "Meanwhile, in Phoenix..." ],
      "photo" : [ "https://pbs.twimg.com/media/Ebz7mGZU0AAc_hP.jpg", "https://pbs.twimg.com/media/Ebz7mF-UcAE1sm_.jpg", "https://pbs.twimg.com/media/Ebz7mHzUEAAFUEM.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
