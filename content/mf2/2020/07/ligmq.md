{
  "date" : "2020-07-28T16:24:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/StevenPears/status/1288123475157106690" ],
    "name" : [ "Like of @StevenPears's tweet" ],
    "published" : [ "2020-07-28T16:24:00+01:00" ],
    "like-of" : [ "https://twitter.com/StevenPears/status/1288123475157106690" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/ligmq",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1288123475157106690" ],
      "url" : [ "https://twitter.com/StevenPears/status/1288123475157106690" ],
      "published" : [ "2020-07-28T14:45:43+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:StevenPears" ],
          "numeric-id" : [ "404697859" ],
          "name" : [ "Steven Pears" ],
          "nickname" : [ "StevenPears" ],
          "url" : [ "https://twitter.com/StevenPears", "https://github.com/stoiveyp" ],
          "published" : [ "2011-11-04T08:54:04+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1210474450824642560/lmh3mZjU.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Don't be afraid to ask \"why are we're doing that?\"  at work. I do it quite a lot.\n\nIt helps me understand the requirements for a story I've not been close to, or it helps me understand what a team are working that might affect me, or just gets me their point of view. Always ask.",
        "html" : "<div style=\"white-space: pre\">Don't be afraid to ask \"why are we're doing that?\"  at work. I do it quite a lot.\n\nIt helps me understand the requirements for a story I've not been close to, or it helps me understand what a team are working that might affect me, or just gets me their point of view. Always ask.</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
