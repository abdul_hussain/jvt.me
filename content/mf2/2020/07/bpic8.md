{
  "date" : "2020-07-31T20:04:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/272270554/" ],
    "syndication" : [ "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/272270554/#rsvp-by-https%3A%2F%2Fwww.jvt.me%2Fmf2%2F2020%2F07%2Fbpic8%2F" ],
    "name" : [ "RSVP yes to https://www.meetup.com/PHPMiNDS-in-Nottingham/events/272270554/" ],
    "published" : [ "2020-07-31T20:04:00+01:00" ],
    "event" : {
      "start" : [ "2020-08-13T18:30:00+01:00" ],
      "name" : [ "An Introduction to OpenAPI by Lorna Mitchell" ],
      "end" : [ "2020-08-13T20:30:00+01:00" ],
      "location" : [ "Online" ],
      "url" : [ "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/272270554/" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/07/bpic8",
  "client_id" : "https://indigenous.realize.be"
}
