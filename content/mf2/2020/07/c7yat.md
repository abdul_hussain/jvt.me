{
  "date" : "2020-07-16T21:47:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/edent/status/1283861969754370049" ],
    "name" : [ "Like of @edent's tweet" ],
    "published" : [ "2020-07-16T21:47:00+01:00" ],
    "category" : [ "remote-work" ],
    "like-of" : [ "https://twitter.com/edent/status/1283861969754370049" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/c7yat",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1283861969754370049" ],
      "url" : [ "https://twitter.com/edent/status/1283861969754370049" ],
      "published" : [ "2020-07-16T20:32:01+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:edent" ],
          "numeric-id" : [ "14054507" ],
          "name" : [ "Terence Eden" ],
          "nickname" : [ "edent" ],
          "url" : [ "https://twitter.com/edent", "https://shkspr.mobi/blog/", "https://edent.tel" ],
          "published" : [ "2008-02-28T13:10:25+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1228067445153452033/_A8Uq2VY.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I've just realised something I adore about Endless Video Calls™.\n\nI am *useless* at remembering people's names. I'm utterly hopeless.\n\nI used to joke that it would be brilliant if everyone I worked with wore prominent name badges.\n\nNow, thanks to video calls, they all do!",
        "html" : "<div style=\"white-space: pre\">I've just realised something I adore about Endless Video Calls™.\n\nI am *useless* at remembering people's names. I'm utterly hopeless.\n\nI used to joke that it would be brilliant if everyone I worked with wore prominent name badges.\n\nNow, thanks to video calls, they all do!</div>"
      } ]
    }
  },
  "tags" : [ "remote-work" ],
  "client_id" : "https://indigenous.realize.be"
}
