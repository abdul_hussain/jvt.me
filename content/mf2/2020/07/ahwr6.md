{
  "date" : "2020-07-05T21:19:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/katie_fenn/status/1279867469725917189" ],
    "name" : [ "Like of @katie_fenn's tweet" ],
    "published" : [ "2020-07-05T21:19:00+01:00" ],
    "category" : [ "star-trek" ],
    "like-of" : [ "https://twitter.com/katie_fenn/status/1279867469725917189" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/ahwr6",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1279867469725917189" ],
      "url" : [ "https://twitter.com/katie_fenn/status/1279867469725917189" ],
      "published" : [ "2020-07-05T19:59:18+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:katie_fenn" ],
          "numeric-id" : [ "14979616" ],
          "name" : [ "Katie Fenn" ],
          "nickname" : [ "katie_fenn" ],
          "url" : [ "https://twitter.com/katie_fenn" ],
          "published" : [ "2008-06-02T12:02:44+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Sheffield" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1094575150517891072/sGJJqyzP.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Watching Dax episodes.\n\nDax had meaningful trans storylines long before conservative society would accept it.\n\nRediscovering them is beautiful, and I’m sad I couldn’t appreciate them more at the time.",
        "html" : "<div style=\"white-space: pre\">Watching Dax episodes.\n\nDax had meaningful trans storylines long before conservative society would accept it.\n\nRediscovering them is beautiful, and I’m sad I couldn’t appreciate them more at the time.</div>"
      } ]
    }
  },
  "tags" : [ "star-trek" ],
  "client_id" : "https://indigenous.realize.be"
}
