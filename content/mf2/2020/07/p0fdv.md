{
  "date" : "2020-07-05T23:48:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [  ],
    "name" : [ "Like of @crablabuk's tweet" ],
    "published" : [ "2020-07-05T23:48:00+01:00" ],
    "like-of" : [ "https://twitter.com/crablabuk/status/1279748324069793795" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/p0fdv",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1279748324069793795" ],
      "url" : [ "https://twitter.com/crablabuk/status/1279748324069793795" ],
      "published" : [ "2020-07-05T12:05:51+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:crablabuk" ],
          "numeric-id" : [ "1194670910" ],
          "name" : [ "Hugh Wells" ],
          "nickname" : [ "crablabuk" ],
          "url" : [ "https://twitter.com/crablabuk", "http://crablab.co.uk" ],
          "published" : [ "2013-02-18T19:50:32+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "DE060" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/938415670278213637/CB7ZbdcA.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I have many tech t-shirts to give away! \n\n- Travis CI \n- Circle CI Pride \n- Cloudflare (XL) \n- Heroku \n- GitKracken \n- Digital Ocean \n- ZenHub\n- Gitcoin \n\n(T to B, L to R) \n\n2XL unless stated. \n\nFREE. I will ship them anywhere Royal Mail will let me :)",
        "html" : "<div style=\"white-space: pre\">I have many tech t-shirts to give away! \n\n- Travis CI \n- Circle CI Pride \n- Cloudflare (XL) \n- Heroku \n- GitKracken \n- Digital Ocean \n- ZenHub\n- Gitcoin \n\n(T to B, L to R) \n\n2XL unless stated. \n\nFREE. I will ship them anywhere Royal Mail will let me :)</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EcKUUQFWoAEECrk.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
