{
  "date" : "2020-07-20T07:52:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/techgirl1908/status/1285071812955529216" ],
    "name" : [ "Like of @techgirl1908's tweet" ],
    "published" : [ "2020-07-20T07:52:00+01:00" ],
    "like-of" : [ "https://twitter.com/techgirl1908/status/1285071812955529216" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/c8pzb",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1285071812955529216" ],
      "url" : [ "https://twitter.com/techgirl1908/status/1285071812955529216" ],
      "published" : [ "2020-07-20T04:39:30+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:techgirl1908" ],
          "numeric-id" : [ "2932738834" ],
          "name" : [ "Angie Jones" ],
          "nickname" : [ "techgirl1908" ],
          "url" : [ "https://twitter.com/techgirl1908", "http://angiejones.tech" ],
          "published" : [ "2014-12-19T22:13:09+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "San Francisco, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1141742921240104961/ylpMiYrr.jpg" ]
        }
      } ],
      "content" : [ "bad bugs suck the joy out of coding 😫" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
