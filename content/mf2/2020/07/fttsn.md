{
  "date" : "2020-07-01T18:31:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/ASpittel/status/1278372496929165312" ],
    "name" : [ "Like of @ASpittel's tweet" ],
    "published" : [ "2020-07-01T18:31:00+01:00" ],
    "like-of" : [ "https://twitter.com/ASpittel/status/1278372496929165312" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/fttsn",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1278372496929165312" ],
      "url" : [ "https://twitter.com/ASpittel/status/1278372496929165312" ],
      "published" : [ "2020-07-01T16:58:48+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:ASpittel" ],
          "numeric-id" : [ "3092104835" ],
          "name" : [ "Ali Spittel 🐞" ],
          "nickname" : [ "ASpittel" ],
          "url" : [ "https://twitter.com/ASpittel", "http://welearncode.com" ],
          "published" : [ "2015-03-15T16:51:40+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Chicago, IL" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1273315516887838720/pTF-DgbL.jpg" ]
        }
      } ],
      "content" : [ "idk who needs to hear this but double check what's on your desktop before you share your screen" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
