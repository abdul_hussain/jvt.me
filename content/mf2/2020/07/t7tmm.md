{
  "date" : "2020-07-30T16:32:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/CuriousZelda/status/1288543207605907462" ],
    "name" : [ "Like of @CuriousZelda's tweet" ],
    "published" : [ "2020-07-30T16:32:00+01:00" ],
    "category" : [ "cute" ],
    "like-of" : [ "https://twitter.com/CuriousZelda/status/1288543207605907462" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/t7tmm",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1288543207605907462" ],
      "url" : [ "https://twitter.com/CuriousZelda/status/1288543207605907462" ],
      "published" : [ "2020-07-29T18:33:35+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:CuriousZelda" ],
          "numeric-id" : [ "4167213255" ],
          "name" : [ "Curious Zelda" ],
          "nickname" : [ "CuriousZelda" ],
          "url" : [ "https://twitter.com/CuriousZelda", "https://amzn.to/2LOlX6o" ],
          "published" : [ "2015-11-11T23:38:37+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/867761095909543937/hL_NQ7AZ.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "(1/2)\nSome years ago\nI found my place\nI wrote some words\nI shared my face\n\nI had no plan\nExpected nil\nBut when we met\nI got a thrill\n\nI tweeted more\nI learnt to rhyme\nMy following\nBegan to climb...",
        "html" : "<div style=\"white-space: pre\">(1/2)\nSome years ago\nI found my place\nI wrote some words\nI shared my face\n\nI had no plan\nExpected nil\nBut when we met\nI got a thrill\n\nI tweeted more\nI learnt to rhyme\nMy following\nBegan to climb...</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EeHTN5eXYAIaV7v.jpg" ]
    }
  },
  "tags" : [ "cute" ],
  "client_id" : "https://indigenous.realize.be"
}
