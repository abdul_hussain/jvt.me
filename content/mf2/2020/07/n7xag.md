{
  "date" : "2020-07-15T12:20:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/CarolSaysThings/status/1283284525611659264" ],
    "name" : [ "Like of @CarolSaysThings's tweet" ],
    "published" : [ "2020-07-15T12:20:00+01:00" ],
    "like-of" : [ "https://twitter.com/CarolSaysThings/status/1283284525611659264" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/n7xag",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1283284525611659264" ],
      "url" : [ "https://twitter.com/CarolSaysThings/status/1283284525611659264" ],
      "published" : [ "2020-07-15T06:17:27+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:CarolSaysThings" ],
          "numeric-id" : [ "36382927" ],
          "name" : [ "Carol 😅" ],
          "nickname" : [ "CarolSaysThings" ],
          "url" : [ "https://twitter.com/CarolSaysThings", "https://carolgilabert.me/" ],
          "published" : [ "2009-04-29T15:22:13+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "🇧🇷🇪🇸🇬🇧 · Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1238515159594917889/C5994QPa.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "So I was moving my notes around last night, and the last sentence on my ‘2019 in review’ post caught my eye. \n\nSo it turns out this is all my fault 😬\n\nI’m really sorry 🙇🏼‍♀️",
        "html" : "<div style=\"white-space: pre\">So I was moving my notes around last night, and the last sentence on my ‘2019 in review’ post caught my eye. \n\nSo it turns out this is all my fault 😬\n\nI’m really sorry 🙇🏼‍♀️</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/Ec8keQSWAAI1jM5.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
