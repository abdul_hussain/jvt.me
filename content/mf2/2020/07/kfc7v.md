{
  "date" : "2020-07-21T12:28:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamieTanna/status/1285538157082169344" ],
    "published" : [ "2020-07-21T12:28:00+01:00" ],
    "repost-of" : [ "https://twitter.com/AKGrenier/status/1285275433282359296" ],
    "category" : [ "mental-health" ]
  },
  "kind" : "reposts",
  "slug" : "2020/07/kfc7v",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1285275433282359296" ],
      "url" : [ "https://twitter.com/AKGrenier/status/1285275433282359296" ],
      "published" : [ "2020-07-20T18:08:37+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:AKGrenier" ],
          "numeric-id" : [ "7484292" ],
          "name" : [ "Adam Grenier" ],
          "nickname" : [ "AKGrenier" ],
          "url" : [ "https://twitter.com/AKGrenier", "http://www.linkedin.com/in/akgrenier" ],
          "published" : [ "2007-07-15T09:24:28+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Oakland" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1267635877729071104/TNVEOWUz.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Your \"burn out\" may actually be depression. \n\nIt was for me. \n\nI'm guessing it wasn't just me.\n\n👇Thread/",
        "html" : "<div style=\"white-space: pre\">Your \"burn out\" may actually be depression. \n\nIt was for me. \n\nI'm guessing it wasn't just me.\n\n👇Thread/</div>"
      } ]
    }
  },
  "tags" : [ "mental-health" ],
  "client_id" : "https://indigenous.realize.be"
}
