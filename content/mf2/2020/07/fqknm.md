{
  "date" : "2020-07-13T20:55:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamieTanna/status/1282768316398960640" ],
    "published" : [ "2020-07-13T20:55:00+01:00" ],
    "repost-of" : [ "https://twitter.com/technottingham/status/1282756638693306369" ],
    "category" : [ "tech-nottingham" ],
    "content" : [ {
      "html" : "",
      "value" : "<a href=\"/tags/tech-nottingham/\">#TechNott</a> has been brilliant fun!"
    } ]
  },
  "kind" : "reposts",
  "slug" : "2020/07/fqknm",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1282756638693306369" ],
      "url" : [ "https://twitter.com/technottingham/status/1282756638693306369" ],
      "published" : [ "2020-07-13T19:19:49+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:technottingham" ],
          "numeric-id" : [ "384492431" ],
          "name" : [ "Tech Nottingham" ],
          "nickname" : [ "technottingham" ],
          "url" : [ "https://twitter.com/technottingham", "http://technottingham.com" ],
          "published" : [ "2011-10-03T19:47:31+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1023974499757293570/ZoPc_QsO.jpg" ]
        }
      } ],
      "content" : [ "It's game show time! Thanks for your patience with the *slight* technical issues, folks" ],
      "photo" : [ "https://pbs.twimg.com/media/Ec1Dd47WkAYdize.jpg" ]
    }
  },
  "tags" : [ "tech-nottingham" ],
  "client_id" : "https://indigenous.realize.be"
}
