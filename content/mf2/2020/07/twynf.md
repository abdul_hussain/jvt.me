{
  "date" : "2020-07-09T18:28:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/PandelisZ/status/1281273812470894593" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1281286624417132546" ],
    "published" : [ "2020-07-09T18:28:00+01:00" ],
    "category" : [ "gravatar" ],
    "content" : [ {
      "html" : "",
      "value" : "I've got https://www.jvt.me/img/profile.png set up so I can easily reference it, but agree it'd be super helpful if it was possible to not re-import everywhere. https://www.libravatar.org/ seems like an interesting alternative, but like <a href=\"/tags/gravatar/\">#Gravatar</a> it doesn't work for me as I don't usually use the same email address across services"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/07/twynf",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1281273812470894593" ],
      "url" : [ "https://twitter.com/PandelisZ/status/1281273812470894593" ],
      "published" : [ "2020-07-09T17:07:36+00:00" ],
      "in-reply-to" : [ "https://twitter.com/jna_sh/status/1281204428654227459" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:PandelisZ" ],
          "numeric-id" : [ "2837561099" ],
          "name" : [ "‏Pα̹̖̺̃̊ͩ̑̽ͥν͍̯̳̳τ̗̘̺̥̦͊̉̃ͥͫͅε̝͗̎͗ͮλ̒̓̿͆is🇪🇺" ],
          "nickname" : [ "PandelisZ" ],
          "url" : [ "https://twitter.com/PandelisZ", "https://pa.ndel.is/" ],
          "published" : [ "2014-10-19T20:06:41+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Beerlin" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1090981237722877952/m04GP5lj.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I for one absolutely adore gravatar. Do you have any idea how many times I've uploaded the same image to slack?",
        "html" : "I for one absolutely adore gravatar. Do you have any idea how many times I've uploaded the same image to slack?\n<a class=\"u-mention\" href=\"https://twitter.com/benholmen\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/jna_sh\"></a>"
      } ]
    }
  },
  "tags" : [ "gravatar" ],
  "client_id" : "https://indigenous.realize.be"
}
