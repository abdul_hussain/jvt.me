{
  "date" : "2020-07-05T11:53:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/tokyosexwhale/status/1279320045861187584" ],
    "name" : [ "Like of @tokyosexwhale's tweet" ],
    "published" : [ "2020-07-05T11:53:00+01:00" ],
    "like-of" : [ "https://twitter.com/tokyosexwhale/status/1279320045861187584" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/z5exz",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1279320045861187584" ],
      "url" : [ "https://twitter.com/tokyosexwhale/status/1279320045861187584" ],
      "published" : [ "2020-07-04T07:44:02+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:tokyosexwhale" ],
          "numeric-id" : [ "291197113" ],
          "name" : [ "Tokyo Sexwhale" ],
          "nickname" : [ "tokyosexwhale" ],
          "url" : [ "https://twitter.com/tokyosexwhale", "http://ko-fi.com/tokyosexwhale" ],
          "published" : [ "2011-05-01T16:23:40+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Norwich" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/660757634035904512/K5Kw-ea0.png" ]
        }
      } ],
      "content" : [ "Their faces look like a before and after pic of a cartoon character looking down the barrel of a just lit cannon." ],
      "photo" : [ "https://pbs.twimg.com/media/EcEOzKrWkAA6XVp.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
