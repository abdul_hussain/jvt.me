{
  "date" : "2020-07-09T15:02:02.603Z",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/chris_emerson/status/1281168609507762176" ],
    "name" : [ "Like of @chris_emerson's tweet" ],
    "published" : [ "2020-07-09T15:02:02.603Z" ],
    "like-of" : [ "https://twitter.com/chris_emerson/status/1281168609507762176" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/xmhl3",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1281168609507762176" ],
      "url" : [ "https://twitter.com/chris_emerson/status/1281168609507762176" ],
      "published" : [ "2020-07-09T10:09:34+00:00" ],
      "in-reply-to" : [ "https://twitter.com/JamieTanna/status/1281166228845928449" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:chris_emerson" ],
          "numeric-id" : [ "20874917" ],
          "name" : [ "Chris Emerson" ],
          "nickname" : [ "chris_emerson" ],
          "url" : [ "https://twitter.com/chris_emerson", "http://www.cemerson.co.uk", "http://qi.com" ],
          "published" : [ "2009-02-14T22:13:16+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1176566131429171201/CPu0vbVR.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Congrats!",
        "html" : "Congrats!\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>"
      } ]
    }
  },
  "client_id" : "https://micropublish.net"
}
