{
  "date" : "2020-07-01T19:56:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/evnsio/status/1278392987299516418" ],
    "name" : [ "Like of @evnsio's tweet" ],
    "published" : [ "2020-07-01T19:56:00+01:00" ],
    "category" : [ "remote-work" ],
    "like-of" : [ "https://twitter.com/evnsio/status/1278392987299516418" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/qi8gz",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1278392987299516418" ],
      "url" : [ "https://twitter.com/evnsio/status/1278392987299516418" ],
      "published" : [ "2020-07-01T18:20:14+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:evnsio" ],
          "numeric-id" : [ "20139298" ],
          "name" : [ "Chris Evans" ],
          "nickname" : [ "evnsio" ],
          "url" : [ "https://twitter.com/evnsio", "https://evns.io" ],
          "published" : [ "2009-02-05T11:23:40+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Hertfordshire" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1246480689622667275/2aaBFKOP.jpg" ]
        }
      } ],
      "content" : [ "My new favourite game is spotting the people who are clearly talking over slack during video calls. Big giveaway when two people start smiling simultaneously 🙂" ]
    }
  },
  "tags" : [ "remote-work" ],
  "client_id" : "https://indigenous.realize.be"
}
