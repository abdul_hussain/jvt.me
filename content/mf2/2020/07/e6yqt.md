{
  "date" : "2020-07-11T13:18:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/hoochlover1917/status/1281669584085549064" ],
    "name" : [ "Like of @hoochlover1917's tweet" ],
    "published" : [ "2020-07-11T13:18:00+01:00" ],
    "like-of" : [ "https://twitter.com/hoochlover1917/status/1281669584085549064" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/e6yqt",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1281669584085549064" ],
      "url" : [ "https://twitter.com/hoochlover1917/status/1281669584085549064" ],
      "published" : [ "2020-07-10T19:20:15+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:hoochlover1917" ],
          "numeric-id" : [ "782622743325835267" ],
          "name" : [ "Al🍸an🍹na🍺h🍻!🍾🍷" ],
          "nickname" : [ "hoochlover1917" ],
          "url" : [ "https://twitter.com/hoochlover1917" ],
          "published" : [ "2016-10-02T16:46:17+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1135675944813944832/4U2IzVIQ.png" ]
        }
      } ],
      "content" : [ {
        "value" : "£42,246. \nThat's how much money I've paid in rent since I moved out my mum's house. \nImagine if instead of using that to pay off other people's second houses I could use it to start buy one of my own. \n\nReckon it's probably not lattes and avocados that are the issue here lads.",
        "html" : "<div style=\"white-space: pre\">£42,246. \nThat's how much money I've paid in rent since I moved out my mum's house. \nImagine if instead of using that to pay off other people's second houses I could use it to start buy one of my own. \n\nReckon it's probably not lattes and avocados that are the issue here lads.</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
