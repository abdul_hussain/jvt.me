{
  "date" : "2020-07-27T17:09:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/mspowahs/status/1287527588395651073" ],
    "name" : [ "Like of @mspowahs's tweet" ],
    "published" : [ "2020-07-27T17:09:00+01:00" ],
    "category" : [ "mechanical-keyboard" ],
    "like-of" : [ "https://twitter.com/mspowahs/status/1287527588395651073" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/vhiyz",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1287527588395651073" ],
      "url" : [ "https://twitter.com/mspowahs/status/1287527588395651073" ],
      "published" : [ "2020-07-26T23:17:52+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:mspowahs" ],
          "numeric-id" : [ "6473302" ],
          "name" : [ "Ada Powers (looking for design and writing work!)" ],
          "nickname" : [ "mspowahs" ],
          "url" : [ "https://twitter.com/mspowahs", "http://ko-fi.com/adapowers" ],
          "published" : [ "2007-05-31T14:25:29+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Atlanta by way of San Diego" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1245083917654335490/IciOpNNz.jpg" ]
        }
      } ],
      "location" : [ {
        "type" : [ "h-card", "p-location" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:9902fe95fc7596a7" ],
          "name" : [ "Belvedere Park, GA" ]
        }
      } ],
      "content" : [ "do you have a mechanical keyboard or do people enjoy being around you while you're working" ]
    }
  },
  "tags" : [ "mechanical-keyboard" ],
  "client_id" : "https://indigenous.realize.be"
}
