{
  "date" : "2020-07-11T11:29:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/EmmaBostian/status/1281890042739396608" ],
    "name" : [ "Like of @EmmaBostian's tweet" ],
    "published" : [ "2020-07-11T11:29:00+01:00" ],
    "like-of" : [ "https://twitter.com/EmmaBostian/status/1281890042739396608" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/5kd0l",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1281890042739396608" ],
      "url" : [ "https://twitter.com/EmmaBostian/status/1281890042739396608" ],
      "published" : [ "2020-07-11T09:56:17+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:EmmaBostian" ],
          "numeric-id" : [ "491821358" ],
          "name" : [ "Emma Bostian 🐞" ],
          "nickname" : [ "EmmaBostian" ],
          "url" : [ "https://twitter.com/EmmaBostian", "http://compiled.blog" ],
          "published" : [ "2012-02-14T01:57:07+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Karlsruhe, Germany" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1262289899786559489/wxFM6Q30.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Just a friendly reminder that senior and staff software engineers are not “above” or “better than”entry level software engineers.\n\nPlease don’t let your job title inflate your ego 🙅‍♀️",
        "html" : "<div style=\"white-space: pre\">Just a friendly reminder that senior and staff software engineers are not “above” or “better than”entry level software engineers.\n\nPlease don’t let your job title inflate your ego 🙅‍♀️</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
