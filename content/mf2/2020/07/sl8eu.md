{
  "date" : "2020-07-21T17:15:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/Loftio/status/1285587148297707521" ],
    "name" : [ "Like of @Loftio's tweet" ],
    "published" : [ "2020-07-21T17:15:00+01:00" ],
    "category" : [ "star-wars", "hamilton", "tiktok" ],
    "like-of" : [ "https://twitter.com/Loftio/status/1285587148297707521" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/sl8eu",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1285587148297707521" ],
      "url" : [ "https://twitter.com/Loftio/status/1285587148297707521" ],
      "video" : [ "https://video.twimg.com/ext_tw_video/1285587118706831361/pu/vid/540x960/hM4R8R1PNb5J2a_k.mp4?tag=10" ],
      "published" : [ "2020-07-21T14:47:15+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Loftio" ],
          "numeric-id" : [ "19788371" ],
          "name" : [ "Lex Lofthouse ✨" ],
          "nickname" : [ "Loftio" ],
          "url" : [ "https://twitter.com/Loftio", "http://loftio.co.uk" ],
          "published" : [ "2009-01-30T21:03:02+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1138757469176508417/Hc1FJP7a.png" ]
        }
      } ],
      "content" : [ "My Star Wars TikTok is finally combining with my Hamilton TikTok for the ultimate algorithm" ]
    }
  },
  "tags" : [ "star-wars", "hamilton", "tiktok" ],
  "client_id" : "https://indigenous.realize.be"
}
