{
  "date" : "2020-07-30T16:31:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/britnorcodes/status/1288851973786284032" ],
    "name" : [ "Like of @britnorcodes's tweet" ],
    "published" : [ "2020-07-30T16:31:00+01:00" ],
    "like-of" : [ "https://twitter.com/britnorcodes/status/1288851973786284032" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/eiqxw",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1288851973786284032" ],
      "url" : [ "https://twitter.com/britnorcodes/status/1288851973786284032" ],
      "published" : [ "2020-07-30T15:00:30+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:britnorcodes" ],
          "numeric-id" : [ "1124460448936615936" ],
          "name" : [ "Em 💫" ],
          "nickname" : [ "britnorcodes" ],
          "url" : [ "https://twitter.com/britnorcodes" ],
          "published" : [ "2019-05-03T23:47:17+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1283537245488721920/1KWEsynR.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "One of the best interview tips I've ever been given is, when asked:\n\n❓'What is your expected salary?'\n\nrespond with:\n\n❔'What is your salary range for this role?'",
        "html" : "<div style=\"white-space: pre\">One of the best interview tips I've ever been given is, when asked:\n\n❓'What is your expected salary?'\n\nrespond with:\n\n❔'What is your salary range for this role?'</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
