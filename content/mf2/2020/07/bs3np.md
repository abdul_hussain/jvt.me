{
  "date" : "2020-07-27T20:11:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/dev_nikema/status/1287775874444455937" ],
    "name" : [ "Like of @dev_nikema's tweet" ],
    "published" : [ "2020-07-27T20:11:00+01:00" ],
    "like-of" : [ "https://twitter.com/dev_nikema/status/1287775874444455937" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/bs3np",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1287775874444455937" ],
      "url" : [ "https://twitter.com/dev_nikema/status/1287775874444455937" ],
      "published" : [ "2020-07-27T15:44:28+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:dev_nikema" ],
          "numeric-id" : [ "744302714226544640" ],
          "name" : [ "Nikema Prophet ❤️✨ she/her" ],
          "nickname" : [ "dev_nikema" ],
          "url" : [ "https://twitter.com/dev_nikema", "https://github.com/sponsors/prophen", "http://GitHub.com/sponsors/PopSchools" ],
          "published" : [ "2016-06-18T22:56:10+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "California, USA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1251319075579424768/ZTcWcIpF.jpg" ]
        }
      } ],
      "content" : [ "I feel like there's a fine line between stretching with the intent to grow within your abilities and forcing yourself to do a thing you don't enjoy because you ”should.”" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
