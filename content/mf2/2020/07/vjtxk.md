{
  "date" : "2020-07-30T20:17:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/MurielComedy/status/1288457206875250688" ],
    "name" : [ "Like of @MurielComedy's tweet" ],
    "published" : [ "2020-07-30T20:17:00+01:00" ],
    "like-of" : [ "https://twitter.com/MurielComedy/status/1288457206875250688" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/vjtxk",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1288457206875250688" ],
      "url" : [ "https://twitter.com/MurielComedy/status/1288457206875250688" ],
      "video" : [ "https://video.twimg.com/ext_tw_video/1288456075797303296/pu/vid/1280x720/JtA8bvXg-rkhzjXQ.mp4?tag=10" ],
      "published" : [ "2020-07-29T12:51:51+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:MurielComedy" ],
          "numeric-id" : [ "4197607979" ],
          "name" : [ "Muriel" ],
          "nickname" : [ "MurielComedy" ],
          "url" : [ "https://twitter.com/MurielComedy", "http://www.murielcomedy.com", "http://buymeacoffee.com/murielcomedy" ],
          "published" : [ "2015-11-15T23:34:29+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/858821490803953664/RTjZRbfU.jpg" ]
        }
      } ],
      "content" : [ "What happens when you send a tweet in 2020" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
