{
  "date" : "2020-07-11T20:32:41.7Z",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://events.indieweb.org/2020/07/online-homebrew-website-club-nottingham-J6rKyTq3khYa" ],
    "name" : [ "RSVP yes to https://events.indieweb.org/2020/07/online-homebrew-website-club-nottingham-J6rKyTq3khYa" ],
    "published" : [ "2020-07-11T20:32:41.7Z" ],
    "event" : {
      "start" : [ "2020-07-22T17:30:00+01:00" ],
      "name" : [ "ONLINE: Homebrew Website Club: Nottingham" ],
      "end" : [ "2020-07-22T19:30:00+01:00" ],
      "location" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "name" : [ "Online" ],
          "latitude" : [ "52.9509448" ],
          "longitude" : [ "-1.1522525" ]
        },
        "lang" : "en",
        "value" : "Online"
      } ],
      "url" : [ "https://events.indieweb.org/2020/07/online-homebrew-website-club-nottingham-J6rKyTq3khYa" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/07/wq7s5",
  "client_id" : "https://micropublish.net"
}
