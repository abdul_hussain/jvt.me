{
  "date" : "2020-07-03T11:55:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [  ],
    "name" : [ "Like of @SBinLondon's tweet" ],
    "published" : [ "2020-07-03T11:55:00+01:00" ],
    "like-of" : [ "https://twitter.com/SBinLondon/status/1278641168864161793" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/ubog4",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1278641168864161793" ],
      "url" : [ "https://twitter.com/SBinLondon/status/1278641168864161793" ],
      "published" : [ "2020-07-02T10:46:25+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:SBinLondon" ],
          "numeric-id" : [ "180283911" ],
          "name" : [ "Kate 🍑" ],
          "nickname" : [ "SBinLondon" ],
          "url" : [ "https://twitter.com/SBinLondon", "http://katebeard.co" ],
          "published" : [ "2010-08-19T07:16:23+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Where the good coffee is" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1222510978732036102/Dx46JyVK.jpg" ]
        }
      } ],
      "content" : [ "🙂🙃" ],
      "photo" : [ "https://pbs.twimg.com/media/Eb6k-RIXgAA_rQl.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
