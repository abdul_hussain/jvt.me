{
  "date" : "2020-07-13T17:09:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/iamhirusi/status/1282701017130217473" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1282710942527225857" ],
    "name" : [ "Reply to https://twitter.com/iamhirusi/status/1282701017130217473" ],
    "published" : [ "2020-07-13T17:09:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "Yes it is at https://indieweb.org/commentpara.de but it's not linked to from many pages, so we can probably do something to make it more discoverable!"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/07/yfnd9",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1282701017130217473" ],
      "url" : [ "https://twitter.com/iamhirusi/status/1282701017130217473" ],
      "published" : [ "2020-07-13T15:38:48+00:00" ],
      "in-reply-to" : [ "https://twitter.com/kevinmarks/status/1282575876157308928" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:iamhirusi" ],
          "numeric-id" : [ "1252888172700119041" ],
          "name" : [ "Ru Singh" ],
          "nickname" : [ "iamhirusi" ],
          "url" : [ "https://twitter.com/iamhirusi", "https://rusingh.com" ],
          "published" : [ "2020-04-22T09:14:03+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "India" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1258445925145591814/b0T5aa6E.png" ]
        }
      } ],
      "content" : [ {
        "value" : "Hmm... very interesting idea and I wonder why I hadn't come across it yet. I'll try it out soon. Is this mentioned on the IndieWeb wiki?",
        "html" : "Hmm... very interesting idea and I wonder why I hadn't come across it yet. I'll try it out soon. Is this mentioned on the IndieWeb wiki?\n<a class=\"u-mention\" href=\"https://twitter.com/kevinmarks\"></a>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
