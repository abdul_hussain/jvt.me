{
  "date" : "2020-07-22T11:19:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/aahunsberger/status/1285576040572739585" ],
    "name" : [ "Like of @aahunsberger's tweet" ],
    "published" : [ "2020-07-22T11:19:00+01:00" ],
    "like-of" : [ "https://twitter.com/aahunsberger/status/1285576040572739585" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/xrfb4",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1285576040572739585" ],
      "url" : [ "https://twitter.com/aahunsberger/status/1285576040572739585" ],
      "published" : [ "2020-07-21T14:03:07+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:aahunsberger" ],
          "numeric-id" : [ "1428093824" ],
          "name" : [ "Ashley Hunsberger" ],
          "nickname" : [ "aahunsberger" ],
          "url" : [ "https://twitter.com/aahunsberger" ],
          "published" : [ "2013-05-14T15:08:48+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Raleigh, NC" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1268185908475502599/QyH4nvoJ.jpg" ]
        }
      } ],
      "content" : [ "Someone asked me today what her son should study to do well in tech. I said humanities. You need to know people. I will die on this hill." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
