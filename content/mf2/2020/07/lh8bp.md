{
  "date" : "2020-07-06T11:44:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/McAuleyATL/status/1279248758409216000" ],
    "name" : [ "Like of @McAuleyATL's tweet" ],
    "published" : [ "2020-07-06T11:44:00+01:00" ],
    "like-of" : [ "https://twitter.com/McAuleyATL/status/1279248758409216000" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/lh8bp",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1279248758409216000" ],
      "url" : [ "https://twitter.com/McAuleyATL/status/1279248758409216000" ],
      "published" : [ "2020-07-04T03:00:45+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:McAuleyATL" ],
          "numeric-id" : [ "1218679919913373697" ],
          "name" : [ "McAuleyATL" ],
          "nickname" : [ "McAuleyATL" ],
          "url" : [ "https://twitter.com/McAuleyATL", "https://www.patreon.com/McAuleyATL", "http://Youtube.com/McauleyATL" ],
          "published" : [ "2020-01-18T23:41:58+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Atlanta, GA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1279225022960283648/e7ZWHxV1.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Finland ended homelessness. They provide a small apartment and counseling with no preconditions. 4/5 make their way back into a stable life and guess what - it was CHEAPER than allowing homelessness to continue.\n\n scoop.me/housing-first-…",
        "html" : "<div style=\"white-space: pre\">Finland ended homelessness. They provide a small apartment and counseling with no preconditions. 4/5 make their way back into a stable life and guess what - it was CHEAPER than allowing homelessness to continue.\n\n <a href=\"https://scoop.me/housing-first-finland-homelessness/\">scoop.me/housing-first-…</a></div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
