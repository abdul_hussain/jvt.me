{
  "date" : "2020-07-06T23:43:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/CarolSaysThings/status/1280249754899931141" ],
    "name" : [ "Like of @CarolSaysThings's tweet" ],
    "published" : [ "2020-07-06T23:43:00+01:00" ],
    "like-of" : [ "https://twitter.com/CarolSaysThings/status/1280249754899931141" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/iqzkf",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1280249754899931141" ],
      "url" : [ "https://twitter.com/CarolSaysThings/status/1280249754899931141" ],
      "published" : [ "2020-07-06T21:18:22+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:CarolSaysThings" ],
          "numeric-id" : [ "36382927" ],
          "name" : [ "Carol 😅" ],
          "nickname" : [ "CarolSaysThings" ],
          "url" : [ "https://twitter.com/CarolSaysThings", "https://carolgilabert.me/" ],
          "published" : [ "2009-04-29T15:22:13+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "🇧🇷🇪🇸🇬🇧 · Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1238515159594917889/C5994QPa.jpg" ]
        }
      } ],
      "content" : [ "🤣🤣🤣" ]
    },
    "children" : [ {
      "type" : [ "u-quotation-of", "h-cite" ],
      "properties" : {
        "uid" : [ "tag:twitter.com:1280248464316694531" ],
        "url" : [ "https://twitter.com/javagrifter/status/1280248464316694531" ],
        "published" : [ "2020-07-06T21:13:14+00:00" ],
        "in-reply-to" : [ "https://twitter.com/javagrifter/status/1280248156740096001" ],
        "author" : [ {
          "type" : [ "h-card" ],
          "properties" : {
            "uid" : [ "tag:twitter.com:javagrifter" ],
            "numeric-id" : [ "1278118393036627968" ],
            "name" : [ "JavaGrifter" ],
            "nickname" : [ "javagrifter" ],
            "url" : [ "https://twitter.com/javagrifter" ],
            "published" : [ "2020-07-01T00:09:16+00:00" ],
            "photo" : [ "https://pbs.twimg.com/profile_images/1278379898076348418/UBaCA-vL.jpg" ]
          }
        } ],
        "content" : [ "Here is Uncle Bob inventing the Software CraftsMANship" ],
        "photo" : [ "https://pbs.twimg.com/media/EcRbMLqWAAEX5up.jpg" ]
      }
    } ]
  },
  "client_id" : "https://indigenous.realize.be"
}
