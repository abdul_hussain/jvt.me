{
  "date" : "2020-07-13T17:50:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/technottingham/status/1282717940261978112" ],
    "name" : [ "Like of @technottingham's tweet" ],
    "published" : [ "2020-07-13T17:50:00+01:00" ],
    "like-of" : [ "https://twitter.com/technottingham/status/1282717940261978112" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/5pnf9",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1282717940261978112" ],
      "url" : [ "https://twitter.com/technottingham/status/1282717940261978112" ],
      "video" : [ "https://video.twimg.com/tweet_video/Ec0hKcNXsAAKX2x.mp4" ],
      "published" : [ "2020-07-13T16:46:03+00:00" ],
      "in-reply-to" : [ "https://twitter.com/JamieTanna/status/1282700340597592066" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:technottingham" ],
          "numeric-id" : [ "384492431" ],
          "name" : [ "Tech Nottingham" ],
          "nickname" : [ "technottingham" ],
          "url" : [ "https://twitter.com/technottingham", "http://technottingham.com" ],
          "published" : [ "2011-10-03T19:47:31+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1023974499757293570/ZoPc_QsO.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Oh yes!!!",
        "html" : "Oh yes!!!\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
