{
  "date" : "2020-07-11T13:24:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/jimwaterson/status/1281689172928081926" ],
    "name" : [ "Like of @jimwaterson's tweet" ],
    "published" : [ "2020-07-11T13:24:00+01:00" ],
    "category" : [ "coronavirus" ],
    "like-of" : [ "https://twitter.com/jimwaterson/status/1281689172928081926" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/vyzq6",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1281689172928081926" ],
      "url" : [ "https://twitter.com/jimwaterson/status/1281689172928081926" ],
      "published" : [ "2020-07-10T20:38:06+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:jimwaterson" ],
          "numeric-id" : [ "25275453" ],
          "name" : [ "Jim Waterson" ],
          "nickname" : [ "jimwaterson" ],
          "url" : [ "https://twitter.com/jimwaterson", "http://www.jimwaterson.com" ],
          "published" : [ "2009-03-19T12:03:34+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1110478628897333248/KECDyM16.png" ]
        }
      } ],
      "location" : [ {
        "type" : [ "h-card", "p-location" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:09820bcd45940000" ],
          "name" : [ "Barnard Castle" ]
        }
      } ],
      "content" : [ "Four months without seeing my parents due to lockdown. Mum was shielding due to cancer and the pandemic meant she couldn’t meet her first grandkid for months. But this week rules changed. And mum was given cancer all-clear. So we celebrated with a trip to a suitable market town." ],
      "photo" : [ "https://pbs.twimg.com/media/Ecl5gQIWkAIbQeY.jpg" ]
    }
  },
  "tags" : [ "coronavirus" ],
  "client_id" : "https://indigenous.realize.be"
}
