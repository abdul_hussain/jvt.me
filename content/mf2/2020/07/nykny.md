{
  "date" : "2020-07-04T11:30:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/DekneeDenis/status/1279105193288839168" ],
    "name" : [ "Like of @DekneeDenis's tweet" ],
    "published" : [ "2020-07-04T11:30:00+01:00" ],
    "like-of" : [ "https://twitter.com/DekneeDenis/status/1279105193288839168" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/nykny",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1279105193288839168" ],
      "url" : [ "https://twitter.com/DekneeDenis/status/1279105193288839168" ],
      "published" : [ "2020-07-03T17:30:17+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:DekneeDenis" ],
          "numeric-id" : [ "1132731955" ],
          "name" : [ "Denis (Deknee) Gendron" ],
          "nickname" : [ "DekneeDenis" ],
          "url" : [ "https://twitter.com/DekneeDenis" ],
          "published" : [ "2013-01-30T02:33:26+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Assiniboine and Red. 3 points" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1278515443465097226/j2j9vZ2C.jpg" ]
        }
      } ],
      "content" : [ "The Six Grandfathers.  Before  taken and turned into Mt. Rushmore" ],
      "photo" : [ "https://pbs.twimg.com/media/EcBK4fKWsAEzHMs.png" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
