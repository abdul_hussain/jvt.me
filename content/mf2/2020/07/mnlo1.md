{
  "date" : "2020-07-13T10:36:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/ben_nuttall/status/1282598447049302016" ],
    "name" : [ "Like of @ben_nuttall's tweet" ],
    "published" : [ "2020-07-13T10:36:00+01:00" ],
    "category" : [ "open-source" ],
    "like-of" : [ "https://twitter.com/ben_nuttall/status/1282598447049302016" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/mnlo1",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1282598447049302016" ],
      "url" : [ "https://twitter.com/ben_nuttall/status/1282598447049302016" ],
      "published" : [ "2020-07-13T08:51:13+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:ben_nuttall" ],
          "numeric-id" : [ "27241881" ],
          "name" : [ "Ben Nuttall ☮️♥️" ],
          "nickname" : [ "ben_nuttall" ],
          "url" : [ "https://twitter.com/ben_nuttall", "https://bennuttall.com" ],
          "published" : [ "2009-03-28T15:00:50+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Cambridgeshire, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/528155792013160448/cobxlRc8.jpeg" ]
        }
      } ],
      "content" : [ "REMINDER. I don't have to fix your problems." ],
      "photo" : [ "https://pbs.twimg.com/media/Ecy0fOwWoAYQqda.png" ]
    }
  },
  "tags" : [ "open-source" ],
  "client_id" : "https://indigenous.realize.be"
}
