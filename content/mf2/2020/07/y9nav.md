{
  "date" : "2020-07-23T18:25:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/molly_struve/status/1256018952846741510" ],
    "name" : [ "Like of @molly_struve's tweet" ],
    "published" : [ "2020-07-23T18:25:00+01:00" ],
    "like-of" : [ "https://twitter.com/molly_struve/status/1256018952846741510" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/y9nav",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1256018952846741510" ],
      "url" : [ "https://twitter.com/molly_struve/status/1256018952846741510" ],
      "published" : [ "2020-05-01T00:33:48+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:molly_struve" ],
          "numeric-id" : [ "4542932414" ],
          "name" : [ "Molly Struve 🦄" ],
          "nickname" : [ "molly_struve" ],
          "url" : [ "https://twitter.com/molly_struve", "https://dev.to/molly_struve", "http://dev.to" ],
          "published" : [ "2015-12-13T01:44:07+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Chicago, IL" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1256296425912221696/nETdaboc.jpg" ]
        }
      } ],
      "content" : [ "Be kind to your browser and your mind." ],
      "photo" : [ "https://pbs.twimg.com/media/EW5GlHTXkAAiiGI.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
