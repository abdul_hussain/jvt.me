{
  "date" : "2020-07-17T23:27:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/kvlly/status/1284237538304155648" ],
    "name" : [ "Like of @kvlly's tweet" ],
    "published" : [ "2020-07-17T23:27:00+01:00" ],
    "like-of" : [ "https://twitter.com/kvlly/status/1284237538304155648" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/kb8zx",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1284237538304155648" ],
      "url" : [ "https://twitter.com/kvlly/status/1284237538304155648" ],
      "published" : [ "2020-07-17T21:24:23+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:kvlly" ],
          "numeric-id" : [ "123543103" ],
          "name" : [ "Kelly Vaughn 🐞" ],
          "nickname" : [ "kvlly" ],
          "url" : [ "https://twitter.com/kvlly", "http://kvlly.com", "http://startfreelancing.today" ],
          "published" : [ "2010-03-16T12:15:39+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Atlanta, GA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1273615233018482688/7yvtWlBt.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I swear I did not take down @Shopify",
        "html" : "I swear I did not take down <a href=\"https://twitter.com/Shopify\">@Shopify</a>"
      } ]
    },
    "children" : [ {
      "type" : [ "u-quotation-of", "h-cite" ],
      "properties" : {
        "uid" : [ "tag:twitter.com:1284227246925008896" ],
        "url" : [ "https://twitter.com/kvlly/status/1284227246925008896" ],
        "published" : [ "2020-07-17T20:43:30+00:00" ],
        "author" : [ {
          "type" : [ "h-card" ],
          "properties" : {
            "uid" : [ "tag:twitter.com:kvlly" ],
            "numeric-id" : [ "123543103" ],
            "name" : [ "Kelly Vaughn 🐞" ],
            "nickname" : [ "kvlly" ],
            "url" : [ "https://twitter.com/kvlly", "http://kvlly.com", "http://startfreelancing.today" ],
            "published" : [ "2010-03-16T12:15:39+00:00" ],
            "location" : [ {
              "type" : [ "h-card", "p-location" ],
              "properties" : {
                "name" : [ "Atlanta, GA" ]
              }
            } ],
            "photo" : [ "https://pbs.twimg.com/profile_images/1273615233018482688/7yvtWlBt.jpg" ]
          }
        } ],
        "content" : [ "Pushed to production aaaaaand it's time for vacation. 👋" ]
      }
    } ]
  },
  "client_id" : "https://indigenous.realize.be"
}
