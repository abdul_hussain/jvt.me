{
  "date" : "2020-07-01T14:11:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/cwarzel/status/1278116194621419520" ],
    "name" : [ "Like of @cwarzel's tweet" ],
    "published" : [ "2020-07-01T14:11:00+01:00" ],
    "category" : [ "indieweb", "social-media" ],
    "like-of" : [ "https://twitter.com/cwarzel/status/1278116194621419520" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/hzcvh",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1278116194621419520" ],
      "url" : [ "https://twitter.com/cwarzel/status/1278116194621419520" ],
      "published" : [ "2020-07-01T00:00:21+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:cwarzel" ],
          "numeric-id" : [ "20178419" ],
          "name" : [ "Charlie Warzel" ],
          "nickname" : [ "cwarzel" ],
          "url" : [ "https://twitter.com/cwarzel", "https://www.nytimes.com/by/charlie-warzel" ],
          "published" : [ "2009-02-05T19:36:10+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Montana" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/531522450190651393/HuRaqDlp.png" ]
        }
      } ],
      "content" : [ "odd question but: what are your most far fetched utopian ideas for fixing social media platforms? the stuff that’s likely never ever gonna happen" ]
    }
  },
  "tags" : [ "indieweb", "social-media" ],
  "client_id" : "https://indigenous.realize.be"
}
