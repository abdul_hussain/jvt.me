{
  "date" : "2020-07-26T11:55:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/baddestmamajama/status/1287082136017625089" ],
    "name" : [ "Like of @baddestmamajama's tweet" ],
    "published" : [ "2020-07-26T11:55:00+01:00" ],
    "like-of" : [ "https://twitter.com/baddestmamajama/status/1287082136017625089" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/hfrqk",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1287082136017625089" ],
      "url" : [ "https://twitter.com/baddestmamajama/status/1287082136017625089" ],
      "published" : [ "2020-07-25T17:47:48+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:baddestmamajama" ],
          "numeric-id" : [ "568519130" ],
          "name" : [ "Jessica Ellis" ],
          "nickname" : [ "baddestmamajama" ],
          "url" : [ "https://twitter.com/baddestmamajama", "https://baddestmamajama.substack.com/" ],
          "published" : [ "2012-05-01T20:39:29+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Los Angeles, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1058008238614925312/-Vms_t9y.jpg" ]
        }
      } ],
      "content" : [ "Meghan Markle is very clearly an accomplished knight who rescued a helpless prince from a tower and any stories that don’t frame it like that are wrong, don’t @ me." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
