{
  "date" : "2020-07-11T22:51:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamieTanna/status/1282071502397956096" ],
    "published" : [ "2020-07-11T22:51:00+01:00" ],
    "repost-of" : [ "https://twitter.com/ParissAthena/status/1281958370598178818" ],
    "category" : [ "coronavirus" ],
    "content" : [ {
      "html" : "",
      "value" : "I feel personally attacked by this tweet. I don't know how I'm going to continue to grow my wardrobe in a post-COVID world"
    } ]
  },
  "kind" : "reposts",
  "slug" : "2020/07/sjgvt",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1281958370598178818" ],
      "url" : [ "https://twitter.com/ParissAthena/status/1281958370598178818" ],
      "published" : [ "2020-07-11T14:27:47+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:ParissAthena" ],
          "numeric-id" : [ "1030656907038609408" ],
          "name" : [ "Pariss Athena⚡️Black Tech Pipeline Founder" ],
          "nickname" : [ "ParissAthena" ],
          "url" : [ "https://twitter.com/ParissAthena", "https://blacktechpipeline.substack.com/p/coming-soon" ],
          "published" : [ "2018-08-18T03:25:30+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Boston, MA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1262097427265458178/R7-JQ8RZ.jpg" ]
        }
      } ],
      "content" : [ "My fiancé’s entire wardrobe is all tech “swag” that you get from conferences and joining new communities lol" ]
    }
  },
  "tags" : [ "coronavirus" ],
  "client_id" : "https://indigenous.realize.be"
}
