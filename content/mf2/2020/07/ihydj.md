{
  "date" : "2020-07-24T18:02:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/edent/status/1286703048321097731" ],
    "name" : [ "Like of @edent's tweet" ],
    "published" : [ "2020-07-24T18:02:00+01:00" ],
    "like-of" : [ "https://twitter.com/edent/status/1286703048321097731" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/ihydj",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1286703048321097731" ],
      "url" : [ "https://twitter.com/edent/status/1286703048321097731" ],
      "published" : [ "2020-07-24T16:41:27+00:00" ],
      "in-reply-to" : [ "https://twitter.com/sillypunk/status/1286697593708392449" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:edent" ],
          "numeric-id" : [ "14054507" ],
          "name" : [ "Terence Eden" ],
          "nickname" : [ "edent" ],
          "url" : [ "https://twitter.com/edent", "https://shkspr.mobi/blog/", "https://edent.tel" ],
          "published" : [ "2008-02-28T13:10:25+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1228067445153452033/_A8Uq2VY.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I think there's a problem with Twitter - because the obligatory photo didn't attach.",
        "html" : "I think there's a problem with Twitter - because the obligatory photo didn't attach.\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/sillypunk\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
