{
  "date" : "2020-07-18T12:35:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/mattiasgeniar/status/1284422823839227905" ],
    "name" : [ "Like of @mattiasgeniar's tweet" ],
    "published" : [ "2020-07-18T12:35:00+01:00" ],
    "like-of" : [ "https://twitter.com/mattiasgeniar/status/1284422823839227905" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/nvdpf",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1284422823839227905" ],
      "url" : [ "https://twitter.com/mattiasgeniar/status/1284422823839227905" ],
      "published" : [ "2020-07-18T09:40:39+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:mattiasgeniar" ],
          "numeric-id" : [ "93920255" ],
          "name" : [ "Mattias Geniar" ],
          "nickname" : [ "mattiasgeniar" ],
          "url" : [ "https://twitter.com/mattiasgeniar", "https://ma.ttias.be" ],
          "published" : [ "2009-12-01T19:00:40+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Belgium" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1173566135951876096/Fi5cYHYQ.jpg" ]
        }
      } ],
      "content" : [ "I thought GPT-3 was a new form of open-source licensing  ¯\\_(ツ)_/¯" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
