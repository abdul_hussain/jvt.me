{
  "date" : "2020-07-26T20:41:57.357Z",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/CarolSaysThings/status/1287484837419507714" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1287489897104904192" ],
    "name" : [ "Reply to https://twitter.com/CarolSaysThings/status/1287484837419507714" ],
    "published" : [ "2020-07-26T20:41:57.357Z" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "Luckily there was Mango Lassi and yogurt to save the day, but it was not as comfortable a meal as I wanted!"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/07/ts43h",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1287484837419507714" ],
      "url" : [ "https://twitter.com/CarolSaysThings/status/1287484837419507714" ],
      "published" : [ "2020-07-26T20:28:00+00:00" ],
      "in-reply-to" : [ "https://twitter.com/JamieTanna/status/1287383982338904064" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:CarolSaysThings" ],
          "numeric-id" : [ "36382927" ],
          "name" : [ "Carol 🌻" ],
          "nickname" : [ "CarolSaysThings" ],
          "url" : [ "https://twitter.com/CarolSaysThings", "https://carolgilabert.me/" ],
          "published" : [ "2009-04-29T15:22:13+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "🇧🇷🇪🇸🇬🇧 · Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1238515159594917889/C5994QPa.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Oh noes! Is your mouth ok? 🌶🌶",
        "html" : "Oh noes! Is your mouth ok? 🌶🌶\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/JamieTanna\"></a>\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/rikshawindian\"></a>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://micropublish.net"
}
