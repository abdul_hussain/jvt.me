{
  "date" : "2020-07-25T19:42:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/haskellhutt/status/1286924710400479235" ],
    "name" : [ "Like of @haskellhutt's tweet" ],
    "published" : [ "2020-07-25T19:42:00+01:00" ],
    "category" : [ "haskell" ],
    "like-of" : [ "https://twitter.com/haskellhutt/status/1286924710400479235" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/udjwq",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1286924710400479235" ],
      "url" : [ "https://twitter.com/haskellhutt/status/1286924710400479235" ],
      "published" : [ "2020-07-25T07:22:15+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:haskellhutt" ],
          "numeric-id" : [ "954369145554063360" ],
          "name" : [ "Graham Hutton" ],
          "nickname" : [ "haskellhutt" ],
          "url" : [ "https://twitter.com/haskellhutt", "http://www.cs.nott.ac.uk/~pszgmh", "http://tinyurl.com/PIH-II" ],
          "published" : [ "2018-01-19T15:05:10+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1168230923965480960/kuBTWbzx.jpg" ]
        }
      } ],
      "location" : [ {
        "type" : [ "h-card", "p-location" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:7d7bdec12d2549d4" ],
          "name" : [ "Nottingham, England" ]
        }
      } ],
      "content" : [ "I could tell you a monad joke, but first I'd need to tell you jokes about functors and applicatives." ]
    }
  },
  "tags" : [ "haskell" ],
  "client_id" : "https://indigenous.realize.be"
}
