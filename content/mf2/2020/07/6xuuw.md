{
  "date" : "2020-07-02T16:58:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/StevenPears/status/1278704471825166336" ],
    "name" : [ "Like of @StevenPears's tweet" ],
    "published" : [ "2020-07-02T16:58:00+01:00" ],
    "like-of" : [ "https://twitter.com/StevenPears/status/1278704471825166336" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/6xuuw",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1278704471825166336" ],
      "url" : [ "https://twitter.com/StevenPears/status/1278704471825166336" ],
      "published" : [ "2020-07-02T14:57:57+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:StevenPears" ],
          "numeric-id" : [ "404697859" ],
          "name" : [ "Steven Pears" ],
          "nickname" : [ "StevenPears" ],
          "url" : [ "https://twitter.com/StevenPears", "https://github.com/stoiveyp" ],
          "published" : [ "2011-11-04T08:54:04+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1210474450824642560/lmh3mZjU.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Three times today someone has needed my help with an error - twice they could have handled it themselves if the error message hadn't seemed like it needed translating.\n\nWrite friendly, verbose error messages in your software so devs can fix their own problems 👍",
        "html" : "<div style=\"white-space: pre\">Three times today someone has needed my help with an error - twice they could have handled it themselves if the error message hadn't seemed like it needed translating.\n\nWrite friendly, verbose error messages in your software so devs can fix their own problems 👍</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
