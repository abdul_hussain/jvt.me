{
  "date" : "2020-07-04T12:23:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/foxshaped/status/1279168889474220033" ],
    "name" : [ "Like of @foxshaped's tweet" ],
    "published" : [ "2020-07-04T12:23:00+01:00" ],
    "category" : [ "personal-website" ],
    "like-of" : [ "https://twitter.com/foxshaped/status/1279168889474220033" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/lihaq",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1279168889474220033" ],
      "url" : [ "https://twitter.com/foxshaped/status/1279168889474220033" ],
      "published" : [ "2020-07-03T21:43:23+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:foxshaped" ],
          "numeric-id" : [ "223470629" ],
          "name" : [ "Emma" ],
          "nickname" : [ "foxshaped" ],
          "url" : [ "https://twitter.com/foxshaped" ],
          "published" : [ "2010-12-06T13:48:53+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Leicester, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1273188721555312640/BsC0QXNJ.jpg" ]
        }
      } ],
      "content" : [ "I’m about to have a website! 😮" ]
    }
  },
  "tags" : [ "personal-website" ],
  "client_id" : "https://indigenous.realize.be"
}
