{
  "date" : "2020-07-07T12:21:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/mrmitchell78/status/1280375650516426753" ],
    "name" : [ "Like of @mrmitchell78's tweet" ],
    "published" : [ "2020-07-07T12:21:00+01:00" ],
    "like-of" : [ "https://twitter.com/mrmitchell78/status/1280375650516426753" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/btzau",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1280375650516426753" ],
      "url" : [ "https://twitter.com/mrmitchell78/status/1280375650516426753" ],
      "published" : [ "2020-07-07T05:38:37+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:mrmitchell78" ],
          "numeric-id" : [ "20156434" ],
          "name" : [ "Paul Mitchell" ],
          "nickname" : [ "mrmitchell78" ],
          "url" : [ "https://twitter.com/mrmitchell78" ],
          "published" : [ "2009-02-05T15:28:08+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Mackem in NI" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1274028965984907265/EFGGPa4B.jpg" ]
        }
      } ],
      "content" : [ "15 years ago today I stepped on a tube train at Kings Cross.  6ft away a suicide bomber detonated his bomb, killing 26 in the carriage.  I was lucky.  52 died over 4 sites, many more injured.  Never gets easier.  My love to all the bereaved families." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
