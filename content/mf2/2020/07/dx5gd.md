{
  "date" : "2020-07-21T18:38:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/EatPodcast/status/1285454015409594368" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1285631703936045056" ],
    "name" : [ "Reply to https://twitter.com/EatPodcast/status/1285454015409594368" ],
    "published" : [ "2020-07-21T18:38:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "+1! And you're more than welcome to join https://events.indieweb.org/2020/07/micropub-pop-up-session-kGMIMOXFUdBn this weekend where we're talking about all things Micropub!"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/07/dx5gd",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1285454015409594368" ],
      "url" : [ "https://twitter.com/EatPodcast/status/1285454015409594368" ],
      "published" : [ "2020-07-21T05:58:14+00:00" ],
      "in-reply-to" : [ "https://twitter.com/iamhirusi/status/1285451030080057344" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:EatPodcast" ],
          "numeric-id" : [ "2831349631" ],
          "name" : [ "Eat This Podcast" ],
          "nickname" : [ "EatPodcast" ],
          "url" : [ "https://twitter.com/EatPodcast", "https://eatthispodcast.com" ],
          "published" : [ "2014-09-25T06:58:13+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/516972271647207424/vQLimSp0.jpeg" ]
        }
      } ],
      "content" : [ {
        "value" : "Sure. Micropub is the thing I would really like to get working for my own site. Finding time is so hard for me right now.",
        "html" : "Sure. Micropub is the thing I would really like to get working for my own site. Finding time is so hard for me right now.\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/iamhirusi\"></a>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
