{
  "date" : "2020-07-25T22:04:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://events.indieweb.org/2020/08/indieauth-pop-up-session-6xlxgeCEMgv8" ],
    "name" : [ "RSVP no to https://events.indieweb.org/2020/08/indieauth-pop-up-session-6xlxgeCEMgv8" ],
    "published" : [ "2020-07-25T22:04:00+01:00" ],
    "event" : {
      "start" : [ "2020-08-08T09:30:00-07:00" ],
      "name" : [ "IndieAuth Pop-Up Session" ],
      "end" : [ "2020-08-08T11:30:00-07:00" ],
      "url" : [ "https://events.indieweb.org/2020/08/indieauth-pop-up-session-6xlxgeCEMgv8", "https://indieweb.org/2020/Pop-ups/IndieAuth" ]
    },
    "rsvp" : [ "no" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/07/4q0ua",
  "client_id" : "https://indigenous.realize.be"
}
