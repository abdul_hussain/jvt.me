{
  "date" : "2020-07-30T22:18:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/NoContextTrek/status/1288836737247125504" ],
    "name" : [ "Like of @NoContextTrek's tweet" ],
    "published" : [ "2020-07-30T22:18:00+01:00" ],
    "category" : [ "cute", "star-trek" ],
    "like-of" : [ "https://twitter.com/NoContextTrek/status/1288836737247125504" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/cia8y",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1288836737247125504" ],
      "url" : [ "https://twitter.com/NoContextTrek/status/1288836737247125504" ],
      "published" : [ "2020-07-30T13:59:58+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:NoContextTrek" ],
          "numeric-id" : [ "839246845914251264" ],
          "name" : [ "Star Trek Minus Context" ],
          "nickname" : [ "NoContextTrek" ],
          "url" : [ "https://twitter.com/NoContextTrek" ],
          "published" : [ "2017-03-07T22:50:15+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "that one weird Jefferies tube" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1100138473539940352/u5ydepge.jpg" ]
        }
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EeLeKj3XYAAbGoV.jpg", "https://pbs.twimg.com/media/EeLeK29XkAM_9DE.jpg", "https://pbs.twimg.com/media/EeLeLJYXoAAB_9b.jpg", "https://pbs.twimg.com/media/EeLeLeVWoAAy9rD.jpg" ]
    }
  },
  "tags" : [ "cute", "star-trek" ],
  "client_id" : "https://indigenous.realize.be"
}
