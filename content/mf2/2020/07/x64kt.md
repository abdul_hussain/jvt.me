{
  "date" : "2020-07-01T22:07:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.meetup.com/Notts-Techfast/events/271645480/" ],
    "syndication" : [ "https://brid.gy/publish/meetup" ],
    "name" : [ "RSVP maybe to https://www.meetup.com/Notts-Techfast/events/271645480/" ],
    "published" : [ "2020-07-01T22:07:00+01:00" ],
    "event" : {
      "start" : [ "2020-07-30T08:00:00+01:00" ],
      "name" : [ "Above and beyond your day job...a transformational career story!" ],
      "end" : [ "2020-07-30T09:00:00+01:00" ],
      "location" : [ "Online" ],
      "url" : [ "https://www.meetup.com/Notts-Techfast/events/271645480/" ]
    },
    "rsvp" : [ "maybe" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/07/x64kt",
  "client_id" : "https://indigenous.realize.be"
}
