{
  "date" : "2020-07-05T23:54:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/nursekelsey/status/1279449964020719622" ],
    "name" : [ "Like of @nursekelsey's tweet" ],
    "published" : [ "2020-07-05T23:54:00+01:00" ],
    "category" : [ "coronavirus" ],
    "like-of" : [ "https://twitter.com/nursekelsey/status/1279449964020719622" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/otvti",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1279449964020719622" ],
      "url" : [ "https://twitter.com/nursekelsey/status/1279449964020719622" ],
      "published" : [ "2020-07-04T16:20:17+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:nursekelsey" ],
          "numeric-id" : [ "22030045" ],
          "name" : [ "NurseKelsey" ],
          "nickname" : [ "nursekelsey" ],
          "url" : [ "https://twitter.com/nursekelsey" ],
          "published" : [ "2009-02-26T16:45:00+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/746421001081131010/YsKhkYla.jpg" ]
        }
      } ],
      "content" : [ "If you want to know how we get people to comply with wearing face masks, ask a sexologist. It’s not our first rodeo when it comes to convincing people they should wear a barrier for protection from a deadly virus. Here’s what we’ve learned from 4 decades of research on condoms:" ]
    }
  },
  "tags" : [ "coronavirus" ],
  "client_id" : "https://indigenous.realize.be"
}
