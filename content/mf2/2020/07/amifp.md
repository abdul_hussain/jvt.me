{
  "date" : "2020-07-11T13:19:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/MrDarrenV/status/1281904827484770305" ],
    "name" : [ "Like of @MrDarrenV's tweet" ],
    "published" : [ "2020-07-11T13:19:00+01:00" ],
    "category" : [ "monzo" ],
    "like-of" : [ "https://twitter.com/MrDarrenV/status/1281904827484770305" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/amifp",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1281904827484770305" ],
      "url" : [ "https://twitter.com/MrDarrenV/status/1281904827484770305" ],
      "published" : [ "2020-07-11T10:55:02+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:MrDarrenV" ],
          "numeric-id" : [ "382281508" ],
          "name" : [ "Darren" ],
          "nickname" : [ "MrDarrenV" ],
          "url" : [ "https://twitter.com/MrDarrenV", "https://github.com/DarrenVong" ],
          "published" : [ "2011-09-29T20:05:14+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1191234585197268992/sJti9OJc.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Should I be worried about the stability of @monzo as a bank after seeing all their amazing engineers (+ many other staff) having to leave them? 🤔",
        "html" : "Should I be worried about the stability of <a href=\"https://twitter.com/monzo\">@monzo</a> as a bank after seeing all their amazing engineers (+ many other staff) having to leave them? 🤔"
      } ]
    }
  },
  "tags" : [ "monzo" ],
  "client_id" : "https://indigenous.realize.be"
}
