{
  "date" : "2020-07-05T11:51:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/MrsEmma/status/1279552978052165632" ],
    "name" : [ "Like of @MrsEmma's tweet" ],
    "published" : [ "2020-07-05T11:51:00+01:00" ],
    "like-of" : [ "https://twitter.com/MrsEmma/status/1279552978052165632" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/b96fr",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1279552978052165632" ],
      "url" : [ "https://twitter.com/MrsEmma/status/1279552978052165632" ],
      "published" : [ "2020-07-04T23:09:37+00:00" ],
      "in-reply-to" : [ "https://twitter.com/pstonethompson/status/1279516434884898817" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:MrsEmma" ],
          "numeric-id" : [ "19500955" ],
          "name" : [ "Emma Seward" ],
          "nickname" : [ "MrsEmma" ],
          "url" : [ "https://twitter.com/MrsEmma" ],
          "published" : [ "2009-01-25T19:32:42+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1270492094495371264/P86JBCzO.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "🤷🏻‍♀️ Won't be for everyone 🤷🏻‍♀️",
        "html" : "🤷🏻‍♀️ Won't be for everyone 🤷🏻‍♀️\n<a class=\"u-mention\" href=\"https://twitter.com/maw\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/pstonethompson\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
