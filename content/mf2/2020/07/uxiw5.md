{
  "date" : "2020-07-28T16:23:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamieTanna/status/1288134673047191552" ],
    "published" : [ "2020-07-28T16:23:00+01:00" ],
    "repost-of" : [ "https://twitter.com/rwdrich/status/1288115788679806976" ],
    "category" : [ "slack" ],
    "content" : [ {
      "html" : "",
      "value" : "Usually I send nudge messages to tell people, but I like this idea way more 😂"
    } ]
  },
  "kind" : "reposts",
  "slug" : "2020/07/uxiw5",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1288115788679806976" ],
      "url" : [ "https://twitter.com/rwdrich/status/1288115788679806976" ],
      "published" : [ "2020-07-28T14:15:10+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:rwdrich" ],
          "numeric-id" : [ "107548386" ],
          "name" : [ "Richard Davies" ],
          "nickname" : [ "rwdrich" ],
          "url" : [ "https://twitter.com/rwdrich", "http://rwdrich.co.uk" ],
          "published" : [ "2010-01-22T23:14:30+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Cambridge, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/917481919872491521/BwypzjEE.jpg" ]
        }
      } ],
      "content" : [ "Slack really needs a feature where when someone posts (at)channel, it just shuts their PC down for a little while so they can think about what they've done" ]
    }
  },
  "tags" : [ "slack" ],
  "client_id" : "https://indigenous.realize.be"
}
