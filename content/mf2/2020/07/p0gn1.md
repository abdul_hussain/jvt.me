{
  "date" : "2020-07-25T15:46:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/EmmaBostian/status/1287034245416968197" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1287037514755248129" ],
    "name" : [ "Reply to https://twitter.com/EmmaBostian/status/1287034245416968197" ],
    "published" : [ "2020-07-25T15:46:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "The https://www.gousto.co.uk/cookbook/lamb-recipes/warm-lemony-lamb-tabbouleh from <span class=\"h-card\"><a class=\"u-url\" href=\"https://twitter.com/GoustoCooking\">@GoustoCooking</a></span> is absolutely amazing and one of our favourites"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/07/p0gn1",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1287034245416968197" ],
      "url" : [ "https://twitter.com/EmmaBostian/status/1287034245416968197" ],
      "published" : [ "2020-07-25T14:37:30+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:EmmaBostian" ],
          "numeric-id" : [ "491821358" ],
          "name" : [ "Emma Bostian 🐞" ],
          "nickname" : [ "EmmaBostian" ],
          "url" : [ "https://twitter.com/EmmaBostian", "http://compiled.blog" ],
          "published" : [ "2012-02-14T01:57:07+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Karlsruhe, Germany" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1262289899786559489/wxFM6Q30.jpg" ]
        }
      } ],
      "content" : [ "I need dinner inspiration. What’s your favorite dinner recipe that won’t take longer than 30 minutes to cook and doesn’t have a zillion ingredients? 🥗🍕🌯🌮" ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
