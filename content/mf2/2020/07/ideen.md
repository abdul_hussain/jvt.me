{
  "date" : "2020-07-28T23:29:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/AnimatedAnt/status/1288224613697302529" ],
    "name" : [ "Like of @AnimatedAnt's tweet" ],
    "published" : [ "2020-07-28T23:29:00+01:00" ],
    "category" : [ "pokemon" ],
    "like-of" : [ "https://twitter.com/AnimatedAnt/status/1288224613697302529" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/ideen",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1288224613697302529" ],
      "url" : [ "https://twitter.com/AnimatedAnt/status/1288224613697302529" ],
      "published" : [ "2020-07-28T21:27:36+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:AnimatedAnt" ],
          "numeric-id" : [ "4095766456" ],
          "name" : [ "Antonela Pounder 🌈🦀🐳☔️" ],
          "nickname" : [ "AnimatedAnt" ],
          "url" : [ "https://twitter.com/AnimatedAnt", "http://505games.com/blog" ],
          "published" : [ "2015-11-02T07:31:02+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "United Kingdom" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1284602876782878720/8kJ5bIqy.jpg" ]
        }
      } ],
      "content" : [ "Why does little Snorlax look like a frog when he falls over? 🐸😭" ],
      "photo" : [ "https://pbs.twimg.com/media/EeCxdNZX0AE7_6l.jpg", "https://pbs.twimg.com/media/EeCxdYTWAAIAbre.jpg" ]
    }
  },
  "tags" : [ "pokemon" ],
  "client_id" : "https://indigenous.realize.be"
}
