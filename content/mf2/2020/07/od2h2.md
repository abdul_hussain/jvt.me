{
  "date" : "2020-07-14T08:01:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://events.indieweb.org/2020/07/micropub-pop-up-session-kGMIMOXFUdBn" ],
    "name" : [ "RSVP yes to https://events.indieweb.org/2020/07/micropub-pop-up-session-kGMIMOXFUdBn" ],
    "published" : [ "2020-07-14T08:01:00+01:00" ],
    "event" : {
      "start" : [ "2020-07-25T09:30:00-07:00" ],
      "name" : [ "Micropub Pop-Up Session" ],
      "end" : [ "2020-07-25T11:30:00-07:00" ],
      "url" : [ "https://events.indieweb.org/2020/07/micropub-pop-up-session-kGMIMOXFUdBn" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/07/od2h2",
  "client_id" : "https://indigenous.realize.be"
}
