{
  "date" : "2020-07-20T17:14:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/bit_shifts/status/1285194685070348289" ],
    "name" : [ "Like of @bit_shifts's tweet" ],
    "published" : [ "2020-07-20T17:14:00+01:00" ],
    "like-of" : [ "https://twitter.com/bit_shifts/status/1285194685070348289" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/dlncw",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1285194685070348289" ],
      "url" : [ "https://twitter.com/bit_shifts/status/1285194685070348289" ],
      "published" : [ "2020-07-20T12:47:45+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:bit_shifts" ],
          "numeric-id" : [ "3399167086" ],
          "name" : [ "Toby 🌈" ],
          "nickname" : [ "bit_shifts" ],
          "url" : [ "https://twitter.com/bit_shifts", "https://github.com/bitshiftsdotio" ],
          "published" : [ "2015-08-01T19:13:59+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1268098090046820353/rA0LPeba.jpg" ]
        }
      } ],
      "content" : [ "doctor's appointment booked, time to do something about that brain of mine" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
