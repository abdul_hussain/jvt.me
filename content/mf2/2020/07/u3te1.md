{
  "date" : "2020-07-22T16:55:35.645Z",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.jvt.me/events/personal-events/2020-07-e1zee/" ],
    "name" : [ "RSVP maybe to https://www.jvt.me/events/personal-events/2020-07-e1zee/" ],
    "published" : [ "2020-07-22T16:55:35.645Z" ],
    "event" : {
      "start" : [ "2020-07-23T17:00:00+0100" ],
      "name" : [ "Work Summer (Zoom) Party" ],
      "end" : [ "2020-07-23T20:00:00+0100" ],
      "url" : [ "https://www.jvt.me/events/personal-events/2020-07-e1zee/" ]
    },
    "rsvp" : [ "maybe" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/07/u3te1",
  "client_id" : "https://micropublish.net"
}
