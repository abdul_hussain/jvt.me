{
  "date" : "2020-07-04T16:31:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/chadtomkiss/status/1279425887339937792" ],
    "name" : [ "Like of @chadtomkiss's tweet" ],
    "published" : [ "2020-07-04T16:31:00+01:00" ],
    "category" : [ "diversity-and-inclusion" ],
    "like-of" : [ "https://twitter.com/chadtomkiss/status/1279425887339937792" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/f7xpg",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1279425887339937792" ],
      "url" : [ "https://twitter.com/chadtomkiss/status/1279425887339937792" ],
      "published" : [ "2020-07-04T14:44:36+00:00" ],
      "in-reply-to" : [ "https://twitter.com/alexellisuk/status/1279424232066371586" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:chadtomkiss" ],
          "numeric-id" : [ "33863158" ],
          "name" : [ "Chuck Tomkress" ],
          "nickname" : [ "chadtomkiss" ],
          "url" : [ "https://twitter.com/chadtomkiss", "http://chadtomkiss.co.uk/", "http://musicalchad.tumblr.com" ],
          "published" : [ "2009-04-21T09:56:33+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Manchester, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1092940499768479745/YBd5GYYc.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "github.com/openssl/openss…",
        "html" : "<a href=\"https://github.com/openssl/openssl/pull/12089\">github.com/openssl/openss…</a>\n<a class=\"u-mention\" href=\"https://twitter.com/alexellisuk\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/jm_stuff\"></a>"
      } ]
    }
  },
  "tags" : [ "diversity-and-inclusion" ],
  "client_id" : "https://indigenous.realize.be"
}
