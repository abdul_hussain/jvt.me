{
  "date" : "2020-07-15T23:15:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/mjg59/status/1283513060917837824" ],
    "name" : [ "Like of @mjg59's tweet" ],
    "published" : [ "2020-07-15T23:15:00+01:00" ],
    "category" : [ "twitter" ],
    "like-of" : [ "https://twitter.com/mjg59/status/1283513060917837824" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/g1ufi",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1283513060917837824" ],
      "url" : [ "https://twitter.com/mjg59/status/1283513060917837824" ],
      "published" : [ "2020-07-15T21:25:34+00:00" ],
      "in-reply-to" : [ "https://twitter.com/mjg59/status/1283512307281088514" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:mjg59" ],
          "numeric-id" : [ "229502009" ],
          "name" : [ "Matthew Garrett" ],
          "nickname" : [ "mjg59" ],
          "url" : [ "https://twitter.com/mjg59", "http://mjg59.dreamwidth.org" ],
          "published" : [ "2010-12-22T15:28:52+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Oakland, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/459378005689630720/4f7Dml5q.png" ]
        }
      } ],
      "content" : [ "Imagine being able to do *anything* on Twitter and deciding to run a scam that maybe ends up making you 100k" ]
    }
  },
  "tags" : [ "twitter" ],
  "client_id" : "https://indigenous.realize.be"
}
