{
  "date" : "2020-07-27T07:17:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/darylcecile/status/1287525584562794503" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1287634846827905024" ],
    "name" : [ "Reply to https://twitter.com/darylcecile/status/1287525584562794503" ],
    "published" : [ "2020-07-27T07:17:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "I've found using <span class=\"h-card\"><a class=\"u-url\" href=\"https://twitter.com/headspace\">@headspace</a></span>'s sleepcasts work well because they give you something to focus on that isn't your thoughts"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/07/j2xwe",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1287525584562794503" ],
      "url" : [ "https://twitter.com/darylcecile/status/1287525584562794503" ],
      "published" : [ "2020-07-26T23:09:55+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:darylcecile" ],
          "numeric-id" : [ "796971271124815873" ],
          "name" : [ "Daryl Cecile 🐼" ],
          "nickname" : [ "darylcecile" ],
          "url" : [ "https://twitter.com/darylcecile", "https://darylcecile.net", "http://brave.com/dar899" ],
          "published" : [ "2016-11-11T07:02:12+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1213106387930222592/BxIn2BV2.jpg" ]
        }
      } ],
      "content" : [ "Anyone got tips for when you need to sleep but your brain is like no? 😑" ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
