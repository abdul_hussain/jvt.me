{
  "date" : "2020-07-03T12:25:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/TatianaTMac/status/1278797376975560704" ],
    "name" : [ "Like of @TatianaTMac's tweet" ],
    "published" : [ "2020-07-03T12:25:00+01:00" ],
    "category" : [ "diversity-and-inclusion" ],
    "like-of" : [ "https://twitter.com/TatianaTMac/status/1278797376975560704" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/yzdpk",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1278797376975560704" ],
      "url" : [ "https://twitter.com/TatianaTMac/status/1278797376975560704" ],
      "published" : [ "2020-07-02T21:07:08+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:TatianaTMac" ],
          "numeric-id" : [ "1529722572" ],
          "name" : [ "Tatiana Mac" ],
          "nickname" : [ "TatianaTMac" ],
          "url" : [ "https://twitter.com/TatianaTMac", "https://www.tatianamac.com" ],
          "published" : [ "2013-06-19T04:59:25+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "she/they/hän 💖💛💙" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1266645648280924162/WHcwjW3d.jpg" ]
        }
      } ],
      "content" : [ "If you think tech is a meritocracy and you think all people are equal have an equal shot and equally likely to succeed, then **what, exactly,** is your explanation for the lack of representation of everyone but cis het white abled men and the occasional S or E Asian man?" ]
    }
  },
  "tags" : [ "diversity-and-inclusion" ],
  "client_id" : "https://indigenous.realize.be"
}
