{
  "date" : "2020-07-11T20:33:11.51Z",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://events.indieweb.org/2020/08/online-homebrew-website-club-nottingham-qZ0U2nBYUpZT" ],
    "name" : [ "RSVP yes to https://events.indieweb.org/2020/08/online-homebrew-website-club-nottingham-qZ0U2nBYUpZT" ],
    "published" : [ "2020-07-11T20:33:11.51Z" ],
    "event" : {
      "start" : [ "2020-08-05T17:30:00+01:00" ],
      "name" : [ "ONLINE: Homebrew Website Club: Nottingham" ],
      "end" : [ "2020-08-05T19:30:00+01:00" ],
      "location" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "name" : [ "Online" ],
          "latitude" : [ "52.9509448" ],
          "longitude" : [ "-1.1522525" ]
        },
        "lang" : "en",
        "value" : "Online"
      } ],
      "url" : [ "https://events.indieweb.org/2020/08/online-homebrew-website-club-nottingham-qZ0U2nBYUpZT" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/07/kbbhj",
  "client_id" : "https://micropublish.net"
}
