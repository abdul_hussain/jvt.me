{
  "date" : "2020-07-03T18:25:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/SFF_Writer_Dan/status/1278821676214620160" ],
    "name" : [ "Like of @SFF_Writer_Dan's tweet" ],
    "published" : [ "2020-07-03T18:25:00+01:00" ],
    "like-of" : [ "https://twitter.com/SFF_Writer_Dan/status/1278821676214620160" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/kyf70",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1278821676214620160" ],
      "url" : [ "https://twitter.com/SFF_Writer_Dan/status/1278821676214620160" ],
      "published" : [ "2020-07-02T22:43:41+00:00" ],
      "in-reply-to" : [ "https://twitter.com/RoAnnaSylver/status/1278793209993244672" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:SFF_Writer_Dan" ],
          "numeric-id" : [ "1400913127" ],
          "name" : [ "Strange Currencies" ],
          "nickname" : [ "SFF_Writer_Dan" ],
          "url" : [ "https://twitter.com/SFF_Writer_Dan", "http://www.strangecurrencies.org" ],
          "published" : [ "2013-05-03T22:44:08+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Canada" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1174405706881085440/A6kELit5.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Not sure I'll ever forgive him for making Will Smith say \"Fairy lives don't matter today\" as a cop, while he hassled a civilian.",
        "html" : "Not sure I'll ever forgive him for making Will Smith say \"Fairy lives don't matter today\" as a cop, while he hassled a civilian.\n<a class=\"u-mention\" href=\"https://twitter.com/RoAnnaSylver\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/ithayla\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
