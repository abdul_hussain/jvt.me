{
  "date" : "2020-07-07T16:34:08.86Z",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/wolframkriesing/status/1280534516201918466" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1280542578866294791" ],
    "name" : [ "Reply to https://twitter.com/wolframkriesing/status/1280534516201918466" ],
    "published" : [ "2020-07-07T16:34:08.86Z" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "Personally, I'm not sure if Webmentions do make sense in this case, because Webmentions are meant to be sent - and received - multiple times, from a given webpage, and not necessarily by you. Unless you add some sufficient de-duplication, it may still not work very well."
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/07/kd1wk",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1280534516201918466" ],
      "url" : [ "https://twitter.com/wolframkriesing/status/1280534516201918466" ],
      "published" : [ "2020-07-07T16:09:54+00:00" ],
      "in-reply-to" : [ "https://twitter.com/wolframkriesing/status/1280534218184052737" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:wolframkriesing" ],
          "numeric-id" : [ "15084958" ],
          "name" : [ "Wolfram ⪡JS-- HTML++ CSS++⪢ Kriesing" ],
          "nickname" : [ "wolframkriesing" ],
          "url" : [ "https://twitter.com/wolframkriesing", "https://picostitch.com" ],
          "published" : [ "2008-06-11T14:08:10+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Munich, Bavaria" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1189194552378216448/K1wpIbT4.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "or another example:\nA online/google spreadsheet gets updated, that triggers a site to rebuild.\n\nI am sure there are more interesting examples.\nAre webmentions a good idea, or is there some better technique?",
        "html" : "<div style=\"white-space: pre\">or another example:\nA online/google spreadsheet gets updated, that triggers a site to rebuild.\n\nI am sure there are more interesting examples.\nAre webmentions a good idea, or is there some better technique?</div>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://micropublish.net"
}
