{
  "date" : "2020-07-19T13:32:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JBYoder/status/1284332907356516352" ],
    "name" : [ "Like of @JBYoder's tweet" ],
    "published" : [ "2020-07-19T13:32:00+01:00" ],
    "category" : [ "star-trek" ],
    "like-of" : [ "https://twitter.com/JBYoder/status/1284332907356516352" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/bqkmv",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1284332907356516352" ],
      "url" : [ "https://twitter.com/JBYoder/status/1284332907356516352" ],
      "published" : [ "2020-07-18T03:43:21+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:JBYoder" ],
          "numeric-id" : [ "19984919" ],
          "name" : [ "Jeremy Yoder 🌱📈🏳️‍🌈🖖🏻" ],
          "nickname" : [ "JBYoder" ],
          "url" : [ "https://twitter.com/JBYoder", "http://jbyoder.org" ],
          "published" : [ "2009-02-03T17:18:26+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Los Angeles, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1232915600902475776/AGCyF8Ga.jpg" ]
        }
      } ],
      "content" : [ "Spotted something odd on my run through the park tonight and ... I guess I have good news and bad news" ],
      "photo" : [ "https://pbs.twimg.com/media/EdLd93FUYAMGwYs.jpg", "https://pbs.twimg.com/media/EdLd93FUwAE2L8l.jpg", "https://pbs.twimg.com/media/EdLd93GUYAAZr7H.jpg" ]
    }
  },
  "tags" : [ "star-trek" ],
  "client_id" : "https://indigenous.realize.be"
}
