{
  "date" : "2020-07-25T09:50:34.927Z",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/mr_goodwin/status/1286945025625128961" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1286963267525644288" ],
    "name" : [ "Reply to https://twitter.com/mr_goodwin/status/1286945025625128961" ],
    "published" : [ "2020-07-25T09:50:34.927Z" ],
    "category" : [ "personal-website", "homebrew-website-club" ],
    "content" : [ {
      "html" : "",
      "value" : "Definitely happy to chat more on Slack / at <a href=\"/tags/homebrew-website-club/\">#HomebrewWebsiteClub</a> if you're able to join, but my recommendation is generally Hugo. Templating / documentation is not the best, especially if you're not used to Go's templating. I moved from Jekyll last year https://www.jvt.me/posts/2019/01/04/goodbye-jekyll-hello-hugo/ and have found it a much better experience. It's super quick to build (even for my large multi-purpose site) and has a tonne of stuff built-in to it that makes it fairly batteries included"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/07/f6hko",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1286945025625128961" ],
      "url" : [ "https://twitter.com/mr_goodwin/status/1286945025625128961" ],
      "published" : [ "2020-07-25T08:42:59+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:mr_goodwin" ],
          "numeric-id" : [ "1481941" ],
          "name" : [ "Mark Goodwin" ],
          "nickname" : [ "mr_goodwin" ],
          "url" : [ "https://twitter.com/mr_goodwin" ],
          "published" : [ "2007-03-19T08:27:18+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottinghamshire, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/756072798603411457/SIyD6Fnf.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Alright twits, what static site generation tools do you use?\nNeed:\n- Markdown\n- Blog tool\nWant:\n- Simple / clean output\n- Custom paths",
        "html" : "<div style=\"white-space: pre\">Alright twits, what static site generation tools do you use?\nNeed:\n- Markdown\n- Blog tool\nWant:\n- Simple / clean output\n- Custom paths</div>"
      } ]
    }
  },
  "tags" : [ "personal-website", "homebrew-website-club" ],
  "client_id" : "https://micropublish.net"
}
