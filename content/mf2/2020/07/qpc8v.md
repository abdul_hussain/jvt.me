{
  "date" : "2020-07-28T11:53:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/MrsEmma/status/1288055802666852352" ],
    "name" : [ "Like of @MrsEmma's tweet" ],
    "published" : [ "2020-07-28T11:53:00+01:00" ],
    "like-of" : [ "https://twitter.com/MrsEmma/status/1288055802666852352" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/qpc8v",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1288055802666852352" ],
      "url" : [ "https://twitter.com/MrsEmma/status/1288055802666852352" ],
      "published" : [ "2020-07-28T10:16:48+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:MrsEmma" ],
          "numeric-id" : [ "19500955" ],
          "name" : [ "Emma Seward" ],
          "nickname" : [ "MrsEmma" ],
          "url" : [ "https://twitter.com/MrsEmma" ],
          "published" : [ "2009-01-25T19:32:42+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1283095562389270529/KPUUPzkt.jpg" ]
        }
      } ],
      "content" : [ "Well this is *lovely* post to receive 💜" ],
      "photo" : [ "https://pbs.twimg.com/media/EeAX62sXgAAwCyU.jpg", "https://pbs.twimg.com/media/EeAX7JkWAAEd6AV.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
