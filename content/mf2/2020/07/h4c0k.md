{
  "date" : "2020-07-11T00:31:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/_elletownsend/status/1281470749128237059" ],
    "name" : [ "Like of @_elletownsend's tweet" ],
    "published" : [ "2020-07-11T00:31:00+01:00" ],
    "like-of" : [ "https://twitter.com/_elletownsend/status/1281470749128237059" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/h4c0k",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1281470749128237059" ],
      "url" : [ "https://twitter.com/_elletownsend/status/1281470749128237059" ],
      "published" : [ "2020-07-10T06:10:09+00:00" ],
      "in-reply-to" : [ "https://twitter.com/JamieTanna/status/1281166228845928449" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:_elletownsend" ],
          "numeric-id" : [ "4308006796" ],
          "name" : [ "Elle Townsend ✨" ],
          "nickname" : [ "_elletownsend" ],
          "url" : [ "https://twitter.com/_elletownsend", "http://www.elletownsend.co.uk" ],
          "published" : [ "2015-11-28T14:28:32+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "England, United Kingdom" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1237757944089006086/67Kjy-LA.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Congratulations 🥳",
        "html" : "Congratulations 🥳\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
