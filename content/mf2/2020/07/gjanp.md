{
  "date" : "2020-07-30T16:30:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/elibelly/status/1288834098144911361" ],
    "name" : [ "Like of @elibelly's tweet" ],
    "published" : [ "2020-07-30T16:30:00+01:00" ],
    "category" : [ "monzo" ],
    "like-of" : [ "https://twitter.com/elibelly/status/1288834098144911361" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/gjanp",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1288834098144911361" ],
      "url" : [ "https://twitter.com/elibelly/status/1288834098144911361" ],
      "published" : [ "2020-07-30T13:49:29+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:elibelly" ],
          "numeric-id" : [ "19629753" ],
          "name" : [ "eli schutze" ],
          "nickname" : [ "elibelly" ],
          "url" : [ "https://twitter.com/elibelly", "https://monzo.me/eli" ],
          "published" : [ "2009-01-28T01:36:40+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1209626995233099777/pXbuB6mO.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Monzo employees are called Monzonauts\nNow the rest of us are officially Alumnauts! 😍",
        "html" : "<div style=\"white-space: pre\">Monzo employees are called Monzonauts\nNow the rest of us are officially Alumnauts! 😍</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EeLbt5jWkAAGEgd.png" ]
    }
  },
  "tags" : [ "monzo" ],
  "client_id" : "https://indigenous.realize.be"
}
