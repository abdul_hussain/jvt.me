{
  "date" : "2020-07-25T12:40:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/SassCSS/status/1286761746321956864" ],
    "name" : [ "Like of @SassCSS's tweet" ],
    "published" : [ "2020-07-25T12:40:00+01:00" ],
    "category" : [ "diversity-and-inclusion" ],
    "like-of" : [ "https://twitter.com/SassCSS/status/1286761746321956864" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/rbr7z",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1286761746321956864" ],
      "url" : [ "https://twitter.com/SassCSS/status/1286761746321956864" ],
      "published" : [ "2020-07-24T20:34:41+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:SassCSS" ],
          "numeric-id" : [ "1269955578" ],
          "name" : [ "Sass" ],
          "nickname" : [ "SassCSS" ],
          "url" : [ "https://twitter.com/SassCSS", "http://sass-lang.com" ],
          "published" : [ "2013-03-15T15:22:54+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/988861630443278336/STS1lqpc.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Technical work is inseparable from politics. When you come at us asking us not to take a political position, you're demanding that we abide by your politics of silence. We refuse to be silent in the face of injustice. github.com/sass/sass-site…",
        "html" : "Technical work is inseparable from politics. When you come at us asking us not to take a political position, you're demanding that we abide by your politics of silence. We refuse to be silent in the face of injustice. <a href=\"https://github.com/sass/sass-site/issues/461\">github.com/sass/sass-site…</a>"
      } ]
    }
  },
  "tags" : [ "diversity-and-inclusion" ],
  "client_id" : "https://indigenous.realize.be"
}
