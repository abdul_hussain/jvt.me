{
  "date" : "2020-07-04T15:30:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [  ],
    "name" : [ "Like of @CarolSaysThings's tweet" ],
    "published" : [ "2020-07-04T15:30:00+01:00" ],
    "like-of" : [ "https://twitter.com/CarolSaysThings/status/1279417205554384896" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/0hrtm",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1279417205554384896" ],
      "url" : [ "https://twitter.com/CarolSaysThings/status/1279417205554384896" ],
      "published" : [ "2020-07-04T14:10:06+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:CarolSaysThings" ],
          "numeric-id" : [ "36382927" ],
          "name" : [ "Carol 😅" ],
          "nickname" : [ "CarolSaysThings" ],
          "url" : [ "https://twitter.com/CarolSaysThings", "https://carolgilabert.me/" ],
          "published" : [ "2009-04-29T15:22:13+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "🇧🇷🇪🇸🇬🇧 · Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1238515159594917889/C5994QPa.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Happy birthday to the absolute legend @anna_hax 🥳🎈🎉🎁\n\nAlways thankful to be able to call you a friend 💛\n\nHope you have a lovely day 🙌",
        "html" : "<div style=\"white-space: pre\">Happy birthday to the absolute legend <a href=\"https://twitter.com/anna_hax\">@anna_hax</a> 🥳🎈🎉🎁\n\nAlways thankful to be able to call you a friend 💛\n\nHope you have a lovely day 🙌</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EcFnJ46XkAEaPIW.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
