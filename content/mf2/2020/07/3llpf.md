{
  "date" : "2020-07-04T21:44:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/jacobmparis/status/1279414406816940032" ],
    "name" : [ "Like of @jacobmparis's tweet" ],
    "published" : [ "2020-07-04T21:44:00+01:00" ],
    "like-of" : [ "https://twitter.com/jacobmparis/status/1279414406816940032" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/3llpf",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1279414406816940032" ],
      "url" : [ "https://twitter.com/jacobmparis/status/1279414406816940032" ],
      "published" : [ "2020-07-04T13:58:59+00:00" ],
      "in-reply-to" : [ "https://twitter.com/AreTillery/status/1279324228605677569" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:jacobmparis" ],
          "numeric-id" : [ "58564000" ],
          "name" : [ "Jacob Paris ✊" ],
          "nickname" : [ "jacobmparis" ],
          "url" : [ "https://twitter.com/jacobmparis", "https://www.jacobparis.com/" ],
          "published" : [ "2009-07-20T19:25:39+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Toronto 🇨🇦" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1180225661060964352/dChEdLLr.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "That pattern works for all perfect squares off by one \n\n10 * 10 = 100\n9 * 11 = 100 - 1\n\n16 * 16 = 256\n15 * 17 = 256 - 1\n\n25 * 25 = 625\n24 * 26 = 625 - 1",
        "html" : "<div style=\"white-space: pre\">That pattern works for all perfect squares off by one \n\n10 * 10 = 100\n9 * 11 = 100 - 1\n\n16 * 16 = 256\n15 * 17 = 256 - 1\n\n25 * 25 = 625\n24 * 26 = 625 - 1</div>\n<a class=\"u-mention\" href=\"https://twitter.com/1mentat\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/AreTillery\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
