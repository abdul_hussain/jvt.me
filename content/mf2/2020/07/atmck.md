{
  "date" : "2020-07-19T13:30:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/katie_fenn/status/1284821089131143169" ],
    "name" : [ "Like of @katie_fenn's tweet" ],
    "published" : [ "2020-07-19T13:30:00+01:00" ],
    "like-of" : [ "https://twitter.com/katie_fenn/status/1284821089131143169" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/atmck",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1284821089131143169" ],
      "url" : [ "https://twitter.com/katie_fenn/status/1284821089131143169" ],
      "published" : [ "2020-07-19T12:03:13+00:00" ],
      "in-reply-to" : [ "https://twitter.com/katie_fenn/status/1284820886382686208" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:katie_fenn" ],
          "numeric-id" : [ "14979616" ],
          "name" : [ "Katie Fenn" ],
          "nickname" : [ "katie_fenn" ],
          "url" : [ "https://twitter.com/katie_fenn" ],
          "published" : [ "2008-06-02T12:02:44+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Sheffield" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1094575150517891072/sGJJqyzP.jpg" ]
        }
      } ],
      "content" : [ "This tweet is brought to you by \"yes I'm redesigning my website for the seventh time\"" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
