{
  "date" : "2020-07-14T08:17:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/technottingham/status/1282926352723783680" ],
    "name" : [ "Like of @technottingham's tweet" ],
    "published" : [ "2020-07-14T08:17:00+01:00" ],
    "category" : [ "tech-nottingham" ],
    "like-of" : [ "https://twitter.com/technottingham/status/1282926352723783680" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/bbfno",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1282926352723783680" ],
      "url" : [ "https://twitter.com/technottingham/status/1282926352723783680" ],
      "published" : [ "2020-07-14T06:34:12+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:technottingham" ],
          "numeric-id" : [ "384492431" ],
          "name" : [ "Tech Nottingham" ],
          "nickname" : [ "technottingham" ],
          "url" : [ "https://twitter.com/technottingham", "http://technottingham.com" ],
          "published" : [ "2011-10-03T19:47:31+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1023974499757293570/ZoPc_QsO.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "It was lovely to see all your smiling faces last night 💖\n\nWe had a blast and hope you did too, see you next time 🙌🏼",
        "html" : "<div style=\"white-space: pre\">It was lovely to see all your smiling faces last night 💖\n\nWe had a blast and hope you did too, see you next time 🙌🏼</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/Ec3eNO8WoAEbWu2.png" ]
    }
  },
  "tags" : [ "tech-nottingham" ],
  "client_id" : "https://indigenous.realize.be"
}
