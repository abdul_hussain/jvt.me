{
  "date" : "2020-07-20T22:29:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamieTanna/status/1285329163478618117" ],
    "published" : [ "2020-07-20T22:29:00+01:00" ],
    "repost-of" : [ "https://twitter.com/hacktoberfest/status/1285314733726367744" ],
    "category" : [ "hacktoberfest" ]
  },
  "kind" : "reposts",
  "slug" : "2020/07/pzxlb",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1285314733726367744" ],
      "url" : [ "https://twitter.com/hacktoberfest/status/1285314733726367744" ],
      "published" : [ "2020-07-20T20:44:47+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:hacktoberfest" ],
          "numeric-id" : [ "1040669393255055360" ],
          "name" : [ "Hacktoberfest" ],
          "nickname" : [ "hacktoberfest" ],
          "url" : [ "https://twitter.com/hacktoberfest", "https://hacktoberfest.digitalocean.com/" ],
          "published" : [ "2018-09-14T18:31:32+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Worldwide" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1285314653913059336/aKqLm3rw.png" ]
        }
      } ],
      "content" : [ {
        "value" : "Hello, is this thing on? 👋 We wanted to let you know that #Hacktoberfest2020 is coming up! 💙 Grab a sneak peek of what’s to come on our site: hacktoberfest.digitalocean.com 🔗",
        "html" : "Hello, is this thing on? 👋 We wanted to let you know that <a href=\"https://twitter.com/search?q=%23Hacktoberfest2020\">#Hacktoberfest2020</a> is coming up! 💙 Grab a sneak peek of what’s to come on our site: <a href=\"https://hacktoberfest.digitalocean.com\">hacktoberfest.digitalocean.com</a> 🔗"
      } ]
    }
  },
  "tags" : [ "hacktoberfest" ],
  "client_id" : "https://indigenous.realize.be"
}
