{
  "date" : "2020-07-11T22:46:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/MrAndrew/status/1282058010374942720" ],
    "name" : [ "Like of @MrAndrew's tweet" ],
    "published" : [ "2020-07-11T22:46:00+01:00" ],
    "category" : [ "coronavirus", "marvel" ],
    "like-of" : [ "https://twitter.com/MrAndrew/status/1282058010374942720" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/mhp9m",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1282058010374942720" ],
      "url" : [ "https://twitter.com/MrAndrew/status/1282058010374942720" ],
      "published" : [ "2020-07-11T21:03:43+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:MrAndrew" ],
          "numeric-id" : [ "9626182" ],
          "name" : [ "Andrew Seward" ],
          "nickname" : [ "MrAndrew" ],
          "url" : [ "https://twitter.com/MrAndrew", "http://www.technottingham.com" ],
          "published" : [ "2007-10-23T15:57:18+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Riddings, Derbyshire" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1258160750641606657/CR5g9eOt.jpg" ]
        }
      } ],
      "content" : [ "Watching Avengers Endgame, seeing a world where people are struggling to deal with a worldwide catastrophe, it might be the closest thing we have to a film that depicts a post-Covid world" ],
      "photo" : [ "https://pbs.twimg.com/media/EcrI9nhXsAARojm.jpg", "https://pbs.twimg.com/media/EcrI9xaWkAE6gXf.jpg" ]
    }
  },
  "tags" : [ "coronavirus", "marvel" ],
  "client_id" : "https://indigenous.realize.be"
}
