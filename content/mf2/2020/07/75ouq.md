{
  "date" : "2020-07-04T11:26:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/heyscrape/status/1279196483254968321" ],
    "name" : [ "Like of @heyscrape's tweet" ],
    "published" : [ "2020-07-04T11:26:00+01:00" ],
    "like-of" : [ "https://twitter.com/heyscrape/status/1279196483254968321" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/75ouq",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1279196483254968321" ],
      "url" : [ "https://twitter.com/heyscrape/status/1279196483254968321" ],
      "video" : [ "https://video.twimg.com/tweet_video/EcCeahPXsAM3pMp.mp4" ],
      "published" : [ "2020-07-03T23:33:02+00:00" ],
      "in-reply-to" : [ "https://twitter.com/EricIdle/status/1279186752289165312" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:heyscrape" ],
          "numeric-id" : [ "3001352877" ],
          "name" : [ "Casey Scraper PT, DPT" ],
          "nickname" : [ "heyscrape" ],
          "url" : [ "https://twitter.com/heyscrape" ],
          "published" : [ "2015-01-27T13:46:52+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Owasso, OK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1261121380738830336/fIfhGF9u.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "The only time Sir Robin has been a good role model!",
        "html" : "The only time Sir Robin has been a good role model!\n<a class=\"u-mention\" href=\"https://twitter.com/EricIdle\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
