{
  "date" : "2020-07-03T12:24:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/AriVanider/status/1278993722353557516" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1279015228550807554" ],
    "name" : [ "Reply to https://twitter.com/AriVanider/status/1278993722353557516" ],
    "published" : [ "2020-07-03T12:24:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "Nice, I can make it more clear that I use <span class=\"h-card\"><a class=\"u-url\" href=\"https://twitter.com/GitLab\">@GitLab</a></span> for source control hosting 😁"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/07/cdjod",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1278993722353557516" ],
      "url" : [ "https://twitter.com/AriVanider/status/1278993722353557516" ],
      "published" : [ "2020-07-03T10:07:20+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:AriVanider" ],
          "numeric-id" : [ "346728386" ],
          "name" : [ "Ari V | Black Lives Matter" ],
          "nickname" : [ "AriVanider" ],
          "url" : [ "https://twitter.com/AriVanider", "http://ariv.se", "http://pronoun.is/they" ],
          "published" : [ "2011-08-01T19:01:31+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Stockholm 🏳️‍🌈 they/them" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1197277291329536012/jfeIU8Te.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "New GitHub feature: profile README!\n\nThis is great! Could probably replace personal websites for a lot of folks, and would be an awesome place to ask for sponsorship/funding.\n\nYou can check it out on my profile: github.com/arirawr\n\n(But also @GitHub, drop ICE.)",
        "html" : "<div style=\"white-space: pre\">New GitHub feature: profile README!\n\nThis is great! Could probably replace personal websites for a lot of folks, and would be an awesome place to ask for sponsorship/funding.\n\nYou can check it out on my profile: <a href=\"https://github.com/arirawr\">github.com/arirawr</a>\n\n(But also <a href=\"https://twitter.com/github\">@GitHub</a>, drop ICE.)</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/Eb_loOYWsAAh-CT.jpg", "https://pbs.twimg.com/media/Eb_lwp5XgAgdD5h.jpg" ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
