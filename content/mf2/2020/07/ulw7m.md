{
  "date" : "2020-07-09T22:51:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/houmanB/status/1259928335569817602" ],
    "name" : [ "Like of @houmanB's tweet" ],
    "published" : [ "2020-07-09T22:51:00+01:00" ],
    "category" : [ "personal-website" ],
    "like-of" : [ "https://twitter.com/houmanB/status/1259928335569817602" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/ulw7m",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1259928335569817602" ],
      "url" : [ "https://twitter.com/houmanB/status/1259928335569817602" ],
      "published" : [ "2020-05-11T19:28:18+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:houmanB" ],
          "numeric-id" : [ "306982106" ],
          "name" : [ "Houman Brinj" ],
          "nickname" : [ "houmanB" ],
          "url" : [ "https://twitter.com/houmanB", "http://houman.codes" ],
          "published" : [ "2011-05-28T20:03:22+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1191266778128822272/6XkbHPKB.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "All this isolation has meant I have been able to focus working on some other things I enjoy. I'm working on a little learning project with SwiftUI and I'm going to be documenting things that are useful to know, like ViewBuilders, here: houman.codes/posts/swiftui-…",
        "html" : "All this isolation has meant I have been able to focus working on some other things I enjoy. I'm working on a little learning project with SwiftUI and I'm going to be documenting things that are useful to know, like ViewBuilders, here: <a href=\"https://houman.codes/posts/swiftui-viewbuilders/\">houman.codes/posts/swiftui-…</a>"
      } ]
    }
  },
  "tags" : [ "personal-website" ],
  "client_id" : "https://indigenous.realize.be"
}
