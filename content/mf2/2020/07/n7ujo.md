{
  "date" : "2020-07-27T21:06:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/__katiefenton/status/1287402109957275650" ],
    "name" : [ "Like of @__katiefenton's tweet" ],
    "published" : [ "2020-07-27T21:06:00+01:00" ],
    "like-of" : [ "https://twitter.com/__katiefenton/status/1287402109957275650" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/n7ujo",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1287402109957275650" ],
      "url" : [ "https://twitter.com/__katiefenton/status/1287402109957275650" ],
      "published" : [ "2020-07-26T14:59:16+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:__katiefenton" ],
          "numeric-id" : [ "782970230654005248" ],
          "name" : [ "Katie Fenton" ],
          "nickname" : [ "__katiefenton" ],
          "url" : [ "https://twitter.com/__katiefenton" ],
          "published" : [ "2016-10-03T15:47:04+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Cardiff, Wales" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1222619614376493064/VVqZwHNR.jpg" ]
        }
      } ],
      "content" : [ "Thank you to my postman for reminding me I’ll never be the green-fingered goddess I aspire to be 🤦🏼‍♀️" ],
      "photo" : [ "https://pbs.twimg.com/media/Ed3FZDIWsAATTHF.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
