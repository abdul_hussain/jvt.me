{
  "date" : "2020-07-13T19:51:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "name" : [ "Like of @EmilyKager's tweet" ],
    "published" : [ "2020-07-13T19:51:00+01:00" ],
    "like-of" : [ "https://twitter.com/EmilyKager/status/1282719559900946438" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/hwped",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1282719559900946438" ],
      "url" : [ "https://twitter.com/EmilyKager/status/1282719559900946438" ],
      "published" : [ "2020-07-13T16:52:29+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:EmilyKager" ],
          "numeric-id" : [ "2347475803" ],
          "name" : [ "Emily Kager" ],
          "nickname" : [ "EmilyKager" ],
          "url" : [ "https://twitter.com/EmilyKager", "https://www.emilykager.com/" ],
          "published" : [ "2014-02-16T21:34:12+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Oakland and SF " ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1260968235408605184/XezL8Z3r.jpg" ]
        }
      } ],
      "content" : [ "From the historical archives: the last time I put on real clothes, May 2020" ],
      "photo" : [ "https://pbs.twimg.com/media/Ec0ioNkWkAQrSyH.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
