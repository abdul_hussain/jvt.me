{
  "date" : "2020-07-27T12:19:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/rwdrich/status/1287707175838785537" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1287710374138175488" ],
    "name" : [ "Reply to https://twitter.com/rwdrich/status/1287707175838785537" ],
    "published" : [ "2020-07-27T12:19:00+01:00" ],
    "category" : [ "code-style" ],
    "content" : [ {
      "html" : "",
      "value" : "Enforced consistency is my vote - otherwise you have lots of cases where folks need to manually change it to make it nicer, which inevitably get unknowingly reverted in the future"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/07/lkekm",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1287707175838785537" ],
      "url" : [ "https://twitter.com/rwdrich/status/1287707175838785537" ],
      "published" : [ "2020-07-27T11:11:29+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:rwdrich" ],
          "numeric-id" : [ "107548386" ],
          "name" : [ "Richard Davies" ],
          "nickname" : [ "rwdrich" ],
          "url" : [ "https://twitter.com/rwdrich", "http://rwdrich.co.uk" ],
          "published" : [ "2010-01-22T23:14:30+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Cambridge, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/917481919872491521/BwypzjEE.jpg" ]
        }
      } ],
      "content" : [ "What's better for a code base with regards to formatting, readability or consistency?" ]
    }
  },
  "tags" : [ "code-style" ],
  "client_id" : "https://indigenous.realize.be"
}
