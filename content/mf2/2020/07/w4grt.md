{
  "date" : "2020-07-11T20:35:34.303Z",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://events.indieweb.org/2020/08/online-homebrew-website-club-nottingham-yPtwtlpRRU0p" ],
    "name" : [ "RSVP yes to https://events.indieweb.org/2020/08/online-homebrew-website-club-nottingham-yPtwtlpRRU0p" ],
    "published" : [ "2020-07-11T20:35:34.303Z" ],
    "event" : {
      "start" : [ "2020-08-19T17:30:00+01:00" ],
      "name" : [ "ONLINE: Homebrew Website Club: Nottingham" ],
      "end" : [ "2020-08-19T19:30:00+01:00" ],
      "location" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "name" : [ "Online" ],
          "latitude" : [ "52.9509448" ],
          "longitude" : [ "-1.1522525" ]
        },
        "lang" : "en",
        "value" : "Online"
      } ],
      "url" : [ "https://events.indieweb.org/2020/08/online-homebrew-website-club-nottingham-yPtwtlpRRU0p" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/07/w4grt",
  "client_id" : "https://micropublish.net"
}
