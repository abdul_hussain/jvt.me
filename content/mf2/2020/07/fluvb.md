{
  "date" : "2020-07-22T11:19:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamesSACorey/status/1285857568988921856" ],
    "name" : [ "Like of @JamesSACorey's tweet" ],
    "published" : [ "2020-07-22T11:19:00+01:00" ],
    "category" : [ "the-expanse" ],
    "like-of" : [ "https://twitter.com/JamesSACorey/status/1285857568988921856" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/fluvb",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1285857568988921856" ],
      "url" : [ "https://twitter.com/JamesSACorey/status/1285857568988921856" ],
      "published" : [ "2020-07-22T08:41:49+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:JamesSACorey" ],
          "numeric-id" : [ "550270216" ],
          "name" : [ "James S.A. Corey" ],
          "nickname" : [ "JamesSACorey" ],
          "url" : [ "https://twitter.com/JamesSACorey", "http://www.the-expanse.com" ],
          "published" : [ "2012-04-10T16:59:23+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Rocinante" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1029097103333543936/H4TP9cxr.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I assume everyone in the UK is as broke as everyone in the US is right now, so here is an inexpensive thing if you like things but don't want to spend a lot of your paper money with queens on it and stuff.\n\namazon.co.uk/Leviathan-Wake…",
        "html" : "<div style=\"white-space: pre\">I assume everyone in the UK is as broke as everyone in the US is right now, so here is an inexpensive thing if you like things but don't want to spend a lot of your paper money with queens on it and stuff.\n\n<a href=\"https://www.amazon.co.uk/Leviathan-Wakes-Expanse-major-Netflix-ebook/dp/B004XCGKYQ/ref=sr_1_1?keywords=leviathan+wakes&qid=1595406402&sr=8-1\">amazon.co.uk/Leviathan-Wake…</a></div>"
      } ]
    }
  },
  "tags" : [ "the-expanse" ],
  "client_id" : "https://indigenous.realize.be"
}
