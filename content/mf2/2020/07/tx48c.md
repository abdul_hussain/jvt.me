{
  "date" : "2020-07-08T22:13:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/davidhoang/status/1280867525480202242" ],
    "name" : [ "Like of @davidhoang's tweet" ],
    "published" : [ "2020-07-08T22:13:00+01:00" ],
    "like-of" : [ "https://twitter.com/davidhoang/status/1280867525480202242" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/tx48c",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1280867525480202242" ],
      "url" : [ "https://twitter.com/davidhoang/status/1280867525480202242" ],
      "published" : [ "2020-07-08T14:13:10+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:davidhoang" ],
          "numeric-id" : [ "14338572" ],
          "name" : [ "David Hoang" ],
          "nickname" : [ "davidhoang" ],
          "url" : [ "https://twitter.com/davidhoang", "http://davidhoang.com" ],
          "published" : [ "2008-04-09T03:17:33+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Santa Monica" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1248438525877350400/4ytt99Ba.jpg" ]
        }
      } ],
      "location" : [ {
        "type" : [ "h-card", "p-location" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:59612bd882018c51" ],
          "name" : [ "Santa Monica, CA" ]
        }
      } ],
      "content" : [ "My back has so much tech debt." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
