{
  "date" : "2020-07-09T22:24:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/paulienuh/status/1281303024737103872" ],
    "name" : [ "Like of @paulienuh's tweet" ],
    "published" : [ "2020-07-09T22:24:00+01:00" ],
    "category" : [ "coronavirus" ],
    "like-of" : [ "https://twitter.com/paulienuh/status/1281303024737103872" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/tznmn",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1281303024737103872" ],
      "url" : [ "https://twitter.com/paulienuh/status/1281303024737103872" ],
      "published" : [ "2020-07-09T19:03:41+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:paulienuh" ],
          "numeric-id" : [ "230185453" ],
          "name" : [ "Pauline P. Narvas➖pawlean.com" ],
          "nickname" : [ "paulienuh" ],
          "url" : [ "https://twitter.com/paulienuh", "https://pawlean.com" ],
          "published" : [ "2010-12-24T14:46:00+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Leeds, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1265398650395639813/PfESApun.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Mixed feelings about the gym re-opening on the 25th of July.\n\nI’ve missed going to the gym and lifting heavy, but the thought of going back — maybe with my ‘new’ excessive paranoia — and having people share the same closed up air as me is just 😷",
        "html" : "<div style=\"white-space: pre\">Mixed feelings about the gym re-opening on the 25th of July.\n\nI’ve missed going to the gym and lifting heavy, but the thought of going back — maybe with my ‘new’ excessive paranoia — and having people share the same closed up air as me is just 😷</div>"
      } ]
    }
  },
  "tags" : [ "coronavirus" ],
  "client_id" : "https://indigenous.realize.be"
}
