{
  "date" : "2020-07-08T18:27:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "name" : [ "Like of @abbyfuller's tweet" ],
    "published" : [ "2020-07-08T18:27:00+01:00" ],
    "like-of" : [ "https://twitter.com/abbyfuller/status/1280627279077359617" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/9vt3g",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1280627279077359617" ],
      "url" : [ "https://twitter.com/abbyfuller/status/1280627279077359617" ],
      "published" : [ "2020-07-07T22:18:30+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:abbyfuller" ],
          "numeric-id" : [ "51848919" ],
          "name" : [ "Abby Fuller" ],
          "nickname" : [ "abbyfuller" ],
          "url" : [ "https://twitter.com/abbyfuller", "https://github.com/aws/containers-roadmap/projects/1" ],
          "published" : [ "2009-06-28T21:25:20+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Seattle" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1280340557957488640/fc0h6--T.jpg" ]
        }
      } ],
      "content" : [ "some personal news: still not looking for advice from internet strangers." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
