{
  "date" : "2020-07-25T12:37:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/iamhirusi/status/1286977466955739136" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1286990485081542656" ],
    "name" : [ "Reply to https://twitter.com/iamhirusi/status/1286977466955739136" ],
    "published" : [ "2020-07-25T12:37:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "Nice I hope to see you there. If not, did you want me to bring up https://github.com/w3c/Micropub/issues/118 ? (I keep meaning to reply to it, too)"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/07/vnaot",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1286977466955739136" ],
      "url" : [ "https://twitter.com/iamhirusi/status/1286977466955739136" ],
      "published" : [ "2020-07-25T10:51:53+00:00" ],
      "in-reply-to" : [ "https://twitter.com/JamieTanna/status/1286964247717609474" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:iamhirusi" ],
          "numeric-id" : [ "1252888172700119041" ],
          "name" : [ "Ru Singh" ],
          "nickname" : [ "iamhirusi" ],
          "url" : [ "https://twitter.com/iamhirusi", "https://rusingh.com" ],
          "published" : [ "2020-04-22T09:14:03+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "India" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1258445925145591814/b0T5aa6E.png" ]
        }
      } ],
      "content" : [ {
        "value" : "Had totally forgotten! Thanks for the reminder, will try to make it!",
        "html" : "Had totally forgotten! Thanks for the reminder, will try to make it!\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/JamieTanna\"></a>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
