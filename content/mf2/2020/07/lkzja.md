{
  "date" : "2020-07-11T15:40:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/CMWAshby/status/1281687221238718464" ],
    "name" : [ "Like of @CMWAshby's tweet" ],
    "published" : [ "2020-07-11T15:40:00+01:00" ],
    "like-of" : [ "https://twitter.com/CMWAshby/status/1281687221238718464" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/lkzja",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1281687221238718464" ],
      "url" : [ "https://twitter.com/CMWAshby/status/1281687221238718464" ],
      "published" : [ "2020-07-10T20:30:20+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:CMWAshby" ],
          "numeric-id" : [ "3955170161" ],
          "name" : [ "Charlie Ashby" ],
          "nickname" : [ "CMWAshby" ],
          "url" : [ "https://twitter.com/CMWAshby", "https://muckrack.com/charlie-ashby" ],
          "published" : [ "2015-10-14T10:55:25+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "United Kingdom" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1267237958396059649/HhIw7WL1.jpg" ]
        }
      } ],
      "content" : [ "Jason Isaacs was on The One Show yesterday, and I LOVE his background displays 😂" ],
      "photo" : [ "https://pbs.twimg.com/media/Ecl3u3gWoAcNTFR.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
