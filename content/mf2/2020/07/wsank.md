{
  "date" : "2020-07-03T10:31:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/holly/status/1278773438119903232" ],
    "name" : [ "Like of @holly's tweet" ],
    "published" : [ "2020-07-03T10:31:00+01:00" ],
    "like-of" : [ "https://twitter.com/holly/status/1278773438119903232" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/wsank",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1278773438119903232" ],
      "url" : [ "https://twitter.com/holly/status/1278773438119903232" ],
      "published" : [ "2020-07-02T19:32:00+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:holly" ],
          "numeric-id" : [ "7555262" ],
          "name" : [ "Holly Brockwell" ],
          "nickname" : [ "holly" ],
          "url" : [ "https://twitter.com/holly", "https://www.instagram.com/hollybrocks/" ],
          "published" : [ "2007-07-18T10:27:16+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "From Nottingham, in London" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1248671410714804225/A_9KJ1y8.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Read this with interest and have emailed @TransClinicUK to offer to be a guinea pig. I don't need my womb, if a trans person would like to have a baby and I can help, I'm happy to\ncosmopolitan.com/uk/body/health…",
        "html" : "<div style=\"white-space: pre\">Read this with interest and have emailed <a href=\"https://twitter.com/TransClinicUK\">@TransClinicUK</a> to offer to be a guinea pig. I don't need my womb, if a trans person would like to have a baby and I can help, I'm happy to\n<a href=\"https://www.cosmopolitan.com/uk/body/health/a33020181/womb-transplant/\">cosmopolitan.com/uk/body/health…</a></div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
