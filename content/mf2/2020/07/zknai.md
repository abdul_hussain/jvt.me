{
  "date" : "2020-07-05T23:49:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [  ],
    "name" : [ "Like of @rizbizkits's tweet" ],
    "published" : [ "2020-07-05T23:49:00+01:00" ],
    "category" : [ "hamilton" ],
    "like-of" : [ "https://twitter.com/rizbizkits/status/1279902390532046855" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/zknai",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1279902390532046855" ],
      "url" : [ "https://twitter.com/rizbizkits/status/1279902390532046855" ],
      "video" : [ "https://video.twimg.com/tweet_video/EcMgb2LXQAA8klm.mp4" ],
      "published" : [ "2020-07-05T22:18:03+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:rizbizkits" ],
          "numeric-id" : [ "800085538954878976" ],
          "name" : [ "riz" ],
          "nickname" : [ "rizbizkits" ],
          "url" : [ "https://twitter.com/rizbizkits", "https://www.rizwanakhan.com" ],
          "published" : [ "2016-11-19T21:17:12+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1279903682012774402/G8yaFDCD.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "hi, cheeky reminder: immigrants, we get the job done ✨\n\nover and out ✌",
        "html" : "<div style=\"white-space: pre\">hi, cheeky reminder: immigrants, we get the job done ✨\n\nover and out ✌</div>"
      } ]
    }
  },
  "tags" : [ "hamilton" ],
  "client_id" : "https://indigenous.realize.be"
}
