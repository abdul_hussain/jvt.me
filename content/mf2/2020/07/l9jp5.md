{
  "date" : "2020-07-05T01:12:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/Loftio/status/1279544051268751361" ],
    "name" : [ "Like of @Loftio's tweet" ],
    "published" : [ "2020-07-05T01:12:00+01:00" ],
    "like-of" : [ "https://twitter.com/Loftio/status/1279544051268751361" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/l9jp5",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1279544051268751361" ],
      "url" : [ "https://twitter.com/Loftio/status/1279544051268751361" ],
      "video" : [ "https://video.twimg.com/tweet_video/EcHahzfXgAEer0Z.mp4" ],
      "published" : [ "2020-07-04T22:34:09+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Loftio" ],
          "numeric-id" : [ "19788371" ],
          "name" : [ "Lex Lofthouse ✨" ],
          "nickname" : [ "Loftio" ],
          "url" : [ "https://twitter.com/Loftio", "http://loftio.co.uk" ],
          "published" : [ "2009-01-30T21:03:02+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1138757469176508417/Hc1FJP7a.png" ]
        }
      } ],
      "content" : [ "Just watched The Stolen Earth and Journey's End back to back. So many moments that make me well up, but none more than watching Bernard Cribbins in those last minutes... 😭" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
