{
  "date" : "2020-07-03T11:56:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [  ],
    "name" : [ "Like of @melteddali's tweet" ],
    "published" : [ "2020-07-03T11:56:00+01:00" ],
    "like-of" : [ "https://twitter.com/melteddali/status/1278901607762182144" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/09yaw",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1278901607762182144" ],
      "url" : [ "https://twitter.com/melteddali/status/1278901607762182144" ],
      "published" : [ "2020-07-03T04:01:18+00:00" ],
      "in-reply-to" : [ "https://twitter.com/the_odd1_in/status/1278888484741230594" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:melteddali" ],
          "numeric-id" : [ "2392881661" ],
          "name" : [ "Screaming Donna" ],
          "nickname" : [ "melteddali" ],
          "url" : [ "https://twitter.com/melteddali" ],
          "published" : [ "2014-03-16T15:21:42+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Ohio, USA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1226215018947018753/Hf_oFrR_.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Serena won a grand slam while that baby was still gestating. Kind of poetic to see her playing tennis now as a little kid",
        "html" : "Serena won a grand slam while that baby was still gestating. Kind of poetic to see her playing tennis now as a little kid\n<a class=\"u-mention\" href=\"https://twitter.com/JNealOnTrumpet\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/the_odd1_in\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/zeebruhhhh\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
