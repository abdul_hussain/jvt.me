{
  "date" : "2020-07-01T23:12:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/whoisgraham/status/1278448146167869440" ],
    "name" : [ "Like of @whoisgraham's tweet" ],
    "published" : [ "2020-07-01T23:12:00+01:00" ],
    "like-of" : [ "https://twitter.com/whoisgraham/status/1278448146167869440" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/fngeb",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1278448146167869440" ],
      "url" : [ "https://twitter.com/whoisgraham/status/1278448146167869440" ],
      "published" : [ "2020-07-01T21:59:25+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:whoisgraham" ],
          "numeric-id" : [ "133487462" ],
          "name" : [ "Graham : A Star Wars Story" ],
          "nickname" : [ "whoisgraham" ],
          "url" : [ "https://twitter.com/whoisgraham" ],
          "published" : [ "2010-04-15T23:03:34+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/2907221110/712dfcecc7fd591915d1355bdbe4adcd.jpeg" ]
        }
      } ],
      "content" : [ "Passed probation today 😄🎉" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
