{
  "date" : "2020-07-23T11:58:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/NadiaWhittomeMP/status/1285946905084727296" ],
    "name" : [ "Like of @NadiaWhittomeMP's tweet" ],
    "published" : [ "2020-07-23T11:58:00+01:00" ],
    "like-of" : [ "https://twitter.com/NadiaWhittomeMP/status/1285946905084727296" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/w41un",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1285946905084727296" ],
      "url" : [ "https://twitter.com/NadiaWhittomeMP/status/1285946905084727296" ],
      "video" : [ "https://video.twimg.com/ext_tw_video/1285946836172316675/pu/vid/720x720/aftl6YTh9RTnN7tf.mp4?tag=10" ],
      "published" : [ "2020-07-22T14:36:48+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:NadiaWhittomeMP" ],
          "numeric-id" : [ "576682647" ],
          "name" : [ "Nadia Whittome MP" ],
          "nickname" : [ "NadiaWhittomeMP" ],
          "url" : [ "https://twitter.com/NadiaWhittomeMP", "http://instagram.com/nadiawhittomemp" ],
          "published" : [ "2012-05-10T21:49:17+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1229722962594553857/js8m1Cn1.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Attacks towards trans people are up 40% on last year.\n\nI challenged Liz Truss today to stop fanning the flames of populist hate and publish the GRA consultation.\n\nI'm pleased her response was positive and will be keeping a close eye on what action follows.",
        "html" : "<div style=\"white-space: pre\">Attacks towards trans people are up 40% on last year.\n\nI challenged Liz Truss today to stop fanning the flames of populist hate and publish the GRA consultation.\n\nI'm pleased her response was positive and will be keeping a close eye on what action follows.</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
