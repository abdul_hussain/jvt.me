{
  "date" : "2020-07-30T11:14:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamieTanna/status/1288781911666708480" ],
    "published" : [ "2020-07-30T11:14:00+01:00" ],
    "repost-of" : [ "https://twitter.com/lunasorcery/status/1285704970814332935" ]
  },
  "kind" : "reposts",
  "slug" : "2020/07/mcyq6",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1285704970814332935" ],
      "url" : [ "https://twitter.com/lunasorcery/status/1285704970814332935" ],
      "published" : [ "2020-07-21T22:35:26+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:lunasorcery" ],
          "numeric-id" : [ "29508496" ],
          "name" : [ "✨ Luna 🌙" ],
          "nickname" : [ "lunasorcery" ],
          "url" : [ "https://twitter.com/lunasorcery", "http://twitch.tv/lunasorcery" ],
          "published" : [ "2009-04-07T18:27:55+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "The Moon" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1158068782235049984/le5HvTWF.jpg" ]
        }
      } ],
      "content" : [ "one of the hardest problems in computer science is eradicating the elitist gatekeeping attitudes" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
