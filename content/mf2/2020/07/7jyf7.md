{
  "date" : "2020-07-10T10:05:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/CarolSaysThings/status/1281499603393748994" ],
    "name" : [ "Like of @CarolSaysThings's tweet" ],
    "published" : [ "2020-07-10T10:05:00+01:00" ],
    "like-of" : [ "https://twitter.com/CarolSaysThings/status/1281499603393748994" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/7jyf7",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1281499603393748994" ],
      "url" : [ "https://twitter.com/CarolSaysThings/status/1281499603393748994" ],
      "published" : [ "2020-07-10T08:04:49+00:00" ],
      "in-reply-to" : [ "https://twitter.com/JamieTanna/status/1281499145124159490" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:CarolSaysThings" ],
          "numeric-id" : [ "36382927" ],
          "name" : [ "Carol 😅" ],
          "nickname" : [ "CarolSaysThings" ],
          "url" : [ "https://twitter.com/CarolSaysThings", "https://carolgilabert.me/" ],
          "published" : [ "2009-04-29T15:22:13+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "🇧🇷🇪🇸🇬🇧 · Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1238515159594917889/C5994QPa.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "You two are the best cat parents, stg 💛",
        "html" : "You two are the best cat parents, stg 💛\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/anna_hax\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
