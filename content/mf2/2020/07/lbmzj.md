{
  "date" : "2020-07-09T16:30:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/nefyn92/status/1280939445655670786" ],
    "name" : [ "Like of @nefyn92's tweet" ],
    "published" : [ "2020-07-09T16:30:00+01:00" ],
    "category" : [ "food" ],
    "like-of" : [ "https://twitter.com/nefyn92/status/1280939445655670786" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/lbmzj",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1280939445655670786" ],
      "url" : [ "https://twitter.com/nefyn92/status/1280939445655670786" ],
      "published" : [ "2020-07-08T18:58:57+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:nefyn92" ],
          "numeric-id" : [ "202692459" ],
          "name" : [ "🏴󠁧󠁢󠁷󠁬󠁳󠁿Nefyn🇬🇧" ],
          "nickname" : [ "nefyn92" ],
          "url" : [ "https://twitter.com/nefyn92", "http://www.instagram.com/nefyn92/" ],
          "published" : [ "2010-10-14T16:23:33+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Flint, Wales" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1263879857798291462/o5yVSv3_.jpg" ]
        }
      } ],
      "content" : [ "Life hack - put bread under Kievs to capture garlic butter leakage and make garlic bread." ],
      "photo" : [ "https://pbs.twimg.com/media/EcbPoZVWoAIOKqP.jpg" ]
    }
  },
  "tags" : [ "food" ],
  "client_id" : "https://indigenous.realize.be"
}
