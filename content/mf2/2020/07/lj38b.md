{
  "date" : "2020-07-27T20:11:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/pascaldoesgo/status/1287809390561701888" ],
    "name" : [ "Like of @pascaldoesgo's tweet" ],
    "published" : [ "2020-07-27T20:11:00+01:00" ],
    "like-of" : [ "https://twitter.com/pascaldoesgo/status/1287809390561701888" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/lj38b",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1287809390561701888" ],
      "url" : [ "https://twitter.com/pascaldoesgo/status/1287809390561701888" ],
      "published" : [ "2020-07-27T17:57:39+00:00" ],
      "in-reply-to" : [ "https://twitter.com/dev_nikema/status/1287775874444455937" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:pascaldoesgo" ],
          "numeric-id" : [ "1003529600901767168" ],
          "name" : [ "Pascal Dennerly" ],
          "nickname" : [ "pascaldoesgo" ],
          "url" : [ "https://twitter.com/pascaldoesgo", "http://github.com/dnnrly" ],
          "published" : [ "2018-06-04T06:51:16+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1199757075561222147/eYBkMUrZ.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I have a colleague who lives by \"what's the absolute minimum I can do to meet this requirement\". I'm not quite as brutal as he is about it but being focussed and strict on scope has helped me A LOT!",
        "html" : "I have a colleague who lives by \"what's the absolute minimum I can do to meet this requirement\". I'm not quite as brutal as he is about it but being focussed and strict on scope has helped me A LOT!\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/dev_nikema\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
