{
  "date" : "2020-07-26T11:23:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/lilredridingwud/status/1286754364590186496" ],
    "name" : [ "Like of @lilredridingwud's tweet" ],
    "published" : [ "2020-07-26T11:23:00+01:00" ],
    "like-of" : [ "https://twitter.com/lilredridingwud/status/1286754364590186496" ]
  },
  "kind" : "likes",
  "slug" : "2020/07/dk1vh",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1286754364590186496" ],
      "url" : [ "https://twitter.com/lilredridingwud/status/1286754364590186496" ],
      "published" : [ "2020-07-24T20:05:21+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:lilredridingwud" ],
          "numeric-id" : [ "1957936518" ],
          "name" : [ "lil red" ],
          "nickname" : [ "lilredridingwud" ],
          "url" : [ "https://twitter.com/lilredridingwud" ],
          "published" : [ "2013-10-13T04:21:55+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "she/her - 18+" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1240190939341053952/FkSsCAof.jpg" ]
        }
      } ],
      "content" : [ "putting a hot frying pan into a sink running with cold water makes me feel like a blacksmith" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
