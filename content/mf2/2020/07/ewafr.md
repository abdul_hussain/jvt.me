{
  "date" : "2020-07-12T12:32:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/CarolSaysThings/status/1282269872613924864" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1282278373155385347" ],
    "name" : [ "Reply to https://twitter.com/CarolSaysThings/status/1282269872613924864" ],
    "published" : [ "2020-07-12T12:32:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "I have this happen to me a lot when I'm trying to get to bed, it's so annoying 🙄"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/07/ewafr",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1282269872613924864" ],
      "url" : [ "https://twitter.com/CarolSaysThings/status/1282269872613924864" ],
      "published" : [ "2020-07-12T11:05:35+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:CarolSaysThings" ],
          "numeric-id" : [ "36382927" ],
          "name" : [ "Carol 😅" ],
          "nickname" : [ "CarolSaysThings" ],
          "url" : [ "https://twitter.com/CarolSaysThings", "https://carolgilabert.me/" ],
          "published" : [ "2009-04-29T15:22:13+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "🇧🇷🇪🇸🇬🇧 · Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1238515159594917889/C5994QPa.jpg" ]
        }
      } ],
      "content" : [ "Love how I haven’t written a blog post in 6 months but now that I’m getting ready for something I’m already late for, the words are emphatically trying to break out of my brain 😅" ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
