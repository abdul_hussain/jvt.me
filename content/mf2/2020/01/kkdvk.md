{
  "kind": "rsvps",
  "slug": "2020/01/kkdvk",
  "client_id": "https://indigenous.realize.be",
  "date": "2020-01-07T22:56:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/NottsJS/events/265428203/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/NottsJS/events/265428203/"
    ],
    "published": [
      "2020-01-07T22:56:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "syndication": [
      "https://www.meetup.com/NottsJS/events/265428203/#rsvp-by-https%3A%2F%2Fwww.jvt.me"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/NottsJS/events/265428203/"
      ],
      "name": [
        "Community project evening!"
      ],
      "start": [
        "2020-01-14T18:00:00Z"
      ],
      "end": [
        "2020-01-14T21:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Capital One (Europe) plc, Station St"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
