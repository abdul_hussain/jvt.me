{
  "kind": "rsvps",
  "slug": "2020/01/ckdnt",
  "client_id": "https://micropublish.net",
  "date": "2020-01-30T22:42:07.493+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Tech-Nottingham/events/268319753/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Tech-Nottingham/events/268319753/"
    ],
    "published": [
      "2020-01-30T22:42:07.493+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "syndication": [
      "https://www.meetup.com/Tech-Nottingham/events/268319753/#rsvp-by-https%3A%2F%2Fwww.jvt.me"
    ],
    "event": {
      "location": [
        "Antenna, Beck Street, Nottingham, United Kingdom"
      ],
      "url": [
        "https://www.meetup.com/Tech-Nottingham/events/268319753/"
      ],
      "name": [
        "Tech Nottingham February 2020 - Turning Ideas Into Plans And Service Design"
      ],
      "start": [
        "2020-02-10T18:30:00Z"
      ],
      "end": [
        "2020-02-10T21:00:00Z"
      ]
    }
  }
}
