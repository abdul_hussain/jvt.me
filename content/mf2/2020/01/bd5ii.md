{
  "kind": "rsvps",
  "slug": "2020/01/bd5ii",
  "client_id": "https://micropublish.net",
  "date": "2020-01-29T07:59:11.01+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/PHPMiNDS-in-Nottingham/events/268209825/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/268209825/"
    ],
    "published": [
      "2020-01-29T07:59:11.01+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "syndication": [
      "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/268209825/#rsvp-by-https%3A%2F%2Fwww.jvt.me"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/268209825/"
      ],
      "name": [
        "Incident Response & Security Operation Centres - responding to a Cyber Incident"
      ],
      "start": [
        "2020-02-13T19:00:00Z"
      ],
      "end": [
        "2020-02-13T21:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "JH,  34a Stoney Street, Nottingham, NG1 1NB."
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
