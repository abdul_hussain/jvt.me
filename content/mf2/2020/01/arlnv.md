{
  "kind": "rsvps",
  "slug": "2020/01/arlnv",
  "date": "2016-02-06T10:00:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to http://banterhack.com/"
    ],
    "in-reply-to": [
      "http://banterhack.com/"
    ],
    "published": [
      "2016-02-06T10:00:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "http://banterhack.com/"
      ],
      "name": [
        "Banter Hack"
      ],
      "start": [
        "2016-02-06T10:00:00Z"
      ],
      "end": [
        "2016-02-07T16:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "15 Bonhill St, Shoreditch"
          ],
          "locality": [
            "London"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
