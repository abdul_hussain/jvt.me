{
  "kind": "reads",
  "slug": "2020/01/co6ye",
  "date": "2020-01-25T17:50:00Z",
  "h": "h-entry",
  "properties": {
    "published": [
      "2020-01-25T17:50:00Z"
    ],
    "read-status": [
      "finished"
    ],
    "read-of": [
      {
        "type": [
          "h-cite"
        ],
        "properties": {
          "name": [
            "Star Wars: Thrawn"
          ],
          "author": [
            "Timothy Zahn"
          ],
          "isbn": [
            "0345511271"
          ]
        }
      }
    ]
  }
}
