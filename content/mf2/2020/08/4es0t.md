{
  "date" : "2020-08-15T16:11:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/lunetics/status/1294597730627723264" ],
    "name" : [ "Like of @lunetics's tweet" ],
    "published" : [ "2020-08-15T16:11:00+01:00" ],
    "like-of" : [ "https://twitter.com/lunetics/status/1294597730627723264" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/4es0t",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1294597730627723264" ],
      "url" : [ "https://twitter.com/lunetics/status/1294597730627723264" ],
      "published" : [ "2020-08-15T11:32:06+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:lunetics" ],
          "numeric-id" : [ "46303723" ],
          "name" : [ "Ambiguity Removal Expert" ],
          "nickname" : [ "lunetics" ],
          "url" : [ "https://twitter.com/lunetics", "http://www.lunetics.com" ],
          "published" : [ "2009-06-11T03:29:34+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Between Cologne / Düsseldorf" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/2229206122/twitter.png" ]
        }
      } ],
      "content" : [ "Doing code reviews:" ],
      "photo" : [ "https://pbs.twimg.com/media/EfdVxkNXoAAChnm.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
