{
  "date" : "2020-08-27T21:44:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "name" : [ "Like of @ExpanseNContext's tweet" ],
    "published" : [ "2020-08-27T21:44:00+01:00" ],
    "like-of" : [ "https://twitter.com/ExpanseNContext/status/1299072257734651904" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/1ufne",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1299072257734651904" ],
      "url" : [ "https://twitter.com/ExpanseNContext/status/1299072257734651904" ],
      "published" : [ "2020-08-27T19:52:16+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:ExpanseNContext" ],
          "numeric-id" : [ "1154031225394253825" ],
          "name" : [ "The Expanse No Context" ],
          "nickname" : [ "ExpanseNContext" ],
          "url" : [ "https://twitter.com/ExpanseNContext" ],
          "published" : [ "2019-07-24T14:10:59+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Trapped inside a Dandelion Sky" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1270532350707994625/ya5MrZfv.jpg" ]
        }
      } ],
      "photo" : [ "https://pbs.twimg.com/media/Egc7VT6WkAEobmK.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
