{
  "date" : "2020-08-08T00:41:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JemYoung/status/1291814747365502976" ],
    "name" : [ "Like of @JemYoung's tweet" ],
    "published" : [ "2020-08-08T00:41:00+01:00" ],
    "like-of" : [ "https://twitter.com/JemYoung/status/1291814747365502976" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/mqj2f",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1291814747365502976" ],
      "url" : [ "https://twitter.com/JemYoung/status/1291814747365502976" ],
      "published" : [ "2020-08-07T19:13:31+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:JemYoung" ],
          "numeric-id" : [ "959539956" ],
          "name" : [ "Jem Young ✊🏾" ],
          "nickname" : [ "JemYoung" ],
          "url" : [ "https://twitter.com/JemYoung", "https://www.jemyoung.com" ],
          "published" : [ "2012-11-20T03:58:03+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "San Francisco, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1135595510838915074/3FdMbWZ3.png" ]
        }
      } ],
      "content" : [ "How most engineers get started with Frontend frameworks" ],
      "photo" : [ "https://pbs.twimg.com/media/Ee1ydsEU8AAHsTB.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
