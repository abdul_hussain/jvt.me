{
  "date" : "2020-08-08T11:38:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.eventbrite.co.uk/e/sneinton-street-food-club-disco-fries-umai-13th-element-more-tickets-115636757655" ],
    "name" : [ "RSVP yes to https://www.eventbrite.co.uk/e/sneinton-street-food-club-disco-fries-umai-13th-element-more-tickets-115636757655" ],
    "published" : [ "2020-08-08T11:38:00+01:00" ],
    "event" : {
      "start" : [ "2020-08-07T16:00:00Z" ],
      "name" : [ "Sneinton Street Food Club • Disco Fries • Umai •  13th Element + More" ],
      "end" : [ "2020-08-08T21:00:00Z" ],
      "location" : [ {
        "type" : [ "h-adr" ],
        "properties" : {
          "name" : [ "Sneinton Market Avenues" ],
          "locality" : [ "Nottingham" ],
          "street-address" : [ "Sneinton Market, Avenue B & C" ],
          "postal-code" : [ "NG1 1DW" ]
        }
      } ],
      "url" : [ "https://www.eventbrite.co.uk/e/sneinton-street-food-club-disco-fries-umai-13th-element-more-tickets-115636757655" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/08/38nzu",
  "client_id" : "https://indigenous.realize.be"
}
