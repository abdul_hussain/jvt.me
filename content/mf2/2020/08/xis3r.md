{
  "date" : "2020-08-11T19:46:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/madebyjenni/status/1293127515989577728" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1293259262890848259" ],
    "name" : [ "Reply to https://twitter.com/madebyjenni/status/1293127515989577728" ],
    "published" : [ "2020-08-11T19:46:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "So sorry to hear that, sending love 🤗"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/08/xis3r",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1293127515989577728" ],
      "url" : [ "https://twitter.com/madebyjenni/status/1293127515989577728" ],
      "published" : [ "2020-08-11T10:09:59+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:madebyjenni" ],
          "numeric-id" : [ "21200040" ],
          "name" : [ "Jenni Brown 🍉" ],
          "nickname" : [ "madebyjenni" ],
          "url" : [ "https://twitter.com/madebyjenni" ],
          "published" : [ "2009-02-18T14:27:52+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/567426387875594241/Inc5VCof.jpeg" ]
        }
      } ],
      "content" : [ "Outside with Ginge for his last few hours before he goes to be put down. My heart is so broken." ],
      "photo" : [ "https://pbs.twimg.com/media/EfIcmgfXgAAejX_.jpg" ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
