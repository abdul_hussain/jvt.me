{
  "date" : "2020-08-02T13:21:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/CarolSaysThings/status/1289848607932166144" ],
    "name" : [ "Like of @CarolSaysThings's tweet" ],
    "published" : [ "2020-08-02T13:21:00+01:00" ],
    "like-of" : [ "https://twitter.com/CarolSaysThings/status/1289848607932166144" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/dciy5",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1289848607932166144" ],
      "url" : [ "https://twitter.com/CarolSaysThings/status/1289848607932166144" ],
      "published" : [ "2020-08-02T09:00:47+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:CarolSaysThings" ],
          "numeric-id" : [ "36382927" ],
          "name" : [ "Carol 🌻" ],
          "nickname" : [ "CarolSaysThings" ],
          "url" : [ "https://twitter.com/CarolSaysThings", "https://carolgilabert.me/" ],
          "published" : [ "2009-04-29T15:22:13+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "🇧🇷🇪🇸🇬🇧 · Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1238515159594917889/C5994QPa.jpg" ]
        }
      } ],
      "content" : [ "Yesterday, for the first time in a while, I didn’t even turn my computer on. Please be proud of me 😅" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
