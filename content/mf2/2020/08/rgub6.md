{
  "date" : "2020-08-20T19:46:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/CSMFHT/status/1296381700897832960" ],
    "name" : [ "Like of @CSMFHT's tweet" ],
    "published" : [ "2020-08-20T19:46:00+01:00" ],
    "like-of" : [ "https://twitter.com/CSMFHT/status/1296381700897832960" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/rgub6",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1296381700897832960" ],
      "url" : [ "https://twitter.com/CSMFHT/status/1296381700897832960" ],
      "published" : [ "2020-08-20T09:40:57+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:CSMFHT" ],
          "numeric-id" : [ "1230431996163178497" ],
          "name" : [ "Classical Studies Memes for Hellenistic Teens" ],
          "nickname" : [ "CSMFHT" ],
          "url" : [ "https://twitter.com/CSMFHT", "http://facebook.com/CSMFHT" ],
          "published" : [ "2020-02-20T10:00:46+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "New Zealand" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1230432197326147584/NOEqWdEO.jpg" ]
        }
      } ],
      "photo" : [ "https://pbs.twimg.com/media/Ef2sSU9UcAU61rr.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
