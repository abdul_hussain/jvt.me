{
  "date" : "2020-08-10T22:19:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/anna_hax/status/1292901975802236929" ],
    "name" : [ "Like of @anna_hax's tweet" ],
    "published" : [ "2020-08-10T22:19:00+01:00" ],
    "like-of" : [ "https://twitter.com/anna_hax/status/1292901975802236929" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/e87he",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1292901975802236929" ],
      "url" : [ "https://twitter.com/anna_hax/status/1292901975802236929" ],
      "video" : [ "https://video.twimg.com/tweet_video/EfFPfShWoAIBE4m.mp4" ],
      "published" : [ "2020-08-10T19:13:46+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:anna_hax" ],
          "numeric-id" : [ "394235377" ],
          "name" : [ "Anna 🏠" ],
          "nickname" : [ "anna_hax" ],
          "url" : [ "https://twitter.com/anna_hax", "https://annadodson.co.uk" ],
          "published" : [ "2011-10-19T19:37:31+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1134736020669378561/0_EwVzms.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I'm a vital and valued member of the #TechNott organising team - what do I do? I make a great beeping noise to signal the end of a 5 minute lighting talk 😂",
        "html" : "I'm a vital and valued member of the <a href=\"https://twitter.com/search?q=%23TechNott\">#TechNott</a> organising team - what do I do? I make a great beeping noise to signal the end of a 5 minute lighting talk 😂"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
