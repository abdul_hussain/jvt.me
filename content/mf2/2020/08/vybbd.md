{
  "date" : "2020-08-25T08:24:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/Brunty/status/1298017666331029512" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1298161017680166912" ],
    "name" : [ "Reply to https://twitter.com/Brunty/status/1298017666331029512" ],
    "published" : [ "2020-08-25T08:24:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "Argh sorry! I don't read good.\n\nDo you know if anything they use needs Python? Maybe they removed it from the image if it's no longer needed for their stuff?"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/08/vybbd",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1298017666331029512" ],
      "url" : [ "https://twitter.com/Brunty/status/1298017666331029512" ],
      "published" : [ "2020-08-24T22:01:42+00:00" ],
      "in-reply-to" : [ "https://twitter.com/JamieTanna/status/1298017506859405312" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Brunty" ],
          "numeric-id" : [ "1099517012" ],
          "name" : [ "Matt Brunt" ],
          "nickname" : [ "Brunty" ],
          "url" : [ "https://twitter.com/Brunty", "https://brunty.me/now" ],
          "published" : [ "2013-01-17T23:19:21+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Leicester, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1151043501288824832/TjB9WT83.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "See the last line of the tweet ;)",
        "html" : "See the last line of the tweet ;)\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/JamieTanna\"></a>\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/home_assistant\"></a>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
