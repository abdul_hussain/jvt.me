{
  "date" : "2020-08-29T09:54:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/soniagupta504/status/1299534219748794374" ],
    "name" : [ "Like of @soniagupta504's tweet" ],
    "published" : [ "2020-08-29T09:54:00+01:00" ],
    "like-of" : [ "https://twitter.com/soniagupta504/status/1299534219748794374" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/5cd0t",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1299534219748794374" ],
      "url" : [ "https://twitter.com/soniagupta504/status/1299534219748794374" ],
      "published" : [ "2020-08-29T02:27:56+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:soniagupta504" ],
          "numeric-id" : [ "705330458842701824" ],
          "name" : [ "Sonia Gupta" ],
          "nickname" : [ "soniagupta504" ],
          "url" : [ "https://twitter.com/soniagupta504", "https://medium.com/@soniagupta504" ],
          "published" : [ "2016-03-03T09:54:20+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "New Orleans, LA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1287093020362514432/gE5qcPYG.jpg" ]
        }
      } ],
      "content" : [ "The first time I saw \"Black Panther\" I walked out of the theater and sat in my car and cried. I was deeply moved to see Blackness portrayed so beautifully, so brilliantly, and with the respect and artistry it is due. That movie changed things for a lot of folks, including me. 💜" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
