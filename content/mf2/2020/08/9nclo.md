{
  "date" : "2020-08-18T09:44:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/MrAndrew/status/1295636933641740288" ],
    "name" : [ "Like of @MrAndrew's tweet" ],
    "published" : [ "2020-08-18T09:44:00+01:00" ],
    "category" : [ "coronavirus" ],
    "like-of" : [ "https://twitter.com/MrAndrew/status/1295636933641740288" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/9nclo",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1295636933641740288" ],
      "url" : [ "https://twitter.com/MrAndrew/status/1295636933641740288" ],
      "published" : [ "2020-08-18T08:21:31+00:00" ],
      "in-reply-to" : [ "https://twitter.com/Marcus_Noble_/status/1295446409362264064" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:MrAndrew" ],
          "numeric-id" : [ "9626182" ],
          "name" : [ "Andrew Seward" ],
          "nickname" : [ "MrAndrew" ],
          "url" : [ "https://twitter.com/MrAndrew", "http://www.technottingham.com" ],
          "published" : [ "2007-10-23T15:57:18+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Riddings, Derbyshire" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1258160750641606657/CR5g9eOt.jpg" ]
        }
      } ],
      "location" : [ {
        "type" : [ "h-card", "p-location" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:50f2d0272381533f" ],
          "name" : [ "East Midlands, England" ]
        }
      } ],
      "content" : [ {
        "value" : "The wicker face mask also works for trips to the supermarket",
        "html" : "The wicker face mask also works for trips to the supermarket\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/Marcus_Noble_\"></a>\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/MrsEmma\"></a>"
      } ]
    }
  },
  "tags" : [ "coronavirus" ],
  "client_id" : "https://indigenous.realize.be"
}
