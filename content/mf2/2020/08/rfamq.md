{
  "date" : "2020-08-28T12:00:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/BerniceKing/status/1299182655775203331" ],
    "name" : [ "Like of @BerniceKing's tweet" ],
    "published" : [ "2020-08-28T12:00:00+01:00" ],
    "like-of" : [ "https://twitter.com/BerniceKing/status/1299182655775203331" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/rfamq",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1299182655775203331" ],
      "url" : [ "https://twitter.com/BerniceKing/status/1299182655775203331" ],
      "published" : [ "2020-08-28T03:10:57+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:BerniceKing" ],
          "numeric-id" : [ "54617733" ],
          "name" : [ "Be A King" ],
          "nickname" : [ "BerniceKing" ],
          "url" : [ "https://twitter.com/BerniceKing", "https://thekingcenter.org" ],
          "published" : [ "2009-07-07T17:30:48+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Atlanta, GA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1293205511060553736/z9eAOJK-.jpg" ]
        }
      } ],
      "content" : [ "True story." ],
      "photo" : [ "https://pbs.twimg.com/media/EgefvNDXgAA_5Ra.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
