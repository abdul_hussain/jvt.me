{
  "date" : "2020-08-27T17:13:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/lornajane/status/1298975902227816458" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1299025456344305665" ],
    "name" : [ "Reply to https://twitter.com/lornajane/status/1298975902227816458" ],
    "published" : [ "2020-08-27T17:13:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "I don't trust (and can't use) jwt.io for anything I do at work, but I've got https://www.jvt.me/posts/2019/06/13/pretty-printing-jwt-openssl/ and https://www.jvt.me/posts/2018/08/31/pretty-printing-jwt-ruby/ as solutions you can run locally and have confidence that your secret tokens aren't being leaked anywhere!"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/08/zebqf",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1298975902227816458" ],
      "url" : [ "https://twitter.com/lornajane/status/1298975902227816458" ],
      "published" : [ "2020-08-27T13:29:23+00:00" ],
      "in-reply-to" : [ "https://twitter.com/messypixels/status/1298973415102177281" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:lornajane" ],
          "numeric-id" : [ "7356002" ],
          "name" : [ "Lorna Mitchell" ],
          "nickname" : [ "lornajane" ],
          "url" : [ "https://twitter.com/lornajane", "http://www.lornajane.net" ],
          "published" : [ "2007-07-09T21:29:09+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Huddersfield, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1509896668/lorna-square.png" ]
        }
      } ],
      "content" : [ {
        "value" : "Maybe there is a better way! Hopefully twitter will tell us ....",
        "html" : "Maybe there is a better way! Hopefully twitter will tell us ....\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/messypixels\"></a>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
