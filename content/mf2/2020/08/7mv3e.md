{
  "date" : "2020-08-18T14:25:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/CarolSaysThings/status/1295709149523783681" ],
    "name" : [ "Like of @CarolSaysThings's tweet" ],
    "published" : [ "2020-08-18T14:25:00+01:00" ],
    "like-of" : [ "https://twitter.com/CarolSaysThings/status/1295709149523783681" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/7mv3e",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1295709149523783681" ],
      "url" : [ "https://twitter.com/CarolSaysThings/status/1295709149523783681" ],
      "published" : [ "2020-08-18T13:08:29+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:CarolSaysThings" ],
          "numeric-id" : [ "36382927" ],
          "name" : [ "Carol 🌻" ],
          "nickname" : [ "CarolSaysThings" ],
          "url" : [ "https://twitter.com/CarolSaysThings", "https://carolgilabert.me/" ],
          "published" : [ "2009-04-29T15:22:13+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "🇧🇷🇪🇸🇬🇧 · Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1238515159594917889/C5994QPa.jpg" ]
        }
      } ],
      "location" : [ {
        "type" : [ "h-card", "p-location" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:1db4f0a70fc5c9db" ],
          "name" : [ "Bath, England" ]
        }
      } ],
      "content" : [ "Omg y’all! A /good/ newsletter sign up modal 😍" ],
      "photo" : [ "https://pbs.twimg.com/media/EftIlcAWAAMAhRf.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
