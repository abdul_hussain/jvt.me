{
  "date" : "2020-08-10T07:15:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/littlelizziev/status/1292363538388983808" ],
    "name" : [ "Like of @littlelizziev's tweet" ],
    "published" : [ "2020-08-10T07:15:00+01:00" ],
    "category" : [ "diversity-and-inclusion" ],
    "like-of" : [ "https://twitter.com/littlelizziev/status/1292363538388983808" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/zfiyq",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1292363538388983808" ],
      "url" : [ "https://twitter.com/littlelizziev/status/1292363538388983808" ],
      "video" : [ "https://video.twimg.com/ext_tw_video/1292363331140034561/pu/vid/720x1280/hPkWg59fQjoCF3x4.mp4?tag=10" ],
      "published" : [ "2020-08-09T07:34:13+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:littlelizziev" ],
          "numeric-id" : [ "23354702" ],
          "name" : [ "Lizzie Velasquez" ],
          "nickname" : [ "littlelizziev" ],
          "url" : [ "https://twitter.com/littlelizziev", "https://bit.ly/32UIF1v" ],
          "published" : [ "2009-03-08T21:09:07+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Austin, TX" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1235753554729750528/scgY3rSd.jpg" ]
        }
      } ],
      "location" : [ {
        "type" : [ "h-card", "p-location" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:c3f37afa9efcf94b" ],
          "name" : [ "Austin, TX" ]
        }
      } ],
      "content" : [ {
        "value" : "Please help spread the word! @tiktok_us #facetimeprank",
        "html" : "Please help spread the word! <a href=\"https://twitter.com/tiktok_us\">@tiktok_us</a> <a href=\"https://twitter.com/search?q=%23facetimeprank\">#facetimeprank</a>"
      } ]
    }
  },
  "tags" : [ "diversity-and-inclusion" ],
  "client_id" : "https://indigenous.realize.be"
}
