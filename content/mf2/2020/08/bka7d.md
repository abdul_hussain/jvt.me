{
  "date" : "2020-08-14T12:33:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/ElleArmageddon/status/1294097943306280961" ],
    "name" : [ "Like of @ElleArmageddon's tweet" ],
    "published" : [ "2020-08-14T12:33:00+01:00" ],
    "category" : [ "career" ],
    "like-of" : [ "https://twitter.com/ElleArmageddon/status/1294097943306280961" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/bka7d",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1294097943306280961" ],
      "url" : [ "https://twitter.com/ElleArmageddon/status/1294097943306280961" ],
      "published" : [ "2020-08-14T02:26:07+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:ElleArmageddon" ],
          "numeric-id" : [ "385847731" ],
          "name" : [ "the apocalypse, but fashion 🦝" ],
          "nickname" : [ "ElleArmageddon" ],
          "url" : [ "https://twitter.com/ElleArmageddon", "http://blog.totallynotmalware.net" ],
          "published" : [ "2011-10-06T07:23:59+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Oakland, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1182485650894835713/knpWHlV3.jpg" ]
        }
      } ],
      "content" : [ "If you are a senior/staff software engineer or architect, what advice would you give someone hoping to pivot into tech fresh now and be where you are in 10 years?" ]
    }
  },
  "tags" : [ "career" ],
  "client_id" : "https://indigenous.realize.be"
}
