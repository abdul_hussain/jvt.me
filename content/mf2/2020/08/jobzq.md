{
  "date" : "2020-08-09T11:25:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://events.indieweb.org/2020/08/indieauth-1-1-identity-protocol-standards-session-session-2--uCtG38aoJyju", "https://indieweb.org/2020/Pop-ups/IndieAuth" ],
    "name" : [ "RSVP maybe to https://events.indieweb.org/2020/08/indieauth-1-1-identity-protocol-standards-session-session-2--uCtG38aoJyju" ],
    "published" : [ "2020-08-09T11:25:00+01:00" ],
    "event" : {
      "start" : [ "2020-08-22T09:30:00-07:00" ],
      "name" : [ "IndieAuth 1.1 Identity Protocol Standards Session (Session 2)" ],
      "end" : [ "2020-08-22T11:30:00-07:00" ],
      "url" : [ "https://events.indieweb.org/2020/08/indieauth-1-1-identity-protocol-standards-session-session-2--uCtG38aoJyju", "https://indieweb.org/2020/Pop-ups/IndieAuth" ]
    },
    "rsvp" : [ "maybe" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/08/jobzq",
  "client_id" : "https://indigenous.realize.be"
}
