{
  "date" : "2020-08-26T20:17:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.meetup.com/Women-In-Tech-Nottingham/events/272818213/" ],
    "syndication" : [ "https://www.meetup.com/Women-In-Tech-Nottingham/events/272818213/#rsvp-by-https%3A%2F%2Fwww.jvt.me%2Fmf2%2F2020%2F08%2Fm8eom%2F" ],
    "name" : [ "RSVP yes to https://www.meetup.com/Women-In-Tech-Nottingham/events/272818213/" ],
    "published" : [ "2020-08-26T20:17:00+01:00" ],
    "event" : {
      "start" : [ "2020-09-03T18:30:00+01:00" ],
      "name" : [ "Women in Tech 3rd September - What's in a game?" ],
      "end" : [ "2020-09-03T20:30:00+01:00" ],
      "location" : [ "Online" ],
      "url" : [ "https://www.meetup.com/Women-In-Tech-Nottingham/events/272818213/" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/08/m8eom",
  "client_id" : "https://indigenous.realize.be"
}
