{
  "date" : "2020-08-24T22:56:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/Brunty/status/1297961625618612224" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1298017506859405312" ],
    "name" : [ "Reply to https://twitter.com/Brunty/status/1297961625618612224" ],
    "published" : [ "2020-08-24T22:56:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "Is it maybe now just `python`? 🤔"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/08/yqdw9",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1297961625618612224" ],
      "url" : [ "https://twitter.com/Brunty/status/1297961625618612224" ],
      "published" : [ "2020-08-24T18:19:01+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Brunty" ],
          "numeric-id" : [ "1099517012" ],
          "name" : [ "Matt Brunt" ],
          "nickname" : [ "Brunty" ],
          "url" : [ "https://twitter.com/Brunty", "https://brunty.me/now" ],
          "published" : [ "2013-01-17T23:19:21+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Leicester, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1151043501288824832/TjB9WT83.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Interesting, it seems @home_assistant has lost python3 in my install.\n\nSo my command line switches that used python scripts run via `python3 script_name.py` don't work as it's just \"bash: python3: command not found\"\n\nWut. (also it can't find python)",
        "html" : "<div style=\"white-space: pre\">Interesting, it seems <a href=\"https://twitter.com/home_assistant\">@home_assistant</a> has lost python3 in my install.\n\nSo my command line switches that used python scripts run via `python3 script_name.py` don't work as it's just \"bash: python3: command not found\"\n\nWut. (also it can't find python)</div>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
