{
  "date" : "2020-08-11T10:26:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/warofthesuburbs/status/1292560039971037185" ],
    "name" : [ "Like of @warofthesuburbs's tweet" ],
    "published" : [ "2020-08-11T10:26:00+01:00" ],
    "category" : [ "coronavirus" ],
    "like-of" : [ "https://twitter.com/warofthesuburbs/status/1292560039971037185" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/ajuxs",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1292560039971037185" ],
      "url" : [ "https://twitter.com/warofthesuburbs/status/1292560039971037185" ],
      "published" : [ "2020-08-09T20:35:02+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:warofthesuburbs" ],
          "numeric-id" : [ "954807380855939073" ],
          "name" : [ "Tim Zook (he/him)" ],
          "nickname" : [ "warofthesuburbs" ],
          "url" : [ "https://twitter.com/warofthesuburbs", "https://breathless.media/" ],
          "published" : [ "2018-01-20T20:06:33+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Kansas City, MO" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/954812512066666496/uAHvUp3M.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I had to say goodbye to my mom dying of COVID over zoom.\n\nBut I'm sure your brunch, your bar hop, your party was worth it.",
        "html" : "<div style=\"white-space: pre\">I had to say goodbye to my mom dying of COVID over zoom.\n\nBut I'm sure your brunch, your bar hop, your party was worth it.</div>"
      } ]
    }
  },
  "tags" : [ "coronavirus" ],
  "client_id" : "https://indigenous.realize.be"
}
