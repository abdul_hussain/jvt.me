{
  "date" : "2020-08-15T21:28:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/niamh__power/status/1294590859216920577" ],
    "name" : [ "Like of @niamh__power's tweet" ],
    "published" : [ "2020-08-15T21:28:00+01:00" ],
    "category" : [ "nature" ],
    "like-of" : [ "https://twitter.com/niamh__power/status/1294590859216920577" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/xuxfi",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1294590859216920577" ],
      "url" : [ "https://twitter.com/niamh__power/status/1294590859216920577" ],
      "published" : [ "2020-08-15T11:04:47+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:niamh__power" ],
          "numeric-id" : [ "66313256" ],
          "name" : [ "Niamh Power 🦕💙" ],
          "nickname" : [ "niamh__power" ],
          "url" : [ "https://twitter.com/niamh__power" ],
          "published" : [ "2009-08-17T08:48:38+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Llangollen, Wales" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1207026635461017601/yXXSI1nR.jpg" ]
        }
      } ],
      "location" : [ {
        "type" : [ "h-card", "p-location" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:07d9e41146c85001" ],
          "name" : [ "Hotel Le Pic Blanc" ]
        }
      } ],
      "content" : [ {
        "value" : "When your phone can get a picture of the Milky Way, Jupiter and Saturn @alpedhuez 🤯🤯🤯",
        "html" : "When your phone can get a picture of the Milky Way, Jupiter and Saturn <a href=\"https://twitter.com/alpedhuez\">@alpedhuez</a> 🤯🤯🤯"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EfdPgtVXgAIUfid.jpg" ]
    }
  },
  "tags" : [ "nature" ],
  "client_id" : "https://indigenous.realize.be"
}
