{
  "date" : "2020-08-06T16:34:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/XMPPwocky/status/1291144278953955328" ],
    "name" : [ "Like of @XMPPwocky's tweet" ],
    "published" : [ "2020-08-06T16:34:00+01:00" ],
    "category" : [ "pgp" ],
    "like-of" : [ "https://twitter.com/XMPPwocky/status/1291144278953955328" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/uhzfx",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1291144278953955328" ],
      "url" : [ "https://twitter.com/XMPPwocky/status/1291144278953955328" ],
      "published" : [ "2020-08-05T22:49:19+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:XMPPwocky" ],
          "numeric-id" : [ "451735131" ],
          "name" : [ "your least favorite construct" ],
          "nickname" : [ "XMPPwocky" ],
          "url" : [ "https://twitter.com/XMPPwocky" ],
          "published" : [ "2011-12-31T23:13:20+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "not far" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/930521362313175042/Kcwjt6QJ.jpg" ]
        }
      } ],
      "content" : [ "Critical PGP bug just disclosed: if an attacker responds to an encrypted message with \"This doesn't decrypt for me, could you try without PGP\", the sender will be unsurprised and provide the attacker a copy of the encrypted message in plaintext" ]
    }
  },
  "tags" : [ "pgp" ],
  "client_id" : "https://indigenous.realize.be"
}
