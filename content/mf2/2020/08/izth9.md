{
  "date" : "2020-08-26T21:06:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/EmilyKager/status/1298711067581755392" ],
    "name" : [ "Like of @EmilyKager's tweet" ],
    "published" : [ "2020-08-26T21:06:00+01:00" ],
    "category" : [ "shitpost" ],
    "like-of" : [ "https://twitter.com/EmilyKager/status/1298711067581755392" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/izth9",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1298711067581755392" ],
      "url" : [ "https://twitter.com/EmilyKager/status/1298711067581755392" ],
      "published" : [ "2020-08-26T19:57:02+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:EmilyKager" ],
          "numeric-id" : [ "2347475803" ],
          "name" : [ "Emily Kager" ],
          "nickname" : [ "EmilyKager" ],
          "url" : [ "https://twitter.com/EmilyKager", "https://www.emilykager.com/" ],
          "published" : [ "2014-02-16T21:34:12+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Oakland and SF " ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1296572390252716034/jTxFoueZ.jpg" ]
        }
      } ],
      "content" : [ "If I built software I would simply make every user happy at once, maintain multiple versions of everything, let users roll back to older versions, never ship any bugs, and be feature complete from the get-go but maybe that's just me" ]
    }
  },
  "tags" : [ "shitpost" ],
  "client_id" : "https://indigenous.realize.be"
}
