{
  "date" : "2020-08-16T11:15:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/jongold/status/1294692810801557504" ],
    "name" : [ "Like of @jongold's tweet" ],
    "published" : [ "2020-08-16T11:15:00+01:00" ],
    "category" : [ "food" ],
    "like-of" : [ "https://twitter.com/jongold/status/1294692810801557504" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/pcvtr",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1294692810801557504" ],
      "url" : [ "https://twitter.com/jongold/status/1294692810801557504" ],
      "published" : [ "2020-08-15T17:49:54+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:jongold" ],
          "numeric-id" : [ "14199907" ],
          "name" : [ "J.H. Gold" ],
          "nickname" : [ "jongold" ],
          "url" : [ "https://twitter.com/jongold", "https://cosmiccomputation.org/" ],
          "published" : [ "2008-03-23T00:50:47+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Portland, Cascadia" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1232496435595116544/rMoyoHfL.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "one of the best things about living with a bunch of roommates is making elaborate dinners.\n\nmango tempeh curry (me!), cauliflower rice, homemade samosas, homemade naan. i gotta learn to make the naan next.",
        "html" : "<div style=\"white-space: pre\">one of the best things about living with a bunch of roommates is making elaborate dinners.\n\nmango tempeh curry (me!), cauliflower rice, homemade samosas, homemade naan. i gotta learn to make the naan next.</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/Efer0DFUMAURXpU.jpg", "https://pbs.twimg.com/media/Efer1VaUEAAdgSD.jpg" ]
    }
  },
  "tags" : [ "food" ],
  "client_id" : "https://indigenous.realize.be"
}
