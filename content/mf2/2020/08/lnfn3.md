{
  "date" : "2020-08-11T21:22:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/danblackroyd/status/1293076366556975110" ],
    "name" : [ "Like of @danblackroyd's tweet" ],
    "published" : [ "2020-08-11T21:22:00+01:00" ],
    "like-of" : [ "https://twitter.com/danblackroyd/status/1293076366556975110" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/lnfn3",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1293076366556975110" ],
      "url" : [ "https://twitter.com/danblackroyd/status/1293076366556975110" ],
      "published" : [ "2020-08-11T06:46:44+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:danblackroyd" ],
          "numeric-id" : [ "95021503" ],
          "name" : [ "[kie.ran]" ],
          "nickname" : [ "danblackroyd" ],
          "url" : [ "https://twitter.com/danblackroyd", "https://rewire.news/author/kieran-scarlett/", "http://cash.me/$KieranSc" ],
          "published" : [ "2009-12-06T15:54:51+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Los Angeles, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1275994852958724097/lgDZ9tMO.jpg" ]
        }
      } ],
      "content" : [ "Friend breakups (I mean good, longterm, very close and intimate friendships) are, in my experience, much more emotionally devastating than romantic breakups. And yet, unlike romantic relationships, we have almost no socialization (even in media images) on how to navigate them." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
