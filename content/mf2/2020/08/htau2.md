{
  "date" : "2020-08-26T12:30:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/kieranmch/status/1298334764169035781" ],
    "name" : [ "Like of @kieranmch's tweet" ],
    "published" : [ "2020-08-26T12:30:00+01:00" ],
    "like-of" : [ "https://twitter.com/kieranmch/status/1298334764169035781" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/htau2",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1298334764169035781" ],
      "url" : [ "https://twitter.com/kieranmch/status/1298334764169035781" ],
      "published" : [ "2020-08-25T19:01:44+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:kieranmch" ],
          "numeric-id" : [ "50420707" ],
          "name" : [ "Kieran McHugh" ],
          "nickname" : [ "kieranmch" ],
          "url" : [ "https://twitter.com/kieranmch", "https://kieran.engineer" ],
          "published" : [ "2009-06-24T20:13:29+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1170973779675275264/cCZMxVEg.jpg" ]
        }
      } ],
      "content" : [ "I chose the name \"Daybridge\" for the new company because it's easy to spell, pronounced as it's written, can be spelled unambiguously, and the .com was available to buy. I think these are way more important than people realise when naming a new startup." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
