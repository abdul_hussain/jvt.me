{
  "date" : "2020-08-24T15:37:10.593Z",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.jvt.me/events/personal-events/2020-08-2vptv/" ],
    "name" : [ "RSVP yes to https://www.jvt.me/events/personal-events/2020-08-2vptv/" ],
    "published" : [ "2020-08-24T15:37:10.593Z" ],
    "event" : {
      "start" : [ "2020-11-20T00:00:00+0000" ],
      "name" : [ "Northumberland trip 🌌" ],
      "end" : [ "2020-11-24T00:00:00+0000" ],
      "url" : [ "https://www.jvt.me/events/personal-events/2020-08-2vptv/" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/08/z9c0z",
  "client_id" : "https://micropublish.net"
}
