{
  "date" : "2020-08-10T21:08:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/brittnaynay3/status/1292891250551554053" ],
    "name" : [ "Like of @brittnaynay3's tweet" ],
    "published" : [ "2020-08-10T21:08:00+01:00" ],
    "category" : [ "nanoleaf" ],
    "like-of" : [ "https://twitter.com/brittnaynay3/status/1292891250551554053" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/bp7kb",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1292891250551554053" ],
      "url" : [ "https://twitter.com/brittnaynay3/status/1292891250551554053" ],
      "published" : [ "2020-08-10T18:31:09+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:brittnaynay3" ],
          "numeric-id" : [ "1108890021149532161" ],
          "name" : [ "Brittnaynay" ],
          "nickname" : [ "brittnaynay3" ],
          "url" : [ "https://twitter.com/brittnaynay3", "https://linktr.ee/Brittnaynay" ],
          "published" : [ "2019-03-22T00:35:57+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1282746444307472384/MZ_klvsv.jpg" ]
        }
      } ],
      "content" : [ "Nanoleaf lights really bring a room together!" ],
      "photo" : [ "https://pbs.twimg.com/media/EfFFvDGWsAIUeCT.jpg" ]
    }
  },
  "tags" : [ "nanoleaf" ],
  "client_id" : "https://indigenous.realize.be"
}
