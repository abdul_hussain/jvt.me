{
  "date" : "2020-08-27T23:17:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/swyx/status/1299100770969690112" ],
    "name" : [ "Like of @swyx's tweet" ],
    "published" : [ "2020-08-27T23:17:00+01:00" ],
    "category" : [ "open-source" ],
    "like-of" : [ "https://twitter.com/swyx/status/1299100770969690112" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/jnadl",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1299100770969690112" ],
      "url" : [ "https://twitter.com/swyx/status/1299100770969690112" ],
      "published" : [ "2020-08-27T21:45:34+00:00" ],
      "in-reply-to" : [ "https://twitter.com/brian_d_vaughn/status/1299098767011065856" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:swyx" ],
          "numeric-id" : [ "33521530" ],
          "name" : [ "shawn swyx wang" ],
          "nickname" : [ "swyx" ],
          "url" : [ "https://twitter.com/swyx", "http://swyx.io", "http://learninpublic.org" ],
          "published" : [ "2009-04-20T14:04:41+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Singapore 🇸🇬 " ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1201029434054041606/efWs7Lr9.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "iS THIs prOjeCT MAIntAined?",
        "html" : "iS THIs prOjeCT MAIntAined?\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/brian_d_vaughn\"></a>\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/github\"></a>"
      } ]
    }
  },
  "tags" : [ "open-source" ],
  "client_id" : "https://indigenous.realize.be"
}
