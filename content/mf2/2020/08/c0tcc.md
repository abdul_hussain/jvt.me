{
  "date" : "2020-08-23T11:05:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/tmcw/status/1297265801049726977" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1297476341848330240" ],
    "name" : [ "Reply to https://twitter.com/tmcw/status/1297265801049726977" ],
    "published" : [ "2020-08-23T11:05:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "Have you looked into https://indieweb.org by any chance? There's some great stuff we're doing with standards like https://microformats.io/ to make a better Web work alongside the old Web, because adding a hard switchover between new/old unfortunately isn't going to work until big players move over. And if the new Web isn't a good commercial idea, that won't happen"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/08/c0tcc",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1297265801049726977" ],
      "url" : [ "https://twitter.com/tmcw/status/1297265801049726977" ],
      "published" : [ "2020-08-22T20:14:03+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:tmcw" ],
          "numeric-id" : [ "1458271" ],
          "name" : [ "Tom MacWright" ],
          "nickname" : [ "tmcw" ],
          "url" : [ "https://twitter.com/tmcw", "https://macwright.com/" ],
          "published" : [ "2007-03-19T01:06:50+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "lon, lat" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1172554769237336065/CIdcAG0d.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "okay, here's my pitch: let's redo the web macwright.com/2020/08/22/cle…",
        "html" : "okay, here's my pitch: let's redo the web <a href=\"https://macwright.com/2020/08/22/clean-starts-for-the-web.html\">macwright.com/2020/08/22/cle…</a>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
