{
  "date" : "2020-08-06T22:39:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/WiT_Notts/status/1291477201964212224" ],
    "name" : [ "Like of @WiT_Notts's tweet" ],
    "published" : [ "2020-08-06T22:39:00+01:00" ],
    "category" : [ "wit-notts" ],
    "like-of" : [ "https://twitter.com/WiT_Notts/status/1291477201964212224" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/lz1fk",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1291477201964212224" ],
      "url" : [ "https://twitter.com/WiT_Notts/status/1291477201964212224" ],
      "published" : [ "2020-08-06T20:52:14+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:WiT_Notts" ],
          "numeric-id" : [ "4339158441" ],
          "name" : [ "Women In Tech, Nottingham 🌈✨" ],
          "nickname" : [ "WiT_Notts" ],
          "url" : [ "https://twitter.com/WiT_Notts", "https://nott.tech/wit" ],
          "published" : [ "2015-12-01T11:22:25+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/913380298829844481/YvPcjPlE.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Thank you to all our lovely #WiTNotts attendees this evening! It was great seeing you all as usual 🙌 big thanks to our sponsors and @katemonkey for speaking too 🧡✨",
        "html" : "Thank you to all our lovely <a href=\"https://twitter.com/search?q=%23WiTNotts\">#WiTNotts</a> attendees this evening! It was great seeing you all as usual 🙌 big thanks to our sponsors and <a href=\"https://twitter.com/katemonkey\">@katemonkey</a> for speaking too 🧡✨"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/Eew_q2sX0AItpij.jpg" ]
    }
  },
  "tags" : [ "wit-notts" ],
  "client_id" : "https://indigenous.realize.be"
}
