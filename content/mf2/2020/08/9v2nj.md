{
  "date" : "2020-08-22T20:44:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/type__error/status/1297226546348994563" ],
    "name" : [ "Like of @type__error's tweet" ],
    "published" : [ "2020-08-22T20:44:00+01:00" ],
    "category" : [ "public-speaking" ],
    "like-of" : [ "https://twitter.com/type__error/status/1297226546348994563" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/9v2nj",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1297226546348994563" ],
      "url" : [ "https://twitter.com/type__error/status/1297226546348994563" ],
      "published" : [ "2020-08-22T17:38:04+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:type__error" ],
          "numeric-id" : [ "733123902" ],
          "name" : [ "Sophie Koonin" ],
          "nickname" : [ "type__error" ],
          "url" : [ "https://twitter.com/type__error", "http://localghost.dev" ],
          "published" : [ "2012-08-02T16:24:42+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1291691505254641664/yXXSYMBL.jpg" ]
        }
      } ],
      "content" : [ "Nobody told me prerecording talks was this stressful 😭" ]
    }
  },
  "tags" : [ "public-speaking" ],
  "client_id" : "https://indigenous.realize.be"
}
