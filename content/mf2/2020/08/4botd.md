{
  "date" : "2020-08-29T12:00:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/syllaband/status/1299336266031476737" ],
    "name" : [ "Like of @syllaband's tweet" ],
    "published" : [ "2020-08-29T12:00:00+01:00" ],
    "like-of" : [ "https://twitter.com/syllaband/status/1299336266031476737" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/4botd",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1299336266031476737" ],
      "url" : [ "https://twitter.com/syllaband/status/1299336266031476737" ],
      "published" : [ "2020-08-28T13:21:20+00:00" ],
      "in-reply-to" : [ "https://twitter.com/sonniesedge/status/1299333420183584769" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:syllaband" ],
          "numeric-id" : [ "953563484821901312" ],
          "name" : [ "Mathieu" ],
          "nickname" : [ "syllaband" ],
          "url" : [ "https://twitter.com/syllaband", "http://matand.dev" ],
          "published" : [ "2018-01-17T09:43:45+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Berlin, Deutschland" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1256914052460220417/O0PH-6CS.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Don't know if related, but your blog came up recently in a big tweet I can't find again asking for blogs whose writers have a \"unique voice\". Your was qualified as \"techno-anarchist\" as I recall, which is rather nifty.",
        "html" : "Don't know if related, but your blog came up recently in a big tweet I can't find again asking for blogs whose writers have a \"unique voice\". Your was qualified as \"techno-anarchist\" as I recall, which is rather nifty.\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/sonniesedge\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
