{
  "date" : "2020-08-15T11:18:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/KrakenFuego/status/1294513414333771776" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1294581173038075904" ],
    "name" : [ "Reply to https://twitter.com/KrakenFuego/status/1294513414333771776" ],
    "published" : [ "2020-08-15T11:18:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "Hey! Do you mean instead of having my base image downloading each of the gems and the Netlify CLI? Yes I did, but I was a bit lazy so didn't get round to it. I've since moved to Netlify's build https://www.jvt.me/posts/2020/05/27/migrate-netlify-deploy-gitlab/, although I'm wondering about going back to GitLab again to cut costs, and hopefully speed it up a little"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/08/f0au0",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1294513414333771776" ],
      "url" : [ "https://twitter.com/KrakenFuego/status/1294513414333771776" ],
      "published" : [ "2020-08-15T05:57:03+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:KrakenFuego" ],
          "numeric-id" : [ "875383331449339904" ],
          "name" : [ "Kraken" ],
          "nickname" : [ "KrakenFuego" ],
          "url" : [ "https://twitter.com/KrakenFuego", "https://krakenfuego.com" ],
          "published" : [ "2017-06-15T16:03:44+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "/usr/bin/bash" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/941032958869327872/bbSMf8tA.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "@JamieTanna saw your post on Gitlab+Nellify. Have you thought about having a docker image that is built on schedule and then using that rather than building each time from your base image?",
        "html" : "<a href=\"https://twitter.com/JamieTanna\">@JamieTanna</a> saw your post on Gitlab+Nellify. Have you thought about having a docker image that is built on schedule and then using that rather than building each time from your base image?"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
