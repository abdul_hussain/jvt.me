{
  "date" : "2020-08-23T00:07:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/holly/status/1297205688343695361" ],
    "name" : [ "Like of @holly's tweet" ],
    "published" : [ "2020-08-23T00:07:00+01:00" ],
    "category" : [ "cute" ],
    "like-of" : [ "https://twitter.com/holly/status/1297205688343695361" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/cjhqq",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1297205688343695361" ],
      "url" : [ "https://twitter.com/holly/status/1297205688343695361" ],
      "published" : [ "2020-08-22T16:15:11+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:holly" ],
          "numeric-id" : [ "7555262" ],
          "name" : [ "Holly Brockwell" ],
          "nickname" : [ "holly" ],
          "url" : [ "https://twitter.com/holly", "https://www.instagram.com/hollybrocks/" ],
          "published" : [ "2007-07-18T10:27:16+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "From Nottingham, in London" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1248671410714804225/A_9KJ1y8.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Look how thrilled Mabel is to be here 😂\n\nI LOVE HER",
        "html" : "<div style=\"white-space: pre\">Look how thrilled Mabel is to be here 😂\n\nI LOVE HER</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EgCZsp6XYAA5i-R.jpg" ]
    }
  },
  "tags" : [ "cute" ],
  "client_id" : "https://indigenous.realize.be"
}
