{
  "date" : "2020-08-20T22:57:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/RupertMyers/status/1296388400631644161" ],
    "name" : [ "Like of @RupertMyers's tweet" ],
    "published" : [ "2020-08-20T22:57:00+01:00" ],
    "like-of" : [ "https://twitter.com/RupertMyers/status/1296388400631644161" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/dzzu4",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1296388400631644161" ],
      "url" : [ "https://twitter.com/RupertMyers/status/1296388400631644161" ],
      "published" : [ "2020-08-20T10:07:35+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:RupertMyers" ],
          "numeric-id" : [ "21282561" ],
          "name" : [ "Rupert Myers" ],
          "nickname" : [ "RupertMyers" ],
          "url" : [ "https://twitter.com/RupertMyers", "http://rupertmyers.com", "https://www.ealaw.co.uk/our-people/rupert-myers/" ],
          "published" : [ "2009-02-19T08:01:41+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1273351335077953547/SCW78mv8.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "2020 in a single remarkable photo\n\n- by @noahberger3884 in California",
        "html" : "<div style=\"white-space: pre\">2020 in a single remarkable photo\n\n- by <a href=\"https://twitter.com/noahberger3884\">@noahberger3884</a> in California</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/Ef2yYFKWAAADtiE.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
