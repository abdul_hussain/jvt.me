{
  "date" : "2020-08-06T19:31:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/swyx/status/994610675430449152" ],
    "name" : [ "Like of @swyx's tweet" ],
    "published" : [ "2020-08-06T19:31:00+01:00" ],
    "like-of" : [ "https://twitter.com/swyx/status/994610675430449152" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/mrmzh",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:994610675430449152" ],
      "url" : [ "https://twitter.com/swyx/status/994610675430449152" ],
      "published" : [ "2018-05-10T16:10:38+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:swyx" ],
          "numeric-id" : [ "33521530" ],
          "name" : [ "shawn swyx wang" ],
          "nickname" : [ "swyx" ],
          "url" : [ "https://twitter.com/swyx", "http://swyx.io", "http://learninpublic.org" ],
          "published" : [ "2009-04-20T14:04:41+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Singapore 🇸🇬 " ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1201029434054041606/efWs7Lr9.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Interview: *Designing entire system architecture for Twitter in 30 minutes*\n\nJob: *Renaming classes to conform to CSS conventions for a week*",
        "html" : "<div style=\"white-space: pre\">Interview: *Designing entire system architecture for Twitter in 30 minutes*\n\nJob: *Renaming classes to conform to CSS conventions for a week*</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
