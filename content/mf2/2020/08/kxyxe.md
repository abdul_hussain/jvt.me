{
  "date" : "2020-08-01T10:47:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/timunken/status/1289328202490421249" ],
    "name" : [ "Like of @timunken's tweet" ],
    "published" : [ "2020-08-01T10:47:00+01:00" ],
    "like-of" : [ "https://twitter.com/timunken/status/1289328202490421249" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/kxyxe",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1289328202490421249" ],
      "url" : [ "https://twitter.com/timunken/status/1289328202490421249" ],
      "published" : [ "2020-07-31T22:32:52+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:timunken" ],
          "numeric-id" : [ "270635330" ],
          "name" : [ "Tim Unkenholz" ],
          "nickname" : [ "timunken" ],
          "url" : [ "https://twitter.com/timunken" ],
          "published" : [ "2011-03-23T00:05:48+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Queens, NY" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1087542808678989824/g4QPOV8r.jpg" ]
        }
      } ],
      "content" : [ "4 months into quarantine finally figured out the best angle for video calls" ],
      "photo" : [ "https://pbs.twimg.com/media/EeSch9hXoAMpv1J.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
