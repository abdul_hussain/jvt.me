{
  "date" : "2020-08-26T20:16:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.meetup.com/Tech-Nottingham/events/272818587/" ],
    "syndication" : [ "https://brid.gy/publish/meetup" ],
    "name" : [ "RSVP yes to https://www.meetup.com/Tech-Nottingham/events/272818587/" ],
    "published" : [ "2020-08-26T20:16:00+01:00" ],
    "event" : {
      "start" : [ "2020-10-12T18:30:00+01:00" ],
      "name" : [ "Tech Nottingham 12th October - The only constant in life is change" ],
      "end" : [ "2020-10-12T20:30:00+01:00" ],
      "location" : [ "Online" ],
      "url" : [ "https://www.meetup.com/Tech-Nottingham/events/272818587/" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/08/gzfrq",
  "client_id" : "https://indigenous.realize.be"
}
