{
  "date" : "2020-08-10T09:56:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/geeksareforlife/status/1292746293652197377" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1292749788996403202" ],
    "name" : [ "Reply to https://twitter.com/geeksareforlife/status/1292746293652197377" ],
    "published" : [ "2020-08-10T09:56:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "I look forward to it 🤓"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/08/jsx5t",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1292746293652197377" ],
      "url" : [ "https://twitter.com/geeksareforlife/status/1292746293652197377" ],
      "published" : [ "2020-08-10T08:55:09+00:00" ],
      "in-reply-to" : [ "https://twitter.com/JamieTanna/status/1292745767908790272" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:geeksareforlife" ],
          "numeric-id" : [ "47751632" ],
          "name" : [ "🏳️‍🌈 James Hayward" ],
          "nickname" : [ "geeksareforlife" ],
          "url" : [ "https://twitter.com/geeksareforlife", "https://www.geeksareforlife.com/", "http://pronoun.is/he" ],
          "published" : [ "2009-06-16T22:13:53+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Derby, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1273885821083373569/ewm5y1t1.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Thank you! One day, these likes will be in my site!",
        "html" : "Thank you! One day, these likes will be in my site!\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/JamieTanna\"></a>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
