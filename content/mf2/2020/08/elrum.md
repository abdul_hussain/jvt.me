{
  "date" : "2020-08-09T20:42:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "name" : [ "Like of @glitchliz's tweet" ],
    "published" : [ "2020-08-09T20:42:00+01:00" ],
    "like-of" : [ "https://twitter.com/glitchliz/status/1292475069466202112" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/elrum",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1292475069466202112" ],
      "url" : [ "https://twitter.com/glitchliz/status/1292475069466202112" ],
      "published" : [ "2020-08-09T14:57:24+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:glitchliz" ],
          "numeric-id" : [ "869676628586242048" ],
          "name" : [ "Liz 💜🌈" ],
          "nickname" : [ "glitchliz" ],
          "url" : [ "https://twitter.com/glitchliz" ],
          "published" : [ "2017-05-30T22:07:20+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "PNW" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1212157055307436032/74jr6Qzc.jpg" ]
        }
      } ],
      "content" : [ "\"The JIRA team uses a post-it wall to organize JIRA development\" is both terrifying and comforting news" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
