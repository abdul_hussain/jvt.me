{
  "date" : "2020-08-02T17:13:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/ZoeTomalin/status/1289854186213974017" ],
    "name" : [ "Like of @ZoeTomalin's tweet" ],
    "published" : [ "2020-08-02T17:13:00+01:00" ],
    "category" : [ "coronavirus" ],
    "like-of" : [ "https://twitter.com/ZoeTomalin/status/1289854186213974017" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/uzeqs",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1289854186213974017" ],
      "url" : [ "https://twitter.com/ZoeTomalin/status/1289854186213974017" ],
      "published" : [ "2020-08-02T09:22:56+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:ZoeTomalin" ],
          "numeric-id" : [ "2356286772" ],
          "name" : [ "Zoë Tomalin" ],
          "nickname" : [ "ZoeTomalin" ],
          "url" : [ "https://twitter.com/ZoeTomalin" ],
          "published" : [ "2014-02-22T12:35:23+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/821393031291109377/OfKg1CEz.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I’m so sorry",
        "html" : "I’m so sorry\n<a class=\"tag\" href=\"https://twitter.com/zoetomalin/status/1059397594630287360\">twitter.com/zoetomalin/sta…</a>"
      } ]
    },
    "children" : [ {
      "type" : [ "u-quotation-of", "h-cite" ],
      "properties" : {
        "uid" : [ "tag:twitter.com:1059397594630287360" ],
        "url" : [ "https://twitter.com/ZoeTomalin/status/1059397594630287360" ],
        "published" : [ "2018-11-05T10:50:43+00:00" ],
        "author" : [ {
          "type" : [ "h-card" ],
          "properties" : {
            "uid" : [ "tag:twitter.com:ZoeTomalin" ],
            "numeric-id" : [ "2356286772" ],
            "name" : [ "Zoë Tomalin" ],
            "nickname" : [ "ZoeTomalin" ],
            "url" : [ "https://twitter.com/ZoeTomalin" ],
            "published" : [ "2014-02-22T12:35:23+00:00" ],
            "photo" : [ "https://pbs.twimg.com/profile_images/821393031291109377/OfKg1CEz.jpg" ]
          }
        } ],
        "content" : [ "Make 👏 it 👏 socially 👏 acceptable 👏 to 👏 wear 👏 a 👏 plague 👏 doctor 👏 mask 👏 on 👏 the 👏 tube 👏" ]
      }
    }, {
      "type" : [ "h-cite" ],
      "properties" : {
        "name" : [ "twitter.com/zoetomalin/sta…" ],
        "url" : [ "https://twitter.com/zoetomalin/status/1059397594630287360" ]
      }
    } ]
  },
  "tags" : [ "coronavirus" ],
  "client_id" : "https://indigenous.realize.be"
}
