{
  "date" : "2020-08-27T10:56:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/Names_Alice/status/1298322354616107009" ],
    "name" : [ "Like of @Names_Alice's tweet" ],
    "published" : [ "2020-08-27T10:56:00+01:00" ],
    "like-of" : [ "https://twitter.com/Names_Alice/status/1298322354616107009" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/diowf",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1298322354616107009" ],
      "url" : [ "https://twitter.com/Names_Alice/status/1298322354616107009" ],
      "published" : [ "2020-08-25T18:12:25+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Names_Alice" ],
          "numeric-id" : [ "519607043" ],
          "name" : [ "Alice Yuan: Alice in Techland creator" ],
          "nickname" : [ "Names_Alice" ],
          "url" : [ "https://twitter.com/Names_Alice", "http://aliceintechland.org", "http://AliceinTechland.org" ],
          "published" : [ "2012-03-09T16:13:28+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1230080752508919808/bf6QcdmM.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "When I first started, felt bad for being different from what society thinks a “typical” programmer might be. \n\nI pushed myself to fit in, to read more tech blogs, attend conferences, go to hackathons because I felt like if I didn’t... then I wasn’t a good programmer. \n1/4",
        "html" : "<div style=\"white-space: pre\">When I first started, felt bad for being different from what society thinks a “typical” programmer might be. \n\nI pushed myself to fit in, to read more tech blogs, attend conferences, go to hackathons because I felt like if I didn’t... then I wasn’t a good programmer. \n1/4</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EgSQ4vpWAAI1wZ0.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
