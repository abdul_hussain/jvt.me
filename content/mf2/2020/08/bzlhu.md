{
  "date" : "2020-08-02T20:23:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/lewisking/status/1289586296671760396" ],
    "name" : [ "Like of @lewisking's tweet" ],
    "published" : [ "2020-08-02T20:23:00+01:00" ],
    "like-of" : [ "https://twitter.com/lewisking/status/1289586296671760396" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/bzlhu",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1289586296671760396" ],
      "url" : [ "https://twitter.com/lewisking/status/1289586296671760396" ],
      "published" : [ "2020-08-01T15:38:27+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:lewisking" ],
          "numeric-id" : [ "9911362" ],
          "name" : [ "Lewis King" ],
          "nickname" : [ "lewisking" ],
          "url" : [ "https://twitter.com/lewisking", "http://lewisking.co" ],
          "published" : [ "2007-11-03T10:12:03+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Chiang Mai, Thailand" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1280418315010052096/G8JfVzpy.jpg" ]
        }
      } ],
      "location" : [ {
        "type" : [ "h-card", "p-location" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:0031c4de11d579f1" ],
          "name" : [ "Nong Hoi, Thailand" ]
        }
      } ],
      "content" : [ "Don’t work with friends." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
