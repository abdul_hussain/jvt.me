{
  "date" : "2020-08-19T13:35:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/vicbergquist/status/1295831117325651969" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1296064516036407296" ],
    "name" : [ "Reply to https://twitter.com/vicbergquist/status/1295831117325651969" ],
    "published" : [ "2020-08-19T13:35:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "`git commit --fixup` is great for addressing code review comments and keeping a good commit history https://www.jvt.me/posts/2019/01/10/git-commit-fixup/"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/08/tifpv",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1295831117325651969" ],
      "url" : [ "https://twitter.com/vicbergquist/status/1295831117325651969" ],
      "published" : [ "2020-08-18T21:13:08+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:vicbergquist" ],
          "numeric-id" : [ "390661101" ],
          "name" : [ "Victoria 🦄" ],
          "nickname" : [ "vicbergquist" ],
          "url" : [ "https://twitter.com/vicbergquist", "https://github.com/vicbergquist" ],
          "published" : [ "2011-10-14T10:07:12+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Oslo, Norway" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1057780870722080768/Y7bSlrtp.jpg" ]
        }
      } ],
      "content" : [ "Most useful git command?" ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
