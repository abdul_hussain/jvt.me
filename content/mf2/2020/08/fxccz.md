{
  "date" : "2020-08-12T16:26:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamieTanna/status/1293572694164865027" ],
    "published" : [ "2020-08-12T16:26:00+01:00" ],
    "repost-of" : [ "https://twitter.com/TartanLlama/status/1293520940157018113" ],
    "category" : [ "diversity-and-inclusion" ]
  },
  "kind" : "reposts",
  "slug" : "2020/08/fxccz",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1293520940157018113" ],
      "url" : [ "https://twitter.com/TartanLlama/status/1293520940157018113" ],
      "published" : [ "2020-08-12T12:13:19+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:TartanLlama" ],
          "numeric-id" : [ "36158849" ],
          "name" : [ "Sy Brand" ],
          "nickname" : [ "TartanLlama" ],
          "url" : [ "https://twitter.com/TartanLlama" ],
          "published" : [ "2009-04-28T19:38:21+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Edinburgh, Scotland" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1289115924193312769/SuWeDoYw.jpg" ]
        }
      } ],
      "content" : [ "Inspiring! This Woman In Tech Printed All the Unsolicited Advice She's Got From People She's Smarter Than and Burned It All" ],
      "photo" : [ "https://pbs.twimg.com/media/EfOCcCTWoAAJJNS.jpg" ]
    }
  },
  "tags" : [ "diversity-and-inclusion" ],
  "client_id" : "https://indigenous.realize.be"
}
