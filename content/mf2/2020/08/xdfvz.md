{
  "date" : "2020-08-14T22:58:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/Spellacy/status/1294023919171756033" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1294394871055298560" ],
    "name" : [ "Reply to https://twitter.com/Spellacy/status/1294023919171756033" ],
    "published" : [ "2020-08-14T22:58:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "A fair few of us using static sites use https://webmention.io as our webmention server, and then you have the choice of how you want to display them. I used to do it on site builds, but moved to dynamically doing it with client-side JavaScript for a more up-to-date feel as mentioned in https://www.jvt.me/posts/2019/06/30/client-side-webmentions/"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/08/xdfvz",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1294023919171756033" ],
      "url" : [ "https://twitter.com/Spellacy/status/1294023919171756033" ],
      "published" : [ "2020-08-13T21:31:58+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Spellacy" ],
          "numeric-id" : [ "132282117" ],
          "name" : [ "Michael Spellacy (Spell) #BLM" ],
          "nickname" : [ "Spellacy" ],
          "url" : [ "https://twitter.com/Spellacy", "https://michaelspellacy.com", "http://a11y.info/@spellacy" ],
          "published" : [ "2010-04-12T20:20:38+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Allentown, PA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1111450414497583105/Pvxuon3K.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Any advice out there on best blog platform to use if you are looking to incorporate webmentions? Currently on Jekyll, and I dig the static site scene, but handling webmentions there is a pain in the butt. #indieweb",
        "html" : "Any advice out there on best blog platform to use if you are looking to incorporate webmentions? Currently on Jekyll, and I dig the static site scene, but handling webmentions there is a pain in the butt. <a href=\"https://twitter.com/search?q=%23indieweb\">#indieweb</a>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
