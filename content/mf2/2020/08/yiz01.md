{
  "date" : "2020-08-17T20:55:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/mathiasverraes/status/1295232583043821568" ],
    "name" : [ "Like of @mathiasverraes's tweet" ],
    "published" : [ "2020-08-17T20:55:00+01:00" ],
    "category" : [ "monolith" ],
    "like-of" : [ "https://twitter.com/mathiasverraes/status/1295232583043821568" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/yiz01",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1295232583043821568" ],
      "url" : [ "https://twitter.com/mathiasverraes/status/1295232583043821568" ],
      "published" : [ "2020-08-17T05:34:46+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:mathiasverraes" ],
          "numeric-id" : [ "17210045" ],
          "name" : [ "Mathias Verraes" ],
          "nickname" : [ "mathiasverraes" ],
          "url" : [ "https://twitter.com/mathiasverraes", "http://verraes.net" ],
          "published" : [ "2008-11-06T12:35:30+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Belgium" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/695594250315010050/SZXk1Z6b.jpg" ]
        }
      } ],
      "content" : [ "\"Monolith\" is the word you use when you want to blame the brokenness of your system on its size, instead of on 15 years of bad development practices." ]
    }
  },
  "tags" : [ "monolith" ],
  "client_id" : "https://indigenous.realize.be"
}
