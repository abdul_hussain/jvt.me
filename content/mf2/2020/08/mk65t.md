{
  "date" : "2020-08-11T22:08:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamieTanna/status/1293296413783273472" ],
    "published" : [ "2020-08-11T22:08:00+01:00" ],
    "repost-of" : [ "https://twitter.com/mr_goodwin/status/1293198041592233984" ],
    "category" : [ "mozilla" ]
  },
  "kind" : "reposts",
  "slug" : "2020/08/mk65t",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1293198041592233984" ],
      "url" : [ "https://twitter.com/mr_goodwin/status/1293198041592233984" ],
      "published" : [ "2020-08-11T14:50:14+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:mr_goodwin" ],
          "numeric-id" : [ "1481941" ],
          "name" : [ "Mark Goodwin" ],
          "nickname" : [ "mr_goodwin" ],
          "url" : [ "https://twitter.com/mr_goodwin" ],
          "published" : [ "2007-03-19T08:27:18+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottinghamshire, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/756072798603411457/SIyD6Fnf.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "\"Approximately 250\" of the best folks I've worked with are looking for work today.\n\nWhile it hurts to see friends lose their jobs, the growing fear of a web wholly controlled by big tech is more uncomfortable.\n\nThe world needs Mozilla. *You* need Mozilla. Use it or lose it.",
        "html" : "<div style=\"white-space: pre\">\"Approximately 250\" of the best folks I've worked with are looking for work today.\n\nWhile it hurts to see friends lose their jobs, the growing fear of a web wholly controlled by big tech is more uncomfortable.\n\nThe world needs Mozilla. *You* need Mozilla. Use it or lose it.</div>"
      } ]
    }
  },
  "tags" : [ "mozilla" ],
  "client_id" : "https://indigenous.realize.be"
}
