{
  "date" : "2020-08-15T16:11:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "name" : [ "Like of @tesseralis's tweet" ],
    "published" : [ "2020-08-15T16:11:00+01:00" ],
    "category" : [ "ethics" ],
    "like-of" : [ "https://twitter.com/tesseralis/status/1294407157434073090" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/ffxr1",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1294407157434073090" ],
      "url" : [ "https://twitter.com/tesseralis/status/1294407157434073090" ],
      "published" : [ "2020-08-14T22:54:49+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:tesseralis" ],
          "numeric-id" : [ "471768380" ],
          "name" : [ "Nat Alison" ],
          "nickname" : [ "tesseralis" ],
          "url" : [ "https://twitter.com/tesseralis", "http://www.tessera.li", "http://Ko-fi.com/tesseralis" ],
          "published" : [ "2012-01-23T07:18:57+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Koda Island" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1231468297649979392/t_7jReNz.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I would like everyone asking me \"what framework should I use instead?\" to remember\n\n💖 there is no ethical consumption under capitalism 💖",
        "html" : "<div style=\"white-space: pre\">I would like everyone asking me \"what framework should I use instead?\" to remember\n\n💖 there is no ethical consumption under capitalism 💖</div>"
      } ]
    }
  },
  "tags" : [ "ethics" ],
  "client_id" : "https://indigenous.realize.be"
}
