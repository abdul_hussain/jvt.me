{
  "date" : "2020-08-03T22:55:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/issyl0/status/1290389713338224651" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1290413573265596416" ],
    "name" : [ "Reply to https://twitter.com/issyl0/status/1290389713338224651" ],
    "published" : [ "2020-08-03T22:55:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "This is interesting. I wonder if there is some way to make the error message more clear what the root cause is in this case 🤔"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/08/bduvo",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1290389713338224651" ],
      "url" : [ "https://twitter.com/issyl0/status/1290389713338224651" ],
      "published" : [ "2020-08-03T20:50:56+00:00" ],
      "in-reply-to" : [ "https://twitter.com/edent/status/1290389187867353088" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:issyl0" ],
          "numeric-id" : [ "14662919" ],
          "name" : [ "Issy Long" ],
          "nickname" : [ "issyl0" ],
          "url" : [ "https://twitter.com/issyl0", "https://github.com/issyl0" ],
          "published" : [ "2008-05-05T18:29:14+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1139115590336077824/Q8u2O7OM.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "You've probably already tried this, but the signup page says \"If you want to submit a Merge Request, please contact us on IRC (Freenode #xiph ) or send an email to webmaster@xiph.org to have personal repositories enabled for your account.\" so it's probably worth asking there.",
        "html" : "You've probably already tried this, but the signup page says \"If you want to submit a Merge Request, please contact us on IRC (Freenode <a href=\"https://twitter.com/search?q=%23xiph\">#xiph</a> ) or send an email to webmaster@xiph.org to have personal repositories enabled for your account.\" so it's probably worth asking there.\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/edent\"></a>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
