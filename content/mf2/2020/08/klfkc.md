{
  "date" : "2020-08-11T08:32:20.077Z",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://springone.io" ],
    "name" : [ "RSVP yes to SpringOne" ],
    "published" : [ "2020-08-11T08:32:20.077Z" ],
    "event" : {
      "start" : [ "2020-09-02T13:00:00+0100" ],
      "name" : [ "SpringOne" ],
      "end" : [ "2020-09-03T22:00:00+0100" ],
      "url" : [ "https://springone.io" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/08/klfkc",
  "client_id" : "https://micropublish.net"
}
