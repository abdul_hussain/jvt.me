{
  "date" : "2020-08-27T17:08:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "name" : [ "Like of @EmilyKager's tweet" ],
    "published" : [ "2020-08-27T17:08:00+01:00" ],
    "like-of" : [ "https://twitter.com/EmilyKager/status/1299014634888933376" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/xinl7",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1299014634888933376" ],
      "url" : [ "https://twitter.com/EmilyKager/status/1299014634888933376" ],
      "published" : [ "2020-08-27T16:03:18+00:00" ],
      "in-reply-to" : [ "https://twitter.com/Co_Piloter/status/1299004407774760961" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:EmilyKager" ],
          "numeric-id" : [ "2347475803" ],
          "name" : [ "Emily Kager" ],
          "nickname" : [ "EmilyKager" ],
          "url" : [ "https://twitter.com/EmilyKager", "https://www.emilykager.com/" ],
          "published" : [ "2014-02-16T21:34:12+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Oakland and SF " ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1296572390252716034/jTxFoueZ.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "We're constantly adding new features! Is there anything in particular you're waiting for?",
        "html" : "We're constantly adding new features! Is there anything in particular you're waiting for?\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/Co_Piloter\"></a>\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/firefox\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
