{
  "date" : "2020-08-29T22:30:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/kefimochi/status/1299712069009170432" ],
    "name" : [ "Like of @kefimochi's tweet" ],
    "published" : [ "2020-08-29T22:30:00+01:00" ],
    "like-of" : [ "https://twitter.com/kefimochi/status/1299712069009170432" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/h1u4d",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1299712069009170432" ],
      "url" : [ "https://twitter.com/kefimochi/status/1299712069009170432" ],
      "published" : [ "2020-08-29T14:14:39+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:kefimochi" ],
          "numeric-id" : [ "1009594769817796608" ],
          "name" : [ "Cake is Kate. Always has been. 💫" ],
          "nickname" : [ "kefimochi" ],
          "url" : [ "https://twitter.com/kefimochi", "http://github.com/kefimochi", "http://etsy.com/shop/KefiStore", "http://instagram.com/kefimochi" ],
          "published" : [ "2018-06-21T00:32:05+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Bay Area, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1276011526625746945/gYXb7_9-.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "You know what's better than self-documenting code?\n\nDocumented code.",
        "html" : "<div style=\"white-space: pre\">You know what's better than self-documenting code?\n\nDocumented code.</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
