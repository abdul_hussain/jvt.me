{
  "date" : "2020-08-29T10:19:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/WillWiles/status/1299277659613036549" ],
    "name" : [ "Like of @WillWiles's tweet" ],
    "published" : [ "2020-08-29T10:19:00+01:00" ],
    "like-of" : [ "https://twitter.com/WillWiles/status/1299277659613036549" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/7ppx2",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1299277659613036549" ],
      "url" : [ "https://twitter.com/WillWiles/status/1299277659613036549" ],
      "published" : [ "2020-08-28T09:28:28+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:WillWiles" ],
          "numeric-id" : [ "20784548" ],
          "name" : [ "Will Wiles" ],
          "nickname" : [ "WillWiles" ],
          "url" : [ "https://twitter.com/WillWiles", "http://tinyletter.com/willwiles" ],
          "published" : [ "2009-02-13T16:48:01+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/78283695/twitterpic.jpg" ]
        }
      } ],
      "content" : [ "\"You must resume your long, crowded commute so you can buy the coffees we've been using as the reason you can't afford buy a house close to your work\" really sounds like a house of cards collapsing" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
