{
  "date" : "2020-08-31T22:26:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamieTanna/status/1300546861405790208" ],
    "published" : [ "2020-08-31T22:26:00+01:00" ],
    "repost-of" : [ "https://twitter.com/CassidyJames/status/1300453803918077953" ]
  },
  "kind" : "reposts",
  "slug" : "2020/08/j0sop",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1300453803918077953" ],
      "url" : [ "https://twitter.com/CassidyJames/status/1300453803918077953" ],
      "published" : [ "2020-08-31T15:22:02+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:CassidyJames" ],
          "numeric-id" : [ "17333065" ],
          "name" : [ "Cassidy James Blaede 🏡 at home" ],
          "nickname" : [ "CassidyJames" ],
          "url" : [ "https://twitter.com/CassidyJames", "https://cassidyjames.com" ],
          "published" : [ "2008-11-12T05:47:21+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Denver, CO" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1300448488405241856/DAGd7Z_W.jpg" ]
        }
      } ],
      "content" : [ "Web developers: when you say, “this browser does not support our site,” what you REALLY mean is that you don’t support the browser. Don’t turn it around on the browser/user because you chose not to stick to universally-supported standards, or worse, are doing user agent sniffing." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
