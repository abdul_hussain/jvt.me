{
  "date" : "2020-08-19T22:09:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/zkat__/status/1296130087994052613" ],
    "name" : [ "Like of @zkat__'s tweet" ],
    "published" : [ "2020-08-19T22:09:00+01:00" ],
    "category" : [ "code-review", "code-style" ],
    "like-of" : [ "https://twitter.com/zkat__/status/1296130087994052613" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/uehca",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1296130087994052613" ],
      "url" : [ "https://twitter.com/zkat__/status/1296130087994052613" ],
      "published" : [ "2020-08-19T17:01:08+00:00" ],
      "in-reply-to" : [ "https://twitter.com/zkat__/status/1296130087314534401" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:zkat__" ],
          "numeric-id" : [ "2978661890" ],
          "name" : [ "Kat Marchán 😷" ],
          "nickname" : [ "zkat__" ],
          "url" : [ "https://twitter.com/zkat__", "https://github.com/zkat" ],
          "published" : [ "2015-01-13T06:07:49+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "East Bay Area, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1290148984044941313/iskYvF-D.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "He brought it up because he noticed a stark contrast between what I comment on and what some others comment on\n\nMy 2c is that death by 1000 cuts is never worth it for style comments, and they're usually a waste of everyone's time\n\nAutomate format/linting. You owe it to each other",
        "html" : "<div style=\"white-space: pre\">He brought it up because he noticed a stark contrast between what I comment on and what some others comment on\n\nMy 2c is that death by 1000 cuts is never worth it for style comments, and they're usually a waste of everyone's time\n\nAutomate format/linting. You owe it to each other</div>"
      } ]
    }
  },
  "tags" : [ "code-review", "code-style" ],
  "client_id" : "https://indigenous.realize.be"
}
