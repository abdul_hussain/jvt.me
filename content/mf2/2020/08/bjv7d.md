{
  "date" : "2020-08-10T11:00:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/kieranmch/status/1292758598813208581" ],
    "name" : [ "Like of @kieranmch's tweet" ],
    "published" : [ "2020-08-10T11:00:00+01:00" ],
    "like-of" : [ "https://twitter.com/kieranmch/status/1292758598813208581" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/bjv7d",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1292758598813208581" ],
      "url" : [ "https://twitter.com/kieranmch/status/1292758598813208581" ],
      "published" : [ "2020-08-10T09:44:02+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:kieranmch" ],
          "numeric-id" : [ "50420707" ],
          "name" : [ "Kieran McHugh" ],
          "nickname" : [ "kieranmch" ],
          "url" : [ "https://twitter.com/kieranmch", "https://kieran.engineer" ],
          "published" : [ "2009-06-24T20:13:29+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1170973779675275264/cCZMxVEg.jpg" ]
        }
      } ],
      "content" : [ "I’m the only person in my company Slack and I still can’t stop checking it every 5 minutes why am I like this" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
