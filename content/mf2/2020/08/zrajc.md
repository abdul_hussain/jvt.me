{
  "date" : "2020-08-23T10:27:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/djbaskin/status/1297431114294779910" ],
    "name" : [ "Like of @djbaskin's tweet" ],
    "published" : [ "2020-08-23T10:27:00+01:00" ],
    "like-of" : [ "https://twitter.com/djbaskin/status/1297431114294779910" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/zrajc",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1297431114294779910" ],
      "url" : [ "https://twitter.com/djbaskin/status/1297431114294779910" ],
      "published" : [ "2020-08-23T07:10:57+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:djbaskin" ],
          "numeric-id" : [ "55662470" ],
          "name" : [ "Danielle Baskin" ],
          "nickname" : [ "djbaskin" ],
          "url" : [ "https://twitter.com/djbaskin", "https://www.daniellebaskin.com", "http://dialup.com" ],
          "published" : [ "2009-07-10T21:00:48+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "San Francisco, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1292039027705577472/BiIMhH84.jpg" ]
        }
      } ],
      "content" : [ "Stumbled into a website tonight called UberBeats that I didn’t think was real, but I signed up anyway. 2.5 hrs later I get a surprise text! A DJ wearing a branded UberBeats mask pulls up in a car, smoke machine in their trunk, and plays a ~10m set. Best pandemic dance party. 🎵💖" ],
      "photo" : [ "https://pbs.twimg.com/media/EgFj-7uVoAAndN2.jpg", "https://pbs.twimg.com/media/EgFj__BUcAAwjp1.jpg", "https://pbs.twimg.com/media/EgFkBv6UYAAFU_-.jpg", "https://pbs.twimg.com/media/EgFkDW0U8AAZJQJ.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
