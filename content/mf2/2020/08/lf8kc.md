{
  "date" : "2020-08-30T21:01:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/ICooper/status/1300149972625694722" ],
    "name" : [ "Like of @ICooper's tweet" ],
    "published" : [ "2020-08-30T21:01:00+01:00" ],
    "like-of" : [ "https://twitter.com/ICooper/status/1300149972625694722" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/lf8kc",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1300149972625694722" ],
      "url" : [ "https://twitter.com/ICooper/status/1300149972625694722" ],
      "published" : [ "2020-08-30T19:14:43+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:ICooper" ],
          "numeric-id" : [ "11872932" ],
          "name" : [ "Ian Cooper" ],
          "nickname" : [ "ICooper" ],
          "url" : [ "https://twitter.com/ICooper", "https://medium.com/@ICooper" ],
          "published" : [ "2008-01-05T12:26:00+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/807278466668367872/2JWCn6d_.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Writing code the \"wrong way\" to use an example of bad practice in a workshop is really hard. \n\nI am fighting my own instincts and asking \"how would someone using this approach do this?\"",
        "html" : "<div style=\"white-space: pre\">Writing code the \"wrong way\" to use an example of bad practice in a workshop is really hard. \n\nI am fighting my own instincts and asking \"how would someone using this approach do this?\"</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
