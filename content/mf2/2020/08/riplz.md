{
  "date" : "2020-08-24T18:29:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/CarolSaysThings/status/1297822553520644096" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1297955393914703880" ],
    "name" : [ "Reply to https://twitter.com/CarolSaysThings/status/1297822553520644096" ],
    "published" : [ "2020-08-24T18:29:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "Can't remember if it was only last week, but did you see the Gatsby stuff?"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/08/riplz",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1297822553520644096" ],
      "url" : [ "https://twitter.com/CarolSaysThings/status/1297822553520644096" ],
      "video" : [ "https://video.twimg.com/tweet_video/EgLKtG2XkAE3Tca.mp4" ],
      "published" : [ "2020-08-24T09:06:23+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:CarolSaysThings" ],
          "numeric-id" : [ "36382927" ],
          "name" : [ "Carol 🌻" ],
          "nickname" : [ "CarolSaysThings" ],
          "url" : [ "https://twitter.com/CarolSaysThings", "https://carolgilabert.me/" ],
          "published" : [ "2009-04-29T15:22:13+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "🇧🇷🇪🇸🇬🇧 · Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1295718060301713408/XcKwL0Z8.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I’m back after a week and a bit off. Please tell me all the goss from when I was gone, please 👀\n\nWhat did I miss?",
        "html" : "<div style=\"white-space: pre\">I’m back after a week and a bit off. Please tell me all the goss from when I was gone, please 👀\n\nWhat did I miss?</div>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
