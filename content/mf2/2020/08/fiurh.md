{
  "date" : "2020-08-10T21:07:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/pascaldoesgo/status/1292887098219991041" ],
    "name" : [ "Like of @pascaldoesgo's tweet" ],
    "published" : [ "2020-08-10T21:07:00+01:00" ],
    "category" : [ "politics", "coronavirus" ],
    "like-of" : [ "https://twitter.com/pascaldoesgo/status/1292887098219991041" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/fiurh",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1292887098219991041" ],
      "url" : [ "https://twitter.com/pascaldoesgo/status/1292887098219991041" ],
      "published" : [ "2020-08-10T18:14:39+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:pascaldoesgo" ],
          "numeric-id" : [ "1003529600901767168" ],
          "name" : [ "Pascal Dennerly" ],
          "nickname" : [ "pascaldoesgo" ],
          "url" : [ "https://twitter.com/pascaldoesgo", "http://github.com/dnnrly" ],
          "published" : [ "2018-06-04T06:51:16+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1199757075561222147/eYBkMUrZ.jpg" ]
        }
      } ],
      "content" : [ "If it's so imperative to control the virus, why aren't the government putting their marketing budget to tell us about, perhaps, the most successful strategy for doing so: wearing a mask?" ]
    }
  },
  "tags" : [ "politics", "coronavirus" ],
  "client_id" : "https://indigenous.realize.be"
}
