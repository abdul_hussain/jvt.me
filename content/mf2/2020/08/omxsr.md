{
  "date" : "2020-08-05T22:58:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/britnorcodes/status/1291096346045353987" ],
    "name" : [ "Like of @britnorcodes's tweet" ],
    "published" : [ "2020-08-05T22:58:00+01:00" ],
    "category" : [ "blogging" ],
    "like-of" : [ "https://twitter.com/britnorcodes/status/1291096346045353987" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/omxsr",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1291096346045353987" ],
      "url" : [ "https://twitter.com/britnorcodes/status/1291096346045353987" ],
      "published" : [ "2020-08-05T19:38:50+00:00" ],
      "in-reply-to" : [ "https://twitter.com/chris_codes1/status/1291095790841212928" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:britnorcodes" ],
          "numeric-id" : [ "1124460448936615936" ],
          "name" : [ "Em 💫" ],
          "nickname" : [ "britnorcodes" ],
          "url" : [ "https://twitter.com/britnorcodes", "http://britnor.codes" ],
          "published" : [ "2019-05-03T23:47:17+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1283537245488721920/1KWEsynR.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Ah nice! Well I'm actually going to write a blog post about how I implemented it 👀",
        "html" : "Ah nice! Well I'm actually going to write a blog post about how I implemented it 👀\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/ThePracticalDev\"></a>\n<a class=\"u-mention\" aria-hidden=\"true\" href=\"https://twitter.com/chris_codes1\"></a>"
      } ]
    }
  },
  "tags" : [ "blogging" ],
  "client_id" : "https://indigenous.realize.be"
}
