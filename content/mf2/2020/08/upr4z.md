{
  "date" : "2020-08-13T22:43:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/danielchatfield/status/1294021786586230785" ],
    "name" : [ "Like of @danielchatfield's tweet" ],
    "published" : [ "2020-08-13T22:43:00+01:00" ],
    "category" : [ "nature" ],
    "like-of" : [ "https://twitter.com/danielchatfield/status/1294021786586230785" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/upr4z",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1294021786586230785" ],
      "url" : [ "https://twitter.com/danielchatfield/status/1294021786586230785" ],
      "published" : [ "2020-08-13T21:23:30+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:danielchatfield" ],
          "numeric-id" : [ "113455456" ],
          "name" : [ "Daniel Chatfield" ],
          "nickname" : [ "danielchatfield" ],
          "url" : [ "https://twitter.com/danielchatfield" ],
          "published" : [ "2010-02-11T20:34:24+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "United Kingdom" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/378800000411284589/c038cad4d3c016c4448ec0c772bef678.jpeg" ]
        }
      } ],
      "content" : [ "The sky is literally blue right now - what even is this?" ],
      "photo" : [ "https://pbs.twimg.com/media/EfVJ9CIWoAEFyUM.jpg" ]
    }
  },
  "tags" : [ "nature" ],
  "client_id" : "https://indigenous.realize.be"
}
