{
  "date" : "2020-08-26T21:16:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/QuinnyPig/status/1298693196373991427" ],
    "name" : [ "Like of @QuinnyPig's tweet" ],
    "published" : [ "2020-08-26T21:16:00+01:00" ],
    "category" : [ "interviewing" ],
    "like-of" : [ "https://twitter.com/QuinnyPig/status/1298693196373991427" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/uc8zn",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1298693196373991427" ],
      "url" : [ "https://twitter.com/QuinnyPig/status/1298693196373991427" ],
      "published" : [ "2020-08-26T18:46:01+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:QuinnyPig" ],
          "numeric-id" : [ "97114171" ],
          "name" : [ "HydroxyCoreyQuinn" ],
          "nickname" : [ "QuinnyPig" ],
          "url" : [ "https://twitter.com/QuinnyPig", "http://www.duckbillgroup.com", "http://lastweekinaws.com/t/" ],
          "published" : [ "2009-12-16T02:19:14+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "San Francisco, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1247254348498743300/vTTJS5wW.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "\"We hire the best and the brightest engineers!\"\n\nNow then, to get the job you'll be graded on a series of skills that will not be touched upon ever again for your entire tenure here; some of our interview questions technically qualify as hazing.",
        "html" : "<div style=\"white-space: pre\">\"We hire the best and the brightest engineers!\"\n\nNow then, to get the job you'll be graded on a series of skills that will not be touched upon ever again for your entire tenure here; some of our interview questions technically qualify as hazing.</div>"
      } ]
    }
  },
  "tags" : [ "interviewing" ],
  "client_id" : "https://indigenous.realize.be"
}
