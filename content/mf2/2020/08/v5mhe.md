{
  "date" : "2020-08-19T11:08:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/CSMFHT/status/1296005987157958656" ],
    "name" : [ "Like of @CSMFHT's tweet" ],
    "published" : [ "2020-08-19T11:08:00+01:00" ],
    "like-of" : [ "https://twitter.com/CSMFHT/status/1296005987157958656" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/v5mhe",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1296005987157958656" ],
      "url" : [ "https://twitter.com/CSMFHT/status/1296005987157958656" ],
      "published" : [ "2020-08-19T08:48:00+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:CSMFHT" ],
          "numeric-id" : [ "1230431996163178497" ],
          "name" : [ "Classical Studies Memes for Hellenistic Teens" ],
          "nickname" : [ "CSMFHT" ],
          "url" : [ "https://twitter.com/CSMFHT", "http://facebook.com/CSMFHT" ],
          "published" : [ "2020-02-20T10:00:46+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "New Zealand" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1230432197326147584/NOEqWdEO.jpg" ]
        }
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EfxWk4EUwAAm9qx.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
