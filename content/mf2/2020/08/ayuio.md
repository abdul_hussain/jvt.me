{
  "date" : "2020-08-29T09:40:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "name" : [ "Like of @jemelehill's tweet" ],
    "published" : [ "2020-08-29T09:40:00+01:00" ],
    "like-of" : [ "https://twitter.com/jemelehill/status/1299535144156499968" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/ayuio",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1299535144156499968" ],
      "url" : [ "https://twitter.com/jemelehill/status/1299535144156499968" ],
      "published" : [ "2020-08-29T02:31:37+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:jemelehill" ],
          "numeric-id" : [ "35586563" ],
          "name" : [ "Jemele Hill" ],
          "nickname" : [ "jemelehill" ],
          "url" : [ "https://twitter.com/jemelehill", "http://jemelestore.com" ],
          "published" : [ "2009-04-26T22:38:15+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1001715864926814209/UBT5Pg48.jpg" ]
        }
      } ],
      "content" : [ "Chadwick was diagnosed with colon cancer four years ago, per reports. During that time, he gave us Civil War, Marshall, Black Panther, Infinity War, Endgame, 21 Bridges and Da 5 Bloods. Lord only knows what he was going through on a daily basis." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
