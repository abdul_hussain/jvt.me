{
  "date" : "2020-08-22T11:55:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/_elletownsend/status/1297103348802424832" ],
    "name" : [ "Like of @_elletownsend's tweet" ],
    "published" : [ "2020-08-22T11:55:00+01:00" ],
    "category" : [ "personal-website" ],
    "like-of" : [ "https://twitter.com/_elletownsend/status/1297103348802424832" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/nrppt",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1297103348802424832" ],
      "url" : [ "https://twitter.com/_elletownsend/status/1297103348802424832" ],
      "published" : [ "2020-08-22T09:28:32+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:_elletownsend" ],
          "numeric-id" : [ "4308006796" ],
          "name" : [ "Elle Townsend🌸" ],
          "nickname" : [ "_elletownsend" ],
          "url" : [ "https://twitter.com/_elletownsend", "https://www.elletownsend.co.uk" ],
          "published" : [ "2015-11-28T14:28:32+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1237757944089006086/67Kjy-LA.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I did it! I migrated my blog to my own website 🥳 and I wrote a blog post on how I did it (using Eleventy) elletownsend.co.uk/blog/posts/blo…",
        "html" : "I did it! I migrated my blog to my own website 🥳 and I wrote a blog post on how I did it (using Eleventy) <a href=\"http://www.elletownsend.co.uk/blog/posts/blog-with-eleventy/\">elletownsend.co.uk/blog/posts/blo…</a>"
      } ]
    }
  },
  "tags" : [ "personal-website" ],
  "client_id" : "https://indigenous.realize.be"
}
