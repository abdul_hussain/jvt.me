{
  "date" : "2020-08-03T22:52:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/CuriousUkTelly/status/1290396306918256641" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1290413529892347904" ],
    "name" : [ "Reply to https://twitter.com/CuriousUkTelly/status/1290396306918256641" ],
    "published" : [ "2020-08-03T22:52:00+01:00" ],
    "category" : [ "the-expanse" ],
    "content" : [ {
      "html" : "",
      "value" : "<a href=\"/tags/the-expanse/\">#TheExpanse</a>!"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/08/ipuqt",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1290396306918256641" ],
      "url" : [ "https://twitter.com/CuriousUkTelly/status/1290396306918256641" ],
      "published" : [ "2020-08-03T21:17:08+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:CuriousUkTelly" ],
          "numeric-id" : [ "939506312" ],
          "name" : [ "CuriousBritishTelly" ],
          "nickname" : [ "CuriousUkTelly" ],
          "url" : [ "https://twitter.com/CuriousUkTelly", "http://www.curiousbritishtelly.co.uk" ],
          "published" : [ "2012-11-10T17:15:25+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Britain" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1264210999953031168/_IRIOLOh.png" ]
        }
      } ],
      "content" : [ "Best sci-fi show that isn't Doctor Who?" ]
    }
  },
  "tags" : [ "the-expanse" ],
  "client_id" : "https://indigenous.realize.be"
}
