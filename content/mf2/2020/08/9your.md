{
  "date" : "2020-08-15T16:20:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/kvlly/status/1294622952286294016" ],
    "name" : [ "Like of @kvlly's tweet" ],
    "published" : [ "2020-08-15T16:20:00+01:00" ],
    "category" : [ "coronavirus" ],
    "like-of" : [ "https://twitter.com/kvlly/status/1294622952286294016" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/9your",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1294622952286294016" ],
      "url" : [ "https://twitter.com/kvlly/status/1294622952286294016" ],
      "published" : [ "2020-08-15T13:12:19+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:kvlly" ],
          "numeric-id" : [ "123543103" ],
          "name" : [ "Kelly Vaughn 🐞" ],
          "nickname" : [ "kvlly" ],
          "url" : [ "https://twitter.com/kvlly", "http://kvlly.com", "http://startfreelancing.today", "http://kvlly.com/patreon", "http://shopkvlly.com" ],
          "published" : [ "2010-03-16T12:15:39+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Atlanta, GA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1273615233018482688/7yvtWlBt.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Me: I’m going to skip conferences in 2020 to focus my time and efforts elsewhere\n\n2020: lol yeah it’s “your decision” okay",
        "html" : "<div style=\"white-space: pre\">Me: I’m going to skip conferences in 2020 to focus my time and efforts elsewhere\n\n2020: lol yeah it’s “your decision” okay</div>"
      } ]
    }
  },
  "tags" : [ "coronavirus" ],
  "client_id" : "https://indigenous.realize.be"
}
