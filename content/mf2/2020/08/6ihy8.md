{
  "date" : "2020-08-26T07:52:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/arshia__/status/1298378064712404996" ],
    "name" : [ "Like of @arshia__'s tweet" ],
    "published" : [ "2020-08-26T07:52:00+01:00" ],
    "category" : [ "remote-work" ],
    "like-of" : [ "https://twitter.com/arshia__/status/1298378064712404996" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/6ihy8",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1298378064712404996" ],
      "url" : [ "https://twitter.com/arshia__/status/1298378064712404996" ],
      "published" : [ "2020-08-25T21:53:47+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:arshia__" ],
          "numeric-id" : [ "562291920" ],
          "name" : [ "arshia" ],
          "nickname" : [ "arshia__" ],
          "url" : [ "https://twitter.com/arshia__" ],
          "published" : [ "2012-04-24T17:44:32+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "toronto" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1286742256213426177/4VYGAqoy.jpg" ]
        }
      } ],
      "content" : [ "screen sharing your entire desktop instead of just one window is a radical act of workplace intimacy" ]
    }
  },
  "tags" : [ "remote-work" ],
  "client_id" : "https://indigenous.realize.be"
}
