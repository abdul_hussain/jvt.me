{
  "date" : "2020-08-27T21:19:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/TheSims/status/1299058669687107592" ],
    "name" : [ "Like of @TheSims's tweet" ],
    "published" : [ "2020-08-27T21:19:00+01:00" ],
    "category" : [ "gaming", "star-wars" ],
    "like-of" : [ "https://twitter.com/TheSims/status/1299058669687107592" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/mrhqc",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1299058669687107592" ],
      "url" : [ "https://twitter.com/TheSims/status/1299058669687107592" ],
      "video" : [ "https://video.twimg.com/amplify_video/1299045594086989824/vid/720x720/soMHrENshapydXY4.mp4?tag=13" ],
      "published" : [ "2020-08-27T18:58:16+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:TheSims" ],
          "numeric-id" : [ "16582027" ],
          "name" : [ "The Sims" ],
          "nickname" : [ "TheSims" ],
          "url" : [ "https://twitter.com/TheSims", "http://www.TheSims.com" ],
          "published" : [ "2008-10-03T19:03:26+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Home" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1271427722527551488/_l8jRcx4.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "In a galaxy far, far away, deep into the Outer Rim, the world of Batuu awaits your Sim. The Sims 4 Star Wars Journey to Batuu Game Pack is available September 8! Create your unique #TheSimsxStarWars story: youtu.be/A4swl1VObu8\n#JourneyToBatuu",
        "html" : "<div style=\"white-space: pre\">In a galaxy far, far away, deep into the Outer Rim, the world of Batuu awaits your Sim. The Sims 4 Star Wars Journey to Batuu Game Pack is available September 8! Create your unique <a href=\"https://twitter.com/search?q=%23TheSimsxStarWars\">#TheSimsxStarWars</a> story: <a href=\"https://youtu.be/A4swl1VObu8\">youtu.be/A4swl1VObu8</a>\n<a href=\"https://twitter.com/search?q=%23JourneyToBatuu\">#JourneyToBatuu</a></div>"
      } ]
    }
  },
  "tags" : [ "gaming", "star-wars" ],
  "client_id" : "https://indigenous.realize.be"
}
