{
  "date" : "2020-08-11T22:02:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/jd0dd/status/1293285899023855616" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1293294046262288390" ],
    "name" : [ "Reply to https://twitter.com/jd0dd/status/1293285899023855616" ],
    "published" : [ "2020-08-11T22:02:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "Glad to hear I was able to help!"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/08/vg7q6",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1293285899023855616" ],
      "url" : [ "https://twitter.com/jd0dd/status/1293285899023855616" ],
      "published" : [ "2020-08-11T20:39:21+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:jd0dd" ],
          "numeric-id" : [ "43101935" ],
          "name" : [ "Jonathan Dodd" ],
          "nickname" : [ "jd0dd" ],
          "url" : [ "https://twitter.com/jd0dd", "http://about.me/jonathan.dodd" ],
          "published" : [ "2009-05-28T12:47:06+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "St Neots, Cambridgeshire" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1176759351505240064/UAZyvEB5.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Shout out to @JamieTanna for this blog post on skipping Jenkins builds on branch indexing jvt.me/posts/2020/02/…. Has been bugging me for ages this, so cheers pal! 👍",
        "html" : "Shout out to <a href=\"https://twitter.com/JamieTanna\">@JamieTanna</a> for this blog post on skipping Jenkins builds on branch indexing <a href=\"https://www.jvt.me/posts/2020/02/23/jenkins-multibranch-skip-branch-index/\">jvt.me/posts/2020/02/…</a>. Has been bugging me for ages this, so cheers pal! 👍"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
