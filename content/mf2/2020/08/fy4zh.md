{
  "date" : "2020-08-27T21:31:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/kadikraman/status/1299079038506602496" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1299085091717689344" ],
    "name" : [ "Reply to https://twitter.com/kadikraman/status/1299079038506602496" ],
    "published" : [ "2020-08-27T21:31:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "I did three in a row today on the same PR 😅"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/08/fy4zh",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1299079038506602496" ],
      "url" : [ "https://twitter.com/kadikraman/status/1299079038506602496" ],
      "published" : [ "2020-08-27T20:19:13+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:kadikraman" ],
          "numeric-id" : [ "914620160" ],
          "name" : [ "Kadi Kraman" ],
          "nickname" : [ "kadikraman" ],
          "url" : [ "https://twitter.com/kadikraman" ],
          "published" : [ "2012-10-30T13:16:09+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1180146316267143173/-2ST_BYP.jpg" ]
        }
      } ],
      "content" : [ "Seeing force pushes on GitHub PRs is the worst - the whole point is I want to override the commit, not make it OMG SO SUPER OBVIOUS I MADE A MISTAKE 😅" ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
