{
  "date" : "2020-08-28T20:21:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/forrestbrazeal/status/1299375964087083008" ],
    "name" : [ "Like of @forrestbrazeal's tweet" ],
    "published" : [ "2020-08-28T20:21:00+01:00" ],
    "like-of" : [ "https://twitter.com/forrestbrazeal/status/1299375964087083008" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/npy5s",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1299375964087083008" ],
      "url" : [ "https://twitter.com/forrestbrazeal/status/1299375964087083008" ],
      "published" : [ "2020-08-28T15:59:05+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:forrestbrazeal" ],
          "numeric-id" : [ "3239330680" ],
          "name" : [ "Forrest Brazeal" ],
          "nickname" : [ "forrestbrazeal" ],
          "url" : [ "https://twitter.com/forrestbrazeal", "http://forrestbrazeal.com", "https://amzn.to/3enpfYw" ],
          "published" : [ "2015-05-06T18:59:20+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Charlotte, NC" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1259236310990061575/8poErtri.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "finally learned what SRE stands for \n\n\"Sysadmin (Really Expensive)\"",
        "html" : "<div style=\"white-space: pre\">finally learned what SRE stands for \n\n\"Sysadmin (Really Expensive)\"</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
