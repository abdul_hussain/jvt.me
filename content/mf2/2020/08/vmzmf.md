{
  "date" : "2020-08-25T11:32:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "name" : [ "Like of @AnimalsWorId's tweet" ],
    "published" : [ "2020-08-25T11:32:00+01:00" ],
    "category" : [ "cute" ],
    "like-of" : [ "https://twitter.com/AnimalsWorId/status/1298053626745614336" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/vmzmf",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1298053626745614336" ],
      "url" : [ "https://twitter.com/AnimalsWorId/status/1298053626745614336" ],
      "video" : [ "https://video.twimg.com/ext_tw_video/1298053502220898310/pu/vid/540x960/SRjoR2JILquEmmeZ.mp4?tag=10" ],
      "published" : [ "2020-08-25T00:24:35+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:AnimalsWorId" ],
          "numeric-id" : [ "1186257814953480192" ],
          "name" : [ "Nature & Animals 🌴" ],
          "nickname" : [ "AnimalsWorId" ],
          "url" : [ "https://twitter.com/AnimalsWorId" ],
          "published" : [ "2019-10-21T12:28:07+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "🌍" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1289880617807560706/XZHuHLNb.jpg" ]
        }
      } ],
      "content" : [ "Today I learned that the Red Panda is so unique, it has no close relatives. It's the only living species in its genus and family." ]
    }
  },
  "tags" : [ "cute" ],
  "client_id" : "https://indigenous.realize.be"
}
