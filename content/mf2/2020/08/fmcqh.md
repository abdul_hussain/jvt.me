{
  "date" : "2020-08-15T15:59:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/AliceAvizandum/status/1294383567452209152" ],
    "name" : [ "Like of @AliceAvizandum's tweet" ],
    "published" : [ "2020-08-15T15:59:00+01:00" ],
    "like-of" : [ "https://twitter.com/AliceAvizandum/status/1294383567452209152" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/fmcqh",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1294383567452209152" ],
      "url" : [ "https://twitter.com/AliceAvizandum/status/1294383567452209152" ],
      "published" : [ "2020-08-14T21:21:05+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:AliceAvizandum" ],
          "numeric-id" : [ "20770892" ],
          "name" : [ "cop anthropologist" ],
          "nickname" : [ "AliceAvizandum" ],
          "url" : [ "https://twitter.com/AliceAvizandum", "http://patreon.com/AliceAvizandum" ],
          "published" : [ "2009-02-13T13:48:28+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Glasgow, Scotland" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1273920517997760522/C31ubUjI.jpg" ]
        }
      } ],
      "content" : [ "can I interest you in the fact that if you get promoted as a garbageman enough times in New York City you get/have to dress like a fake general" ],
      "photo" : [ "https://pbs.twimg.com/media/EfaS_gpWoAY3B92.jpg", "https://pbs.twimg.com/media/EfaS_gsXgAAFCSF.jpg", "https://pbs.twimg.com/media/EfaS_hMXkAAK0Tl.jpg", "https://pbs.twimg.com/media/EfaS_h5XkAY7BmY.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
