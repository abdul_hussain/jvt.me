{
  "date" : "2020-08-16T21:30:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/nicolerenee3810/status/1294879460697804800" ],
    "name" : [ "Like of @nicolerenee3810's tweet" ],
    "published" : [ "2020-08-16T21:30:00+01:00" ],
    "like-of" : [ "https://twitter.com/nicolerenee3810/status/1294879460697804800" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/jltok",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1294879460697804800" ],
      "url" : [ "https://twitter.com/nicolerenee3810/status/1294879460697804800" ],
      "published" : [ "2020-08-16T06:11:35+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:nicolerenee3810" ],
          "numeric-id" : [ "14673822" ],
          "name" : [ "Nicole Hubbard" ],
          "nickname" : [ "nicolerenee3810" ],
          "url" : [ "https://twitter.com/nicolerenee3810", "https://nicole.dev" ],
          "published" : [ "2008-05-06T14:23:53+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Fort Worth, Texas, USA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/929894407704211456/ddRtT0aF.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Anyone else optimistically dump their laundry on the bed saying you’ll put it away today and then put it back in the basket when you go to bed. \n\nNo just me? Well this is day 4 of this...send help",
        "html" : "<div style=\"white-space: pre\">Anyone else optimistically dump their laundry on the bed saying you’ll put it away today and then put it back in the basket when you go to bed. \n\nNo just me? Well this is day 4 of this...send help</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
