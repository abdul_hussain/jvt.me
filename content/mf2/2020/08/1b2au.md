{
  "date" : "2020-08-29T11:54:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/StigAbell/status/1299602652372905984" ],
    "name" : [ "Like of @StigAbell's tweet" ],
    "published" : [ "2020-08-29T11:54:00+01:00" ],
    "like-of" : [ "https://twitter.com/StigAbell/status/1299602652372905984" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/1b2au",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1299602652372905984" ],
      "url" : [ "https://twitter.com/StigAbell/status/1299602652372905984" ],
      "published" : [ "2020-08-29T06:59:52+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:StigAbell" ],
          "numeric-id" : [ "509212441" ],
          "name" : [ "Stig Abell" ],
          "nickname" : [ "StigAbell" ],
          "url" : [ "https://twitter.com/StigAbell", "http://www.the-tls.co.uk", "http://bit.ly/Britworks", "https://amzn.to/30NYFDw" ],
          "published" : [ "2012-02-29T20:42:09+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1001518322662477824/1JlVdvbJ.jpg" ]
        }
      } ],
      "content" : [ "I cannot get over the fact that Chadwick Boseman was diagnosed with Stage Three cancer, and then made Civil War, Marshall, Black Panther, Infinity War, Endgame, 21 Bridges and Da 5 Bloods. All the while not mentioning it in public, and inspiring all those people. Extraordinary." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
