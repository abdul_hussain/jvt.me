{
  "date" : "2020-08-08T00:31:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/alicegoldfuss/status/1291864063253733376" ],
    "name" : [ "Like of @alicegoldfuss's tweet" ],
    "published" : [ "2020-08-08T00:31:00+01:00" ],
    "like-of" : [ "https://twitter.com/alicegoldfuss/status/1291864063253733376" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/w7idf",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1291864063253733376" ],
      "url" : [ "https://twitter.com/alicegoldfuss/status/1291864063253733376" ],
      "published" : [ "2020-08-07T22:29:29+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:alicegoldfuss" ],
          "numeric-id" : [ "163154809" ],
          "name" : [ "bletchley punk is a fullmetal engineer" ],
          "nickname" : [ "alicegoldfuss" ],
          "url" : [ "https://twitter.com/alicegoldfuss", "http://blog.alicegoldfuss.com/", "http://twitch.tv/bletchleypunk" ],
          "published" : [ "2010-07-05T17:51:34+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "/usr/local/sin" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1149452202827759616/1blf87Kn.jpg" ]
        }
      } ],
      "photo" : [ "https://pbs.twimg.com/media/Ee2fC53U0AEuk2o.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
