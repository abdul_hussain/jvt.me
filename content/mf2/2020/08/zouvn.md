{
  "date" : "2020-08-16T14:18:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/trevin/status/1294516677015515137" ],
    "name" : [ "Like of @trevin's tweet" ],
    "published" : [ "2020-08-16T14:18:00+01:00" ],
    "like-of" : [ "https://twitter.com/trevin/status/1294516677015515137" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/zouvn",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1294516677015515137" ],
      "url" : [ "https://twitter.com/trevin/status/1294516677015515137" ],
      "published" : [ "2020-08-15T06:10:01+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:trevin" ],
          "numeric-id" : [ "627363" ],
          "name" : [ "Trevin Chow" ],
          "nickname" : [ "trevin" ],
          "url" : [ "https://twitter.com/trevin", "https://trev.in" ],
          "published" : [ "2007-01-11T22:38:04+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Seattle, WA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/2304005040/8x66pamy9vui7acf7pyy.jpeg" ]
        }
      } ],
      "location" : [ {
        "type" : [ "h-card", "p-location" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:300bcc6e23a88361" ],
          "name" : [ "Seattle, WA" ]
        }
      } ],
      "content" : [ "This is so perfect." ],
      "photo" : [ "https://pbs.twimg.com/media/EfcMDmFVAAApy7a.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
