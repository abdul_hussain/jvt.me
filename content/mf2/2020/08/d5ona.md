{
  "date" : "2020-08-29T09:44:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/TheDailyShow/status/1299553510548279296" ],
    "name" : [ "Like of @TheDailyShow's tweet" ],
    "published" : [ "2020-08-29T09:44:00+01:00" ],
    "like-of" : [ "https://twitter.com/TheDailyShow/status/1299553510548279296" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/d5ona",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1299553510548279296" ],
      "url" : [ "https://twitter.com/TheDailyShow/status/1299553510548279296" ],
      "published" : [ "2020-08-29T03:44:36+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:TheDailyShow" ],
          "numeric-id" : [ "158414847" ],
          "name" : [ "The Daily Show" ],
          "nickname" : [ "TheDailyShow" ],
          "url" : [ "https://twitter.com/TheDailyShow", "http://thedailyshow.com", "http://dailyshow.com/donate" ],
          "published" : [ "2010-06-22T16:41:05+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1299421580989267970/VhmsZ1xE.jpg" ]
        }
      } ],
      "content" : [ "Thank you for being our hero 🙏" ],
      "photo" : [ "https://pbs.twimg.com/media/EgjxB28XcAEWIm4.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
