{
  "date" : "2020-08-14T12:08:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/paulienuh/status/1293811544271261696" ],
    "name" : [ "Like of @paulienuh's tweet" ],
    "published" : [ "2020-08-14T12:08:00+01:00" ],
    "category" : [ "hamilton" ],
    "like-of" : [ "https://twitter.com/paulienuh/status/1293811544271261696" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/fhwii",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1293811544271261696" ],
      "url" : [ "https://twitter.com/paulienuh/status/1293811544271261696" ],
      "published" : [ "2020-08-13T07:28:04+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:paulienuh" ],
          "numeric-id" : [ "230185453" ],
          "name" : [ "Pauline P. Narvas➖pawlean.com" ],
          "nickname" : [ "paulienuh" ],
          "url" : [ "https://twitter.com/paulienuh", "https://pawlean.com" ],
          "published" : [ "2010-12-24T14:46:00+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Leeds, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1265398650395639813/PfESApun.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Yesterday I was focusing on writing some docs at work whilst listening to #Hamilton in the background. It got up to “It’s Quiet Uptown” and I just burst into tears? Can anyone relate? 🥴",
        "html" : "Yesterday I was focusing on writing some docs at work whilst listening to <a href=\"https://twitter.com/search?q=%23Hamilton\">#Hamilton</a> in the background. It got up to “It’s Quiet Uptown” and I just burst into tears? Can anyone relate? 🥴"
      } ]
    }
  },
  "tags" : [ "hamilton" ],
  "client_id" : "https://indigenous.realize.be"
}
