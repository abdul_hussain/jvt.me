{
  "date" : "2020-08-27T20:48:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "name" : [ "Like of @drewm's tweet" ],
    "published" : [ "2020-08-27T20:48:00+01:00" ],
    "category" : [ "battlestations" ],
    "like-of" : [ "https://twitter.com/drewm/status/1299035279098077185" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/3xa9x",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1299035279098077185" ],
      "url" : [ "https://twitter.com/drewm/status/1299035279098077185" ],
      "published" : [ "2020-08-27T17:25:20+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:drewm" ],
          "numeric-id" : [ "12158" ],
          "name" : [ "Drew McLellan" ],
          "nickname" : [ "drewm" ],
          "url" : [ "https://twitter.com/drewm", "https://allinthehead.com/", "http://drewm.photo" ],
          "published" : [ "2006-11-12T14:05:53+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Bristol, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1177900325849763840/W2qp7QPI.jpg" ]
        }
      } ],
      "content" : [ "New setup." ],
      "photo" : [ "https://pbs.twimg.com/media/EgcZsyrXYAQSuwm.jpg" ]
    }
  },
  "tags" : [ "battlestations" ],
  "client_id" : "https://indigenous.realize.be"
}
