{
  "date" : "2020-08-21T22:30:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/infiniteobjects/status/1296914570359914496" ],
    "name" : [ "Like of @infiniteobjects's tweet" ],
    "published" : [ "2020-08-21T22:30:00+01:00" ],
    "category" : [ "cute" ],
    "like-of" : [ "https://twitter.com/infiniteobjects/status/1296914570359914496" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/a9vub",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1296914570359914496" ],
      "url" : [ "https://twitter.com/infiniteobjects/status/1296914570359914496" ],
      "video" : [ "https://video.twimg.com/ext_tw_video/1296914558825553920/pu/vid/640x640/ecuUKQCM9wjo-Pdt.mp4?tag=10" ],
      "published" : [ "2020-08-21T20:58:23+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:infiniteobjects" ],
          "numeric-id" : [ "981237049847173120" ],
          "name" : [ "We Print Video" ],
          "nickname" : [ "infiniteobjects" ],
          "url" : [ "https://twitter.com/infiniteobjects", "http://infiniteobjects.com" ],
          "published" : [ "2018-04-03T18:28:37+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "NYC" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1282702801278132224/xP3mcOko.jpg" ]
        }
      } ],
      "content" : [ "Our dog video this week is of Teddy. He’s going to sleep all weekend long and not a single one of you can stop him" ]
    }
  },
  "tags" : [ "cute" ],
  "client_id" : "https://indigenous.realize.be"
}
