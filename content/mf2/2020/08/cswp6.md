{
  "date" : "2020-08-23T12:52:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/chrisalbon/status/1297257448760213504" ],
    "name" : [ "Like of @chrisalbon's tweet" ],
    "published" : [ "2020-08-23T12:52:00+01:00" ],
    "category" : [ "food" ],
    "like-of" : [ "https://twitter.com/chrisalbon/status/1297257448760213504" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/cswp6",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1297257448760213504" ],
      "url" : [ "https://twitter.com/chrisalbon/status/1297257448760213504" ],
      "published" : [ "2020-08-22T19:40:52+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:chrisalbon" ],
          "numeric-id" : [ "11518572" ],
          "name" : [ "Chris Albon" ],
          "nickname" : [ "chrisalbon" ],
          "url" : [ "https://twitter.com/chrisalbon", "https://ChrisAlbon.com", "http://MachineLearningFlashcards.com" ],
          "published" : [ "2007-12-26T01:49:09+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "San Francisco" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/736992518110224384/fmqQxFEr.jpg" ]
        }
      } ],
      "content" : [ "Same amount of spinach" ],
      "photo" : [ "https://pbs.twimg.com/media/EgDIxk-VoAAnDTL.jpg", "https://pbs.twimg.com/media/EgDIxk8VoAArTc7.jpg" ]
    }
  },
  "tags" : [ "food" ],
  "client_id" : "https://indigenous.realize.be"
}
