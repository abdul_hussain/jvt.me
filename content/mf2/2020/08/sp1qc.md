{
  "date" : "2020-08-31T21:44:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/mattiasgeniar/status/1300454568300548096" ],
    "name" : [ "Like of @mattiasgeniar's tweet" ],
    "published" : [ "2020-08-31T21:44:00+01:00" ],
    "like-of" : [ "https://twitter.com/mattiasgeniar/status/1300454568300548096" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/sp1qc",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1300454568300548096" ],
      "url" : [ "https://twitter.com/mattiasgeniar/status/1300454568300548096" ],
      "published" : [ "2020-08-31T15:25:05+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:mattiasgeniar" ],
          "numeric-id" : [ "93920255" ],
          "name" : [ "m@ttias ⚡️" ],
          "nickname" : [ "mattiasgeniar" ],
          "url" : [ "https://twitter.com/mattiasgeniar", "https://ma.ttias.be" ],
          "published" : [ "2009-12-01T19:00:40+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Belgium" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1173566135951876096/Fi5cYHYQ.jpg" ]
        }
      } ],
      "content" : [ "When your URL is lying to you  ¯\\_(ツ)_/¯" ],
      "photo" : [ "https://pbs.twimg.com/media/EgwkiQaWoAEFjt-.png" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
