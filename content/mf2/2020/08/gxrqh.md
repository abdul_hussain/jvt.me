{
  "date" : "2020-08-06T20:48:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/Minette_78/status/1291313264396206080" ],
    "name" : [ "Like of @Minette_78's tweet" ],
    "published" : [ "2020-08-06T20:48:00+01:00" ],
    "like-of" : [ "https://twitter.com/Minette_78/status/1291313264396206080" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/gxrqh",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1291313264396206080" ],
      "url" : [ "https://twitter.com/Minette_78/status/1291313264396206080" ],
      "published" : [ "2020-08-06T10:00:48+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Minette_78" ],
          "numeric-id" : [ "2163745954" ],
          "name" : [ "Rachel McConnell" ],
          "nickname" : [ "Minette_78" ],
          "url" : [ "https://twitter.com/Minette_78", "http://www.rachelmcconnell.me", "http://amzn.eu/d/2j4OrHx" ],
          "published" : [ "2013-11-01T19:11:29+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Brighton, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1251036743115120642/yurLCUro.jpg" ]
        }
      } ],
      "content" : [ "Anyone else find the A11y abbreviation ironically inaccessible?!" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
