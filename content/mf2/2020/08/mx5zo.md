{
  "date" : "2020-08-06T10:29:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/Brunty/status/1291292206624579584" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1291307588081115136" ],
    "name" : [ "Reply to https://twitter.com/Brunty/status/1291292206624579584" ],
    "published" : [ "2020-08-06T10:29:00+01:00" ],
    "category" : [ "vim" ],
    "content" : [ {
      "html" : "",
      "value" : "Using Vim controls in Firefox was actually how I got used to `hjkl` as a thing"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/08/mx5zo",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1291292206624579584" ],
      "url" : [ "https://twitter.com/Brunty/status/1291292206624579584" ],
      "published" : [ "2020-08-06T08:37:07+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Brunty" ],
          "numeric-id" : [ "1099517012" ],
          "name" : [ "Matt Brunt" ],
          "nickname" : [ "Brunty" ],
          "url" : [ "https://twitter.com/Brunty", "https://brunty.me/now" ],
          "published" : [ "2013-01-17T23:19:21+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Leicester, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1151043501288824832/TjB9WT83.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "OH:\n\n\"it's VIM controls for Firefox\"\n\n\"most people can't even use VIM controls for VIM\"",
        "html" : "<div style=\"white-space: pre\">OH:\n\n\"it's VIM controls for Firefox\"\n\n\"most people can't even use VIM controls for VIM\"</div>"
      } ]
    }
  },
  "tags" : [ "vim" ],
  "client_id" : "https://indigenous.realize.be"
}
