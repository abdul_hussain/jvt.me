{
  "date" : "2020-08-25T09:58:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/sovietblobfish/status/1297827669250318336" ],
    "name" : [ "Like of @sovietblobfish's tweet" ],
    "published" : [ "2020-08-25T09:58:00+01:00" ],
    "like-of" : [ "https://twitter.com/sovietblobfish/status/1297827669250318336" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/9sbhx",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1297827669250318336" ],
      "url" : [ "https://twitter.com/sovietblobfish/status/1297827669250318336" ],
      "published" : [ "2020-08-24T09:26:43+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:sovietblobfish" ],
          "numeric-id" : [ "892030040929599488" ],
          "name" : [ "🌈sovietblobfish🌈" ],
          "nickname" : [ "sovietblobfish" ],
          "url" : [ "https://twitter.com/sovietblobfish" ],
          "published" : [ "2017-07-31T14:31:49+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1212169489120448512/1xUupjuv.png" ]
        }
      } ],
      "content" : [ "when your anxiety levels are lower in a pandemic than at school that says a lot about your education system" ],
      "photo" : [ "https://pbs.twimg.com/media/EgLPFcqXsAAq7MH.png" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
