{
  "date" : "2020-08-01T00:03:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/LiveOverflow/status/1289243231100772353" ],
    "name" : [ "Like of @LiveOverflow's tweet" ],
    "published" : [ "2020-08-01T00:03:00+01:00" ],
    "like-of" : [ "https://twitter.com/LiveOverflow/status/1289243231100772353" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/zu3uz",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1289243231100772353" ],
      "url" : [ "https://twitter.com/LiveOverflow/status/1289243231100772353" ],
      "published" : [ "2020-07-31T16:55:13+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:LiveOverflow" ],
          "numeric-id" : [ "3094698976" ],
          "name" : [ "LiveOverflow 🔴" ],
          "nickname" : [ "LiveOverflow" ],
          "url" : [ "https://twitter.com/LiveOverflow", "https://www.youtube.com/LiveOverflowCTF" ],
          "published" : [ "2015-03-18T14:33:53+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Internet" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/804126168869961728/KNGXVh_6.jpg" ]
        }
      } ],
      "content" : [ "0-day zoom hacks dropped in YT chat" ],
      "photo" : [ "https://pbs.twimg.com/media/EeRPxRHWAAkV1L3.png" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
