{
  "date" : "2020-08-08T10:36:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/tesseralis/status/1291839437802844160" ],
    "name" : [ "Like of @tesseralis's tweet" ],
    "published" : [ "2020-08-08T10:36:00+01:00" ],
    "like-of" : [ "https://twitter.com/tesseralis/status/1291839437802844160" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/zkvdi",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1291839437802844160" ],
      "url" : [ "https://twitter.com/tesseralis/status/1291839437802844160" ],
      "published" : [ "2020-08-07T20:51:37+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:tesseralis" ],
          "numeric-id" : [ "471768380" ],
          "name" : [ "Nat Aliscion 🍑" ],
          "nickname" : [ "tesseralis" ],
          "url" : [ "https://twitter.com/tesseralis", "http://www.tessera.li", "http://Ko-fi.com/tesseralis" ],
          "published" : [ "2012-01-23T07:18:57+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Koda Island" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1231468297649979392/t_7jReNz.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "man goes to doctor. says he's depressed. says work is overwhelming. everything is disorganized, swamped with bureaucracy.\n\n\"use the app JIRA\" says doctor. \"It'll help your organization keep track of work.\"\n\n\"but doc,\" says the man. \"I work on JIRA.\"",
        "html" : "<div style=\"white-space: pre\">man goes to doctor. says he's depressed. says work is overwhelming. everything is disorganized, swamped with bureaucracy.\n\n\"use the app JIRA\" says doctor. \"It'll help your organization keep track of work.\"\n\n\"but doc,\" says the man. \"I work on JIRA.\"</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
