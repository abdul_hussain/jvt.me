{
  "date" : "2020-08-26T20:16:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.meetup.com/Tech-Nottingham/events/272818496/" ],
    "syndication" : [ "https://www.meetup.com/Tech-Nottingham/events/272818496/#rsvp-by-https%3A%2F%2Fwww.jvt.me%2Fmf2%2F2020%2F08%2Fohx4i%2F" ],
    "name" : [ "RSVP yes to https://www.meetup.com/Tech-Nottingham/events/272818496/" ],
    "published" : [ "2020-08-26T20:16:00+01:00" ],
    "event" : {
      "start" : [ "2020-09-14T18:30:00+01:00" ],
      "name" : [ "Tech Nottingham 14th September - Big data engineering and DevOps" ],
      "end" : [ "2020-09-14T20:30:00+01:00" ],
      "location" : [ "Online" ],
      "url" : [ "https://www.meetup.com/Tech-Nottingham/events/272818496/" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/08/ohx4i",
  "client_id" : "https://indigenous.realize.be"
}
