{
  "date" : "2020-08-27T20:52:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/rothgar/status/1299028742979780608" ],
    "name" : [ "Like of @rothgar's tweet" ],
    "published" : [ "2020-08-27T20:52:00+01:00" ],
    "category" : [ "aws" ],
    "like-of" : [ "https://twitter.com/rothgar/status/1299028742979780608" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/onxzl",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1299028742979780608" ],
      "url" : [ "https://twitter.com/rothgar/status/1299028742979780608" ],
      "published" : [ "2020-08-27T16:59:21+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:rothgar" ],
          "numeric-id" : [ "14361975" ],
          "name" : [ "Justin Garrison" ],
          "nickname" : [ "rothgar" ],
          "url" : [ "https://twitter.com/rothgar", "https://justingarrison.com", "http://cnibook.info", "http://imdb.to/2FikCiY", "http://bit.ly/2tJM3yt" ],
          "published" : [ "2008-04-11T15:59:21+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "he/him" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1265369680916516864/JkNfHi56.jpg" ]
        }
      } ],
      "content" : [ "who did this? 😆" ],
      "photo" : [ "https://pbs.twimg.com/media/EgcTwYyVAAI4x_B.png" ]
    }
  },
  "tags" : [ "aws" ],
  "client_id" : "https://indigenous.realize.be"
}
