{
  "date" : "2020-08-30T21:01:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/TartanLlama/status/1300136653814673414" ],
    "name" : [ "Like of @TartanLlama's tweet" ],
    "published" : [ "2020-08-30T21:01:00+01:00" ],
    "like-of" : [ "https://twitter.com/TartanLlama/status/1300136653814673414" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/0jgsf",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1300136653814673414" ],
      "url" : [ "https://twitter.com/TartanLlama/status/1300136653814673414" ],
      "published" : [ "2020-08-30T18:21:48+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:TartanLlama" ],
          "numeric-id" : [ "36158849" ],
          "name" : [ "Sy Brand" ],
          "nickname" : [ "TartanLlama" ],
          "url" : [ "https://twitter.com/TartanLlama" ],
          "published" : [ "2009-04-28T19:38:21+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Edinburgh, Scotland" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1289115924193312769/SuWeDoYw.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Here is the #1 most important career advice I can give:\n\nDO NOT HAVE A TWITTER PROFILE FULL OF GATEKEEPY SHIT TAKES.\n\nRoughly, more than 3 consecutive tweets totally missing your own privilege with less than ~18 marginalised people followed is a career danger zone.",
        "html" : "<div style=\"white-space: pre\">Here is the #1 most important career advice I can give:\n\nDO NOT HAVE A TWITTER PROFILE FULL OF GATEKEEPY SHIT TAKES.\n\nRoughly, more than 3 consecutive tweets totally missing your own privilege with less than ~18 marginalised people followed is a career danger zone.</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
