{
  "date" : "2020-08-26T20:16:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.meetup.com/Women-In-Tech-Nottingham/events/272818361/" ],
    "syndication" : [ "https://brid.gy/publish/meetup" ],
    "name" : [ "RSVP yes to https://www.meetup.com/Women-In-Tech-Nottingham/events/272818361/" ],
    "published" : [ "2020-08-26T20:16:00+01:00" ],
    "event" : {
      "start" : [ "2020-11-05T18:30:00Z" ],
      "name" : [ "Women in Tech 5th November - Coding Black Females take over!" ],
      "end" : [ "2020-11-05T20:30:00Z" ],
      "location" : [ "Online" ],
      "url" : [ "https://www.meetup.com/Women-In-Tech-Nottingham/events/272818361/" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/08/0hoet",
  "client_id" : "https://indigenous.realize.be"
}
