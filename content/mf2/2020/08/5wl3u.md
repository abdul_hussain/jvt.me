{
  "date" : "2020-08-10T14:55:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/iamhirusi/status/1292803279072583680" ],
    "name" : [ "Like of @iamhirusi's tweet" ],
    "published" : [ "2020-08-10T14:55:00+01:00" ],
    "like-of" : [ "https://twitter.com/iamhirusi/status/1292803279072583680" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/5wl3u",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1292803279072583680" ],
      "url" : [ "https://twitter.com/iamhirusi/status/1292803279072583680" ],
      "published" : [ "2020-08-10T12:41:35+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:iamhirusi" ],
          "numeric-id" : [ "1252888172700119041" ],
          "name" : [ "Ru Singh" ],
          "nickname" : [ "iamhirusi" ],
          "url" : [ "https://twitter.com/iamhirusi", "https://rusingh.com" ],
          "published" : [ "2020-04-22T09:14:03+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "India" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1287100701965840385/7LCy7NCC.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Alright, time to set up a new #Micropub server, a sample static site, and move Celestial forward. Let's go! 😍\n\n#indieweb",
        "html" : "<div style=\"white-space: pre\">Alright, time to set up a new <a href=\"https://twitter.com/search?q=%23Micropub\">#Micropub</a> server, a sample static site, and move Celestial forward. Let's go! 😍\n\n<a href=\"https://twitter.com/search?q=%23indieweb\">#indieweb</a></div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EfD1uwqUMAE6soT.png" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
