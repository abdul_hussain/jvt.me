{
  "date" : "2020-08-12T12:32:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/zahrataiba/status/1293503079653015553" ],
    "name" : [ "Like of @zahrataiba's tweet" ],
    "published" : [ "2020-08-12T12:32:00+01:00" ],
    "category" : [ "code-review" ],
    "like-of" : [ "https://twitter.com/zahrataiba/status/1293503079653015553" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/ppbei",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1293503079653015553" ],
      "url" : [ "https://twitter.com/zahrataiba/status/1293503079653015553" ],
      "published" : [ "2020-08-12T11:02:20+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:zahrataiba" ],
          "numeric-id" : [ "2822614838" ],
          "name" : [ "زهرة || zahra" ],
          "nickname" : [ "zahrataiba" ],
          "url" : [ "https://twitter.com/zahrataiba", "https://www.zahratraboulsi.com" ],
          "published" : [ "2014-09-20T19:27:39+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1289653028178612224/eOT4raro.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Would be interesting for teams to have (informal) code review reviews once or twice yearly to calibrate nit-picking levels and gauge the split in focus between syntax, structure and UX/DX.\n\nYou can write docs for this stuff but I think many of us can learn from each other.",
        "html" : "<div style=\"white-space: pre\">Would be interesting for teams to have (informal) code review reviews once or twice yearly to calibrate nit-picking levels and gauge the split in focus between syntax, structure and UX/DX.\n\nYou can write docs for this stuff but I think many of us can learn from each other.</div>"
      } ]
    }
  },
  "tags" : [ "code-review" ],
  "client_id" : "https://indigenous.realize.be"
}
