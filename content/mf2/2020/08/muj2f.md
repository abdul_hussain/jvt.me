{
  "date" : "2020-08-01T19:19:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/TinkerSec/status/1289587514240577546" ],
    "name" : [ "Like of @TinkerSec's tweet" ],
    "published" : [ "2020-08-01T19:19:00+01:00" ],
    "like-of" : [ "https://twitter.com/TinkerSec/status/1289587514240577546" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/muj2f",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1289587514240577546" ],
      "url" : [ "https://twitter.com/TinkerSec/status/1289587514240577546" ],
      "published" : [ "2020-08-01T15:43:17+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:TinkerSec" ],
          "numeric-id" : [ "765305590746722304" ],
          "name" : [ "Tinker" ],
          "nickname" : [ "TinkerSec" ],
          "url" : [ "https://twitter.com/TinkerSec", "https://www.tinker.sh" ],
          "published" : [ "2016-08-15T21:54:06+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "/dev/null" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1269323431784349706/qFsRuaaH.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Regarding banning TikTok:\n\nIf we allow the president can ban TikTok, we allow them to ban any app... \n\n...Including privacy and safety apps like Signal.",
        "html" : "<div style=\"white-space: pre\">Regarding banning TikTok:\n\nIf we allow the president can ban TikTok, we allow them to ban any app... \n\n...Including privacy and safety apps like Signal.</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
