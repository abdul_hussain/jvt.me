{
  "date" : "2020-08-27T19:03:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/EmilyKager/status/1299044333484675079" ],
    "name" : [ "Like of @EmilyKager's tweet" ],
    "published" : [ "2020-08-27T19:03:00+01:00" ],
    "like-of" : [ "https://twitter.com/EmilyKager/status/1299044333484675079" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/yjjgg",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1299044333484675079" ],
      "url" : [ "https://twitter.com/EmilyKager/status/1299044333484675079" ],
      "published" : [ "2020-08-27T18:01:18+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:EmilyKager" ],
          "numeric-id" : [ "2347475803" ],
          "name" : [ "Emily Kager" ],
          "nickname" : [ "EmilyKager" ],
          "url" : [ "https://twitter.com/EmilyKager", "https://www.emilykager.com/" ],
          "published" : [ "2014-02-16T21:34:12+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Oakland and SF " ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1296572390252716034/jTxFoueZ.jpg" ]
        }
      } ],
      "content" : [ "User review logic: This app doesn't do everything I want so I guess I have to switch to this other app that does none of the things I want and that'll show you" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
