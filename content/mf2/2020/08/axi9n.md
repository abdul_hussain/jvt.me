{
  "date" : "2020-08-27T23:08:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/EmilyKager/status/1299092693231038466" ],
    "name" : [ "Like of @EmilyKager's tweet" ],
    "published" : [ "2020-08-27T23:08:00+01:00" ],
    "like-of" : [ "https://twitter.com/EmilyKager/status/1299092693231038466" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/axi9n",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1299092693231038466" ],
      "url" : [ "https://twitter.com/EmilyKager/status/1299092693231038466" ],
      "published" : [ "2020-08-27T21:13:28+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:EmilyKager" ],
          "numeric-id" : [ "2347475803" ],
          "name" : [ "Emily Kager" ],
          "nickname" : [ "EmilyKager" ],
          "url" : [ "https://twitter.com/EmilyKager", "https://www.emilykager.com/" ],
          "published" : [ "2014-02-16T21:34:12+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Oakland and SF " ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1296572390252716034/jTxFoueZ.jpg" ]
        }
      } ],
      "content" : [ "If anyone wants me to undo our latest app update for you just reply here" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
