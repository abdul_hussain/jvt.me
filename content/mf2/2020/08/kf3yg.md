{
  "date" : "2020-08-29T09:38:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/EricaJoy/status/1299545972528250881" ],
    "name" : [ "Like of @EricaJoy's tweet" ],
    "published" : [ "2020-08-29T09:38:00+01:00" ],
    "like-of" : [ "https://twitter.com/EricaJoy/status/1299545972528250881" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/kf3yg",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1299545972528250881" ],
      "url" : [ "https://twitter.com/EricaJoy/status/1299545972528250881" ],
      "published" : [ "2020-08-29T03:14:38+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:EricaJoy" ],
          "numeric-id" : [ "817083" ],
          "name" : [ "EricaJoy" ],
          "nickname" : [ "EricaJoy" ],
          "url" : [ "https://twitter.com/EricaJoy", "https://www.ericabaker.com" ],
          "published" : [ "2007-03-07T04:30:38+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Oakland, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1236682440539688960/AFMVsNcD.jpg" ]
        }
      } ],
      "content" : [ "the whole time chadwick boseman was filming black panther, he was fighting cancer. the whole time. imagine the strength. imagine the weariness. imagine the perseverance. imagine the worried family begging him to not go so hard. so that we could experience a movie that centers us." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
