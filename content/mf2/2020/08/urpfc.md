{
  "date" : "2020-08-29T10:21:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/keywilliamss/status/1299533484155744256" ],
    "name" : [ "Like of @keywilliamss's tweet" ],
    "published" : [ "2020-08-29T10:21:00+01:00" ],
    "like-of" : [ "https://twitter.com/keywilliamss/status/1299533484155744256" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/urpfc",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1299533484155744256" ],
      "url" : [ "https://twitter.com/keywilliamss/status/1299533484155744256" ],
      "published" : [ "2020-08-29T02:25:01+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:keywilliamss" ],
          "numeric-id" : [ "378407099" ],
          "name" : [ "key." ],
          "nickname" : [ "keywilliamss" ],
          "url" : [ "https://twitter.com/keywilliamss", "https://www.youtube.com/watch?v=PLEaCUg7g50" ],
          "published" : [ "2011-09-23T04:34:26+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Los Angeles, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1237049408992264192/iBGA4R23.jpg" ]
        }
      } ],
      "content" : [ "It’s not lost on me the way the internet bullied Chadwick Boseman the last two years about his appearance, him looking tired, etc to the point where he began deleting photos while he was fighting for his life. You truly never know what people are going through. BE KIND." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
