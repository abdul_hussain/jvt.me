{
  "date" : "2020-08-16T14:16:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/PhysicsAndAstr1/status/1294670453089488897" ],
    "name" : [ "Like of @PhysicsAndAstr1's tweet" ],
    "published" : [ "2020-08-16T14:16:00+01:00" ],
    "category" : [ "cute" ],
    "like-of" : [ "https://twitter.com/PhysicsAndAstr1/status/1294670453089488897" ]
  },
  "kind" : "likes",
  "slug" : "2020/08/2vajg",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1294670453089488897" ],
      "url" : [ "https://twitter.com/PhysicsAndAstr1/status/1294670453089488897" ],
      "published" : [ "2020-08-15T16:21:04+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:PhysicsAndAstr1" ],
          "numeric-id" : [ "1068830543536631808" ],
          "name" : [ "Earth's Beauty" ],
          "nickname" : [ "PhysicsAndAstr1" ],
          "url" : [ "https://twitter.com/PhysicsAndAstr1", "http://www.thesciearth.com" ],
          "published" : [ "2018-12-01T11:33:54+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "New York,USA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1147905043661774848/EUJctL8P.png" ]
        }
      } ],
      "content" : [ "Animals interrupting wildlife photographers is my new joy." ],
      "photo" : [ "https://pbs.twimg.com/media/EfeX5RBWoAAF9Sb.jpg", "https://pbs.twimg.com/media/EfeX5qVX0AEbheH.jpg", "https://pbs.twimg.com/media/EfeX6CdXYAspa58.jpg", "https://pbs.twimg.com/media/EfeX6kPX0AgXtrC.jpg" ]
    }
  },
  "tags" : [ "cute" ],
  "client_id" : "https://indigenous.realize.be"
}
