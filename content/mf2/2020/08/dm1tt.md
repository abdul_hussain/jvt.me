{
  "date" : "2020-08-24T12:50:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamieTanna/status/1297864893266370561" ],
    "published" : [ "2020-08-24T12:50:00+01:00" ],
    "repost-of" : [ "https://twitter.com/technottingham/status/1297861282109042689" ],
    "category" : [ "tech-nottingham" ],
    "content" : [ {
      "html" : "",
      "value" : "Looks like an awesome <a href=\"/tags/tech-nottingham/\">#TechNott</a> lineup!"
    } ]
  },
  "kind" : "reposts",
  "slug" : "2020/08/dm1tt",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1297861282109042689" ],
      "url" : [ "https://twitter.com/technottingham/status/1297861282109042689" ],
      "video" : [ "https://video.twimg.com/ext_tw_video/1297860239694364674/pu/vid/1280x720/M7WWxGpL7k_uIYqf.mp4?tag=10" ],
      "published" : [ "2020-08-24T11:40:17+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:technottingham" ],
          "numeric-id" : [ "384492431" ],
          "name" : [ "Tech Nottingham" ],
          "nickname" : [ "technottingham" ],
          "url" : [ "https://twitter.com/technottingham", "http://technottingham.com" ],
          "published" : [ "2011-10-03T19:47:31+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1023974499757293570/ZoPc_QsO.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Presenting #TechNott and #WiTNotts  🦊🍃🍂\nAUTUMN  SEASON 🥧🧤🎃\n\n6 awesome events over 3 months!\n\nWe can't wait to welcome our awesome speakers @rosanna20736, @dazfuller, Ruth Lee, @HannahFoxwell, @codingblackfems and @garyshort 🎉\n\nAll the details 👉 nott.tech",
        "html" : "<div style=\"white-space: pre\">Presenting <a href=\"https://twitter.com/search?q=%23TechNott\">#TechNott</a> and <a href=\"https://twitter.com/search?q=%23WiTNotts\">#WiTNotts</a>  🦊🍃🍂\nAUTUMN  SEASON 🥧🧤🎃\n\n6 awesome events over 3 months!\n\nWe can't wait to welcome our awesome speakers <a href=\"https://twitter.com/rosanna20736\">@rosanna20736</a>, <a href=\"https://twitter.com/dazfuller\">@dazfuller</a>, Ruth Lee, <a href=\"https://twitter.com/HannahFoxwell\">@HannahFoxwell</a>, <a href=\"https://twitter.com/codingblackfems\">@codingblackfems</a> and <a href=\"https://twitter.com/garyshort\">@garyshort</a> 🎉\n\nAll the details 👉 <a href=\"http://nott.tech\">nott.tech</a></div>"
      } ]
    }
  },
  "tags" : [ "tech-nottingham" ],
  "client_id" : "https://indigenous.realize.be"
}
