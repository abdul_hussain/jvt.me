{
  "date" : "2020-05-08T11:09:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/CarolSaysThings/status/1258493483196862474" ],
    "name" : [ "Like of @CarolSaysThings's tweet" ],
    "published" : [ "2020-05-08T11:09:00+01:00" ],
    "like-of" : [ "https://twitter.com/CarolSaysThings/status/1258493483196862474" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/8caqt",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1258493483196862474" ],
      "url" : [ "https://twitter.com/CarolSaysThings/status/1258493483196862474" ],
      "published" : [ "2020-05-07T20:26:42+00:00" ],
      "in-reply-to" : [ "https://twitter.com/CarolSaysThings/status/1257056007961874436" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:CarolSaysThings" ],
          "numeric-id" : [ "36382927" ],
          "name" : [ "Carol 😅" ],
          "nickname" : [ "CarolSaysThings" ],
          "url" : [ "https://twitter.com/CarolSaysThings", "https://carolgilabert.me/" ],
          "published" : [ "2009-04-29T15:22:13+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "🇧🇷🇪🇸🇬🇧 · Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1238515159594917889/C5994QPa.jpg" ]
        }
      } ],
      "content" : [ "I forgot to post this earlier in the week, but I got even more cards and presents cause my friends are awesome 💛" ],
      "photo" : [ "https://pbs.twimg.com/media/EXcRIo1X0AcKqsP.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
