{
  "date" : "2020-05-27T21:58:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/bernardTcastle/status/1265624440173453313" ],
    "name" : [ "Like of @bernardTcastle's tweet" ],
    "published" : [ "2020-05-27T21:58:00+01:00" ],
    "category" : [ "coronavirus", "politics" ],
    "like-of" : [ "https://twitter.com/bernardTcastle/status/1265624440173453313" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/cperq",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1265624440173453313" ],
      "url" : [ "https://twitter.com/bernardTcastle/status/1265624440173453313" ],
      "published" : [ "2020-05-27T12:42:35+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:bernardTcastle" ],
          "numeric-id" : [ "1178611878127382529" ],
          "name" : [ "Bernard Castle" ],
          "nickname" : [ "bernardTcastle" ],
          "url" : [ "https://twitter.com/bernardTcastle" ],
          "published" : [ "2019-09-30T10:06:00+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1265533447235342336/RyAmimx4.jpg" ]
        }
      } ],
      "content" : [ "Thinking about retraining as an optician." ]
    }
  },
  "tags" : [ "coronavirus", "politics" ],
  "client_id" : "https://indigenous.realize.be"
}
