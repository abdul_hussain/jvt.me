{
  "date" : "2020-05-10T15:19:52.108Z",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.jvt.me/events/personal-events/2020-05-gbgfb/" ],
    "name" : [ "RSVP yes to https://www.jvt.me/events/personal-events/2020-05-gbgfb/" ],
    "published" : [ "2020-05-10T15:19:52.108Z" ],
    "event" : {
      "start" : [ "2020-05-10T20:00:00+0100" ],
      "name" : [ "The Big Quiz for Cancer Research UK - Live!" ],
      "end" : [ "2020-05-10T21:00:00+0100" ],
      "url" : [ "https://www.jvt.me/events/personal-events/2020-05-gbgfb/" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/05/cyuou",
  "client_id" : "https://micropublish.net"
}
