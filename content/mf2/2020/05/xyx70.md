{
  "date" : "2020-05-14T21:31:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/SparkleClass/status/1260924663900798977" ],
    "name" : [ "Like of @SparkleClass's tweet" ],
    "published" : [ "2020-05-14T21:31:00+01:00" ],
    "like-of" : [ "https://twitter.com/SparkleClass/status/1260924663900798977" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/xyx70",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1260924663900798977" ],
      "url" : [ "https://twitter.com/SparkleClass/status/1260924663900798977" ],
      "published" : [ "2020-05-14T13:27:21+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:SparkleClass" ],
          "numeric-id" : [ "928764030688296960" ],
          "name" : [ "Rachel Morgan-Trimmer" ],
          "nickname" : [ "SparkleClass" ],
          "url" : [ "https://twitter.com/SparkleClass", "http://www.sparkleclass.com" ],
          "published" : [ "2017-11-09T23:19:35+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Manchester, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1235914325514125313/XnCY3M1M.jpg" ]
        }
      } ],
      "content" : [ "I'm writing an article on how to make your Zoom event amazing (and accessible). Anyone want to have a guess at who I'm holding up as examples?!" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
