{
  "date" : "2020-05-22T23:32:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/Flaminhaystack/status/1263499263012737024" ],
    "name" : [ "Like of @Flaminhaystack's tweet" ],
    "published" : [ "2020-05-22T23:32:00+01:00" ],
    "like-of" : [ "https://twitter.com/Flaminhaystack/status/1263499263012737024" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/os1rt",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1263499263012737024" ],
      "url" : [ "https://twitter.com/Flaminhaystack/status/1263499263012737024" ],
      "published" : [ "2020-05-21T15:57:53+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Flaminhaystack" ],
          "numeric-id" : [ "46105004" ],
          "name" : [ "Antonia" ],
          "nickname" : [ "Flaminhaystack" ],
          "url" : [ "https://twitter.com/Flaminhaystack" ],
          "published" : [ "2009-06-10T12:58:12+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1257960471870623744/ArZp8yxN.jpg" ]
        }
      } ],
      "content" : [ "Not one person who went for a job interview in 2015 got the answer right to, \"So where do you see yourself 5 years from now?\"" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
