{
  "date" : "2020-05-26T22:45:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/josierones/status/1265307403962273794" ],
    "name" : [ "Like of @josierones's tweet" ],
    "published" : [ "2020-05-26T22:45:00+01:00" ],
    "category" : [ "coronavirus" ],
    "like-of" : [ "https://twitter.com/josierones/status/1265307403962273794" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/3libh",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1265307403962273794" ],
      "url" : [ "https://twitter.com/josierones/status/1265307403962273794" ],
      "published" : [ "2020-05-26T15:42:48+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:josierones" ],
          "numeric-id" : [ "42033241" ],
          "name" : [ "Rosie Jones" ],
          "nickname" : [ "josierones" ],
          "url" : [ "https://twitter.com/josierones", "http://www.rosiejonescomedy.com" ],
          "published" : [ "2009-05-23T14:22:43+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/889446872854745088/utV7m2n0.jpg" ]
        }
      } ],
      "content" : [ "Just driven to Barnard Castle to check whether I still have Cerebral Palsy. I do." ]
    }
  },
  "tags" : [ "coronavirus" ],
  "client_id" : "https://indigenous.realize.be"
}
