{
  "date" : "2020-05-18T20:15:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/holly/status/1262373785849733124" ],
    "name" : [ "Like of @holly's tweet" ],
    "published" : [ "2020-05-18T20:15:00+01:00" ],
    "like-of" : [ "https://twitter.com/holly/status/1262373785849733124" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/pks1v",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1262373785849733124" ],
      "url" : [ "https://twitter.com/holly/status/1262373785849733124" ],
      "published" : [ "2020-05-18T13:25:38+00:00" ],
      "in-reply-to" : [ "https://twitter.com/jesslynnrose/status/1262373276719943680" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:holly" ],
          "numeric-id" : [ "7555262" ],
          "name" : [ "Holly Brockwell" ],
          "nickname" : [ "holly" ],
          "url" : [ "https://twitter.com/holly", "https://www.instagram.com/hollybrocks/" ],
          "published" : [ "2007-07-18T10:27:16+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "From Nottingham, in London" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1248671410714804225/A_9KJ1y8.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Well, considering all the sun damage you're missing out on during lockdown, you're still ahead... right?!",
        "html" : "Well, considering all the sun damage you're missing out on during lockdown, you're still ahead... right?!\n<a class=\"u-mention\" href=\"https://twitter.com/jesslynnrose\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
