{
  "date" : "2020-05-28T09:59:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/Sara_Rose_G/status/1265584820165513217" ],
    "name" : [ "Like of @Sara_Rose_G's tweet" ],
    "published" : [ "2020-05-28T09:59:00+01:00" ],
    "category" : [ "coronavirus", "politics" ],
    "like-of" : [ "https://twitter.com/Sara_Rose_G/status/1265584820165513217" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/grbfs",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1265584820165513217" ],
      "url" : [ "https://twitter.com/Sara_Rose_G/status/1265584820165513217" ],
      "published" : [ "2020-05-27T10:05:09+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Sara_Rose_G" ],
          "numeric-id" : [ "317861977" ],
          "name" : [ "Sara Gibbs" ],
          "nickname" : [ "Sara_Rose_G" ],
          "url" : [ "https://twitter.com/Sara_Rose_G", "https://www.theblairpartnership.com/clients/" ],
          "published" : [ "2011-06-15T15:58:15+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1184767418138349568/-6KhpBjR.jpg" ]
        }
      } ],
      "content" : [ "Sick of people having a go at Dominic Cummings, a brave man who risked our lives to look after his family" ]
    }
  },
  "tags" : [ "coronavirus", "politics" ],
  "client_id" : "https://indigenous.realize.be"
}
