{
  "date" : "2020-05-20T14:44:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/CardsToast/status/1262101317922635776" ],
    "name" : [ "Like of @CardsToast's tweet" ],
    "published" : [ "2020-05-20T14:44:00+01:00" ],
    "category" : [ "coronavirus" ],
    "like-of" : [ "https://twitter.com/CardsToast/status/1262101317922635776" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/53jay",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1262101317922635776" ],
      "url" : [ "https://twitter.com/CardsToast/status/1262101317922635776" ],
      "published" : [ "2020-05-17T19:22:57+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:CardsToast" ],
          "numeric-id" : [ "1216401428148670464" ],
          "name" : [ "Sporran Peace" ],
          "nickname" : [ "CardsToast" ],
          "url" : [ "https://twitter.com/CardsToast" ],
          "published" : [ "2020-01-12T16:48:20+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Anywhere you want. " ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1244692121073770498/jf_ShT_4.jpg" ]
        }
      } ],
      "content" : [ "today in New Zealand our kids can go back to school because we have a compassionate government and we listen to science." ],
      "photo" : [ "https://pbs.twimg.com/media/EYPiUJDUEAA7j61.jpg" ]
    }
  },
  "tags" : [ "coronavirus" ],
  "client_id" : "https://indigenous.realize.be"
}
