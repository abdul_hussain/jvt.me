{
  "date" : "2020-05-24T18:38:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/Simon_Pegg/status/1264507003503869953" ],
    "name" : [ "Like of @Simon_Pegg's tweet" ],
    "published" : [ "2020-05-24T18:38:00+01:00" ],
    "category" : [ "coronavirus", "politics" ],
    "like-of" : [ "https://twitter.com/Simon_Pegg/status/1264507003503869953" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/pb6fn",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1264507003503869953" ],
      "url" : [ "https://twitter.com/Simon_Pegg/status/1264507003503869953" ],
      "published" : [ "2020-05-24T10:42:17+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Simon_Pegg" ],
          "numeric-id" : [ "24155737" ],
          "name" : [ "Simon Pegg" ],
          "nickname" : [ "Simon_Pegg" ],
          "url" : [ "https://twitter.com/Simon_Pegg", "http://ko-fi.com/simonpegg" ],
          "published" : [ "2009-03-13T11:01:33+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "North Norfolk" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/964886087498960898/YyD54dUP.jpg" ]
        }
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EYxuaW_WsAAzhgv.jpg" ]
    }
  },
  "tags" : [ "coronavirus", "politics" ],
  "client_id" : "https://indigenous.realize.be"
}
