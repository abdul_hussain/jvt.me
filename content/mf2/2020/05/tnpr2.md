{
  "date" : "2020-05-19T22:21:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/garybernhardt/status/1262847255976898560" ],
    "name" : [ "Like of @garybernhardt's tweet" ],
    "published" : [ "2020-05-19T22:21:00+01:00" ],
    "like-of" : [ "https://twitter.com/garybernhardt/status/1262847255976898560" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/tnpr2",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1262847255976898560" ],
      "url" : [ "https://twitter.com/garybernhardt/status/1262847255976898560" ],
      "published" : [ "2020-05-19T20:47:03+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:garybernhardt" ],
          "numeric-id" : [ "809685" ],
          "name" : [ "Gary Bernhardt" ],
          "nickname" : [ "garybernhardt" ],
          "url" : [ "https://twitter.com/garybernhardt", "http://destroyallsoftware.com" ],
          "published" : [ "2007-03-04T05:12:14+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Seattle, WA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1170938305/twitter_headshot.png" ]
        }
      } ],
      "content" : [ "I can't believe that Spotify has the audacity to even call its paywalled audio feeds \"podcasts\". They're trying to take ownership of the open ecosystem that grew podcasting from nothing to what it is now over 15 years. How can anyone at Spotify feel good about doing this?" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
