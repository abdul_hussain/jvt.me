{
  "date" : "2020-05-30T17:12:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/ElunedAnderson/status/1266715936884764673" ],
    "name" : [ "Like of @ElunedAnderson's tweet" ],
    "published" : [ "2020-05-30T17:12:00+01:00" ],
    "category" : [ "coronavirus" ],
    "like-of" : [ "https://twitter.com/ElunedAnderson/status/1266715936884764673" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/xtdad",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1266715936884764673" ],
      "url" : [ "https://twitter.com/ElunedAnderson/status/1266715936884764673" ],
      "published" : [ "2020-05-30T12:59:48+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:ElunedAnderson" ],
          "numeric-id" : [ "843534598105710596" ],
          "name" : [ "Ellie" ],
          "nickname" : [ "ElunedAnderson" ],
          "url" : [ "https://twitter.com/ElunedAnderson", "http://instagram.com/elunedanderson" ],
          "published" : [ "2017-03-19T18:48:15+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Caerdydd - Liverpool" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1243597648092901376/lkg_fw2Q.jpg" ]
        }
      } ],
      "location" : [ {
        "type" : [ "h-card", "p-location" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:6d2fd8974bfa6807" ],
          "name" : [ "Backwell, England" ]
        }
      } ],
      "content" : [ {
        "value" : "Yesterday, 324 people died of covid in the UK. To put that into context, if you combine the deaths of Germany, France, Italy, Ireland, Spain, Portugal Poland and Belgium in the last 24 hours, it is still less than the UKs. \n\nWhy for the love of god, are we easing lockdown?",
        "html" : "<div style=\"white-space: pre\">Yesterday, 324 people died of covid in the UK. To put that into context, if you combine the deaths of Germany, France, Italy, Ireland, Spain, Portugal Poland and Belgium in the last 24 hours, it is still less than the UKs. \n\nWhy for the love of god, are we easing lockdown?</div>"
      } ]
    }
  },
  "tags" : [ "coronavirus" ],
  "client_id" : "https://indigenous.realize.be"
}
