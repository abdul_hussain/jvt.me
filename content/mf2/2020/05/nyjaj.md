{
  "date" : "2020-05-08T22:01:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/holly/status/1258856072363868168" ],
    "name" : [ "Like of @holly's tweet" ],
    "published" : [ "2020-05-08T22:01:00+01:00" ],
    "like-of" : [ "https://twitter.com/holly/status/1258856072363868168" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/nyjaj",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1258856072363868168" ],
      "url" : [ "https://twitter.com/holly/status/1258856072363868168" ],
      "published" : [ "2020-05-08T20:27:30+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:holly" ],
          "numeric-id" : [ "7555262" ],
          "name" : [ "Holly Brockwell" ],
          "nickname" : [ "holly" ],
          "url" : [ "https://twitter.com/holly", "https://www.instagram.com/hollybrocks/" ],
          "published" : [ "2007-07-18T10:27:16+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "From Nottingham, in London" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1248671410714804225/A_9KJ1y8.jpg" ]
        }
      } ],
      "content" : [ "Someone's sent me a press release about expensive furniture you buy specifically for cats, thus proving they've never met a cat" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
