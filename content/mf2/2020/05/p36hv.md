{
  "date" : "2020-05-19T18:17:17.19Z",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.meetup.com/DevOps-Notts/events/267477518/" ],
    "syndication" : [ "https://www.meetup.com/DevOps-Notts/events/267477518/#rsvp-by-https%3A%2F%2Fwww.jvt.me%2Fmf2%2F2020%2F05%2Fp36hv%2F" ],
    "name" : [ "RSVP yes to https://www.meetup.com/DevOps-Notts/events/267477518/" ],
    "published" : [ "2020-05-19T18:17:17.19Z" ],
    "event" : {
      "start" : [ "2020-05-26T18:30:00+01:00" ],
      "name" : [ "DevOps Notts - May 2020" ],
      "end" : [ "2020-05-26T21:30:00+01:00" ],
      "location" : [ "Online" ],
      "url" : [ "https://www.meetup.com/DevOps-Notts/events/267477518/" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/05/p36hv",
  "client_id" : "https://micropublish.net"
}
