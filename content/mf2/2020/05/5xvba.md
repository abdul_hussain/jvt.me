{
  "date" : "2020-05-21T17:32:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/pascaldoesgo/status/1263476133259075589" ],
    "name" : [ "Like of @pascaldoesgo's tweet" ],
    "published" : [ "2020-05-21T17:32:00+01:00" ],
    "like-of" : [ "https://twitter.com/pascaldoesgo/status/1263476133259075589" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/5xvba",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1263476133259075589" ],
      "url" : [ "https://twitter.com/pascaldoesgo/status/1263476133259075589" ],
      "published" : [ "2020-05-21T14:25:59+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:pascaldoesgo" ],
          "numeric-id" : [ "1003529600901767168" ],
          "name" : [ "Pascal Dennerly" ],
          "nickname" : [ "pascaldoesgo" ],
          "url" : [ "https://twitter.com/pascaldoesgo" ],
          "published" : [ "2018-06-04T06:51:16+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1199757075561222147/eYBkMUrZ.jpg" ]
        }
      } ],
      "content" : [ "Now you may have thought that I'm out of touch with the younglings but look at me, I think I just nailed that Untitled Goose Crossings game." ],
      "photo" : [ "https://pbs.twimg.com/media/EYjE1SAWsAEa6QO.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
