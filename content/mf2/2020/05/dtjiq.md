{
  "date" : "2020-05-19T17:06:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/edent/status/1262762526078427139" ],
    "name" : [ "Like of @edent's tweet" ],
    "published" : [ "2020-05-19T17:06:00+01:00" ],
    "like-of" : [ "https://twitter.com/edent/status/1262762526078427139" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/dtjiq",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1262762526078427139" ],
      "url" : [ "https://twitter.com/edent/status/1262762526078427139" ],
      "published" : [ "2020-05-19T15:10:21+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:edent" ],
          "numeric-id" : [ "14054507" ],
          "name" : [ "Terence Eden" ],
          "nickname" : [ "edent" ],
          "url" : [ "https://twitter.com/edent", "https://shkspr.mobi/blog/", "https://edent.tel" ],
          "published" : [ "2008-02-28T13:10:25+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1228067445153452033/_A8Uq2VY.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "In the last 50 days since being commissioned, @Edent_Solar has generated ONE MEGAWATT HOUR of electricity!\nThat's over double what we use.",
        "html" : "<div style=\"white-space: pre\">In the last 50 days since being commissioned, <a href=\"https://twitter.com/Edent_Solar\">@Edent_Solar</a> has generated ONE MEGAWATT HOUR of electricity!\nThat's over double what we use.</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EYY7WS4UYAUQU3W.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
