{
  "date" : "2020-05-05T22:14:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [  ],
    "name" : [ "Like of @jesslynnrose's tweet" ],
    "published" : [ "2020-05-05T22:14:00+01:00" ],
    "like-of" : [ "https://twitter.com/jesslynnrose/status/1257768119256387586" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/6fcmi",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1257768119256387586" ],
      "url" : [ "https://twitter.com/jesslynnrose/status/1257768119256387586" ],
      "published" : [ "2020-05-05T20:24:22+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:jesslynnrose" ],
          "numeric-id" : [ "1530096708" ],
          "name" : [ "Jessica Rose" ],
          "nickname" : [ "jesslynnrose" ],
          "url" : [ "https://twitter.com/jesslynnrose", "http://jessica.tech" ],
          "published" : [ "2013-06-19T07:56:27+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Birmingham/どこでも" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1252594551807754246/--PG7H_D.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "The best thing about Twitter is you can ask people to show you their pets and they do.\n\nThen if you tell them to give the pets treats or pet them, they often comply and send photos of their compliance.\n\nThis is the power I have always craved.",
        "html" : "<div style=\"white-space: pre\">The best thing about Twitter is you can ask people to show you their pets and they do.\n\nThen if you tell them to give the pets treats or pet them, they often comply and send photos of their compliance.\n\nThis is the power I have always craved.</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
