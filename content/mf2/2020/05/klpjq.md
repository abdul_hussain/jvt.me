{
  "date" : "2020-05-07T23:13:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [  ],
    "name" : [ "Like of @WillDHislop's tweet" ],
    "published" : [ "2020-05-07T23:13:00+01:00" ],
    "like-of" : [ "https://twitter.com/WillDHislop/status/1258457955189641224" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/klpjq",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1258457955189641224" ],
      "url" : [ "https://twitter.com/WillDHislop/status/1258457955189641224" ],
      "video" : [ "https://video.twimg.com/ext_tw_video/1258457441236389888/pu/vid/720x1280/LxPKDXFWqUiWc3Vq.mp4?tag=10" ],
      "published" : [ "2020-05-07T18:05:32+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:WillDHislop" ],
          "numeric-id" : [ "431614488" ],
          "name" : [ "Will Hislop" ],
          "nickname" : [ "WillDHislop" ],
          "url" : [ "https://twitter.com/WillDHislop", "https://www.youtube.com/channel/UCPdBnYNr-aRZat0b8M8522g" ],
          "published" : [ "2011-12-08T13:51:40+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London / Oxford" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/512933176339992576/IWMDFNII.jpeg" ]
        }
      } ],
      "content" : [ {
        "value" : "your aunt at the NHS clap #nhsclap",
        "html" : "your aunt at the NHS clap <a href=\"https://twitter.com/search?q=%23nhsclap\">#nhsclap</a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
