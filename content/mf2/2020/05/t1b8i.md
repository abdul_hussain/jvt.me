{
  "date" : "2020-05-22T21:51:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/ccmiller2018/status/1263921133033869312" ],
    "name" : [ "Like of @ccmiller2018's tweet" ],
    "published" : [ "2020-05-22T21:51:00+01:00" ],
    "like-of" : [ "https://twitter.com/ccmiller2018/status/1263921133033869312" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/t1b8i",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1263921133033869312" ],
      "url" : [ "https://twitter.com/ccmiller2018/status/1263921133033869312" ],
      "published" : [ "2020-05-22T19:54:15+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:ccmiller2018" ],
          "numeric-id" : [ "896386404468609025" ],
          "name" : [ "Christopher Miller" ],
          "nickname" : [ "ccmiller2018" ],
          "url" : [ "https://twitter.com/ccmiller2018" ],
          "published" : [ "2017-08-12T15:02:27+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Cannock, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1005503475353047040/x9aV5e2E.jpg" ]
        }
      } ],
      "content" : [ "Is it just me that really wants to have a sign I can put outside my house that reads “if you can see this sign go to 127.0.0.1”" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
