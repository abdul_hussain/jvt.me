{
  "date" : "2020-05-06T21:21:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/MrsEmma/status/1258023078430478337" ],
    "name" : [ "Like of @MrsEmma's tweet" ],
    "published" : [ "2020-05-06T21:21:00+01:00" ],
    "like-of" : [ "https://twitter.com/MrsEmma/status/1258023078430478337" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/2n4pu",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1258023078430478337" ],
      "url" : [ "https://twitter.com/MrsEmma/status/1258023078430478337" ],
      "published" : [ "2020-05-06T13:17:29+00:00" ],
      "in-reply-to" : [ "https://twitter.com/MrsEmma/status/1258022699852578818" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:MrsEmma" ],
          "numeric-id" : [ "19500955" ],
          "name" : [ "Emma Seward" ],
          "nickname" : [ "MrsEmma" ],
          "url" : [ "https://twitter.com/MrsEmma" ],
          "published" : [ "2009-01-25T19:32:42+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1257444638941683719/mhnu1CKS.jpg" ]
        }
      } ],
      "content" : [ "Am so glad she was too busy talking about herself to ask what I'd achieved because I'd have to then admit that today I put sliced banana and biscoff spread between two pieces of bread and essentially created the banoffee pie sandwich 😐" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
