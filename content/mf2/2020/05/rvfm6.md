{
  "date" : "2020-05-24T18:36:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/OwenJones84/status/1264606110348185600" ],
    "name" : [ "Like of @OwenJones84's tweet" ],
    "published" : [ "2020-05-24T18:36:00+01:00" ],
    "category" : [ "coronavirus", "politics" ],
    "like-of" : [ "https://twitter.com/OwenJones84/status/1264606110348185600" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/rvfm6",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1264606110348185600" ],
      "url" : [ "https://twitter.com/OwenJones84/status/1264606110348185600" ],
      "published" : [ "2020-05-24T17:16:06+00:00" ],
      "in-reply-to" : [ "https://twitter.com/OwenJones84/status/1264603999594053632" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:OwenJones84" ],
          "numeric-id" : [ "65045121" ],
          "name" : [ "Owen Jones says join a union🌹" ],
          "nickname" : [ "OwenJones84" ],
          "url" : [ "https://twitter.com/OwenJones84", "http://www.theguardian.com/profile/owen-jones" ],
          "published" : [ "2009-08-12T14:09:35+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1253067195485126659/x5claxvX.jpg" ]
        }
      } ],
      "content" : [ "Who wants to bet the government spends more time and energy investigating who the hero civil service tweeter is than Dominic Cummings" ]
    }
  },
  "tags" : [ "coronavirus", "politics" ],
  "client_id" : "https://indigenous.realize.be"
}
