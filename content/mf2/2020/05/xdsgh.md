{
  "date" : "2020-05-28T23:31:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [  ],
    "name" : [ "Like of @laurabolton_xo's tweet" ],
    "published" : [ "2020-05-28T23:31:00+01:00" ],
    "like-of" : [ "https://twitter.com/laurabolton_xo/status/1265613896766885889" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/xdsgh",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1265613896766885889" ],
      "url" : [ "https://twitter.com/laurabolton_xo/status/1265613896766885889" ],
      "published" : [ "2020-05-27T12:00:41+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:laurabolton_xo" ],
          "numeric-id" : [ "254685949" ],
          "name" : [ "Laura 💕" ],
          "nickname" : [ "laurabolton_xo" ],
          "url" : [ "https://twitter.com/laurabolton_xo" ],
          "published" : [ "2011-02-19T19:56:42+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Marlow, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1265556375549235200/vkcJqAZF.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Bit of a controversial subject...\n\nLast year I donated eggs to a clinic - today I’ve got an email to say the couple that chose them are pregnant 🥰 no words. ❤️❤️❤️❤️ #ONELOVE",
        "html" : "<div style=\"white-space: pre\">Bit of a controversial subject...\n\nLast year I donated eggs to a clinic - today I’ve got an email to say the couple that chose them are pregnant 🥰 no words. ❤️❤️❤️❤️ <a href=\"https://twitter.com/search?q=%23ONELOVE\">#ONELOVE</a></div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
