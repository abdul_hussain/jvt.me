{
  "date" : "2020-05-16T20:43:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/neil_killick/status/1261535908953600000" ],
    "name" : [ "Like of @neil_killick's tweet" ],
    "published" : [ "2020-05-16T20:43:00+01:00" ],
    "like-of" : [ "https://twitter.com/neil_killick/status/1261535908953600000" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/e7bnx",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1261535908953600000" ],
      "url" : [ "https://twitter.com/neil_killick/status/1261535908953600000" ],
      "published" : [ "2020-05-16T05:56:13+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:neil_killick" ],
          "numeric-id" : [ "47562870" ],
          "name" : [ "Neil Killick" ],
          "nickname" : [ "neil_killick" ],
          "url" : [ "https://twitter.com/neil_killick", "https://www.hypothesis.co/" ],
          "published" : [ "2009-06-16T06:42:39+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Melbourne, Australia" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1244762641483976705/-KPqpUzW.jpg" ]
        }
      } ],
      "content" : [ "About 2.5 years ago, I estimated that my book on software estimation would be finished within 6 months. It is still nowhere near finished." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
