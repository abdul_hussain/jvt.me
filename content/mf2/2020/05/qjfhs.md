{
  "date" : "2020-05-08T10:42:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/ReverseICS/status/1258410379128037376" ],
    "name" : [ "Like of @ReverseICS's tweet" ],
    "published" : [ "2020-05-08T10:42:00+01:00" ],
    "like-of" : [ "https://twitter.com/ReverseICS/status/1258410379128037376" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/qjfhs",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1258410379128037376" ],
      "url" : [ "https://twitter.com/ReverseICS/status/1258410379128037376" ],
      "published" : [ "2020-05-07T14:56:29+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:ReverseICS" ],
          "numeric-id" : [ "341902267" ],
          "name" : [ "' or 1=1; drop table tweets;-- K. Reid Wightman" ],
          "nickname" : [ "ReverseICS" ],
          "url" : [ "https://twitter.com/ReverseICS" ],
          "published" : [ "2011-07-25T04:14:08+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Des Moines, IA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/866712631503093764/nBjuqsi8.jpg" ]
        }
      } ],
      "content" : [ "Prediction: Zoom will get end-to-end encryption, while Keybase will get a websockets listener that binds to 0.0.0.0:8888 and allows anyone to silently install software on your computer." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
