{
  "date" : "2020-05-17T11:47:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/EmmaBostian/status/1261908114128941057" ],
    "name" : [ "Like of @EmmaBostian's tweet" ],
    "published" : [ "2020-05-17T11:47:00+01:00" ],
    "like-of" : [ "https://twitter.com/EmmaBostian/status/1261908114128941057" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/ufmqf",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1261908114128941057" ],
      "url" : [ "https://twitter.com/EmmaBostian/status/1261908114128941057" ],
      "published" : [ "2020-05-17T06:35:14+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:EmmaBostian" ],
          "numeric-id" : [ "491821358" ],
          "name" : [ "Emma Bostian 🐞" ],
          "nickname" : [ "EmmaBostian" ],
          "url" : [ "https://twitter.com/EmmaBostian", "http://compiled.blog" ],
          "published" : [ "2012-02-14T01:57:07+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Karlsruhe, Germany" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1260562338199097345/AqIFOnUo.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "How to compliment a woman in tech... or any field:\n\n✅ Tell her that her course/blog/work is great and you learned a lot\n\n❌ Tell her she’s pretty/beautiful",
        "html" : "<div style=\"white-space: pre\">How to compliment a woman in tech... or any field:\n\n✅ Tell her that her course/blog/work is great and you learned a lot\n\n❌ Tell her she’s pretty/beautiful</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EYMys9ZXgAAxgWB.jpg", "https://pbs.twimg.com/media/EYMys9gXQAEjlaf.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
