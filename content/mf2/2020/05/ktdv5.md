{
  "date" : "2020-05-28T16:55:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://events.indieweb.org/2020/06/microformats-server-admin-livestream-session-2--aBfWilFmJ2cR" ],
    "name" : [ "RSVP interested to https://events.indieweb.org/2020/06/microformats-server-admin-livestream-session-2--aBfWilFmJ2cR" ],
    "published" : [ "2020-05-28T16:55:00+01:00" ],
    "event" : {
      "start" : [ "2020-06-07T09:00:00-07:00" ],
      "name" : [ "Microformats Server Admin Livestream (Session 2)" ],
      "end" : [ "2020-06-07T15:00:00-07:00" ],
      "location" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "name" : [ "Online" ]
        },
        "lang" : "en",
        "value" : "Online"
      } ],
      "url" : [ "https://events.indieweb.org/2020/06/microformats-server-admin-livestream-session-2--aBfWilFmJ2cR" ]
    },
    "rsvp" : [ "interested" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/05/ktdv5",
  "client_id" : "https://indigenous.realize.be"
}
