{
  "date" : "2020-05-10T11:56:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/gitlabstatus/status/1259436943986708480" ],
    "name" : [ "Like of @gitlabstatus's tweet" ],
    "published" : [ "2020-05-10T11:56:00+01:00" ],
    "like-of" : [ "https://twitter.com/gitlabstatus/status/1259436943986708480" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/1ew5t",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1259436943986708480" ],
      "url" : [ "https://twitter.com/gitlabstatus/status/1259436943986708480" ],
      "published" : [ "2020-05-10T10:55:41+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:gitlabstatus" ],
          "numeric-id" : [ "1113698070" ],
          "name" : [ "GitLab.com Status" ],
          "nickname" : [ "gitlabstatus" ],
          "url" : [ "https://twitter.com/gitlabstatus", "http://status.gitlab.com/", "http://GitLab.com" ],
          "published" : [ "2013-01-23T07:17:47+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/694241544899923968/Yj5sO9P4.png" ]
        }
      } ],
      "content" : [ {
        "value" : "Our maintenance finished successfully. gitlab.com is fully up again. Thanks for your patience! status.gitlab.com",
        "html" : "Our maintenance finished successfully. <a href=\"http://gitlab.com\">gitlab.com</a> is fully up again. Thanks for your patience! <a href=\"http://status.gitlab.com\">status.gitlab.com</a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
