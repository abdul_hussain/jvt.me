{
  "date" : "2020-05-25T14:19:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.meetup.com/Tech-Nottingham/events/270794866/" ],
    "syndication" : [ "https://brid.gy/publish/meetup" ],
    "name" : [ "RSVP yes to https://www.meetup.com/Tech-Nottingham/events/270794866/" ],
    "published" : [ "2020-05-25T14:19:00+01:00" ],
    "event" : {
      "start" : [ "2020-08-10T18:30:00+01:00" ],
      "name" : [ "Tech Nottingham 10th August - Lightning Talks! ⚡️" ],
      "end" : [ "2020-08-10T20:30:00+01:00" ],
      "location" : [ "Online" ],
      "url" : [ "https://www.meetup.com/Tech-Nottingham/events/270794866/" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/05/lo7pb",
  "client_id" : "https://indigenous.realize.be"
}
