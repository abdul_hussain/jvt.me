{
  "date" : "2020-05-28T07:23:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/nocontextpawnee/status/1265710702536220673" ],
    "name" : [ "Like of @nocontextpawnee's tweet" ],
    "published" : [ "2020-05-28T07:23:00+01:00" ],
    "category" : [ "parks-and-recreation" ],
    "like-of" : [ "https://twitter.com/nocontextpawnee/status/1265710702536220673" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/gkmgt",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1265710702536220673" ],
      "url" : [ "https://twitter.com/nocontextpawnee/status/1265710702536220673" ],
      "published" : [ "2020-05-27T18:25:21+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:nocontextpawnee" ],
          "numeric-id" : [ "2863580894" ],
          "name" : [ "out of context parks & rec" ],
          "nickname" : [ "nocontextpawnee" ],
          "url" : [ "https://twitter.com/nocontextpawnee", "http://instagram.com/nocontextpawnee" ],
          "published" : [ "2014-10-18T22:28:26+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Pawnee, IN" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1245567416358154240/Q-HCuDe5.jpg" ]
        }
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EZC1KcCUEAECCTq.jpg", "https://pbs.twimg.com/media/EZC1Ke2U0AEYUdI.jpg" ]
    }
  },
  "tags" : [ "parks-and-recreation" ],
  "client_id" : "https://indigenous.realize.be"
}
