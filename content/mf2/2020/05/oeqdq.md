{
  "date" : "2020-05-17T22:26:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/kat_wlodarczyk/status/1262083538108788738" ],
    "name" : [ "Like of @kat_wlodarczyk's tweet" ],
    "published" : [ "2020-05-17T22:26:00+01:00" ],
    "like-of" : [ "https://twitter.com/kat_wlodarczyk/status/1262083538108788738" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/oeqdq",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1262083538108788738" ],
      "url" : [ "https://twitter.com/kat_wlodarczyk/status/1262083538108788738" ],
      "published" : [ "2020-05-17T18:12:18+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:kat_wlodarczyk" ],
          "numeric-id" : [ "1173633756156112897" ],
          "name" : [ "Kat Wlodarczyk" ],
          "nickname" : [ "kat_wlodarczyk" ],
          "url" : [ "https://twitter.com/kat_wlodarczyk", "http://katcodes.home.blog" ],
          "published" : [ "2019-09-16T16:24:34+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Southampton, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1237018361617428480/MuJzikRa.jpg" ]
        }
      } ],
      "content" : [ "I know I said it for the last 5 days, but now my website is REALLY finished. Hoping I won’t find another thing to add-on/enhance tomorrow 🙊😂🤷‍♀️" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
