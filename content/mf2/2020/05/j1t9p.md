{
  "date" : "2020-05-12T07:41:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/smurtagh/status/1259904274340941824" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1260099539224219648" ],
    "name" : [ "Reply to https://twitter.com/smurtagh/status/1259904274340941824" ],
    "published" : [ "2020-05-12T07:41:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "Alternatively `git config --global push.default current` https://www.jvt.me/posts/2019/09/22/git-push-matching/"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/05/j1t9p",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1259904274340941824" ],
      "url" : [ "https://twitter.com/smurtagh/status/1259904274340941824" ],
      "published" : [ "2020-05-11T17:52:41+00:00" ],
      "in-reply-to" : [ "https://twitter.com/laurieontech/status/1259901862163210247" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:smurtagh" ],
          "numeric-id" : [ "18482041" ],
          "name" : [ "Santi Murtagh" ],
          "nickname" : [ "smurtagh" ],
          "url" : [ "https://twitter.com/smurtagh" ],
          "published" : [ "2008-12-30T17:15:28+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Lincoln, NE" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/819763170503847937/uF8nq8F9.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Me: git push\n\nGit: did you mean git push --set-upstream origin branchname?\n\nMe: copy/paste what git said.",
        "html" : "<div style=\"white-space: pre\">Me: git push\n\nGit: did you mean git push --set-upstream origin branchname?\n\nMe: copy/paste what git said.</div>\n<a class=\"u-mention\" href=\"https://twitter.com/laurieontech\"></a>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
