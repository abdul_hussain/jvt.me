{
  "date" : "2020-05-10T11:57:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/Benpowelluk/status/1258837036942966784" ],
    "name" : [ "Like of @Benpowelluk's tweet" ],
    "published" : [ "2020-05-10T11:57:00+01:00" ],
    "category" : [ "coronavirus" ],
    "like-of" : [ "https://twitter.com/Benpowelluk/status/1258837036942966784" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/h4zen",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1258837036942966784" ],
      "url" : [ "https://twitter.com/Benpowelluk/status/1258837036942966784" ],
      "published" : [ "2020-05-08T19:11:52+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Benpowelluk" ],
          "numeric-id" : [ "20147994" ],
          "name" : [ "Ben Powell" ],
          "nickname" : [ "Benpowelluk" ],
          "url" : [ "https://twitter.com/Benpowelluk" ],
          "published" : [ "2009-02-05T13:35:25+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Frodsham, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1207666383745638400/BjyJKPIg.jpg" ]
        }
      } ],
      "content" : [ "Nothing says \"thank you for your sacrifice\" like refusing to make one yourself" ],
      "photo" : [ "https://pbs.twimg.com/media/EXhJmV8XYAAbnUa.jpg" ]
    }
  },
  "tags" : [ "coronavirus" ],
  "client_id" : "https://indigenous.realize.be"
}
