{
  "date" : "2020-05-11T10:11:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/nora_js/status/1259593769101344768" ],
    "name" : [ "Like of @nora_js's tweet" ],
    "published" : [ "2020-05-11T10:11:00+01:00" ],
    "like-of" : [ "https://twitter.com/nora_js/status/1259593769101344768" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/fndvc",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1259593769101344768" ],
      "url" : [ "https://twitter.com/nora_js/status/1259593769101344768" ],
      "published" : [ "2020-05-10T21:18:51+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:nora_js" ],
          "numeric-id" : [ "37360448" ],
          "name" : [ "Nora Jones" ],
          "nickname" : [ "nora_js" ],
          "url" : [ "https://twitter.com/nora_js", "https://www.youtube.com/watch?v=rgfww8tLM0A" ],
          "published" : [ "2009-05-03T04:06:40+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "San Francisco, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/947195421310402560/OknXjx-3.jpg" ]
        }
      } ],
      "content" : [ "Certainly wasn’t the May 9th wedding we had dreamed of, but we found a (very) remote spot outside, propped out a camera and a tripod, and got married. It was a pretty perfect day 😊. Like true tech nerds, we’re calling it our “soft launch” until a real wedding 😆" ],
      "photo" : [ "https://pbs.twimg.com/media/EXr522FU8AMTcj2.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
