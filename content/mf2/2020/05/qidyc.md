{
  "date" : "2020-05-14T12:53:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/ianmiell/status/1260866540490104833" ],
    "name" : [ "Like of @ianmiell's tweet" ],
    "published" : [ "2020-05-14T12:53:00+01:00" ],
    "like-of" : [ "https://twitter.com/ianmiell/status/1260866540490104833" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/qidyc",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1260866540490104833" ],
      "url" : [ "https://twitter.com/ianmiell/status/1260866540490104833" ],
      "published" : [ "2020-05-14T09:36:23+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:ianmiell" ],
          "numeric-id" : [ "58017706" ],
          "name" : [ "Ian Miell" ],
          "nickname" : [ "ianmiell" ],
          "url" : [ "https://twitter.com/ianmiell", "http://zwischenzugs.com", "http://bit.ly/35X34Ew" ],
          "published" : [ "2009-07-18T19:59:32+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1120618937253793793/xb6_9WMl.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I'm seriously considering getting a colleague to change his name from greg to grep.\n\nFrankly, it's inconsiderate that he hasn't already. I lose minutes every day because of this.",
        "html" : "<div style=\"white-space: pre\">I'm seriously considering getting a colleague to change his name from greg to grep.\n\nFrankly, it's inconsiderate that he hasn't already. I lose minutes every day because of this.</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
