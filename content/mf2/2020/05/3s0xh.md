{
  "date" : "2020-05-30T17:11:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamieTanna/status/1266765293109592066" ],
    "published" : [ "2020-05-30T17:11:00+01:00" ],
    "repost-of" : [ "https://twitter.com/sk8wolfhard/status/1266081329533370370" ]
  },
  "kind" : "reposts",
  "slug" : "2020/05/3s0xh",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1266081329533370370" ],
      "url" : [ "https://twitter.com/sk8wolfhard/status/1266081329533370370" ],
      "published" : [ "2020-05-28T18:58:06+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:sk8wolfhard" ],
          "numeric-id" : [ "1052707059039260678" ],
          "name" : [ "𝐧𝐨𝐚𝐡 ᵇˡᵐ" ],
          "nickname" : [ "sk8wolfhard" ],
          "url" : [ "https://twitter.com/sk8wolfhard" ],
          "published" : [ "2018-10-17T23:44:56+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "𝗁𝖾/𝗁𝗂𝗆 • 𝟣𝟪+ " ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1266703015559528449/qZD4K26H.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "don’t scroll \n\nno matter how big or small your platform is, use your voice for a good cause. i'm begging you to stop by and reply or quote this tweet with #BlackLivesMatter",
        "html" : "<div style=\"white-space: pre\">don’t scroll \n\nno matter how big or small your platform is, use your voice for a good cause. i'm begging you to stop by and reply or quote this tweet with <a href=\"https://twitter.com/search?q=%23BlackLivesMatter\">#BlackLivesMatter</a></div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EZIGP6tXYAACj-W.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
