{
  "date" : "2020-05-28T07:36:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/holly/status/1265771578047987713" ],
    "name" : [ "Like of @holly's tweet" ],
    "published" : [ "2020-05-28T07:36:00+01:00" ],
    "like-of" : [ "https://twitter.com/holly/status/1265771578047987713" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/e770c",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1265771578047987713" ],
      "url" : [ "https://twitter.com/holly/status/1265771578047987713" ],
      "published" : [ "2020-05-27T22:27:15+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:holly" ],
          "numeric-id" : [ "7555262" ],
          "name" : [ "Holly Brockwell" ],
          "nickname" : [ "holly" ],
          "url" : [ "https://twitter.com/holly", "https://www.instagram.com/hollybrocks/" ],
          "published" : [ "2007-07-18T10:27:16+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "From Nottingham, in London" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1248671410714804225/A_9KJ1y8.jpg" ]
        }
      } ],
      "content" : [ "I think this is the sexiest thing I've ever seen" ],
      "photo" : [ "https://pbs.twimg.com/media/EZDshQsWoAEzvb-.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
