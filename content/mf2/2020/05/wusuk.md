{
  "date" : "2020-05-16T12:46:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/edent/status/1261612738029240320" ],
    "name" : [ "Like of @edent's tweet" ],
    "published" : [ "2020-05-16T12:46:00+01:00" ],
    "like-of" : [ "https://twitter.com/edent/status/1261612738029240320" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/wusuk",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1261612738029240320" ],
      "url" : [ "https://twitter.com/edent/status/1261612738029240320" ],
      "published" : [ "2020-05-16T11:01:31+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:edent" ],
          "numeric-id" : [ "14054507" ],
          "name" : [ "Terence Eden" ],
          "nickname" : [ "edent" ],
          "url" : [ "https://twitter.com/edent", "https://shkspr.mobi/blog/", "https://edent.tel" ],
          "published" : [ "2008-02-28T13:10:25+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1228067445153452033/_A8Uq2VY.jpg" ]
        }
      } ],
      "content" : [ "Upgrading my book so I can read books on my book." ],
      "photo" : [ "https://pbs.twimg.com/media/EYIl_jNXsAE5h_s.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
