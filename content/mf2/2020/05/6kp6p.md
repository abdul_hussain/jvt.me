{
  "date" : "2020-05-17T00:00:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/rwdrich/status/1261760226878988288" ],
    "name" : [ "Like of @rwdrich's tweet" ],
    "published" : [ "2020-05-17T00:00:00+01:00" ],
    "like-of" : [ "https://twitter.com/rwdrich/status/1261760226878988288" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/6kp6p",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1261760226878988288" ],
      "url" : [ "https://twitter.com/rwdrich/status/1261760226878988288" ],
      "published" : [ "2020-05-16T20:47:35+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:rwdrich" ],
          "numeric-id" : [ "107548386" ],
          "name" : [ "Richard Davies" ],
          "nickname" : [ "rwdrich" ],
          "url" : [ "https://twitter.com/rwdrich", "http://rwdrich.co.uk" ],
          "published" : [ "2010-01-22T23:14:30+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Cambridge, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/917481919872491521/BwypzjEE.jpg" ]
        }
      } ],
      "content" : [ "Also, if I'm engaged, why is my marital status on forms for like everywhere 'Single' !?" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
