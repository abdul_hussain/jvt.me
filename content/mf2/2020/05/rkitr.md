{
  "date" : "2020-05-26T21:52:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/pleia2/status/1265367906507882497" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1265386282739433483" ],
    "name" : [ "Reply to https://twitter.com/pleia2/status/1265367906507882497" ],
    "published" : [ "2020-05-26T21:52:00+01:00" ],
    "category" : [ "linux", "command-line" ],
    "content" : [ {
      "html" : "",
      "value" : "@annadodson.co.uk and I had good fun a couple of years ago with https://github.com/veltman/clmystery and https://overthewire.org/wargames/"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/05/rkitr",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1265367906507882497" ],
      "url" : [ "https://twitter.com/pleia2/status/1265367906507882497" ],
      "published" : [ "2020-05-26T19:43:12+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:pleia2" ],
          "numeric-id" : [ "14695816" ],
          "name" : [ "Elizabeth K. Joseph" ],
          "nickname" : [ "pleia2" ],
          "url" : [ "https://twitter.com/pleia2", "http://princessleia.com" ],
          "published" : [ "2008-05-08T03:50:04+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "SF Bay Area ✈️ Philadelphia 🚆" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1160570696373686272/GSfbpXEF.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Linux Twitterverse,\n\nWhat's your favorite \"Linux 101\" project? Something small, a 10-15 minute kind of deal.\n\nAnd what's your favorite command line game on Linux? The obvious go-to is nethack, but others?\n\nYes, I can do a Google search, but I'm asking YOU.",
        "html" : "<div style=\"white-space: pre\">Linux Twitterverse,\n\nWhat's your favorite \"Linux 101\" project? Something small, a 10-15 minute kind of deal.\n\nAnd what's your favorite command line game on Linux? The obvious go-to is nethack, but others?\n\nYes, I can do a Google search, but I'm asking YOU.</div>"
      } ]
    }
  },
  "tags" : [ "linux", "command-line" ],
  "client_id" : "https://indigenous.realize.be"
}
