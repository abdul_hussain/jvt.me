{
  "date" : "2020-05-31T10:39:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/andymilonakis/status/1266821069823975430" ],
    "name" : [ "Like of @andymilonakis's tweet" ],
    "published" : [ "2020-05-31T10:39:00+01:00" ],
    "like-of" : [ "https://twitter.com/andymilonakis/status/1266821069823975430" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/gqrx4",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1266821069823975430" ],
      "url" : [ "https://twitter.com/andymilonakis/status/1266821069823975430" ],
      "published" : [ "2020-05-30T19:57:34+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:andymilonakis" ],
          "numeric-id" : [ "17883546" ],
          "name" : [ "Andy Milonakis" ],
          "nickname" : [ "andymilonakis" ],
          "url" : [ "https://twitter.com/andymilonakis", "http://www.twitch.tv/andymilonakis" ],
          "published" : [ "2008-12-04T23:42:52+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Koreatown, Los Angeles" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1265781630301548545/RjzMqhTF.jpg" ]
        }
      } ],
      "content" : [ "Congratulations to the Astronauts that left Earth today. Good choice" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
