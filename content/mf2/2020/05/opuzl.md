{
  "date" : "2020-05-29T12:16:00.986Z",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://events.indieweb.org/2020/07/online-homebrew-website-club-nottingham-xZ25kFM3qUYx" ],
    "name" : [ "RSVP yes to https://events.indieweb.org/2020/07/online-homebrew-website-club-nottingham-xZ25kFM3qUYx" ],
    "published" : [ "2020-05-29T12:16:00.986Z" ],
    "event" : {
      "start" : [ "2020-07-08T17:30:00+01:00" ],
      "name" : [ "ONLINE: Homebrew Website Club: Nottingham" ],
      "end" : [ "2020-07-08T19:30:00+01:00" ],
      "location" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "name" : [ "Online" ],
          "latitude" : [ "52.9509448" ],
          "longitude" : [ "-1.1522525" ]
        },
        "lang" : "en",
        "value" : "Online"
      } ],
      "url" : [ "https://events.indieweb.org/2020/07/online-homebrew-website-club-nottingham-xZ25kFM3qUYx" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/05/opuzl",
  "client_id" : "https://micropublish.net"
}
