{
  "date" : "2020-05-04T22:20:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/MrAndrew/status/1257416328652173312" ],
    "name" : [ "Like of @MrAndrew's tweet" ],
    "published" : [ "2020-05-04T22:20:00+01:00" ],
    "like-of" : [ "https://twitter.com/MrAndrew/status/1257416328652173312" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/9tbp8",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1257416328652173312" ],
      "url" : [ "https://twitter.com/MrAndrew/status/1257416328652173312" ],
      "published" : [ "2020-05-04T21:06:29+00:00" ],
      "in-reply-to" : [ "https://twitter.com/CarolSaysThings/status/1257369145802395650" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:MrAndrew" ],
          "numeric-id" : [ "9626182" ],
          "name" : [ "Andrew Seward" ],
          "nickname" : [ "MrAndrew" ],
          "url" : [ "https://twitter.com/MrAndrew", "http://www.technottingham.com" ],
          "published" : [ "2007-10-23T15:57:18+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Riddings, Derbyshire" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1246456234452140032/romkM0EV.jpg" ]
        }
      } ],
      "location" : [ {
        "type" : [ "h-card", "p-location" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:50f2d0272381533f" ],
          "name" : [ "East Midlands, England" ]
        }
      } ],
      "content" : [ {
        "value" : "My PlayStation has started doing a thing where it won't turn on and you have to turn it off and on again at the wall, sometimes a few times over before it works. \nShould I buy a PS4 Pro or put up with the power thing and wait for the PS5 at the end of the year?",
        "html" : "<div style=\"white-space: pre\">My PlayStation has started doing a thing where it won't turn on and you have to turn it off and on again at the wall, sometimes a few times over before it works. \nShould I buy a PS4 Pro or put up with the power thing and wait for the PS5 at the end of the year?</div>\n<a class=\"u-mention\" href=\"https://twitter.com/CarolSaysThings\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
