{
  "date" : "2020-05-03T17:14:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/_elletownsend/status/1256937696469094400" ],
    "name" : [ "Like of @_elletownsend's tweet" ],
    "published" : [ "2020-05-03T17:14:00+01:00" ],
    "like-of" : [ "https://twitter.com/_elletownsend/status/1256937696469094400" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/rnsuw",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1256937696469094400" ],
      "url" : [ "https://twitter.com/_elletownsend/status/1256937696469094400" ],
      "published" : [ "2020-05-03T13:24:34+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:_elletownsend" ],
          "numeric-id" : [ "4308006796" ],
          "name" : [ "Elle Townsend✨" ],
          "nickname" : [ "_elletownsend" ],
          "url" : [ "https://twitter.com/_elletownsend", "http://www.elletownsend.co.uk" ],
          "published" : [ "2015-11-28T14:28:32+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1237757944089006086/67Kjy-LA.jpg" ]
        }
      } ],
      "content" : [ "I have so many ideas, I think this calls for some changes to my personal site 👩‍💻💡" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
