{
  "date" : "2020-05-01T23:53:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/bryanl/status/1256300226408783872" ],
    "name" : [ "Like of @bryanl's tweet" ],
    "published" : [ "2020-05-01T23:53:00+01:00" ],
    "like-of" : [ "https://twitter.com/bryanl/status/1256300226408783872" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/grqkg",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1256300226408783872" ],
      "url" : [ "https://twitter.com/bryanl/status/1256300226408783872" ],
      "published" : [ "2020-05-01T19:11:29+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:bryanl" ],
          "numeric-id" : [ "659933" ],
          "name" : [ "Bryan Liles" ],
          "nickname" : [ "bryanl" ],
          "url" : [ "https://twitter.com/bryanl", "http://blil.es" ],
          "published" : [ "2007-01-18T12:47:55+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Bowie, MD" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1204051801638678528/yZNnhAQP.jpg" ]
        }
      } ],
      "content" : [ "Learning vim is a life skill. It's like tying your shoes and learning to put your shirt on the right way. It's also more useful than both of the previous examples." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
