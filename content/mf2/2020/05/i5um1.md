{
  "date" : "2020-05-06T21:18:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/rwdrich/status/1258119342857900032" ],
    "name" : [ "Like of @rwdrich's tweet" ],
    "published" : [ "2020-05-06T21:18:00+01:00" ],
    "like-of" : [ "https://twitter.com/rwdrich/status/1258119342857900032" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/i5um1",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1258119342857900032" ],
      "url" : [ "https://twitter.com/rwdrich/status/1258119342857900032" ],
      "published" : [ "2020-05-06T19:40:00+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:rwdrich" ],
          "numeric-id" : [ "107548386" ],
          "name" : [ "Richard Davies" ],
          "nickname" : [ "rwdrich" ],
          "url" : [ "https://twitter.com/rwdrich", "http://rwdrich.co.uk" ],
          "published" : [ "2010-01-22T23:14:30+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Cambridge, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/917481919872491521/BwypzjEE.jpg" ]
        }
      } ],
      "content" : [ "I wish I had this level of confidence. Of style. Of sophistication. In real life..." ],
      "photo" : [ "https://pbs.twimg.com/media/EXW820CVcAEX6YO.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
