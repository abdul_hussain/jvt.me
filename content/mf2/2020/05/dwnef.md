{
  "date" : "2020-05-23T19:59:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/holly/status/1264261822011576320" ],
    "name" : [ "Like of @holly's tweet" ],
    "published" : [ "2020-05-23T19:59:00+01:00" ],
    "like-of" : [ "https://twitter.com/holly/status/1264261822011576320" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/dwnef",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1264261822011576320" ],
      "url" : [ "https://twitter.com/holly/status/1264261822011576320" ],
      "published" : [ "2020-05-23T18:28:01+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:holly" ],
          "numeric-id" : [ "7555262" ],
          "name" : [ "Holly Brockwell" ],
          "nickname" : [ "holly" ],
          "url" : [ "https://twitter.com/holly", "https://www.instagram.com/hollybrocks/" ],
          "published" : [ "2007-07-18T10:27:16+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "From Nottingham, in London" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1248671410714804225/A_9KJ1y8.jpg" ]
        }
      } ],
      "content" : [ "Has anyone done 'Cummings and Goings' as a headline yet?" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
