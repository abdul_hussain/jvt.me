{
  "date" : "2020-05-18T21:02:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/hankchizljaw/status/1262462304471388160" ],
    "name" : [ "Like of @hankchizljaw's tweet" ],
    "published" : [ "2020-05-18T21:02:00+01:00" ],
    "like-of" : [ "https://twitter.com/hankchizljaw/status/1262462304471388160" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/4qbmk",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1262462304471388160" ],
      "url" : [ "https://twitter.com/hankchizljaw/status/1262462304471388160" ],
      "published" : [ "2020-05-18T19:17:23+00:00" ],
      "in-reply-to" : [ "https://twitter.com/hankchizljaw/status/1262458354481467395" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:hankchizljaw" ],
          "numeric-id" : [ "98734097" ],
          "name" : [ "Andy Bell" ],
          "nickname" : [ "hankchizljaw" ],
          "url" : [ "https://twitter.com/hankchizljaw", "https://hankchizljaw.com", "http://hankchizljaw.redbubble.com" ],
          "published" : [ "2009-12-22T22:28:06+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Cheltenham, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1219208545138290689/XoucaVnN.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "zach i trust that my one edge-case project means that you will cancel this venture",
        "html" : "zach i trust that my one edge-case project means that you will cancel this venture\n<a class=\"u-mention\" href=\"https://twitter.com/zachleat\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
