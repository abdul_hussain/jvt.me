{
  "date" : "2020-05-28T16:45:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/efinlay24/status/1265974653413097474" ],
    "name" : [ "Like of @efinlay24's tweet" ],
    "published" : [ "2020-05-28T16:45:00+01:00" ],
    "category" : [ "plants" ],
    "like-of" : [ "https://twitter.com/efinlay24/status/1265974653413097474" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/q7red",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1265974653413097474" ],
      "url" : [ "https://twitter.com/efinlay24/status/1265974653413097474" ],
      "published" : [ "2020-05-28T11:54:12+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:efinlay24" ],
          "numeric-id" : [ "921314477907304448" ],
          "name" : [ "Euan Finlay" ],
          "nickname" : [ "efinlay24" ],
          "url" : [ "https://twitter.com/efinlay24" ],
          "published" : [ "2017-10-20T09:57:43+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1195902695959339014/YWhBj5RQ.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "my mum decided to buy me a terrarium as a moving in present, which is really nice! 🌵\n\nbit crowded though, there's no room for succulent social distancing",
        "html" : "<div style=\"white-space: pre\">my mum decided to buy me a terrarium as a moving in present, which is really nice! 🌵\n\nbit crowded though, there's no room for succulent social distancing</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EZGlKZuWkAEJJBC.jpg", "https://pbs.twimg.com/media/EZGlKZvX0AAe3_l.jpg", "https://pbs.twimg.com/media/EZGlNV8WsAAnGJz.jpg" ]
    }
  },
  "tags" : [ "plants" ],
  "client_id" : "https://indigenous.realize.be"
}
