{
  "date" : "2020-05-07T20:07:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamieTanna/status/1258475100870934530" ],
    "published" : [ "2020-05-07T20:07:00+01:00" ],
    "repost-of" : [ "https://twitter.com/KarinaVoggel/status/1257624269191811073" ],
    "category" : [ "diversity-and-inclusion" ]
  },
  "kind" : "reposts",
  "slug" : "2020/05/xwakb",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1257624269191811073" ],
      "url" : [ "https://twitter.com/KarinaVoggel/status/1257624269191811073" ],
      "published" : [ "2020-05-05T10:52:45+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:KarinaVoggel" ],
          "numeric-id" : [ "19149703" ],
          "name" : [ "Karina Voggel ✨🔭" ],
          "nickname" : [ "KarinaVoggel" ],
          "url" : [ "https://twitter.com/KarinaVoggel", "https://sites.google.com/view/karina-voggel/home" ],
          "published" : [ "2009-01-18T16:58:34+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Salt Lake City, UT" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1131953085415329793/4arnwCQu.jpg" ]
        }
      } ],
      "content" : [ "Instead of \"Advice for being a woman in Science\", can we agree to just have \"How to not be an asshole that drives women out of science\" course for men? I am sick of all the advice. It puts the work on women how to navigate a shitty system made for and by men." ]
    }
  },
  "tags" : [ "diversity-and-inclusion" ],
  "client_id" : "https://indigenous.realize.be"
}
