{
  "date" : "2020-05-06T08:19:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/ctrlshifti/status/1257289850014883841" ],
    "name" : [ "Like of @ctrlshifti's tweet" ],
    "published" : [ "2020-05-06T08:19:00+01:00" ],
    "like-of" : [ "https://twitter.com/ctrlshifti/status/1257289850014883841" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/ev3h4",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1257289850014883841" ],
      "url" : [ "https://twitter.com/ctrlshifti/status/1257289850014883841" ],
      "published" : [ "2020-05-04T12:43:54+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:ctrlshifti" ],
          "numeric-id" : [ "1047644343874465792" ],
          "name" : [ "Katerina Borodina 🦄" ],
          "nickname" : [ "ctrlshifti" ],
          "url" : [ "https://twitter.com/ctrlshifti", "https://www.codemopolitan.com/", "https://www.instagram.com/kat.on.art/" ],
          "published" : [ "2018-10-04T00:27:30+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Sydney, Australia" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1247738400523730947/-X9NPaxI.jpg" ]
        }
      } ],
      "content" : [ "SOLID code? no, my code is LIQUID: Low In Quality, Unrivaled In Despair" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
