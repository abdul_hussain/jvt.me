{
  "date" : "2020-05-08T10:30:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/edent/status/1258671476418514944" ],
    "name" : [ "Like of @edent's tweet" ],
    "published" : [ "2020-05-08T10:30:00+01:00" ],
    "like-of" : [ "https://twitter.com/edent/status/1258671476418514944" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/l7sgs",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1258671476418514944" ],
      "url" : [ "https://twitter.com/edent/status/1258671476418514944" ],
      "published" : [ "2020-05-08T08:13:59+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:edent" ],
          "numeric-id" : [ "14054507" ],
          "name" : [ "Terence Eden" ],
          "nickname" : [ "edent" ],
          "url" : [ "https://twitter.com/edent", "https://shkspr.mobi/blog/", "https://edent.tel" ],
          "published" : [ "2008-02-28T13:10:25+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1228067445153452033/_A8Uq2VY.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Bit of home improvement on a bank holiday weekend.\nFound this Mondrian-inspired privacy film. Splash of colour and less of a dust trap than net curtains.\n\nIs… is this what being grown-up feels like?",
        "html" : "<div style=\"white-space: pre\">Bit of home improvement on a bank holiday weekend.\nFound this Mondrian-inspired privacy film. Splash of colour and less of a dust trap than net curtains.\n\nIs… is this what being grown-up feels like?</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EXey0h4WkAAP5HQ.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
