{
  "date" : "2020-05-16T17:18:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [  ],
    "name" : [ "Like of @RabbiLLevenberg's tweet" ],
    "published" : [ "2020-05-16T17:18:00+01:00" ],
    "like-of" : [ "https://twitter.com/RabbiLLevenberg/status/1261330714890362882" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/17tvf",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1261330714890362882" ],
      "url" : [ "https://twitter.com/RabbiLLevenberg/status/1261330714890362882" ],
      "published" : [ "2020-05-15T16:20:51+00:00" ],
      "in-reply-to" : [ "https://twitter.com/clapifyoulikeme/status/1261198285886435329" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:RabbiLLevenberg" ],
          "numeric-id" : [ "122276976" ],
          "name" : [ "Lisa Levenberg 🌈" ],
          "nickname" : [ "RabbiLLevenberg" ],
          "url" : [ "https://twitter.com/RabbiLLevenberg" ],
          "published" : [ "2010-03-12T05:17:25+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Bay Area" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1252801664764936193/eXpo6GFm.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "My partner has a follow-up observation that further disrupts the status quo: doesn’t that mean that the “p” is silent, like in pterodactyl and pterosaur? So we should be saying HELICOTTER",
        "html" : "My partner has a follow-up observation that further disrupts the status quo: doesn’t that mean that the “p” is silent, like in pterodactyl and pterosaur? So we should be saying HELICOTTER\n<a class=\"u-mention\" href=\"https://twitter.com/clapifyoulikeme\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
