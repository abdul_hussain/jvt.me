{
  "date" : "2020-05-05T20:03:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [  ],
    "name" : [ "Like of @yahoo_pete's tweet" ],
    "published" : [ "2020-05-05T20:03:00+01:00" ],
    "like-of" : [ "https://twitter.com/yahoo_pete/status/1257701863832670209" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/fbepe",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1257701863832670209" ],
      "url" : [ "https://twitter.com/yahoo_pete/status/1257701863832670209" ],
      "published" : [ "2020-05-05T16:01:05+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:yahoo_pete" ],
          "numeric-id" : [ "383303458" ],
          "name" : [ "Pete Herlihy" ],
          "nickname" : [ "yahoo_pete" ],
          "url" : [ "https://twitter.com/yahoo_pete" ],
          "published" : [ "2011-10-01T16:49:58+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "www" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1568171796/cropped-kiwi_love_sc1.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "GOV.​UK Notify has just sent its 1 billionth message... 🥳💥\n\nOur little team has come a hell of a long way in less than 4 years. From a fledgling startup inside government, scrapping for funding - to a thriving business, critical to the entire public sector.\n\nAn amazing team 🙌🏼",
        "html" : "<div style=\"white-space: pre\">GOV.​UK Notify has just sent its 1 billionth message... 🥳💥\n\nOur little team has come a hell of a long way in less than 4 years. From a fledgling startup inside government, scrapping for funding - to a thriving business, critical to the entire public sector.\n\nAn amazing team 🙌🏼</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
