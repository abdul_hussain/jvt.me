{
  "date" : "2020-05-10T22:24:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamesCoyne/status/1259094157735444480" ],
    "name" : [ "Like of @JamesCoyne's tweet" ],
    "published" : [ "2020-05-10T22:24:00+01:00" ],
    "like-of" : [ "https://twitter.com/JamesCoyne/status/1259094157735444480" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/mcvpd",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1259094157735444480" ],
      "url" : [ "https://twitter.com/JamesCoyne/status/1259094157735444480" ],
      "published" : [ "2020-05-09T12:13:34+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:JamesCoyne" ],
          "numeric-id" : [ "23354211" ],
          "name" : [ "Jimmy Two Shoes 👟👟" ],
          "nickname" : [ "JamesCoyne" ],
          "url" : [ "https://twitter.com/JamesCoyne", "https://linktr.ee/JamesCoyne" ],
          "published" : [ "2009-03-08T21:04:50+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1079171494054449152/DIaJWZxR.jpg" ]
        }
      } ],
      "content" : [ "So much to think about" ],
      "photo" : [ "https://pbs.twimg.com/media/EXkzcu3WsAAhkHL.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
