{
  "kind": "reads",
  "slug": "2020/05/8yryq",
  "date": "2020-05-12T00:20:00Z",
  "h": "h-entry",
  "properties": {
    "published": [
      "2020-05-12T00:20:00Z"
    ],
    "read-status": [
      "finished"
    ],
    "read-of": [
      {
        "type": [
          "h-cite"
        ],
        "properties": {
          "name": [
            "Leviathan Wakes"
          ],
          "author": [
            "James S. A. Corey"
          ],
          "isbn": [
            "1841499889"
          ]
        }
      }
    ]
  }
}

