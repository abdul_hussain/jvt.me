{
  "date" : "2020-05-25T15:39:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/NYGovCuomo/status/1264558205683843073" ],
    "name" : [ "Like of @NYGovCuomo's tweet" ],
    "published" : [ "2020-05-25T15:39:00+01:00" ],
    "category" : [ "coronavirus" ],
    "like-of" : [ "https://twitter.com/NYGovCuomo/status/1264558205683843073" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/wrnte",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1264558205683843073" ],
      "url" : [ "https://twitter.com/NYGovCuomo/status/1264558205683843073" ],
      "published" : [ "2020-05-24T14:05:45+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:NYGovCuomo" ],
          "numeric-id" : [ "232268199" ],
          "name" : [ "Andrew Cuomo" ],
          "nickname" : [ "NYGovCuomo" ],
          "url" : [ "https://twitter.com/NYGovCuomo", "http://www.ny.gov/social-media-policy" ],
          "published" : [ "2010-12-30T17:54:04+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "New York" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1135023027668824064/NkGFOCe6.png" ]
        }
      } ],
      "content" : [ "Health care workers wear masks for 5+ hours non-stop. You can wear one for 45 minutes." ]
    }
  },
  "tags" : [ "coronavirus" ],
  "client_id" : "https://indigenous.realize.be"
}
