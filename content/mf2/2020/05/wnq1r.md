{
  "date" : "2020-05-12T21:26:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/halkyardo/status/1259923179021221889" ],
    "name" : [ "Like of @halkyardo's tweet" ],
    "published" : [ "2020-05-12T21:26:00+01:00" ],
    "category" : [ "cute" ],
    "like-of" : [ "https://twitter.com/halkyardo/status/1259923179021221889" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/wnq1r",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1259923179021221889" ],
      "url" : [ "https://twitter.com/halkyardo/status/1259923179021221889" ],
      "published" : [ "2020-05-11T19:07:48+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:halkyardo" ],
          "numeric-id" : [ "15001249" ],
          "name" : [ "Richard" ],
          "nickname" : [ "halkyardo" ],
          "url" : [ "https://twitter.com/halkyardo", "http://instagram.com/halkyardo" ],
          "published" : [ "2008-06-04T04:18:20+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Minnesota" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1235630770871558144/pynKpuHp.jpg" ]
        }
      } ],
      "content" : [ "Mocha has just discovered the joy of eating small morsels of damp meat, and is now unstoppable." ],
      "photo" : [ "https://pbs.twimg.com/media/EXwlbAUWsAQE9Vw.jpg" ]
    }
  },
  "tags" : [ "cute" ],
  "client_id" : "https://indigenous.realize.be"
}
