{
  "date" : "2020-05-10T15:03:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/WiT_Notts/status/1259480238062538752" ],
    "name" : [ "Like of @WiT_Notts's tweet" ],
    "published" : [ "2020-05-10T15:03:00+01:00" ],
    "like-of" : [ "https://twitter.com/WiT_Notts/status/1259480238062538752" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/dt6yj",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1259480238062538752" ],
      "url" : [ "https://twitter.com/WiT_Notts/status/1259480238062538752" ],
      "video" : [ "https://video.twimg.com/tweet_video/EXqSl9yXYAECbou.mp4" ],
      "published" : [ "2020-05-10T13:47:43+00:00" ],
      "in-reply-to" : [ "https://twitter.com/JamieTanna/status/1259463615586975745" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:WiT_Notts" ],
          "numeric-id" : [ "4339158441" ],
          "name" : [ "Women In Tech, Nottingham 🌈✨" ],
          "nickname" : [ "WiT_Notts" ],
          "url" : [ "https://twitter.com/WiT_Notts", "https://nott.tech/wit" ],
          "published" : [ "2015-12-01T11:22:25+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/913380298829844481/YvPcjPlE.jpg" ]
        }
      } ],
      "content" : [ {
        "html" : "\n<a class=\"u-mention\" href=\"https://twitter.com/CarolSaysThings\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/LittleHelli\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/anna_hax\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/marmaladegirl\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/short_louise\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
