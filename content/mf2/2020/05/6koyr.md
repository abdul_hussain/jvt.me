{
  "date" : "2020-05-25T15:38:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [  ],
    "name" : [ "Like of @paulbiggar's tweet" ],
    "published" : [ "2020-05-25T15:38:00+01:00" ],
    "category" : [ "serverless" ],
    "like-of" : [ "https://twitter.com/paulbiggar/status/1264257243911991305" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/6koyr",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1264257243911991305" ],
      "url" : [ "https://twitter.com/paulbiggar/status/1264257243911991305" ],
      "published" : [ "2020-05-23T18:09:50+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:paulbiggar" ],
          "numeric-id" : [ "86938585" ],
          "name" : [ "Paul Biggar" ],
          "nickname" : [ "paulbiggar" ],
          "url" : [ "https://twitter.com/paulbiggar", "https://darklang.com" ],
          "published" : [ "2009-11-02T13:08:57+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Williamsburg" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1085753285171064832/lfl-GoxH.jpg" ]
        }
      } ],
      "content" : [ "Crying at this typical Serverless infrastructure. wtf happened to the \"simple\" functions-as-a-service idea?" ],
      "photo" : [ "https://pbs.twimg.com/media/EYuKpzCXYAEoNAA.jpg" ]
    }
  },
  "tags" : [ "serverless" ],
  "client_id" : "https://indigenous.realize.be"
}
