{
  "date" : "2020-05-21T18:39:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/garybernhardt/status/1263321315601035265" ],
    "name" : [ "Like of @garybernhardt's tweet" ],
    "published" : [ "2020-05-21T18:39:00+01:00" ],
    "like-of" : [ "https://twitter.com/garybernhardt/status/1263321315601035265" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/rbesr",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1263321315601035265" ],
      "url" : [ "https://twitter.com/garybernhardt/status/1263321315601035265" ],
      "published" : [ "2020-05-21T04:10:47+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:garybernhardt" ],
          "numeric-id" : [ "809685" ],
          "name" : [ "Gary Bernhardt" ],
          "nickname" : [ "garybernhardt" ],
          "url" : [ "https://twitter.com/garybernhardt", "http://destroyallsoftware.com" ],
          "published" : [ "2007-03-04T05:12:14+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Seattle, WA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1170938305/twitter_headshot.png" ]
        }
      } ],
      "content" : [ "anything that doesn't reward pedantry definitely isn't programming" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
