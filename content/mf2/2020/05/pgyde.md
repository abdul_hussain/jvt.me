{
  "date" : "2020-05-19T12:36:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JessPWhite/status/1262460297723420672" ],
    "name" : [ "Like of @JessPWhite's tweet" ],
    "published" : [ "2020-05-19T12:36:00+01:00" ],
    "like-of" : [ "https://twitter.com/JessPWhite/status/1262460297723420672" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/pgyde",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1262460297723420672" ],
      "url" : [ "https://twitter.com/JessPWhite/status/1262460297723420672" ],
      "published" : [ "2020-05-18T19:09:25+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:JessPWhite" ],
          "numeric-id" : [ "3021209961" ],
          "name" : [ "Jessica White 🦎" ],
          "nickname" : [ "JessPWhite" ],
          "url" : [ "https://twitter.com/JessPWhite", "https://jesswhite.co.uk" ],
          "published" : [ "2015-02-06T10:02:36+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1260533296708104193/p4aE5SAz.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "A distant memory ago at a conference where we got to meet people in person, I promised @MeganKSlater I'd play Stardew Valley.\n\nI'm now completely addicted. There's a real danger when we return to normality I'm going to try to make friends by giving people jam.",
        "html" : "<div style=\"white-space: pre\">A distant memory ago at a conference where we got to meet people in person, I promised <a href=\"https://twitter.com/MeganKSlater\">@MeganKSlater</a> I'd play Stardew Valley.\n\nI'm now completely addicted. There's a real danger when we return to normality I'm going to try to make friends by giving people jam.</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
