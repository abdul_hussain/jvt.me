{
  "date" : "2020-05-08T12:21:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/Brill_Burrito/status/1258138178873065474" ],
    "name" : [ "Like of @Brill_Burrito's tweet" ],
    "published" : [ "2020-05-08T12:21:00+01:00" ],
    "like-of" : [ "https://twitter.com/Brill_Burrito/status/1258138178873065474" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/uou1h",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1258138178873065474" ],
      "url" : [ "https://twitter.com/Brill_Burrito/status/1258138178873065474" ],
      "published" : [ "2020-05-06T20:54:51+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Brill_Burrito" ],
          "numeric-id" : [ "283990086" ],
          "name" : [ "JB" ],
          "nickname" : [ "Brill_Burrito" ],
          "url" : [ "https://twitter.com/Brill_Burrito" ],
          "published" : [ "2011-04-18T12:10:27+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Worcester" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1243980521690054657/kcJ2Plne.jpg" ]
        }
      } ],
      "content" : [ "If the first ‘pub’ you want to go to after this is all over is a Wetherspoons then you don’t deserve to come out of lockdown" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
