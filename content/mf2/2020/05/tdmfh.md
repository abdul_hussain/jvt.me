{
  "date" : "2020-05-25T14:19:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.meetup.com/Tech-Nottingham/events/270794112/" ],
    "syndication" : [ "https://brid.gy/publish/meetup" ],
    "name" : [ "RSVP yes to https://www.meetup.com/Tech-Nottingham/events/270794112/" ],
    "published" : [ "2020-05-25T14:19:00+01:00" ],
    "event" : {
      "start" : [ "2020-06-08T18:30:00+01:00" ],
      "name" : [ "Tech Nottingham 8th June - 0 to 100 Real Quick - Making your First Days Count" ],
      "end" : [ "2020-06-08T20:30:00+01:00" ],
      "location" : [ "Online" ],
      "url" : [ "https://www.meetup.com/Tech-Nottingham/events/270794112/" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/05/tdmfh",
  "client_id" : "https://indigenous.realize.be"
}
