{
  "date" : "2020-05-24T22:30:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/AnTheMaker/status/1264665346239279104" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1264846434634997762" ],
    "name" : [ "Reply to https://twitter.com/AnTheMaker/status/1264665346239279104" ],
    "published" : [ "2020-05-24T22:30:00+01:00" ],
    "category" : [ "indieweb" ],
    "content" : [ {
      "html" : "",
      "value" : "You can make it work with tools like https://twitter-atom.appspot.com/ or https://granary.io (from @snarfed.org) which help convert to a feed format of choice. Those of us in the <a href=\"/tags/indieweb/\">#IndieWeb</a> are still heavily using open standards 👍🏽"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/05/hi99f",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1264665346239279104" ],
      "url" : [ "https://twitter.com/AnTheMaker/status/1264665346239279104" ],
      "published" : [ "2020-05-24T21:11:29+00:00" ],
      "in-reply-to" : [ "https://twitter.com/mnlwldr/status/1264661681826869256" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:AnTheMaker" ],
          "numeric-id" : [ "882305677808590850" ],
          "name" : [ "An ⎙" ],
          "nickname" : [ "AnTheMaker" ],
          "url" : [ "https://twitter.com/AnTheMaker", "https://xn--n-rfa.de", "http://shrtco.de", "http://rssapi.net", "http://talkco.de", "http://tele.rocks" ],
          "published" : [ "2017-07-04T18:30:40+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "localhost\\\\germany" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1235624417075568641/7anuCuYN.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "No. I wish social sites (like Twitter) didn't abandon their RSS feeds...",
        "html" : "No. I wish social sites (like Twitter) didn't abandon their RSS feeds...\n<a class=\"u-mention\" href=\"https://twitter.com/mnlwldr\"></a>"
      } ]
    }
  },
  "tags" : [ "indieweb" ],
  "client_id" : "https://indigenous.realize.be"
}
