{
  "date" : "2020-05-25T14:19:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.meetup.com/Tech-Nottingham/events/270794684/" ],
    "syndication" : [ "https://brid.gy/publish/meetup" ],
    "name" : [ "RSVP yes to https://www.meetup.com/Tech-Nottingham/events/270794684/" ],
    "published" : [ "2020-05-25T14:19:00+01:00" ],
    "event" : {
      "start" : [ "2020-07-13T18:30:00+01:00" ],
      "name" : [ "Tech Nottingham 13th July - The connected house of horrors" ],
      "end" : [ "2020-07-13T20:30:00+01:00" ],
      "location" : [ "Online" ],
      "url" : [ "https://www.meetup.com/Tech-Nottingham/events/270794684/" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/05/ywhzc",
  "client_id" : "https://indigenous.realize.be"
}
