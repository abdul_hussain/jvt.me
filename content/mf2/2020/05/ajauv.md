{
  "date" : "2020-05-16T17:18:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/clapifyoulikeme/status/1261198285886435329" ],
    "name" : [ "Like of @clapifyoulikeme's tweet" ],
    "published" : [ "2020-05-16T17:18:00+01:00" ],
    "like-of" : [ "https://twitter.com/clapifyoulikeme/status/1261198285886435329" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/ajauv",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1261198285886435329" ],
      "url" : [ "https://twitter.com/clapifyoulikeme/status/1261198285886435329" ],
      "published" : [ "2020-05-15T07:34:37+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:clapifyoulikeme" ],
          "numeric-id" : [ "14381110" ],
          "name" : [ "Detective Pikajew" ],
          "nickname" : [ "clapifyoulikeme" ],
          "url" : [ "https://twitter.com/clapifyoulikeme" ],
          "published" : [ "2008-04-14T04:16:03+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1059223678569852928/sHq-qPzZ.jpg" ]
        }
      } ],
      "content" : [ "Every so often I remember that \"helicopter\" is not made up of \"heli\" and \"copter\" but of \"helico\" and \"pter\" and I am filled with a deep rage" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
