{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2017-07-12T22:52:00+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to Anonymous Classes: Behind the Mask"
    ],
    "in-reply-to": [
      "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/240647717/"
    ],
    "published": [
      "2017-07-12T22:52:00+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/240647717/"
      ],
      "name": [
        "Anonymous Classes: Behind the Mask"
      ],
      "start": [
        "2017-07-13T19:00:00+01:00"
      ],
      "end": [
        "2017-07-13T22:15:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "JH, 34a Stoney Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2017/07/8cwcr",
  "aliases": [
    "/mf2/a2172635-0d8c-4c48-8ed2-f1f19559a574/"
  ]
}
