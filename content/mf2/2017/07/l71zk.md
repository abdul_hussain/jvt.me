{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2017-07-10T15:10:18+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/NottsJS/events/240755008/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/NottsJS/events/240755008/"
    ],
    "published": [
      "2017-07-10T15:10:18+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/NottsJS/events/240755008/"
      ],
      "name": [
        "July 2017 - Making Games with Phaser.io"
      ],
      "start": [
        "2017-07-11T18:30:00+01:00"
      ],
      "end": [
        "2017-07-11T18:30:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "JH, 34a Stoney Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2017/07/l71zk",
  "aliases": [
    "/mf2/821002e7-611c-4cec-97f6-767fa2f9829b/"
  ]
}
