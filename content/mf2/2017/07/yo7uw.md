{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2017-07-20T08:20:54+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/NottsJS/events/241857364/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/NottsJS/events/241857364/"
    ],
    "published": [
      "2017-07-20T08:20:54+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/NottsJS/events/241857364/"
      ],
      "name": [
        "August 2017 - Hands on with React.Native"
      ],
      "start": [
        "2017-08-08T18:30:00+01:00"
      ],
      "end": [
        "2017-08-08T18:30:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "JH, 34a Stoney Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2017/07/yo7uw",
  "aliases": [
    "/mf2/112659ea-3e53-4d2c-ae75-13008539265f/"
  ]
}
