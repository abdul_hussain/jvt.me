{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2017-05-22T18:30:00+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "DevOps Nottingham"
    ],
    "in-reply-to": [
      "https://web.archive.org/*/https://www.meetup.com/DevOps-Nottingham/events/238856987/"
    ],
    "published": [
      "2017-05-22T18:30:00+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://web.archive.org/*/https://www.meetup.com/DevOps-Nottingham/events/238856987/"
      ],
      "name": [
        "DevOps Nottingham"
      ],
      "start": [
        "2017-05-22T18:30:00+01:00"
      ],
      "end": [
        "2017-05-22T20:30:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Accelerate Places, The Poynt, 45 Wollaton Street, Nottingham, NG1 5FW"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2017/05/kntd6",
  "aliases": [
    "/mf2/e5e845bf-2a75-40a1-8ffb-80c7a04054a7/"
  ]
}
