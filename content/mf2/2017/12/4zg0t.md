{
  "kind": "rsvps",
  "slug": "2017/12/4zg0t",
  "date": "2017-12-07T18:30:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.eventbrite.co.uk/e/notttechparty-the-nottingham-tech-community-christmas-party-2017-tickets-39727366623"
    ],
    "in-reply-to": [
      "https://www.eventbrite.co.uk/e/notttechparty-the-nottingham-tech-community-christmas-party-2017-tickets-39727366623"
    ],
    "published": [
      "2017-12-07T18:30:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.eventbrite.co.uk/e/notttechparty-the-nottingham-tech-community-christmas-party-2017-tickets-39727366623"
      ],
      "name": [
        "#NottTechParty - The Nottingham Tech Community Christmas Party 2017"
      ],
      "start": [
        "2017-12-07T18:30:00Z"
      ],
      "end": [
        "2017-12-07T22:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "24-32 Carlton Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "postal-code": [
            "NG1 1NN"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
