{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2017-09-26T01:24:08+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Women-In-Tech-Nottingham/events/243440319/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Women-In-Tech-Nottingham/events/243440319/"
    ],
    "published": [
      "2017-09-26T01:24:08+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Women-In-Tech-Nottingham/events/243440319/"
      ],
      "name": [
        "Women in Tech October 2017: The Power of Community"
      ],
      "start": [
        "2017-10-05T18:30:00+01:00"
      ],
      "end": [
        "2017-10-05T21:00:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Accelerate Places, The Poynt, 45 Wollaton Street, Nottingham, NG1 5FW"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2017/09/ut3hq",
  "aliases": [
    "/mf2/ccbcb200-cad7-46ba-bf7c-50fb04720b42/"
  ]
}
