{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2017-06-11T20:23:03+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Tech-Nottingham/events/240711662/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Tech-Nottingham/events/240711662/"
    ],
    "published": [
      "2017-06-11T20:23:03+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Tech-Nottingham/events/240711662/"
      ],
      "name": [
        "Tech Nottingham July 2017: Important news from the future"
      ],
      "start": [
        "2017-07-03T18:30:00+01:00"
      ],
      "end": [
        "2017-07-03T21:00:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, Beck Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2017/06/xcjtl",
  "aliases": [
    "/mf2/f76cab3a-fe2e-4e16-bc13-4647e3571f7e/"
  ]
}
