{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2017-06-12T08:05:21+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/NottsJS/events/239135010/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/NottsJS/events/239135010/"
    ],
    "published": [
      "2017-06-12T08:05:21+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/NottsJS/events/239135010/"
      ],
      "name": [
        "June 2017 - Machine Learning for Muggles"
      ],
      "start": [
        "2017-06-13T18:30:00+01:00"
      ],
      "end": [
        "2017-06-13T18:30:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "JH, 34a Stoney Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2017/06/ubwjn",
  "aliases": [
    "/mf2/1d5967df-d115-41b6-afd2-e69e3b6cf99d/"
  ]
}
