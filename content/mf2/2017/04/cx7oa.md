{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2017-04-22T19:52:10+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/PHPMiNDS-in-Nottingham/events/239386165/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/239386165/"
    ],
    "published": [
      "2017-04-22T19:52:10+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/239386165/"
      ],
      "name": [
        "Kickstarting your Automated Build and Continuous Delivery platform"
      ],
      "start": [
        "2017-05-11T19:00:00+01:00"
      ],
      "end": [
        "2017-05-11T19:00:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "JH, 34a Stoney Street, Nottingham, NG1 1NB"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2017/04/cx7oa",
  "aliases": [
    "/mf2/cd44c799-56f5-447f-9e05-18e183543210/"
  ]
}
