{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2017-04-24T21:35:00+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Women-In-Tech-Nottingham/events/238948627/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Women-In-Tech-Nottingham/events/238948627/"
    ],
    "published": [
      "2017-04-24T21:35:00+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Women-In-Tech-Nottingham/events/238948627/"
      ],
      "name": [
        "May the 4th Be With You - Sharepoint & Browser Security"
      ],
      "start": [
        "2017-05-04T18:30:00+01:00"
      ],
      "end": [
        "2017-05-04T18:30:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Accelerate Places, The Poynt, 45 Wollaton Street, Nottingham, NG1 5FW"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2017/04/myj2r",
  "aliases": [
    "/mf2/7a08130f-1195-4d39-a485-551718a47685/"
  ]
}
