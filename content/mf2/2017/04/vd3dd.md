{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2017-04-07T11:44:48+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/NottsJS/events/239020788/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/NottsJS/events/239020788/"
    ],
    "published": [
      "2017-04-07T11:44:48+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/NottsJS/events/239020788/"
      ],
      "name": [
        "May 2017 - Chat Bots"
      ],
      "start": [
        "2017-05-09T18:30:00+01:00"
      ],
      "end": [
        "2017-05-09T18:30:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "JH, 34a Stoney Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2017/04/vd3dd",
  "aliases": [
    "/mf2/f82e6cfc-d0dd-4e18-a5be-3828ae868419/"
  ]
}
