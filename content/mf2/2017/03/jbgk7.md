{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2017-03-25T15:40:14+00:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/PHPMiNDS-in-Nottingham/events/238408087/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/238408087/"
    ],
    "published": [
      "2017-03-25T15:40:14+00:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/238408087/"
      ],
      "name": [
        "Content Security Policies: Let’s Break Stuff"
      ],
      "start": [
        "2017-04-13T19:00:00+01:00"
      ],
      "end": [
        "2017-04-13T19:00:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "JH, 34a Stoney Street, Nottingham, NG1 1NB"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2017/03/jbgk7",
  "aliases": [
    "/mf2/7aac30a6-f486-458f-89e1-847da7533a23/"
  ]
}
