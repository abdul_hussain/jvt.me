{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2017-03-25T15:43:01+00:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/NottsJS/events/236607159/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/NottsJS/events/236607159/"
    ],
    "published": [
      "2017-03-25T15:43:01+00:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/NottsJS/events/236607159/"
      ],
      "name": [
        "April 2017 - Firefox Containers, isolating the web whilst hacking web extensions"
      ],
      "start": [
        "2017-04-11T18:30:00+01:00"
      ],
      "end": [
        "2017-04-11T21:30:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "JH, 34a Stoney Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2017/03/dg8hy",
  "aliases": [
    "/mf2/f60d7ccb-f710-4976-a2b5-7183e400e748/"
  ]
}
