{
  "kind": "rsvps",
  "slug": "2017/03/xd9x2",
  "date": "2017-03-18T10:00:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.eventbrite.co.uk/e/hack24-2017-registration-31805254372"
    ],
    "in-reply-to": [
      "https://www.eventbrite.co.uk/e/hack24-2017-registration-31805254372"
    ],
    "published": [
      "2017-03-18T10:00:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.eventbrite.co.uk/e/hack24-2017-registration-31805254372"
      ],
      "name": [
        "Hack24 2017"
      ],
      "start": [
        "2017-03-18T10:00:00Z"
      ],
      "end": [
        "2017-03-19T18:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Old Market Square"
          ],
          "locality": [
            "Nottingham"
          ],
          "postal-code": [
            "NG1 2DT"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
