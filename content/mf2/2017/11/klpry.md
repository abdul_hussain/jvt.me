{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2017-11-21T08:16:19+00:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Nott-Tuesday/events/244539564/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Nott-Tuesday/events/244539564/"
    ],
    "published": [
      "2017-11-21T08:16:19+00:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Nott-Tuesday/events/244539564/"
      ],
      "name": [
        "Nott Tuesday November 2017: Design Thinking & Product Management"
      ],
      "start": [
        "2017-11-21T18:30:00Z"
      ],
      "end": [
        "2017-11-21T21:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, 9A Beck St, NG1 1EQ"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2017/11/klpry",
  "aliases": [
    "/mf2/d3892686-ea37-493b-940c-82c903d36a89/"
  ]
}
