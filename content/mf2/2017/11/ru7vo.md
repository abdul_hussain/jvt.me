{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2017-11-21T08:53:03+00:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Nottingham-AWS-Meetup/events/244449815/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Nottingham-AWS-Meetup/events/244449815/"
    ],
    "published": [
      "2017-11-21T08:53:03+00:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Nottingham-AWS-Meetup/events/244449815/"
      ],
      "name": [
        "ONESOURCE Statutory Reporting: The Pipeline to Production"
      ],
      "start": [
        "2017-11-28T19:00:00Z"
      ],
      "end": [
        "2017-11-28T19:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "UNiDAYS,  2 Castle Blvd"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2017/11/ru7vo",
  "aliases": [
    "/mf2/94f575ad-b8e4-4fc1-9bf7-f4ce3cf2a4f0/"
  ]
}
