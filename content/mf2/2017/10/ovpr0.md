{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2017-10-03T12:36:16+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Tech-Nottingham/events/243879660/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Tech-Nottingham/events/243879660/"
    ],
    "published": [
      "2017-10-03T12:36:16+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Tech-Nottingham/events/243879660/"
      ],
      "name": [
        "TECH NOTTINGHAM NOVEMBER 2017: KICKSTARTING CONTINUOUS DELIVERY WITH GITLAB CI"
      ],
      "start": [
        "2017-11-06T18:30:00Z"
      ],
      "end": [
        "2017-11-06T21:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, Beck Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2017/10/ovpr0",
  "aliases": [
    "/mf2/4464e9a3-2433-4d43-9d9a-297e0cdc42f9/"
  ]
}
