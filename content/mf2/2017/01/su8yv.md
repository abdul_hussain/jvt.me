{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2017-01-30T13:22:28+00:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Tech-Nottingham/events/237310567/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Tech-Nottingham/events/237310567/"
    ],
    "published": [
      "2017-01-30T13:22:28+00:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Tech-Nottingham/events/237310567/"
      ],
      "name": [
        "Tech Nottingham February 2017 - Chatbots and Chatops!"
      ],
      "start": [
        "2017-02-06T18:30:00Z"
      ],
      "end": [
        "2017-02-06T18:30:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, Beck Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2017/01/su8yv",
  "aliases": [
    "/mf2/7a4e1db3-c84d-438b-a45d-26fac8c76f79/"
  ]
}
