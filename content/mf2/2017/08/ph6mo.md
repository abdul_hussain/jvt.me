{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2017-08-30T18:48:30+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Tech-Nottingham/events/242558043/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Tech-Nottingham/events/242558043/"
    ],
    "published": [
      "2017-08-30T18:48:30+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Tech-Nottingham/events/242558043/"
      ],
      "name": [
        "TECH NOTTINGHAM SEPTEMBER 2017: DEVEL-OPS: BRINGING DEV PRACTICES TO OPS TASKS"
      ],
      "start": [
        "2017-09-04T18:30:00+01:00"
      ],
      "end": [
        "2017-09-04T21:00:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, Beck Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2017/08/ph6mo",
  "aliases": [
    "/mf2/6ee247e9-efcf-460e-8441-3a154ae2a98f/"
  ]
}
