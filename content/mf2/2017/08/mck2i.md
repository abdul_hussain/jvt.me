{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2017-08-14T14:36:04+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Nott-Tuesday/events/rgbvwlywlbtb/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Nott-Tuesday/events/rgbvwlywlbtb/"
    ],
    "published": [
      "2017-08-14T14:36:04+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Nott-Tuesday/events/rgbvwlywlbtb/"
      ],
      "name": [
        "Nott Tuesday August 2017 - Hiring better people and getting better jobs"
      ],
      "start": [
        "2017-08-15T18:30:00+01:00"
      ],
      "end": [
        "2017-08-15T21:00:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, 9A Beck St, NG1 1EQ"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2017/08/mck2i",
  "aliases": [
    "/mf2/8c954aaf-1e2e-4055-92e1-e87de890aaf8/"
  ]
}
