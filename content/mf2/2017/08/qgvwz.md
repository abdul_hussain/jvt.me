{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2017-08-08T19:02:26+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/NottsJS/events/242384014/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/NottsJS/events/242384014/"
    ],
    "published": [
      "2017-08-08T19:02:26+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/NottsJS/events/242384014/"
      ],
      "name": [
        "November - Node JS in the AWS Cloud"
      ],
      "start": [
        "2017-11-14T18:30:00Z"
      ],
      "end": [
        "2017-11-14T21:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "JH, 34a Stoney Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2017/08/qgvwz",
  "aliases": [
    "/mf2/df0a1f0e-b996-4c7b-af2f-558178885a72/"
  ]
}
