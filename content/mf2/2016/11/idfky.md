{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2016-11-06T13:26:35+00:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Tech-Nottingham/events/234615041/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Tech-Nottingham/events/234615041/"
    ],
    "published": [
      "2016-11-06T13:26:35+00:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Tech-Nottingham/events/234615041/"
      ],
      "name": [
        "Tech Nottingham November 2016 - How to talk about APIs & Technical Products and"
      ],
      "start": [
        "2016-11-07T18:30:00Z"
      ],
      "end": [
        "2016-11-07T21:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, Beck Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2016/11/idfky",
  "aliases": [
    "/mf2/2aec2939-42f3-46eb-88ca-8c6b7a32ee5a/"
  ]
}
