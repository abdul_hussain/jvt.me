{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2016-03-07T14:17:34+00:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Tech-Nottingham/events/228563875/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Tech-Nottingham/events/228563875/"
    ],
    "published": [
      "2016-03-07T14:17:34+00:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Tech-Nottingham/events/228563875/"
      ],
      "name": [
        "Hack24 2016 - 24 hours of Code and Creativity in the Heart of Nottingham"
      ],
      "start": [
        "2016-03-19T10:30:00Z"
      ],
      "end": [
        "2016-03-20T18:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Nottingham Council House, Smithy Row"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2016/03/fysw2",
  "aliases": [
    "/mf2/e843817c-8a20-42a0-8048-873835b3d225/"
  ]
}
