{
  "kind": "rsvps",
  "slug": "2016/06/z0sy6",
  "date": "2016-06-09T17:30:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.eventbrite.co.uk/e/code-club-and-yrs-hyperlocal-nottingham-meetup-tickets-24783149066"
    ],
    "in-reply-to": [
      "https://www.eventbrite.co.uk/e/code-club-and-yrs-hyperlocal-nottingham-meetup-tickets-24783149066"
    ],
    "published": [
      "2016-06-09T17:30:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.eventbrite.co.uk/e/code-club-and-yrs-hyperlocal-nottingham-meetup-tickets-24783149066"
      ],
      "name": [
        "Code Club and YRS Hyperlocal Nottingham meetup"
      ],
      "start": [
        "2016-06-09T17:30:00Z"
      ],
      "end": [
        "2016-06-09T19:30:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "24-32 Carlton Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "postal-code": [
            "NG1 1NN"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
