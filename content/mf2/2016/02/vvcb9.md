{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2016-02-15T00:25:02+00:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Mobile-Notts/events/228641782/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Mobile-Notts/events/228641782/"
    ],
    "published": [
      "2016-02-15T00:25:02+00:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Mobile-Notts/events/228641782/"
      ],
      "name": [
        "Mobile Notts February 2016 - iOS for Android Developers & Lean Coffee"
      ],
      "start": [
        "2016-02-24T18:30:00Z"
      ],
      "end": [
        "2016-02-24T21:30:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Software Studio - Capital One, Station Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2016/02/vvcb9",
  "aliases": [
    "/mf2/ea702ccd-17c4-498b-81a3-3e88be7a8a66/"
  ]
}
