{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2016-02-24T11:28:47+00:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Tech-Nottingham/events/229014573/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Tech-Nottingham/events/229014573/"
    ],
    "published": [
      "2016-02-24T11:28:47+00:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Tech-Nottingham/events/229014573/"
      ],
      "name": [
        "Tech Nottingham March 2016 - Joe Nash on Kafka"
      ],
      "start": [
        "2016-03-07T18:30:00Z"
      ],
      "end": [
        "2016-03-07T21:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, Beck Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2016/02/ojhyv",
  "aliases": [
    "/mf2/a0d3fc23-e9ad-482b-b186-a4c73b642b6b/"
  ]
}
