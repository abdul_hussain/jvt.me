---
title: RSVPs
---
I attend a lot of events, and as such I've started to own my RSVPs on this website.

If you would like to see the RSVPs as I add them, please check out [/kind/rsvps/](/kind/rsvps/).

It may be more helpful to see the events in a calendar view, which I've set up below with a Google Calendar embed:

<p>
<iframe src="https://calendar.google.com/calendar/embed?src=9fmhhbc4hml1ebohduatk2bml9tm5ds0%40import.calendar.google.com&ctz=Europe%2FLondon" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
</p>

If you're interested in keeping up to date on what events I'm attending, you add the calendar itself to your own calendar with the URL [`https://www.jvt.me/rsvps/index.ics`](https://www.jvt.me/rsvps/index.ics).
