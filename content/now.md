---
title: /now
aliases:
- /about/
---
This page is inspired by <span class="h-card"><a class="p-name u-url" href="https://sivers.org">Derek Sivers</a></span>'s post [_The /now page movement_](https://sivers.org/nowff) and [the Now Movement](https://nownownow.com/about). The purpose of this page is to share what I'm currently focussed on, and will be updated as things change.

This is both for longer-lived interests and shorter-lived "what I'm currently doing".

I've been in COVID-19/Coronavirus self-quarantine since 2020-03-12.

I'm currently looking to buy a house!

What are my interests?

- Blogging
- Decentralisation
- GNU/Linux
- Free Software Movement
- Self-hosting services
- Software Testing
- Automation
- Infrastructure-as-code
- DevOps
- Tech Meetups

What am I up to right now?

- Living with my partner <span class="h-card"><a class="p-name u-url" href="https://annadodson.co.uk">Anna Dodson</a></span> and our beautiful terror of a cat [Morph](/tags/morph/)
- I'm a Software Development Engineer at Capital One, working on Third Party API engineering
  - Currently fulfilling the PSD2 and Open Banking regulations
- I'm actively blogging about technical and personal things
- I'm stepping into the IndieWeb movement, one [#IndieWeb](/tags/indieweb) post at a time
- I'm organising [Homebrew Website Club Nottingham](/events/homebrew-website-club-nottingham/)
- I'm co-organising [PHPMiNDS](https://phpminds.org)
- I'm looking to play more board games and socialise more

What are my Operating System preferences?

- On Mobile, I'm an Android user
- On Laptop/Desktop, I'm an Arch Linux user
- On the server, I'm a Debian Linux user

My primary programming languages at this time are Java and Ruby.

My daily drivers for work and play are:

- My phone is a Pixel 3A
- My laptop is an XPS 13 9343 (Broadwell)
- My desktop is a custom build with:
  - Intel i7-6900k
  - NVIDIA TITAN X

My current colourscheme is [Gruvbox](https://github.com/morhetz/gruvbox).
