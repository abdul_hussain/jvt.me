baseURL = ""

theme = "www.jvt.me-theme"

title = "Jamie Tanna | Software Engineer"

languageCode = "en-GB"

# allow fenced code blocks, instead of `{{< highlight ... >}}` shortcodes,
# which are unwieldly and less fun than using pure Markdown syntax
pygmentsCodeFences = true
# allow us to hook in custom CSS to render the site's styles
pygmentsUseClasses = true

# get information about the Git repo, such as last commit to a file
enableGitInfo = true

paginate = 50

[params]
  author = "Jamie Tanna"
  liveSiteUrl = "https://www.jvt.me"
  image = "/img/profile.png"
  WebMentionDomain = "www.jvt.me"
  MaxWebmentions = "100"
  AuthorizationEndpoint = "https://indieauth.com/auth"
  TokenEndpoint = "https://tokens.indieauth.com/token"
  MicroSubEndpoint = "https://aperture.p3k.io/microsub/262"
  MicroPubEndpoint = "https://www-api.jvt.me/micropub"
  ShowInMainFeed = ["articles", "bookmarks", "notes", "photos", "reposts", "rsvps"]
  IgnoreInFeeds = ["", "contacts", "projects", "talks"]
[[params.redirect]]
from = "/tags/archlinux/"
to = "/tags/arch-linux/"
[[params.redirect]]
from = "/tags/capitalone/"
to = "/tags/capital-one/"
[[params.redirect]]
from = "/tags/diversity/"
to = "/tags/diversity-and-inclusion/"
[[params.redirect]]
from = "/tags/inclusivity/"
to = "/tags/diversity-and-inclusion/"
[[params.redirect]]
from = "/.git"
to = "https://gitlab.com/jamietanna/jvt.me"
[[params.redirect]]
from = "/mf2/2020/03/nh9y0"
to = "/mf2/2020/03/bl33l/"

[permalinks]
  events = "/:sections/:slug/"
  mf2 = "/mf2/:slug/"
  posts = "/posts/:year/:month/:day/:slug/"
  tags = "/tags/:title/"
  kinds = "/kind/:title/"

[taxonomies]
  kind = "kind"
  tag = "tags"
  series = "series"

[outputs]
home = ["HTML", "RSS", "contacts", "jsonFeed", "postFrequency", "rsvpCalendar", "redir", "contentDeduplication", "search", "source", "taxonomies"]
section = ["HTML", "eventsCalendar", "RSS"]
page = ["HTML", "eventsCalendar"]

[mediaTypes]
[mediaTypes."text/netlify"]
delimiter = ""

[outputFormats]
[outputFormats.RSS]
mediatype = "application/rss"
baseName = "feed"
[outputFormats.contacts]
mediatype = "application/json"
baseName = "contacts"
notAlternative = true
[outputFormats.jsonFeed]
mediaType = "application/json"
baseName = "feed"
[outputFormats.postFrequency]
mediaType = "application/json"
baseName = "post-frequency"
[outputFormats.eventsCalendar]
mediaType = "text/calendar"
baseName = "index"
[outputFormats.contentDeduplication]
mediatype = "application/json"
baseName = "content-deduplication"
[outputFormats.rsvpCalendar]
mediaType = "text/calendar"
baseName = "/rsvps/index"
[outputFormats.redir]
mediatype = "text/netlify"
baseName = "_redirects"
isPlainText = true
notAlternative = true
[outputFormats.search]
mediatype = "application/json"
baseName = "search"
notAlternative = true
[outputFormats.source]
mediatype = "application/json"
baseName = "source"
notAlternative = true
[outputFormats.taxonomies]
mediatype = "application/json"
baseName = "taxonomies"
notAlternative = true

[markup]
  [markup.goldmark]
    [markup.goldmark.extensions]
    # disable stupid "smart" quotes
    typographer = false
    [markup.goldmark.renderer]
    # allow inline HTML, such as spoiler warnings
    unsafe = true
  [markup.tableOfContents]
  startLevel = 1
  endLevel = 6
  ordered = false

[menu]
  [[menu.main]]
    identifier = "now"
    name = "/now"
    title = "/now"
    url = "/now/"
    weight = 1
  [[menu.main]]
    identifier = "blog"
    name = "Blog"
    title = "Blog"
    url = "/kind/articles/"
    weight = 10
  [[menu.main]]
    identifier = "links"
    name = "Links"
    title = "Links"
    url = "/kind/bookmarks/"
    weight = 11
  [[menu.main]]
    identifier = "rsvps"
    name = "RSVPs"
    title = "RSVPs"
    url = "/rsvps/"
    weight = 20
  [[menu.main]]
    identifier = "kind"
    name = "Post by Kind"
    title = "Post by Kind"
    url = "/kind/"
    weight = 30
  [[menu.main]]
    identifier = "search"
    name = "Search"
    title = "Search"
    url = "/search/"
    weight = 50
  [[menu.main]]
    identifier = "support-me"
    name = "Support Me"
    title = "Support Me"
    url = "/support-me/"
    weight = 90

  [[menu.footer]]
    identifier = "all"
    name = "All Posts"
    title = "All Posts"
    url = "/all/"
    weight = 10
  [[menu.footer]]
    identifier = "subscribe"
    name = "Follow This Blog"
    title = "Follow This Blog"
    url = "/subscribe/"
    weight = 11
  [[menu.footer]]
    identifier = "talks"
    name = "Talks"
    title = "Talks"
    url = "/talks/"
    weight = 25
  [[menu.footer]]
    identifier = "why-wwwjvtme"
    name = "Why is my site www.jvt.me?"
    title = "Why is my site www.jvt.me?"
    url = "/posts/2019/05/26/why-wwwjvtme/"
    weight = 40
  [[menu.footer]]
    identifier = "blogroll"
    name = "Blogroll"
    title = "Blogroll"
    url = "/blogroll/"
    weight = 40
  [[menu.footer]]
    identifier = "popular-posts"
    name = "Popular Posts"
    title = "Popular Posts"
    url = "/popular-posts/"
    weight = 41
  [[menu.footer]]
    identifier = "postvisualisation"
    name = "Post Visualisation"
    title = "Post Visualisation"
    url = "/post-frequency/"
    weight = 45
